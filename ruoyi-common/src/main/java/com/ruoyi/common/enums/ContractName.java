package com.ruoyi.common.enums;

public enum ContractName {

    SJCDY("0", "试乘试驾车抵押合同（E已更改）");

    private final String code;
    private final String info;

    ContractName(String code, String info)
    {
        this.code = code;
        this.info = info;
    }

    public String getCode()
    {
        return code;
    }

    public String getInfo()
    {
        return info;
    }
}
