package com.ruoyi.common.constant;

/**
 * 合同生成优先级常量
 *
 *
 */
public class Priority {
    /**
     * New application
     */
    public static final Integer NEW_APPLICATION = 4;

    /**
     * Change guarantee conditions
     */
    public static final Integer CHANGE_GUARANTEE_CONDITIONS = 3;

    /**
     * Limit amount/ratio change
     */
    public static final Integer LIMIT_AMOUNT_RATIO_CHANGE = 2;

    /**
     * Limit expiry date extension
     */
    public static final Integer LIMIT_EXPIRY_DATE_EXTENSION = 1;
}
