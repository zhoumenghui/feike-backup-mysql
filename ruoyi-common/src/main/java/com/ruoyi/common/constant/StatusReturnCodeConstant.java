package com.ruoyi.common.constant;

/**
 * @Description: 维护请求状态返回码
 * @Author: daisy
 * @Date: Created in 2022/7/12 16:03
 * @Version: 1.0
 */
public class StatusReturnCodeConstant {

    /**页面请求成功返回码，并且前台会提示成功消息**/
    public static final int RETURN_CODE_SUCCESS=200;

    /**页面请求成功返回码，并且前台不会提示成功消息**/
    public static final int RETURN_CODE_SUCCESS_NOT_ALERT=201;

    /**页面请求失败返回码，并且前台会提示失败消息**/
    public static final int RETURN_CODE_FAILED=100;




}
