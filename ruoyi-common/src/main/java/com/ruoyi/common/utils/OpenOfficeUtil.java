/*
package com.ruoyi.common.utils;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.StreamOpenOfficeDocumentConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.ConnectException;

*/
/**
 * @Description: office转换pdf工具类
 * @author: djq
 *//*

@Component
@Slf4j
public class OpenOfficeUtil {
    */
/**
     * @Description: office转换pdf
     * @author: djq
     * @param file
     * @param outFile
     * @return
     *//*

    public  void office2Pdf(File file, File outFile) {
        // 判断源文件是否存在
        if (!file.exists()) {
            log.error("转换源文件不存在!");
            throw new RuntimeException("转换源文件不存在!");
        }
        // 创建连接
        OpenOfficeConnection connection = null;
        try {
            // 远程连接OpenOffice服务
            log.info("远程连接OpenOffice服务");
            connection = new SocketOpenOfficeConnection(host, port);
            connection.connect();
            // 创建文件转换器
            DocumentConverter converter = new StreamOpenOfficeDocumentConverter(connection);
            // 开始转换
            converter.convert(file, outFile);
            if (outFile.exists()) {
                log.info("文件转换成功");
            } else {
                log.info("文件转换失败");
            }
        } catch (ConnectException e) {
            e.printStackTrace();
            log.error("OpenOffice服务启动失败");
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
    @Value("${openOffice.host}")
    private  String host;
    @Value("${openOffice.port}")
    private  int port;
}
*/
