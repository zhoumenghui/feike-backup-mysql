package com.ruoyi.common.utils;


import com.ruoyi.common.core.redis.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.Format;
import java.util.HashMap;
import java.util.Map;

@Component
public class InstanceIdUtil {


    @Autowired
    RedisCache redisCache;


    public String getAbbreviationToMap(String key){
        HashMap<String, String> map = new HashMap<>();
        map.put("annualVerification","AR");
        map.put("permanentLimit","PLID");
        map.put("tempLimit","TL");
        map.put("termination","TM");
        map.put("creditBridge","CB");
        map.put("CreditBridge","CB");
        map.put("WFSLimitSetUp","WLS");
        map.put("ndCashDeposit","2T");
        map.put("activeLimitDes","ALD");
        map.put("newApplication","NA");
        map.put("annualReviewExtension","ARE");
        map.put("depositDecrease","DD");
        map.put("depositIncrease","DI");
        map.put("parkingLocation","PL");
        map.put("reviewExtension","ARE");
        map.put("changeConditions","CD");
        map.put("limitReActivation","LRA");
        map.put("activeLimitTransfer","ALT");
        map.put("permanentLimitTransfer","PLT");
        map.put("contractApproval","CA");
        map.put("InsuranceChange","IC");

        String s = map.get(key);
        if(null == s ){
            return key;
        }else{
            return  s;
        }
    }

    public  String getIstanceId(String keyName , String currentVal){
        StringBuffer instanceId = new StringBuffer();
        instanceId.append(keyName);
        instanceId.append(DateUtils.getDateyyyymmdd());
        instanceId.append(currentVal);
        return  instanceId.toString();
    }

    //获取 InstanceId 没有则获取1 并且设置缓存
    public String getRedisInstanceId(String keys){
        Format f1 = new DecimalFormat("000");
        Integer currentVal = 1;
        boolean flag = redisCache.hasKey(keys);
        if(flag){
             currentVal = (Integer) redisCache.getCacheObject(keys);
            //进行累加
            redisCache.setCacheObjectAutoFailure(keys ,currentVal+1);
            return f1.format(currentVal);
        }else{
            //不存在
            redisCache.setCacheObjectAutoFailure(keys ,currentVal);
            //进行累加
            redisCache.setCacheObjectAutoFailure(keys ,currentVal+1);
            return f1.format(currentVal);
        }
    }

    public String getRedisUW(String keys){
        Integer currentVal = 1;
        boolean flag = redisCache.hasKey(keys);
        if(flag){
            currentVal = (Integer) redisCache.getCacheObject(keys);
            //进行累加
            if(currentVal+1 >= 6){
                redisCache.setCacheObjectByNotTime(keys ,1);
                return String.valueOf(1);
            }else{
                redisCache.setCacheObjectByNotTime(keys ,currentVal+1);
                return String.valueOf(currentVal);
            }
        }else{
            //不存在
            redisCache.setCacheObjectByNotTime(keys ,currentVal);
            //进行累加
            redisCache.setCacheObjectByNotTime(keys ,currentVal+1);
            return String.valueOf(currentVal);
        }
    }

}
