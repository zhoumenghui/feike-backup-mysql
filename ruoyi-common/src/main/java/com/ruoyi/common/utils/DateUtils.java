package com.ruoyi.common.utils;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;

import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 时间工具类
 *
 * @author ruoyi
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils
{
    public static String YYYY = "yyyy";
    public static String MM = "MM";
    public static String DD = "dd";



    public static String YYYY_MM = "yyyy-MM";

    public static String YYYYmm = "yyyyMM";

    public static String YYYYMMDD = "YYYYMMdd";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String HHMMSS = "HHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     * 获取当前Date型日期
     *
     * @return Date() 当前日期
     */
    public static Date getNowDate()
    {
        return new Date();
    }

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     *
     * @return String
     */
    public static String getDate()
    {
        return dateTimeNow(YYYY_MM_DD);
    }

    public static String getDatehhmmss()
    {
        return dateTimeNow(HHMMSS);
    }

    public static String getDateyyyymmdd()
    {
        return dateTimeNow(YYYYMMDD);
    }

    public static final String getTime()
    {
        return dateTimeNow(YYYY_MM_DD_HH_MM_SS);
    }

    public static final String dateTimeNow()
    {
        return dateTimeNow(YYYYMMDDHHMMSS);
    }

    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }

    public static final String dateTime(final Date date)
    {
        return parseDateToStr(YYYY_MM_DD, date);
    }

    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    public static final Date dateTime(final String format, final String ts)
    {
        try
        {
            return new SimpleDateFormat(format).parse(ts);
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String datePath()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy/MM/dd");
    }

    /**
     * 日期路径 即年/月/日 如2018年08月08日
     */
    public static final String datePathCn()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyy年MM月dd日");
    }


    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String getCurrentYear()
    {
        Date now = new Date();
        return new SimpleDateFormat(YYYY).format(now);
    }

    public static final String getCurrentMonth()
    {
        Date now = new Date();
        return new SimpleDateFormat(MM).format(now);
    }

    public static final String getCurrentDay()
    {
        Date now = new Date();
        return new SimpleDateFormat(DD).format(now);
    }

    public static final String getLastMonth()
    {
        Calendar ca = Calendar.getInstance();//得到一个Calendar的实例
        ca.setTime(new Date());
        ca.add(Calendar.MONTH, -3);
        return new SimpleDateFormat(YYYY_MM).format(ca.getTime());
    }

    public static final String getLastMonthByNum(int num)
    {
        Calendar ca = Calendar.getInstance();//得到一个Calendar的实例
        ca.setTime(new Date());
        ca.add(Calendar.MONTH, num);
        return new SimpleDateFormat(YYYYmm).format(ca.getTime());
    }

    /**
     * @description: 获取当前年月往前推两个月月份
     * @param:
     * @return:
     * @author lss
     * @date: 2022/8/11 10:42
     */
    public static String[] getPreYearMonth(){
        String[] last12Months = new String[3];
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1); //要先+1,才能把本月的算进去
        cal.set(Calendar.DATE,1);
        for(int i=0; i<3; i++){
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); //逐次往前推1个月
            if (cal.get(Calendar.MONTH)+1<10){
                last12Months[2-i] = cal.get(Calendar.YEAR)+ "0" + (cal.get(Calendar.MONTH)+1);
            }else {
                last12Months[2-i] = String.valueOf(cal.get(Calendar.YEAR))+ String.valueOf(cal.get(Calendar.MONTH)+1);
            }
        }
        return last12Months;
    }

    public static String[] getPreYearMonth(int num){
        String[] last12Months = new String[num];
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1); //要先+1,才能把本月的算进去
        cal.set(Calendar.DATE,1);
        for(int i=0; i<num; i++){
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); //逐次往前推1个月
            if (cal.get(Calendar.MONTH)+1<10){
                last12Months[num-1-i] = cal.get(Calendar.YEAR)+ "0" + (cal.get(Calendar.MONTH)+1);
            }else {
                last12Months[num-1-i] = String.valueOf(cal.get(Calendar.YEAR))+ String.valueOf(cal.get(Calendar.MONTH)+1);
            }
        }
        return last12Months;
    }

    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static final String getLastYear()
    {
        Calendar ca = Calendar.getInstance();//得到一个Calendar的实例
        ca.setTime(new Date());
        ca.add(Calendar.YEAR, -1);
        return new SimpleDateFormat(YYYY).format(ca.getTime());
    }

    /**
     * 日期路径 即年/月/日 如20180808
     */
    public static final String dateTime()
    {
        Date now = new Date();
        return DateFormatUtils.format(now, "yyyyMMdd");
    }

    /**
     * 日期型字符串转化为日期 格式
     */
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    /**
     * 获取服务器启动时间
     */
    public static Date getServerStartDate()
    {
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }

    /**
     *
     * 计算指定日期加天数得到新的日期
     */
    public static String getEndTime(Date date,int day) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // 日期格式
//        Date date = dateFormat.parse(currentTime); // 指定日期
        Date newDate = addDate(date, day); // 指定日期加上10天
        return dateFormat.format(newDate);
    }


    public static Date addDate(Date date, long day) throws ParseException {
        long time = date.getTime(); // 得到指定日期的毫秒数
        day = (day-1) * 24 * 60 * 60 * 1000; // 要加上的天数转换成毫秒数
        time += day; // 相加得到新的毫秒数
        return new Date(time); // 将毫秒数转换成日期
    }

    /**
     * 获取月份英文大写 如11月 -- NOV
     * @return
     */
    public static String getMonthEN(){
        LocalDate localDate = LocalDate.now();
        Month month1 = localDate.getMonth();
        String month2 = month1.getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
        String month = month2.toUpperCase();
        return month;
    }

    /**
     * 日期秒加减
     */
    public static String addDate(Date date,int field,int amount){
        if(date == null){
            return null;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(field,amount);
        return parseDateToStr(HHMMSS,instance.getTime());
    }

    /**
     * 格林时间转换
     */
    public static String geLinToDate(String date) {
        try {
            if(date.equals("") || StringUtils.isEmpty(date)){
                return "";
            }
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
            Date d = sdf.parse(date);

            sdf = new SimpleDateFormat("yyyy-MM-dd");

            return sdf.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    //日期格式转换
    public static String conversion(String dateStr){
        if(StringUtils.isEmpty(dateStr) || dateStr.equals("")){
            return null;
        }
        //yyyyMM转yyyy-MM
        Date formatDat = null;
        try {
            formatDat = new SimpleDateFormat("yyyyMM").parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String str = new SimpleDateFormat("yyyy-MM").format(formatDat);
        return str;
    }


    public static boolean isOK(String str){
        java.text.SimpleDateFormat format=new java.text.SimpleDateFormat
                ("yyyy-MM");
        try{
            format.parse(str);
        }catch(Exception e){
            return false;
        }
        return true;
    }


}
