package com.ruoyi.common.utils;

import lombok.Data;

import java.util.List;

@Data
public class ExcelSheet {
    /*** sheet的名称*/
    private String fileName;

    /*** sheet里的标题*/
    private String[] handers;

    /*** sheet里的数据集*/
    private List<String[]> dataset;

    public ExcelSheet(String fileName, String[] handers, List<String[]> dataset) {
        this.fileName = fileName;
        this.handers = handers;
        this.dataset = dataset;
    }

}
