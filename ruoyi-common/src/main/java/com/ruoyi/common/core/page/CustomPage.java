package com.ruoyi.common.core.page;

import com.github.pagehelper.Page;

import java.util.List;

public class CustomPage {

	private long yfqNum;
	private long yclNum;
	private long jxzNum;
	private long ywcNum;


	public long getYfqNum() {
		return yfqNum;
	}

	public void setYfqNum(long yfqNum) {
		this.yfqNum = yfqNum;
	}

	public long getYclNum() {
		return yclNum;
	}

	public void setYclNum(long yclNum) {
		this.yclNum = yclNum;
	}

	public long getJxzNum() {
		return jxzNum;
	}

	public void setJxzNum(long jxzNum) {
		this.jxzNum = jxzNum;
	}

	public long getYwcNum() {
		return ywcNum;
	}

	public void setYwcNum(long ywcNum) {
		this.ywcNum = ywcNum;
	}
}
