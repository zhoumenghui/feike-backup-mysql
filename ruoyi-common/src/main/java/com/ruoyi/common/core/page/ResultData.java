package com.ruoyi.common.core.page;

public class ResultData<T> {


	/** 消息状态码 */
	private int code;
	/** 消息内容 */
	private String msg;

	T t;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}
}
