package com.ruoyi.web.controller.common;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.framework.config.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * 通用请求处理
 *
 * @author ruoyi
 */
@RestController
public class CommonController
{
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 通用下载请求
     *
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("/common/download")
    public void fileDownload(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            if (!FileUtils.isValidFilename(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }
            String realFileName ="";
            if(fileName.contains("approvalStatusReport")){
                String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
                realFileName = "Approval_Status_Report"+format+".xlsx";
            }else if(fileName.contains("condition")){
                String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
                realFileName = "TemporaryConditionFollowUp"+format+".xlsx";
            }
            else if(fileName.contains("InformationMaintain")){
                String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
                realFileName = "DealerInfo"+format+".xlsx";
            }
            else if(fileName.contains("aml")){
                String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
                realFileName = "AML"+format+".xlsx";
            }
            else if(fileName.contains("rpt")){
                String format = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                realFileName = "RPT" + format+".xlsx";
            }
            else{
                realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            }
            String filePath = RuoYiConfig.getDownloadPath() + fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    /**
     * 通用上传请求
     */
    @PostMapping("/common/upload")
    @Log(title = "文件上传")
    public AjaxResult uploadFile(MultipartFile file) throws Exception
    {
        log.info("文件上传开始,文件名{}",file.getOriginalFilename());
        try {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            log.info("文件上传路径{}",filePath);
            // 上传并返回新文件名称
            return getAjaxResult(file, filePath);
        }
        catch (Exception e)
        {
            log.error("文件上传失败，错误描述{}",e.getMessage());
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 通用上传请求,原文件存在则删除原文件
     */
    @PostMapping("/common/uploadOrCover")
    public AjaxResult uploadOrCover(String fileName,MultipartFile file)
    {
        try {
            log.info("op试驾车合同上传开始，文件名={}",fileName);
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            log.info("op试驾车合同上传开始，上传文件路径={}",filePath);
            String replace = fileName.replace("/dev-api/profile/upload", "");
            log.info("op试驾车合同上传开始，上传文件路径，全路径={}",filePath+"/"+replace);
            File opFile = new File(filePath+"/"+replace);
            if(opFile.exists()){
                opFile.delete();//删除文件
            }
            // 上传并返回新文件名称
            return getAjaxResult(file, filePath);
        }
        catch (Exception e)
        {
            log.error("文件上传失败，错误描述{}",e.getMessage());
            return AjaxResult.error(e.getMessage());
        }
    }

    private AjaxResult getAjaxResult(MultipartFile file, String filePath) throws IOException {
            String opFileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + opFileName;
            String originalFilename = file.getOriginalFilename();       // 文件的中文名
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", opFileName);
            ajax.put("url", url);
            ajax.put("originalFilename", originalFilename);
            return ajax;
    }

    /**
     * 多文件上传请求
     */
    @PostMapping("/common/uploads")
    public AjaxResult uploadFiles(MultipartFile[] files) throws Exception
    {
        try
        {
            AjaxResult ajax = AjaxResult.error("上传失败");
            log.error("文件上传失败{}",files);
            for (MultipartFile file : files) {
                // 上传文件路径
                String filePath = RuoYiConfig.getUploadPath();
                // 上传并返回新文件名称
                String fileName = FileUploadUtils.upload(filePath, file);   // 文件完整路径
                String url = serverConfig.getUrl() + fileName;              // 浏览器可以访问的url路径
                String originalFilename = file.getOriginalFilename();       // 文件的中文名
                System.out.println("fileName=="+fileName);
                ajax = AjaxResult.success();
                ajax.put("fileName", fileName);
                ajax.put("url", url);
                ajax.put("originalFilename", originalFilename);
            }
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 本地资源通用下载
     */
    @GetMapping("/common/download/resource")
    public void resourceDownload(String name, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        // 本地资源路径
        String localPath = RuoYiConfig.getProfile();
        // 数据库资源地址
        String downloadPath = localPath + StringUtils.substringAfter(name, Constants.RESOURCE_PREFIX);
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, downloadName));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
    }
     /**
     * 合同归档pdf下载
     *
     * @param fileName 文件名称
     * @param delete 是否删除
     */
    @GetMapping("/common/download/pdf")
    public void fileDownloadpdf(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
//            if (!FileUtils.isValidFilename(fileName))
//            {
//                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
//            }
            String realFileName = System.currentTimeMillis() + fileName.substring(fileName.indexOf("_") + 1);
            fileName = fileName.substring(8);
            String filePath = RuoYiConfig.getProfile()+fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, realFileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }

    @GetMapping("/common/download/paction")
    public void fileDownloadpaction(String fileName, Boolean delete, HttpServletResponse response, HttpServletRequest request)
    {
        try
        {
            /*if (!FileUtils.isValidFilename(fileName))
            {
                throw new Exception(StringUtils.format("文件名称({})非法，不允许下载。 ", fileName));
            }*/
            String filePath = RuoYiConfig.getProfile()+"/profile/paction/"+ fileName;

            response.setCharacterEncoding("utf-8");
            response.setContentType("multipart/form-data");
            response.setHeader("Content-Disposition",
                    "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, fileName));
            FileUtils.writeBytes(filePath, response.getOutputStream());
            if (delete)
            {
                FileUtils.deleteFile(filePath);
            }
        }
        catch (Exception e)
        {
            log.error("下载文件失败", e);
        }
    }
}
