package com.ruoyi.activiti.domain.model;

/**
 * @author 神
 * @date 2022年07月14日 13:18
 *
 * 年审任务
 */
public class AnnualReviewyTask {

    /**
     * 任务ID
     */
    private String taskId = "";

    /**
     * 任务名称
     */
    private String name;

    /**
     * 流程定义ID
     */
    private String processDefinitionId = "";

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    @Override
    public String toString() {
        return "AnnualReviewyTask{" +
                "taskId='" + taskId + '\'' +
                ", name='" + name + '\'' +
                ", processDefinitionId='" + processDefinitionId + '\'' +
                '}';
    }
}
