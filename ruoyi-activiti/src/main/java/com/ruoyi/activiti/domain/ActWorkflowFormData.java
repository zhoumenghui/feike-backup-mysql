package com.ruoyi.activiti.domain;

import com.ruoyi.activiti.domain.dto.ActWorkflowFormDataDTO;
import lombok.Data;
import org.activiti.api.task.model.Task;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 动态单对象 act_workflow_formdata
 *
 * @author danny
 * @date 2020-11-02
 */
@Data
public class ActWorkflowFormData extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 事务Id */
    private String businessKey;

    /** 表单Key */
    private String formKey;


    /** 表单id */
    private String controlId;
    /** 表单名称 */
    private String controlName;

    /** 表单值 */
    private String controlValue;

    /** 任务节点名称 */
    private String taskNodeName;

    private String createName;

    /** 提交岗位 */
    private String approvalPost;



    public ActWorkflowFormData() {
    }

    public ActWorkflowFormData(String businessKey,ActWorkflowFormDataDTO actWorkflowFormDataDTO, Task task) {
        this.businessKey = businessKey;
        this.formKey = task.getFormKey();
        this.controlId = actWorkflowFormDataDTO.getControlId();
        this.controlName = actWorkflowFormDataDTO.getControlLable();
        if ("radio".equals(actWorkflowFormDataDTO.getControlType())) {
            int i = Integer.parseInt(actWorkflowFormDataDTO.getControlValue());
            this.controlValue =  actWorkflowFormDataDTO.getControlDefault().split("--__--")[i];
        }else {
            this.controlValue = actWorkflowFormDataDTO.getControlValue();
        }
        this.taskNodeName = task.getName();
    }

}
