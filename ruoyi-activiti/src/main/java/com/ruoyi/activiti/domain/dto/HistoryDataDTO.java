package com.ruoyi.activiti.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class HistoryDataDTO {
    private String taskNodeName;
    private String createName;
    private String createdDate;

    /** 提交岗位 */
    private String approvalPost;

    private List<HistoryFormDataDTO> formHistoryDataDTO;

}
