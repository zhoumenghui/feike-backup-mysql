package com.ruoyi.activiti.service.impl;

import com.ruoyi.activiti.domain.ActWorkflowFormData;
import com.ruoyi.activiti.domain.dto.HistoryFormDataDTO;
import com.ruoyi.activiti.domain.dto.HistoryDataDTO;
import com.ruoyi.activiti.service.IActWorkflowFormDataService;
import com.ruoyi.activiti.service.IFormHistoryDataService;
import com.ruoyi.common.utils.StringUtils;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 汇讯数码科技(深圳)有限公司
 * 创建日期:2020/11/3-10:20
 * 版本   开发者     日期
 * 1.0    Danny    2020/11/3
 */
@Service
public class FormHistoryDataServiceImpl implements IFormHistoryDataService {
    @Autowired
    private IActWorkflowFormDataService actWorkflowFormDataService;



    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public List<HistoryDataDTO> historyDataShow(String businessKey) {
        List<HistoryDataDTO> returnHistoryFromDataDTOS=new ArrayList<>();
        List<ActWorkflowFormData> actWorkflowFormData = actWorkflowFormDataService.selectActWorkflowFormDataByBusinessKey(businessKey);
        for (ActWorkflowFormData actWorkflowFormDatum : actWorkflowFormData) {
            HistoryDataDTO returnHistoryFromDataDTO = new HistoryDataDTO();
            returnHistoryFromDataDTO.setCreatedDate(sdf.format(actWorkflowFormDatum.getCreateTime()));
            returnHistoryFromDataDTO.setCreateName(actWorkflowFormDatum.getCreateName());
            returnHistoryFromDataDTO.setTaskNodeName(actWorkflowFormDatum.getTaskNodeName());
            if(StringUtils.isNotEmpty(actWorkflowFormDatum.getApprovalPost())){
                returnHistoryFromDataDTO.setApprovalPost(actWorkflowFormDatum.getApprovalPost());
            }

            ActWorkflowFormData actWorkflowFormDataTemp = new ActWorkflowFormData();
            actWorkflowFormDataTemp.setCreateTime(actWorkflowFormDatum.getCreateTime());
            actWorkflowFormDataTemp.setBusinessKey(actWorkflowFormDatum.getBusinessKey());
            actWorkflowFormDataTemp.setFormKey(actWorkflowFormDatum.getFormKey());
            List<ActWorkflowFormData> actWorkflowFormData1 = actWorkflowFormDataService.selectActWorkflowFormDataByBusinessKey1(actWorkflowFormDataTemp);
            ArrayList<HistoryFormDataDTO> historyFormDataDTOS = new ArrayList<HistoryFormDataDTO>();
            for (ActWorkflowFormData workflowFormData : actWorkflowFormData1) {
                HistoryFormDataDTO historyFormDataDTO = new HistoryFormDataDTO();
                historyFormDataDTO.setTitle(workflowFormData.getControlName());
                historyFormDataDTO.setValue(workflowFormData.getControlValue());
                historyFormDataDTOS.add(historyFormDataDTO);
            }
            returnHistoryFromDataDTO.setFormHistoryDataDTO(historyFormDataDTOS);
            returnHistoryFromDataDTOS.add(returnHistoryFromDataDTO);
        }

       // List<HistoryDataDTO> collect1 = returnHistoryFromDataDTOS.stream().sorted((x, y) -> x.getCreatedDate().compareTo(y.getCreatedDate())).collect(Collectors.toList());

        return returnHistoryFromDataDTOS;
    }



}
