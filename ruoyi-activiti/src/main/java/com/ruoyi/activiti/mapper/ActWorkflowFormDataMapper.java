package com.ruoyi.activiti.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.ruoyi.activiti.domain.ActWorkflowFormData;
import org.apache.ibatis.annotations.Param;

import javax.xml.crypto.Data;

/**
 * 动态单Mapper接口
 *
 * @author danny
 * @date 2020-11-02
 */
public interface ActWorkflowFormDataMapper
{
    /**
     * 查询动态单
     *
     * @param id 动态单ID
     * @return 动态单
     */
    public ActWorkflowFormData selectActWorkflowFormDataById(Long id);
    /**
     * 查询动态单
     *
     * @param businessKey 动态单ID
     * @return 动态单
     */
    public List<ActWorkflowFormData> selectActWorkflowFormDataByBusinessKey(String businessKey);


    public List<ActWorkflowFormData> selectActWorkflowFormDataByBusinessKey1(ActWorkflowFormData ActWorkflowFormData);
    /**
     * 查询动态单列表
     *
     * @param ActWorkflowFormData 动态单
     * @return 动态单集合
     */
    public List<ActWorkflowFormData> selectActWorkflowFormDataList(ActWorkflowFormData ActWorkflowFormData);

    /**
     * 新增动态单
     *
     * @param ActWorkflowFormData 动态单
     * @return 结果
     */
    public int insertActWorkflowFormData(ActWorkflowFormData ActWorkflowFormData);


    /**
     * 新增动态单
     *
     * @param
     * @return 结果
     */
    public int insertActWorkflowFormDatas(@Param("createBy") String createBy, @Param("ActWorkflowFormData")List<ActWorkflowFormData> ActWorkflowFormData, Date date ,String createName);




    /**
     * 修改动态单
     *
     * @param ActWorkflowFormData 动态单
     * @return 结果
     */
    public int updateActWorkflowFormData(ActWorkflowFormData ActWorkflowFormData);

    /**
     * 删除动态单
     *
     * @param id 动态单ID
     * @return 结果
     */
    public int deleteActWorkflowFormDataById(Long id);

    /**
     * 批量删除动态单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActWorkflowFormDataByIds(Long[] ids);

    /**
     * 通过instanceId获取title申请类型
     * @param instanceId
     * @return
     */
    public String selectByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 通过instanceId获取经销商名称
     * @param instanceId
     * @return
     */
    public List<Map<String,String>> getDealerNameByInstanceId(@Param("instanceId") String instanceId);


    public List<Map<String,Object>> getLimit(@Param("instanceId") String instanceId);


    public int insertActWorkflowLock(@Param("id") String id,@Param("taskid") String taskid);


    public int deleteActWorkflowLockById(@Param("id") String id);



    /**
     * 通过dealerName获取Dm名称
     * @param dealerName
     * @return
     */
    public String getDmByDealerName(@Param("dealerName") String dealerName);

    public String getUWByDealerName(@Param("dealerName") String dealerName);

    /**
     * 获取经销商名称
     */
    public List<String> getDealerNameBySubProcessId(@Param("subProcessId") String subProcessId);


    public String getInstanceIdBySubProcessId(@Param("subProcessId") String subProcessId);

    /**
     * 修改文件
     */
    public void updateFileUrlByInstanceId(@Param("instanceId") String instanceId, @Param("fileUrl") String fileUrl, @Param("fileName") String fileName);

    /**
     * 获取文件路径
     */
    public Map<String,String> getFileUrlByInstanceId(@Param("instanceId") String instanceId);


    /**
     * 获取流程最后审批人
     *
     * @param businessKey
     * @return 结果
     */
    public ActWorkflowFormData getActWorkflowFormDataByBusinessKey(@Param("businessKey") String businessKey);

    public List<ActWorkflowFormData> getUWByBusinessKey(@Param("businessKey") String businessKey,@Param("taskName") String taskName);

    public List<ActWorkflowFormData> getOpByBusinessKey(@Param("businessKey") String businessKey,@Param("taskName") String taskName);

    public String getCreateBy(@Param("businessKey") String businessKey);


    public String getSalesHead(@Param("name") String name);

    public String getStartNode(@Param("businessKey") String businessKey);

    public Map<String,Object> getAnnualReviewyByInstanceId(@Param("instanceId") String instanceId);

    public int getProcessByInstanceId(@Param("instanceId") String instanceId);

    ActWorkflowFormData getSubmit(@Param("businessKey") String businessKey,@Param("taskName") String taskName);

    void updateAnnualReviewy(@Param("businessKey") String businessKey, @Param("date") String date);

    void updateAnnualReviewy2(@Param("businessKey") String businessKey, @Param("date") String date);

    void updateReport(@Param("businessKey") String businessKey, @Param("date") String date);

    void updateReportByUw(@Param("businessKey") String businessKey, @Param("date") String date);

    void updateAnnualReviewy3(@Param("businessKey") String businessKey, @Param("date") Date date);

    void updateAnnualReviewy4(@Param("businessKey") String businessKey, @Param("name") String name);


}
