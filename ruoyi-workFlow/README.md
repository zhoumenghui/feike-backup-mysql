# _页面转pdf_

参考 https://zhuanlan.zhihu.com/p/150005402 这个网址

一、需要使用转换插件

可根据不同的系统来下载对应的插件，
下载地址：
windows-x86.zip,  https://link.zhihu.com/?target=https%3A//www.e-iceblue.cn/downloads/plugins/plugins-windows-x86.zip
windows-x64.zip,  https://link.zhihu.com/?target=https%3A//www.e-iceblue.cn/downloads/plugins/plugins-windows-x64.zip
macosx_x64.zip,   https://link.zhihu.com/?target=https%3A//www.e-iceblue.cn/downloads/plugins/java/plugins-macosx-x64.zip
及linux_x64.zip， https://link.zhihu.com/?target=https%3A//www.e-iceblue.cn/downloads/plugins/java/plugins-linux-x64.zip
下载后需要将插件包解压到本地指定文件夹路径，
我将其解压放到项目工程路径下。



二、maven中引入免费版的依赖和仓库

```xml
<dependencies>
		<dependency>
            <groupId>e-iceblue</groupId>
            <artifactId>spire.pdf.free</artifactId>
            <version>5.1.0</version>
        </dependency>

        <dependency>
            <groupId>e-iceblue</groupId>
            <artifactId>spire.doc.free</artifactId>
            <version>5.2.0</version>
        </dependency>

        <dependency>
            <groupId>e-iceblue</groupId>
            <artifactId>spire.xls.free</artifactId>
            <version>5.1.0</version>
        </dependency>

        <dependency>
            <groupId>e-iceblue</groupId>
            <artifactId>spire.presentation.free</artifactId>
            <version>5.1.0</version>
        </dependency>

        <dependency>
            <groupId>e-iceblue</groupId>
            <artifactId>spire.office.free</artifactId>
            <version>5.3.1</version>
        </dependency>

    </dependencies>
    
    
	<repositories>
        <repository>
            <id>com.e-iceblue</id>
            <name>e-iceblue</name>
            <url>https://repo.e-iceblue.cn/repository/maven-public/</url>
        </repository>
    </repositories>
```



三、测试使用的代码

```Java
    @Test
    public void testHtmlToPdf() {
        //定义需要转换的HTML
        String url = "https://baike.baidu.com/";

        String formkey="123456789";	// 表单id

        //创建年月日文件夹
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String res = simpleDateFormat.format(new Date());
        String year = res.substring(0, 4);   //2022
        String month = res.substring(4, 6);  //07
        String day = res.substring(6);      //08

        //转换后的结果文档（结果文档保存在Java项目程序文件下）
        String fileName = "doc"+ File.separator+year+File.separator+month+File.separator+day+File.separator+formkey+".pdf";

        //解压后的插件本地地址（这里是把插件包放在了Java项目文件夹下，也可以自定义其他本地路径）
        // String pluginPath = "C:\\Users\\Administrator\\IdeaProjects\\Conversion_PDF\\plugins-windows-x64";
        String pluginPath = "E:\\workspace\\activiti\\actSpringBootDemo\\plugins";
        HtmlConverter.setPluginPath(pluginPath);

        //调用方法转换到PDF并设置PDF尺寸
        HtmlConverter.convert(url, fileName, true, 1000, new Size(700f, 800f), new PdfMargins(0));

    }
```

