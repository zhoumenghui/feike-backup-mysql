package com.ruoyi.feike.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.dto.RefreshDTO;
import com.ruoyi.feike.domain.vo.ProposalByCommericalAndMarketingVO;
import com.ruoyi.feike.domain.vo.ThirdCreditInfoSimpleChild;
import com.ruoyi.feike.domain.vo.ThirdCreditInfoSimpleVO;
import com.ruoyi.feike.mapper.ActiveLimitAdjustmentProposalMapper;
import com.ruoyi.feike.mapper.ProposalByCommericalAndMarketingMapper;
import com.ruoyi.feike.mapper.WholesalePerformanceCurrentlyMapper;
import com.ruoyi.feike.service.IAnnualReviewyService;
import com.ruoyi.feike.service.ICreditConditionService;
import com.ruoyi.feike.service.IGradeService;
import com.ruoyi.feike.service.IRefreshService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author lss
 * @version 1.0
 * @description: TODO
 * @date 2022/8/28 16:37
 */

@RestController
@RequestMapping("/feike/refresh")
@Slf4j
public class RefreshController {

    @Value("${wfs.url}")
    private String wfsUrl;
    @Autowired
    private IRefreshService refreshService;

    @Autowired
    private ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper;

    @Autowired
    private ICreditConditionService creditConditionService;

    @Autowired
    private WholesalePerformanceCurrentlyMapper wholesalePerformanceCurrentlyMapper;

    @Autowired
    private IGradeService gradeService;

    @Autowired
    private IAnnualReviewyService annualReviewyService;

    @Autowired
    private ActiveLimitAdjustmentProposalMapper activeLimitAdjustmentProposalMapper;

    @PostMapping(value = "/refreshInfomation")
    public AjaxResult refreshInfomation(@RequestBody RefreshDTO refreshDTO) throws IOException {
        Map<String, Object> resultMap= refreshService.refreshInfoMation(refreshDTO);
        return  AjaxResult.success(resultMap);
    }

    @PostMapping(value = "/update")
    public AjaxResult update(@RequestBody Map<String, Object> map) throws IOException {
        log.info("刷新wfs数据开始");
        String instanceId = (String) map.get("instanceId");
        if(instanceId.contains("WLS")) {
            throw  new BaseException("数据异常，请退出账号，重新登录");
        }
        if(instanceId.contains("ALD") || instanceId.contains("ALT")) {
            ActiveLimitAdjustmentProposal adjustmentProposalTemp = new ActiveLimitAdjustmentProposal();
            adjustmentProposalTemp.setInstanceId(instanceId);
            List<ActiveLimitAdjustmentProposal> activeLimitAdjustmentProposals1 = activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalList(adjustmentProposalTemp);
            ArrayList<String[]> DealerCodes = new ArrayList<>();
            ArrayList<String> strings = new ArrayList<>();
            for (ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal : activeLimitAdjustmentProposals1) {
                strings.add(activeLimitAdjustmentProposal.getDealerCode());
            }
            DealerCodes.add(strings.toArray(new String[strings.size()]));
            DepositDecreaseDTO depositDecreaseDTO = new DepositDecreaseDTO();
            depositDecreaseDTO.setDealerCodeArr(DealerCodes);
            AjaxResult deposit = getDeposit(depositDecreaseDTO);
            if(deposit.get("code").toString().equals("200")){
                Object data = deposit.get("data");
                List<Map<String, Object>> CreditLimitMap = (List<Map<String, Object>>) data;
                for (Map<String, Object> stringObjectMap : CreditLimitMap) {
                    //查询
                    Object dealerCode = stringObjectMap.get("DealerCode");
                    ActiveLimitAdjustmentProposal adjustmentProposalT = new ActiveLimitAdjustmentProposal();
                    adjustmentProposalT.setInstanceId(instanceId);
                    adjustmentProposalT.setDealerCode((String) dealerCode);
                    List<ActiveLimitAdjustmentProposal> Proposals = activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalList(adjustmentProposalT);
                    for (ActiveLimitAdjustmentProposal proposal : Proposals) {
                        if(proposal.getLimitType().equals("CB")){
                            Object cbDeposit = stringObjectMap.get("CBDeposit");
                            proposal.setDeposit(cbDeposit.toString());
                        }else if (proposal.getLimitType().equals("TEMP")){
                            proposal.setDeposit("-");
                        }else{
                            if(proposal.getLimitType().equals("NORMAL")){
                                Object Deposit = stringObjectMap.get("Deposit");
                                proposal.setDeposit(Deposit.toString());
                            }
                            if(proposal.getLimitType().equals("PART")){
                                Object Deposit = stringObjectMap.get("PartDeposit");
                                proposal.setDeposit(Deposit.toString());
                            }
                        }
                        activeLimitAdjustmentProposalMapper.updateActiveLimitAdjustmentProposal(proposal);
                    }
                }

            }

            ActiveLimitAdjustmentProposal adjustmentProposalTemp2 = new ActiveLimitAdjustmentProposal();
            adjustmentProposalTemp2.setInstanceId(instanceId);
            List<ActiveLimitAdjustmentProposal> activeLimitAdjustmentProposals = activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalList(adjustmentProposalTemp2);

            List<ProposalByCommericalAndMarketing> newProposalByCommericalAndMarketingList = new ArrayList<>();
            Object proposal_by_commerical_and_marketing = map.get("proposal_by_commerical_and_marketing_1");
            if (proposal_by_commerical_and_marketing != null) {
                List<ProposalByCommericalAndMarketingVO> proposalByCommericalAndMarketings = (List<ProposalByCommericalAndMarketingVO>) proposal_by_commerical_and_marketing;
                String s = JSON.toJSONString(proposalByCommericalAndMarketings);
                ArrayList<ThirdCreditInfoSimpleVO> thirdCreditInfoSimpleVOS = new ArrayList<>();
                proposalByCommericalAndMarketings = JSON.parseArray(s, ProposalByCommericalAndMarketingVO.class);
                for (ProposalByCommericalAndMarketingVO proposalByCommericalAndMarketingVO : proposalByCommericalAndMarketings) {
                    ProposalByCommericalAndMarketing proposalByCommericalAndMarketing = new ProposalByCommericalAndMarketing();
                    BeanUtils.copyProperties(proposalByCommericalAndMarketingVO, proposalByCommericalAndMarketing);
                    newProposalByCommericalAndMarketingList.add(proposalByCommericalAndMarketing);
                }
            }
            if(activeLimitAdjustmentProposals!=null && activeLimitAdjustmentProposals.size()>0
                    && newProposalByCommericalAndMarketingList!=null && newProposalByCommericalAndMarketingList.size()>0
            ){
                for (ActiveLimitAdjustmentProposal proposalByCommericalAndMarketing : activeLimitAdjustmentProposals) {
                    for (ProposalByCommericalAndMarketing byCommericalAndMarketing : newProposalByCommericalAndMarketingList) {
                        if(byCommericalAndMarketing.getSector().equals(proposalByCommericalAndMarketing.getSector())
                                && byCommericalAndMarketing.getLimitType().equals(proposalByCommericalAndMarketing.getLimitType())
                                && byCommericalAndMarketing.getDealerCode().equals(proposalByCommericalAndMarketing.getDealerCode())){
                            System.out.println(byCommericalAndMarketing);
                            proposalByCommericalAndMarketing.setCreditLimit(byCommericalAndMarketing.getApprovedLimit());
                            proposalByCommericalAndMarketing.setActiveOS(new BigDecimal(byCommericalAndMarketing.getOs()));

                            if(proposalByCommericalAndMarketing.getId() !=null){
                                activeLimitAdjustmentProposalMapper.updateActiveLimitAdjustmentProposal(proposalByCommericalAndMarketing);
                            }
                        }
                    }
                }
            }
        }
        List<Grade> gradeLists = gradeService.selectGradeByInstanceId(instanceId);
        if (gradeLists != null && gradeLists.size() > 0) {
            for (Grade grade : gradeLists) {
                gradeService.deleteGradeById(grade.getId());
                annualReviewyService.saveDbLog("2","刷新时删除表单信息","CRIXP GRADE",instanceId,grade,null,null);

            }
        }

        Object gradeObj = map.get("grade_1");
        if (gradeObj != null) {
            List<Grade> gradeList = (List<Grade>) gradeObj;
            System.out.println("gradeList==" + gradeList);
            String gradeStr = JSON.toJSONString(gradeList);
            gradeList = JSON.parseArray(gradeStr, Grade.class);
            if (gradeList != null && gradeList.size() > 0) {
                for (Grade grade : gradeList) {
                    grade.setId(IdUtils.simpleUUID());
                    grade.setInstanceId(instanceId);
                    gradeService.insertGrade(grade);
                    annualReviewyService.saveDbLog("1","刷新时新增表单信息","CRIXP GRADE",instanceId,null,grade,null);

                }
            }
        }

        List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingList = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingByInstanceId(instanceId);
        List<ProposalByCommericalAndMarketing> newProposalByCommericalAndMarketingList = new ArrayList<>();
        Object proposal_by_commerical_and_marketing = map.get("proposal_by_commerical_and_marketing_1");
        if (proposal_by_commerical_and_marketing != null) {
            List<ProposalByCommericalAndMarketingVO> proposalByCommericalAndMarketings = (List<ProposalByCommericalAndMarketingVO>) proposal_by_commerical_and_marketing;
            String s = JSON.toJSONString(proposalByCommericalAndMarketings);
            ArrayList<ThirdCreditInfoSimpleVO> thirdCreditInfoSimpleVOS = new ArrayList<>();
            proposalByCommericalAndMarketings = JSON.parseArray(s, ProposalByCommericalAndMarketingVO.class);
            for (ProposalByCommericalAndMarketingVO proposalByCommericalAndMarketingVO : proposalByCommericalAndMarketings) {
                ProposalByCommericalAndMarketing proposalByCommericalAndMarketing = new ProposalByCommericalAndMarketing();
                BeanUtils.copyProperties(proposalByCommericalAndMarketingVO, proposalByCommericalAndMarketing);
                newProposalByCommericalAndMarketingList.add(proposalByCommericalAndMarketing);
            }
        }
        if(proposalByCommericalAndMarketingList!=null && proposalByCommericalAndMarketingList.size()>0
                && newProposalByCommericalAndMarketingList!=null && newProposalByCommericalAndMarketingList.size()>0
        ){
            for (ProposalByCommericalAndMarketing proposalByCommericalAndMarketing : proposalByCommericalAndMarketingList) {
                for (ProposalByCommericalAndMarketing byCommericalAndMarketing : newProposalByCommericalAndMarketingList) {
                    if(byCommericalAndMarketing.getDealername().equals(proposalByCommericalAndMarketing.getDealername())
                            && byCommericalAndMarketing.getSector().equals(proposalByCommericalAndMarketing.getSector())
                            && byCommericalAndMarketing.getLimitType().equals(proposalByCommericalAndMarketing.getLimitType())
                            && byCommericalAndMarketing.getDealerCode().equals(proposalByCommericalAndMarketing.getDealerCode())){
                        proposalByCommericalAndMarketing.setActiveLimit(byCommericalAndMarketing.getActiveLimit());
                        proposalByCommericalAndMarketing.setOs(byCommericalAndMarketing.getOs());
                        //proposalByCommericalAndMarketing.setApprovedLimit(byCommericalAndMarketing.getApprovedLimit());
                        //proposalByCommericalAndMarketing.setApprovedCashDeposit(byCommericalAndMarketing.getApprovedCashDeposit());
                        //proposalByCommericalAndMarketing.setProposalLimit(byCommericalAndMarketing.getProposalLimit());
                        //proposalByCommericalAndMarketing.setProposalCashDeposit(byCommericalAndMarketing.getProposalCashDeposit());
                        if(proposalByCommericalAndMarketing.getId() !=null){
                            ProposalByCommericalAndMarketing proposalByCommericalAndMarketing1 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingById(proposalByCommericalAndMarketing.getId());
                            proposalByCommericalAndMarketingMapper.updateProposalByCommericalAndMarketing(proposalByCommericalAndMarketing);
                            annualReviewyService.saveDbLog("3","刷新时修改表单信息","PROPOSAL BY COMMERCIAL & MARKETING",instanceId,proposalByCommericalAndMarketing1,proposalByCommericalAndMarketing,null);
                        }
                    }
                }
            }
        }


        if(proposalByCommericalAndMarketingList!=null && proposalByCommericalAndMarketingList.size()>0
                && newProposalByCommericalAndMarketingList!=null && newProposalByCommericalAndMarketingList.size()>0
        ){
            for (ProposalByCommericalAndMarketing proposalByCommericalAndMarketing : newProposalByCommericalAndMarketingList) {
                boolean flag =false;
                for (ProposalByCommericalAndMarketing byCommericalAndMarketing : proposalByCommericalAndMarketingList) {
                    if(byCommericalAndMarketing.getDealername().equals(proposalByCommericalAndMarketing.getDealername())
                            && byCommericalAndMarketing.getSector().equals(proposalByCommericalAndMarketing.getSector())
                            && byCommericalAndMarketing.getLimitType().equals(proposalByCommericalAndMarketing.getLimitType())
                            && byCommericalAndMarketing.getDealerCode().equals(proposalByCommericalAndMarketing.getDealerCode())){

                        flag = true;

                    }
                }
                if(!flag){
                    proposalByCommericalAndMarketing.setId(IdUtils.simpleUUID());
                    proposalByCommericalAndMarketing.setInstanceId(instanceId);
                    proposalByCommericalAndMarketingMapper.insertProposalByCommericalAndMarketing(proposalByCommericalAndMarketing);
                    annualReviewyService.saveDbLog("1","刷新时新增表单信息","PROPOSAL BY COMMERCIAL & MARKETING",instanceId,null,proposalByCommericalAndMarketing,null);

                }
            }
        }


        Object wholesalePerformanceRecentThreeMonthsOs = map.get("wholesale_performance_currently_1");
        List<WholesalePerformanceCurrently> wholesalePerformanceCurrentlyList = (List<WholesalePerformanceCurrently>) wholesalePerformanceRecentThreeMonthsOs;

        if (wholesalePerformanceRecentThreeMonthsOs != null) {
            System.out.println("wholesalePerformanceCurrentlyList==" + wholesalePerformanceCurrentlyList);
            String wholesalePerformanceCurrentlyStr = JSON.toJSONString(wholesalePerformanceCurrentlyList);
            wholesalePerformanceCurrentlyList = JSON.parseArray(wholesalePerformanceCurrentlyStr, WholesalePerformanceCurrently.class);
        }

        List<WholesalePerformanceCurrently>  wholesalePerformanceCurrentlys = wholesalePerformanceCurrentlyMapper.selectWholesalePerformanceCurrentlyByInstanceId(instanceId);
        if(wholesalePerformanceCurrentlys !=null && wholesalePerformanceCurrentlys.size()>0
                && wholesalePerformanceCurrentlyList !=null && wholesalePerformanceCurrentlyList.size()>0 ){
            for (WholesalePerformanceCurrently wholesalePerformanceCurrently : wholesalePerformanceCurrentlys) {
                for (WholesalePerformanceCurrently performanceCurrently : wholesalePerformanceCurrentlyList) {
                    if(performanceCurrently.getDealername().equals(wholesalePerformanceCurrently.getDealername())
                            && performanceCurrently.getLimitType().equals(wholesalePerformanceCurrently.getLimitType())
                            && performanceCurrently.getSector().equals(wholesalePerformanceCurrently.getSector())
                    ){
                        wholesalePerformanceCurrently.setActiveLimit(performanceCurrently.getActiveLimit());
                        wholesalePerformanceCurrently.setOs(performanceCurrently.getOs());
                        wholesalePerformanceCurrently.setCreditLimit(performanceCurrently.getCreditLimit());
                        // wholesalePerformanceCurrently.setExpiredDate(performanceCurrently.getExpiredDate());
                        if(wholesalePerformanceCurrently.getId()!=null){
                            WholesalePerformanceCurrently wholesalePerformanceCurrently1 = wholesalePerformanceCurrentlyMapper.selectWholesalePerformanceCurrentlyById(wholesalePerformanceCurrently.getId());
                            wholesalePerformanceCurrentlyMapper.updateWholesalePerformanceCurrently(wholesalePerformanceCurrently);
                            annualReviewyService.saveDbLog("3","刷新时修改表单信息","WHOLESALE PERFORMANCE-CURRENTLY",instanceId,wholesalePerformanceCurrently1,wholesalePerformanceCurrently,null);

                        }

                    }
                }
            }

        }
        if(wholesalePerformanceCurrentlys !=null && wholesalePerformanceCurrentlys.size()>0
                && wholesalePerformanceCurrentlyList !=null && wholesalePerformanceCurrentlyList.size()>0 ){
            for (WholesalePerformanceCurrently wholesalePerformanceCurrently : wholesalePerformanceCurrentlyList) {
                boolean flag =false;
                for (WholesalePerformanceCurrently performanceCurrently : wholesalePerformanceCurrentlys) {
                    if(performanceCurrently.getDealername().equals(wholesalePerformanceCurrently.getDealername())
                            && performanceCurrently.getLimitType().equals(wholesalePerformanceCurrently.getLimitType())
                            && performanceCurrently.getSector().equals(wholesalePerformanceCurrently.getSector())
                    ){
                        flag =true;

                    }
                }
                if(!flag){
                    wholesalePerformanceCurrently.setId(IdUtils.simpleUUID());
                    wholesalePerformanceCurrently.setInstanceId(instanceId);
                    wholesalePerformanceCurrentlyMapper.insertWholesalePerformanceCurrently(wholesalePerformanceCurrently);
                    annualReviewyService.saveDbLog("1","刷新时新增表单信息","WHOLESALE PERFORMANCE-CURRENTLY",instanceId,null,wholesalePerformanceCurrently,null);

                }
            }

        }
        if((wholesalePerformanceCurrentlys ==null || wholesalePerformanceCurrentlys.size() ==0 )
                && wholesalePerformanceCurrentlyList !=null && wholesalePerformanceCurrentlyList.size()>0 ){
            for (WholesalePerformanceCurrently wholesalePerformanceCurrently : wholesalePerformanceCurrentlyList) {
                wholesalePerformanceCurrently.setId(IdUtils.simpleUUID());
                wholesalePerformanceCurrently.setInstanceId(instanceId);
                wholesalePerformanceCurrentlyMapper.insertWholesalePerformanceCurrently(wholesalePerformanceCurrently);
            }
        }
        log.info("刷新wfs数据结束");
        return  AjaxResult.success();
    }

    public AjaxResult getDeposit(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException {
        JSONObject jsonObject = new JSONObject();
        List<String[]> dealerCodeArr = depositDecreaseDTO.getDealerCodeArr();
        jsonObject.put("DealerCode", depositDecreaseDTO.getDealerCodeArr());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//        HttpPost httpPost = new HttpPost("http://10.226.186.86:8081/CreditLoanSuspenseAPIUAT7/CreditInfo");
        HttpPost httpPost = new HttpPost(wfsUrl + "SuspenseInfo");
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost.setHeader("Connection", "keep-alive");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String, Object> objectList = new HashMap<>();
        List<Map<String, String>> maps = new ArrayList<>();
        ArrayList<String> stringNew = new ArrayList<>();
        ArrayList<String> stringOld = new ArrayList<>();
        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseStr = EntityUtils.toString(response.getEntity());
            log.info("WFS外部接口数据返回结果=[{}]", responseStr);
            objectList = JSONObject.parseObject(responseStr, Map.class);
            log.info("WFS外部接口数据返回JSON结果=[{}]", JSON.toJSONString(responseStr));
            try{
              /*  stringObjectHashMap = JSONObject.parseObject(String.valueOf(JSONObject.parseObject(String.valueOf(JSONObject.parseArray(JSONObject.toJSONString(JSONObject.parseArray(JSONObject.toJSONString(objectList.get("Data"))).get(0))).get(0)))), Map.class);
                System.out.println(stringObjectHashMap);*/
                Object data = objectList.get("Data");
                JSONArray data1 = JSONObject.parseArray(JSONObject.toJSONString(objectList.get("Data")));
                if(data1!=null && data1.size()>0){
                    for (Object o : data1) {
                        System.out.println(o);
                        JSONArray data2 = JSONObject.parseArray(JSONObject.toJSONString(o));
                        if(data2!=null && data2.size()>0){
                            for (Object o1 : data2) {
                                Map<String, Object>  stringObjectHashMap =  JSONObject.parseObject(JSONObject.toJSONString(o1),Map.class);
                                if(stringObjectHashMap != null && stringObjectHashMap.get("DealerCode")!=null && stringObjectHashMap.get("SuspenseInfo")!=null ){
                                    Map<String, Object>  suspenseInfo = JSONObject.parseObject(JSONObject.toJSONString(stringObjectHashMap.get("SuspenseInfo")), Map.class);
                                    if(suspenseInfo!=null && suspenseInfo.get("Deposit")!=null){
                                        HashMap<String, String> stringStringHashMap = new HashMap<>();
                                        stringStringHashMap.put("DealerCode",stringObjectHashMap.get("DealerCode").toString());
                                        stringNew.add(stringObjectHashMap.get("DealerCode").toString());
                                        stringStringHashMap.put("Deposit",suspenseInfo.get("Deposit").toString());
                                        if(suspenseInfo.get("CBDeposit")!=null){
                                            stringStringHashMap.put("CBDeposit",suspenseInfo.get("CBDeposit").toString());
                                        }
                                        if(suspenseInfo.get("PartDeposit")!=null){
                                            stringStringHashMap.put("PartDeposit",suspenseInfo.get("PartDeposit").toString());
                                        }
                                        if(suspenseInfo.get("UsedDeposit")!=null){
                                            stringStringHashMap.put("UsedDeposit",suspenseInfo.get("UsedDeposit").toString());
                                        }
                                        stringStringHashMap.put("TEMPDeposit","-");
                                        maps.add(stringStringHashMap);
                                    }
                                }
                            }
                        }
                    }
                }
            }catch (NullPointerException e){
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String[]> dealerCodeArr1 = depositDecreaseDTO.getDealerCodeArr();
        for (String[] strings : dealerCodeArr1) {
            if(strings!=null && strings.length>0){
                for (String string : strings) {
                    stringOld.add(string);
                }
            }
        }
        if(stringOld.size()>stringNew.size()){
            stringOld.removeAll(stringNew);
            if(stringOld.size()>0){
                for (String s : stringOld) {
                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                    stringStringHashMap.put("DealerCode",s);
                    stringStringHashMap.put("Deposit","");
                    stringStringHashMap.put("CBDeposit","");
                    stringStringHashMap.put("PartDeposit","");
                    stringStringHashMap.put("TEMPDeposit","-");
                    stringStringHashMap.put("UsedDeposit","");
                    maps.add(stringStringHashMap);
                }
            }
        }
        return AjaxResult.success(maps);
    }

}
