package com.ruoyi.feike.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.Fileupload;
import com.ruoyi.feike.domain.HAml;
import com.ruoyi.feike.domain.HRpt;
import com.ruoyi.feike.mapper.HAmlMapper;
import com.ruoyi.feike.mapper.HRptMapper;
import com.ruoyi.feike.service.IFileuploadService;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.Plm;
import com.ruoyi.feike.service.IPlmService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * plm导入表Controller
 *
 * @author ruoyi
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/feike/plm")
public class PlmController extends BaseController
{
    @Autowired
    private IPlmService plmService;
    @Autowired
    private TokenService tokenService;

    @Autowired
    private HAmlMapper amlMapper;

    @Autowired
    private HRptMapper rptMapper;

    @Autowired
    private IFileuploadService fileuploadService;

    /**
     * 查询plm导入表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Plm plm)
    {
        startPage();
        //yyyyMM转yyyy-MM
        plm.setMonth(DateUtils.conversion(plm.getMonth()));
        List<Plm> list = plmService.selectPlmList(plm);
        return getDataTable(list);
    }


    @GetMapping("/amlList")
    public TableDataInfo amlList(HAml aml)
    {
        startPage();
        String name =null;
        if(StringUtils.isNotEmpty(aml.getName())){
            name = aml.getName();
        }
        List<HAml> amls = amlMapper.selectHAmlInfoList(name);
        return getDataTable(amls);
    }

    @GetMapping("/rptList")
    public TableDataInfo rptList(HRpt rpt)
    {
        Map<String, Object> map = new HashMap<>();
        startPage();
        String name = null;
        if(StringUtils.isNotEmpty(rpt.getName())){
            name = rpt.getName();
        }
        List<HRpt> rptList = rptMapper.selectHRptInfoList(name);
        return getDataTable(rptList);
    }

    @GetMapping("/getRptUpdateTime")
    public AjaxResult getRptUpdateTime(HRpt rpt)
    {
        Map<String, Object> map = new HashMap<>();
        Fileupload fileupload = new Fileupload();
        fileupload.setType("importRpt");
        List<Fileupload> files = fileuploadService.selectFileUploadByMAX(fileupload);
        if(files !=null && files.size() > 0){
            Date fileCreateTime = files.get(0).getFileCreateTime();
            String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fileCreateTime);
            map.put("updateTime",format);
        }
        return AjaxResult.success(map);
    }

    /**
     * 导出plm导入表列表
     */
    @Log(title = "plm导入表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public AjaxResult export(@RequestBody Plm plm)
    {
        if(plm.getIds() !=null && plm.getIds().size()>0){
            List<Plm> list = plmService.selectPlmListByIds(plm.getIds());
            ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
            return util.exportExcel(list, "plm");
        }
        List<Plm> list = plmService.selectPlmList(plm);
        ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
        return util.exportExcel(list, "plm");
    }

    @Log(title = "aml导出表", businessType = BusinessType.EXPORT)
    @PostMapping("/amlExport")
    public AjaxResult amlExport(@RequestBody HAml aml)
    {
        String name =null;
        if(StringUtils.isNotEmpty(aml.getName())){
            name = aml.getName();
        }
        List<HAml> amls = amlMapper.selectHAmlInfoList(name);
        ExcelUtil<HAml> util = new ExcelUtil<HAml>(HAml.class);
        return util.exportExcel(amls, "aml");
    }

    @Log(title = "rpt导出表", businessType = BusinessType.EXPORT)
    @PostMapping("/rptExport")
    public AjaxResult rptExport(@RequestBody HRpt rpt)
    {
        String name =null;
        if(StringUtils.isNotEmpty(rpt.getName())){
            name = rpt.getName();
        }
        List<HRpt> rptList = rptMapper.selectHRptInfoList(name);
        ExcelUtil<HRpt> util = new ExcelUtil<HRpt>(HRpt.class);
        return util.exportExcel(rptList, "rpt");
    }


    /**
     * 获取plm导入表详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(plmService.selectPlmById(id));
    }

    /**
     * 新增plm导入表
     */
    @Log(title = "plm导入表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Plm plm)
    {
        return toAjax(plmService.insertPlm(plm));
    }

    /**
     * 修改plm导入表
     */
    @Log(title = "plm导入表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Plm plm)
    {
        return toAjax(plmService.updatePlm(plm));
    }

    /**
     * 删除plm导入表
     */
    @Log(title = "plm导入表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(plmService.deletePlmByIds(ids));
    }

    @Log(title = "导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
        List<Plm> userList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = plmService.importUser(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }
    @Log(title = "AML批量导入", businessType = BusinessType.INSERT)
    @PostMapping("/importAmlData")
    public AjaxResult importAmlData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<HAml> util = new ExcelUtil<HAml>(HAml.class);
        List<HAml> userList = util.importExcelAml(file.getInputStream());
        String message = "";
        if(userList.size()>0){
            amlMapper.deleteAml();
            amlMapper.insertHAmlInfo(userList);
            message ="导入成功";
            String filePath = RuoYiConfig.getUploadPath();
            String fileName = FileUploadUtils.upload(filePath, file);
            Fileupload fileupload = new Fileupload();
            fileupload.setFileName(fileName);
            fileupload.setId(IdUtils.simpleUUID());
            fileupload.setCreateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
            fileupload.setCreateName(SecurityUtils.getNickName());
            fileupload.setFileCreateTime(DateUtils.getNowDate());
            fileupload.setType("importAml");
            fileuploadService.insertFileupload(fileupload);
        }else{
            message = "导入失败";
        }
        return AjaxResult.success(message);
    }
    @Log(title = "Rpt批量导入", businessType = BusinessType.INSERT)
    @PostMapping("/importRptData")
    public AjaxResult importRptData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<HRpt> util = new ExcelUtil<HRpt>(HRpt.class);
        List<HRpt> userList = util.importExcelAml(file.getInputStream());
        String message = "";
        if(userList.size()>0){
            rptMapper.deleteRpt();
            rptMapper.insertHRptInfo(userList);
            message ="导入成功";
            String filePath = RuoYiConfig.getUploadPath();
            String fileName = FileUploadUtils.upload(filePath, file);
            Fileupload fileupload = new Fileupload();
            fileupload.setFileName(fileName);
            fileupload.setId(IdUtils.simpleUUID());
            fileupload.setCreateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
            fileupload.setCreateName(SecurityUtils.getNickName());
            fileupload.setFileCreateTime(DateUtils.getNowDate());
            fileupload.setType("importRpt");
            fileuploadService.insertFileupload(fileupload);

        }else{
            message = "导入失败";
        }
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    public AjaxResult importTemplate()
    {
        ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
        return util.importTemplateExcel("用户数据");
    }
}
