package com.ruoyi.feike.controller;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.feike.domain.vo.VehicleContract;
import com.ruoyi.feike.mapper.BasicContractMapper;
import com.ruoyi.feike.service.IBasicContractService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ContractApproval;
import com.ruoyi.feike.service.IContractApprovalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * feikeController
 *
 * @author ruoyi
 * @date 2022-09-06
 */
@RestController
    @RequestMapping("/feike/contractApproval")
@Api(value = "op自发合同发起流程页",tags = "op自发合同",description = "op自发合同")
public class ContractApprovalController extends BaseController
{
    @Autowired
    private IContractApprovalService contractApprovalService;
    @Autowired
    private IBasicContractService basicContractService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:contractApproval:list')")
    @GetMapping("/list")
    public TableDataInfo list(ContractApproval contractApproval)
    {
        startPage();
        List<ContractApproval> list = contractApprovalService.selectContractApprovalList(contractApproval);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:contractApproval:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ContractApproval contractApproval)
    {
        List<ContractApproval> list = contractApprovalService.selectContractApprovalList(contractApproval);
        ExcelUtil<ContractApproval> util = new ExcelUtil<ContractApproval>(ContractApproval.class);
        return util.exportExcel(list, "contractApproval");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:contractApproval:query')")
    @GetMapping(value = "/s/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(contractApprovalService.selectContractApprovalById(id));
    }

    /**
     * 获取feike详细信息
     */
//    @PreAuthorize("@ss.hasPermi('feike:contractApproval:query')")
    @GetMapping(value = "/getByInstanceId/{instanceId}")
    public AjaxResult getInfoByInstanceId(@PathVariable("instanceId") String instanceId)
    {
        return AjaxResult.success(contractApprovalService.getInfoByInstanceId(instanceId));
    }

    /**
     * 获取feike详细信息
     */
//    @PreAuthorize("@ss.hasPermi('feike:contractApproval:query')")
    @GetMapping(value = "/getFileInfo")
    public AjaxResult getFileInfo(@RequestParam("sector") String sector,@RequestParam("dealerName") String dealerName)
    {
        return AjaxResult.success(basicContractService.selectBasicContractBySector(sector,dealerName));
    }




    /**
     * 本地资源通用下载
     */
    @GetMapping("/download")
    public void resourceDownload(@RequestParam("fileUrl") String fileUrl, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        // 本地资源路径
        String localPath = RuoYiConfig.getProfile();
        // 数据库资源地址
        String downloadPath = localPath + fileUrl;
        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, downloadName));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
    }

    /**
     * 新增feike
     */
//    @PreAuthorize("@ss.hasPermi('feike:contractApproval:add')")
//    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("insert")
    public AjaxResult add(@RequestBody Map<String, Object> map)
    {
        return toAjax(contractApprovalService.insertContractApproval(map));
    }

    /**
     * 根据合同数生成试驾车流程
     */
//    @PreAuthorize("@ss.hasPermi('feike:contractApproval:add')")
//    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("/addContractList")
    public AjaxResult addContractList(@RequestBody Map<String, Object> map)
    {
        return toAjax(contractApprovalService.insertContractApprovalList(map));
    }

    /**
     * 生成试驾车合同
     */
    @PostMapping("/generateContract")
    public AjaxResult generateContract(@RequestBody List<VehicleContract> vehicleContracts)
    {
        return AjaxResult.success(contractApprovalService.generateContract(vehicleContracts));
    }


    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:contractApproval:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PostMapping("edit")
    public AjaxResult edit(@RequestBody ContractApproval contractApproval)
    {
        return toAjax(contractApprovalService.updateContractApproval(contractApproval));
    }

    /**
     * 根据InstanceId 修改feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:contractApproval:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PostMapping("/editInstanceId")
    public AjaxResult editInstanceId(@RequestBody ContractApproval contractApproval)
    {
        return toAjax(contractApprovalService.updateContractApprovalByInstanceId(contractApproval));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:contractApproval:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(contractApprovalService.deleteContractApprovalByIds(ids));
    }
}
