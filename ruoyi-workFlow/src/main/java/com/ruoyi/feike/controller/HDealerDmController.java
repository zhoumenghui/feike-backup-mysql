package com.ruoyi.feike.controller;

import java.util.List;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.HDealerDm;
import com.ruoyi.feike.service.IAnnualReviewyService;
import com.ruoyi.feike.service.IHDealerDmService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 经销商与dm岗位用户映射Controller
 *
 * @author ruoyi
 * @date 2022-12-23
 */
@RestController
@RequestMapping("/system/dm")
public class HDealerDmController extends BaseController
{
    @Autowired
    private IHDealerDmService hDealerDmService;

    @Autowired
    private IAnnualReviewyService annualReviewyService;

    /**
     * 查询经销商与dm岗位用户映射列表
     */
   // @PreAuthorize("@ss.hasPermi('system:dm:list')")
    @GetMapping("/list")
    public TableDataInfo list(HDealerDm hDealerDm)
    {
        startPage();
        List<HDealerDm> list = hDealerDmService.selectHDealerDmList(hDealerDm);
        return getDataTable(list);
    }

    /**
     * 导出经销商与dm岗位用户映射列表
     */
   // @PreAuthorize("@ss.hasPermi('system:dm:export')")
    @Log(title = "经销商与dm岗位用户映射", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(HDealerDm hDealerDm)
    {
        List<HDealerDm> list = hDealerDmService.selectHDealerDmList(hDealerDm);
        ExcelUtil<HDealerDm> util = new ExcelUtil<HDealerDm>(HDealerDm.class);
        return util.exportExcel(list, "dm");
    }

    /**
     * 获取经销商与dm岗位用户映射详细信息
     */
    //@PreAuthorize("@ss.hasPermi('system:dm:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(hDealerDmService.selectHDealerDmById(id));
    }

    /**
     * 新增经销商与dm岗位用户映射
     */
    //@PreAuthorize("@ss.hasPermi('system:dm:add')")
    @Log(title = "经销商与dm岗位用户映射", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HDealerDm hDealerDm)
    {
        annualReviewyService.saveDbLog("1","新增经销商与用户关系",null,null,null,hDealerDm,"Dealer Dm Mapping/新增");
        return toAjax(hDealerDmService.insertHDealerDm(hDealerDm));
    }

    /**
     * 修改经销商与dm岗位用户映射
     */
    //@PreAuthorize("@ss.hasPermi('system:dm:edit')")
    @Log(title = "经销商与dm岗位用户映射", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HDealerDm hDealerDm)
    {
    if (hDealerDm.getId()!=null){
        HDealerDm hDealerDm1 = hDealerDmService.selectHDealerDmById(hDealerDm.getId());
        int i = hDealerDmService.updateHDealerDm(hDealerDm);
        HDealerDm hDealerDm2 = hDealerDmService.selectHDealerDmById(hDealerDm.getId());
        annualReviewyService.saveDbLog("3","修改经销商与用户关系信息",null,null,hDealerDm1,hDealerDm2,"Dealer Dm Mapping/修改");
    }
        return toAjax(1);
    }

    /**
     * 删除经销商与dm岗位用户映射
     */
    //@PreAuthorize("@ss.hasPermi('system:dm:remove')")
    @Log(title = "经销商与dm岗位用户映射", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            HDealerDm hDealerDm1 = hDealerDmService.selectHDealerDmById(id);
            if(hDealerDm1!=null){
                annualReviewyService.saveDbLog("2","删除经销商与用户关系信息",null,null,hDealerDm1,null,"Dealer Dm Mapping/删除");

            }
        }
        return toAjax(hDealerDmService.deleteHDealerDmByIds(ids));
    }
}
