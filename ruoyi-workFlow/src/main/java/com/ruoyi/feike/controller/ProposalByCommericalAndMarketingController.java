package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ProposalByCommericalAndMarketing;
import com.ruoyi.feike.service.IProposalByCommericalAndMarketingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-18
 */
@RestController
@RequestMapping("/feike/marketing")
public class ProposalByCommericalAndMarketingController extends BaseController
{
    @Autowired
    private IProposalByCommericalAndMarketingService proposalByCommericalAndMarketingService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:marketing:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        startPage();
        List<ProposalByCommericalAndMarketing> list = proposalByCommericalAndMarketingService.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:marketing:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        List<ProposalByCommericalAndMarketing> list = proposalByCommericalAndMarketingService.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing);
        ExcelUtil<ProposalByCommericalAndMarketing> util = new ExcelUtil<ProposalByCommericalAndMarketing>(ProposalByCommericalAndMarketing.class);
        return util.exportExcel(list, "marketing");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:marketing:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(proposalByCommericalAndMarketingService.selectProposalByCommericalAndMarketingById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:marketing:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        return toAjax(proposalByCommericalAndMarketingService.insertProposalByCommericalAndMarketing(proposalByCommericalAndMarketing));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:marketing:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        return toAjax(proposalByCommericalAndMarketingService.updateProposalByCommericalAndMarketing(proposalByCommericalAndMarketing));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:marketing:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(proposalByCommericalAndMarketingService.deleteProposalByCommericalAndMarketingByIds(ids));
    }
}
