package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.DetailInformation;
import com.ruoyi.feike.service.IDetailInformationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-06
 */
@RestController
@RequestMapping("/feike/detailInformation")
public class DetailInformationController extends BaseController
{
    @Autowired
    private IDetailInformationService detailInformationService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:detailInformation:list')")
    @GetMapping("/list")
    public TableDataInfo list(DetailInformation detailInformation)
    {
        startPage();
        List<DetailInformation> list = detailInformationService.selectDetailInformationList(detailInformation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:detailInformation:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DetailInformation detailInformation)
    {
        List<DetailInformation> list = detailInformationService.selectDetailInformationList(detailInformation);
        ExcelUtil<DetailInformation> util = new ExcelUtil<DetailInformation>(DetailInformation.class);
        return util.exportExcel(list, "detailInformation");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:detailInformation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(detailInformationService.selectDetailInformationById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:detailInformation:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DetailInformation detailInformation)
    {
        return toAjax(detailInformationService.insertDetailInformation(detailInformation));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:detailInformation:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DetailInformation detailInformation)
    {
        return toAjax(detailInformationService.updateDetailInformation(detailInformation));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:detailInformation:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(detailInformationService.deleteDetailInformationByIds(ids));
    }
}
