package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ActiveLimitAdjustmentProposal;
import com.ruoyi.feike.service.IActiveLimitAdjustmentProposalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/proposal")
public class ActiveLimitAdjustmentProposalController extends BaseController
{
    @Autowired
    private IActiveLimitAdjustmentProposalService activeLimitAdjustmentProposalService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:proposal:list')")
    @GetMapping("/list")
    public TableDataInfo list(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        startPage();
        List<ActiveLimitAdjustmentProposal> list = activeLimitAdjustmentProposalService.selectActiveLimitAdjustmentProposalList(activeLimitAdjustmentProposal);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:proposal:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        List<ActiveLimitAdjustmentProposal> list = activeLimitAdjustmentProposalService.selectActiveLimitAdjustmentProposalList(activeLimitAdjustmentProposal);
        ExcelUtil<ActiveLimitAdjustmentProposal> util = new ExcelUtil<ActiveLimitAdjustmentProposal>(ActiveLimitAdjustmentProposal.class);
        return util.exportExcel(list, "proposal");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:proposal:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(activeLimitAdjustmentProposalService.selectActiveLimitAdjustmentProposalById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:proposal:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        return toAjax(activeLimitAdjustmentProposalService.insertActiveLimitAdjustmentProposal(activeLimitAdjustmentProposal));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:proposal:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        return toAjax(activeLimitAdjustmentProposalService.updateActiveLimitAdjustmentProposal(activeLimitAdjustmentProposal));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:proposal:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(activeLimitAdjustmentProposalService.deleteActiveLimitAdjustmentProposalByIds(ids));
    }
}
