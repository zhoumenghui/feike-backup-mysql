package com.ruoyi.feike.controller;

import java.util.List;

import com.ruoyi.feike.domain.NewSumKeep;
import com.ruoyi.feike.domain.NewSumKeepFour;
import com.ruoyi.feike.service.INewSumKeepService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ReportInsurance;
import com.ruoyi.feike.service.IReportInsuranceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
@RestController
@RequestMapping("/feike/reportInsurance")
public class ReportInsuranceController extends BaseController
{
    @Autowired
    private IReportInsuranceService reportInsuranceService;

    @Autowired
    private INewSumKeepService newSumKeepService;

    /**
     * 查询feike列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ReportInsurance reportInsurance)
    {
        startPage();
        List<ReportInsurance> list = reportInsuranceService.selectReportInsuranceList(reportInsurance);
        return getDataTable(list);
    }

    @GetMapping("/listFour")
    public TableDataInfo listFour(NewSumKeep newSumKeep)
    {
        startPage();
        List<NewSumKeepFour> list = newSumKeepService.selectNewSumKeepListFour(newSumKeep);
        return getDataTable(list);
    }

    @GetMapping("/exportFour")
    public AjaxResult exportFour(NewSumKeep newSumKeep)
    {
        List<NewSumKeepFour> list = newSumKeepService.selectNewSumKeepListFour(newSumKeep);
        ExcelUtil<NewSumKeepFour> util = new ExcelUtil<NewSumKeepFour>(NewSumKeepFour.class);
        return util.exportExcel(list, "newSumKeepFour");
    }

    /**
     * 导出feike列表
     */
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ReportInsurance reportInsurance)
    {
        List<ReportInsurance> list = reportInsuranceService.selectReportInsuranceList(reportInsurance);
        ExcelUtil<ReportInsurance> util = new ExcelUtil<ReportInsurance>(ReportInsurance.class);
        return util.exportExcel(list, "reportInsurance");
    }

    /**
     * 获取feike详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(reportInsuranceService.selectReportInsuranceById(id));
    }

    /**
     * 新增feike
     */
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReportInsurance reportInsurance)
    {
        return toAjax(reportInsuranceService.insertReportInsurance(reportInsurance));
    }

    /**
     * 修改feike
     */
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReportInsurance reportInsurance)
    {
        return toAjax(reportInsuranceService.updateReportInsurance(reportInsurance));
    }

    /**
     * 删除feike
     */
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(reportInsuranceService.deleteReportInsuranceByIds(ids));
    }
}
