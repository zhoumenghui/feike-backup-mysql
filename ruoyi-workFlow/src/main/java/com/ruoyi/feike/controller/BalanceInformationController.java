package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.BalanceInformation;
import com.ruoyi.feike.service.IBalanceInformationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/balanceInformation")
public class BalanceInformationController extends BaseController
{
    @Autowired
    private IBalanceInformationService balanceInformationService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:balanceInformation:list')")
    @GetMapping("/list")
    public TableDataInfo list(BalanceInformation balanceInformation)
    {
        startPage();
        List<BalanceInformation> list = balanceInformationService.selectBalanceInformationList(balanceInformation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:balanceInformation:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BalanceInformation balanceInformation)
    {
        List<BalanceInformation> list = balanceInformationService.selectBalanceInformationList(balanceInformation);
        ExcelUtil<BalanceInformation> util = new ExcelUtil<BalanceInformation>(BalanceInformation.class);
        return util.exportExcel(list, "balanceInformation");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:balanceInformation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(balanceInformationService.selectBalanceInformationById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:balanceInformation:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BalanceInformation balanceInformation)
    {
        return toAjax(balanceInformationService.insertBalanceInformation(balanceInformation));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:balanceInformation:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BalanceInformation balanceInformation)
    {
        return toAjax(balanceInformationService.updateBalanceInformation(balanceInformation));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:balanceInformation:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(balanceInformationService.deleteBalanceInformationByIds(ids));
    }
}
