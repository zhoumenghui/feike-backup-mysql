package com.ruoyi.feike.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollectionUtil;
import com.ruoyi.common.core.page.ResultData;
import com.ruoyi.feike.domain.vo.OtherFinancingResourceShowVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.OtherFinancingResource;
import com.ruoyi.feike.service.IOtherFinancingResourceService;

/**
 * feikeController
 *
 * @author zmh
 * @date 2022-07-11
 */
@Api(value = "OTHER FINANCING RESOURCE",tags = "OTHER FINANCING RESOURCE",description = "年审列表下面用")
@RestController
@RequestMapping("/feike/resource")
public class OtherFinancingResourceController extends BaseController
{
    @Autowired
    private IOtherFinancingResourceService otherFinancingResourceService;

    /**
     * 查询feike列表
     */
    @ApiOperation("机构信息列表")
   // @PreAuthorize("@ss.hasPermi('feike:resource:list')")
    @PostMapping ("/list")
    public ResultData list(@RequestBody List<String> dealerNames)
    {
        OtherFinancingResourceShowVO vo= new OtherFinancingResourceShowVO();
        if(dealerNames.size() < 1){
            return getResult(null);
        }
        List<OtherFinancingResource> list = otherFinancingResourceService.selectOtherFinancingResourceList(dealerNames);
        List<String> dealNames = new ArrayList<>();
        if(CollectionUtil.isNotEmpty(list)){
            dealNames = list.stream().map(OtherFinancingResource::getDealername).distinct().collect(Collectors.toList());
        }
        vo.setTempList(list);
        vo.setDealNames(dealNames);
        return getResult(vo);
    }

    /**
     * 导出feike列表
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:export')")
/*    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @ApiOperation("机构信息导出")
    @GetMapping("/export")
    public AjaxResult export(OtherFinancingResource otherFinancingResource)
    {
        List<OtherFinancingResource> list = otherFinancingResourceService.selectOtherFinancingResourceList(otherFinancingResource);
        ExcelUtil<OtherFinancingResource> util = new ExcelUtil<OtherFinancingResource>(OtherFinancingResource.class);
        return util.exportExcel(list, "resource");
    }*/

    /**
     * 获取feike详细信息
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:query')")
    @ApiOperation("机构信息查询")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(otherFinancingResourceService.selectOtherFinancingResourceById(id));
    }

    /**
     * 新增feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:add')")

    @ApiOperation("机构信息添加")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OtherFinancingResource otherFinancingResource)
    {
        return toAjax(otherFinancingResourceService.insertOtherFinancingResource(otherFinancingResource));
    }

    /**
     * 修改feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:edit')")
    @ApiOperation("机构信息修改")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OtherFinancingResource otherFinancingResource)
    {
        return toAjax(otherFinancingResourceService.updateOtherFinancingResource(otherFinancingResource));
    }

    /**
     * 删除feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:remove')")
    @ApiOperation("机构信息删除")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(otherFinancingResourceService.deleteOtherFinancingResourceByIds(ids));
    }

    /**
     * 删除feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:remove')")
    @ApiOperation("机构信息删除")
    @Log(title = "feike", businessType = BusinessType.DELETE)
    @DeleteMapping("/deleteById/{id}")
    public AjaxResult deleteById(@PathVariable String id)
    {
        return toAjax(otherFinancingResourceService.deleteOtherFinancingResourceById(id));
    }
}
