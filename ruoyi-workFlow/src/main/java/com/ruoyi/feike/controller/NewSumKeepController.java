package com.ruoyi.feike.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.stream.CollectorUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.feike.domain.PaidSum;
import com.ruoyi.system.mapper.SysPostMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.NewSumKeep;
import com.ruoyi.feike.service.INewSumKeepService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 *
 * @author zmh
 * @date 2022-08-31
 */
@Api("WfsLimitSetUp流程信息维护数据")
@RestController
@RequestMapping("/feike/newSumKeep")
public class NewSumKeepController extends BaseController
{
    @Autowired
    private INewSumKeepService newSumKeepService;

    @Autowired
    private SysPostMapper sysPostMapper;



    /**
     * 查询feike列表
     */
    @ApiOperation("因保险方案变更需补缴的保费")
    @GetMapping("/list")
    public TableDataInfo list(NewSumKeep newSumKeep)
    {
        List<PaidSum> list = newSumKeepService.selectNewSumKeepList(newSumKeep);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(NewSumKeep newSumKeep)
    {
        List<PaidSum> list = newSumKeepService.selectNewSumKeepList(newSumKeep);
        ExcelUtil<PaidSum> util = new ExcelUtil<PaidSum>(PaidSum.class);
        return util.exportExcel(list, "PaidSum");
    }

    /**
     * 获取feike详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(newSumKeepService.selectNewSumKeepById(id));
    }

    /**
     * 新增feike
     */
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NewSumKeep newSumKeep)
    {
        return toAjax(newSumKeepService.insertNewSumKeep(newSumKeep));
    }

    /**
     * 修改feike
     */
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NewSumKeep newSumKeep)
    {
        return toAjax(newSumKeepService.updateNewSumKeep(newSumKeep));
    }

    /**
     * 删除feike
     */
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(newSumKeepService.deleteNewSumKeepByIds(ids));
    }

    /**
     * 新增流程 @RequestBody Map<String, Object> map
     */
    @PostMapping("/addACt")
    public AjaxResult addACt(@RequestBody Map<String, Object> map){
        return toAjax(newSumKeepService.addACt(map));
    }


    @GetMapping("/newSumSelect")
    public TableDataInfo newSum(NewSumKeep newSumKeep) {
        startPage();
        return getDataTable(newSumKeepService.newSumSelect(newSumKeep));
    }

    @GetMapping("/getByInstanceId/{instanceId}")
    public TableDataInfo getByInstanceId(@PathVariable("instanceId")String instanceId) {
        startPage();
        return getDataTable(newSumKeepService.getByInstanceId(instanceId));
    }

    @PostMapping("/update")
    public AjaxResult update(@RequestBody Map<String, Object> map){
        if (ObjectUtil.isNotEmpty(map)) {
            newSumKeepService.update(map);
            return AjaxResult.success();
        }else {
            return AjaxResult.error("数据为空");
        }
    }

    @GetMapping("/getDmNickName")
    public AjaxResult getDmNickName(){
        List<String> dm = sysPostMapper.selectUserListByPostCode("dm").stream().map(SysUser::getNickName).collect(Collectors.toList());
        if(CollectionUtil.isNotEmpty(dm)){
            return AjaxResult.success(dm);
        }else {
            return AjaxResult.error("系统无DM岗位用户");
        }
    }

    @GetMapping("/getUwNickName")
    public AjaxResult getUwNickName(){
        ArrayList<Object> UWS = new ArrayList<>();
        List<String> dm = sysPostMapper.selectUserListByPostCode("underwriter").stream().map(SysUser::getNickName).collect(Collectors.toList());
        for (String s : dm) {
            if(!s.equals("ry2") && !s.contains("test") ){
                UWS.add(s);
            }
        }
        if(CollectionUtil.isNotEmpty(UWS)){
            return AjaxResult.success(UWS);
        }else {
            return AjaxResult.error("系统无Underwriter岗位用户");
        }
    }

}
