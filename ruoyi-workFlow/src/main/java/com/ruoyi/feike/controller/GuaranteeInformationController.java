package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.GuaranteeInformation;
import com.ruoyi.feike.service.IGuaranteeInformationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-14
 */
@RestController
@RequestMapping("/feike/guaranteeInformation")
public class GuaranteeInformationController extends BaseController
{
    @Autowired
    private IGuaranteeInformationService guaranteeInformationService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:guaranteeInformation:list')")
    @GetMapping("/list")
    public TableDataInfo list(GuaranteeInformation guaranteeInformation)
    {
        startPage();
        List<GuaranteeInformation> list = guaranteeInformationService.selectGuaranteeInformationList(guaranteeInformation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:guaranteeInformation:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GuaranteeInformation guaranteeInformation)
    {
        List<GuaranteeInformation> list = guaranteeInformationService.selectGuaranteeInformationList(guaranteeInformation);
        ExcelUtil<GuaranteeInformation> util = new ExcelUtil<GuaranteeInformation>(GuaranteeInformation.class);
        return util.exportExcel(list, "guaranteeInformation");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:guaranteeInformation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(guaranteeInformationService.selectGuaranteeInformationById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:guaranteeInformation:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GuaranteeInformation guaranteeInformation)
    {
        return toAjax(guaranteeInformationService.insertGuaranteeInformation(guaranteeInformation));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:guaranteeInformation:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GuaranteeInformation guaranteeInformation)
    {
        return toAjax(guaranteeInformationService.updateGuaranteeInformation(guaranteeInformation));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:guaranteeInformation:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(guaranteeInformationService.deleteGuaranteeInformationByIds(ids));
    }
}
