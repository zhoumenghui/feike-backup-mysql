package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ApprovalStatusReport;
import com.ruoyi.feike.service.IApprovalStatusReportService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * DM提交流程报表Controller
 * 
 * @author feike
 * @date 2023-01-18
 */
@RestController
@RequestMapping("/feike/approvalStatusReport")
public class ApprovalStatusReportController extends BaseController
{
    @Autowired
    private IApprovalStatusReportService approvalStatusReportService;

    /**
     * 查询DM提交流程报表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ApprovalStatusReport approvalStatusReport)
    {
        startPage();
        List<ApprovalStatusReport> list = approvalStatusReportService.selectApprovalStatusReportList(approvalStatusReport);
        return getDataTable(list);
    }

    /**
     * 导出DM提交流程报表列表
     */
    @Log(title = "DM提交流程报表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ApprovalStatusReport approvalStatusReport)
    {
        List<ApprovalStatusReport> list = approvalStatusReportService.selectApprovalStatusReportList(approvalStatusReport);
        ExcelUtil<ApprovalStatusReport> util = new ExcelUtil<ApprovalStatusReport>(ApprovalStatusReport.class);
        return util.exportExcel(list, "approvalStatusReport");
    }

    /**
     * 获取DM提交流程报表详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(approvalStatusReportService.selectApprovalStatusReportById(id));
    }

    /**
     * 新增DM提交流程报表
     */
    @Log(title = "DM提交流程报表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ApprovalStatusReport approvalStatusReport)
    {
        return toAjax(approvalStatusReportService.insertApprovalStatusReport(approvalStatusReport));
    }

    /**
     * 修改DM提交流程报表
     */
    @Log(title = "DM提交流程报表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ApprovalStatusReport approvalStatusReport)
    {
        return toAjax(approvalStatusReportService.updateApprovalStatusReport(approvalStatusReport));
    }

    /**
     * 删除DM提交流程报表
     */
    @Log(title = "DM提交流程报表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(approvalStatusReportService.deleteApprovalStatusReportByIds(ids));
    }
}
