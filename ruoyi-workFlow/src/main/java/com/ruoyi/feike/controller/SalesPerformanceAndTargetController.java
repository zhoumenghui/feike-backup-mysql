package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.SalesPerformanceAndTarget;
import com.ruoyi.feike.service.ISalesPerformanceAndTargetService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-11
 */
@RestController
@RequestMapping("/feike/target")
public class SalesPerformanceAndTargetController extends BaseController
{
    @Autowired
    private ISalesPerformanceAndTargetService salesPerformanceAndTargetService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:target:list')")
    @GetMapping("/list")
    public TableDataInfo list(SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        startPage();
        List<SalesPerformanceAndTarget> list = salesPerformanceAndTargetService.selectSalesPerformanceAndTargetList(salesPerformanceAndTarget);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:target:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        List<SalesPerformanceAndTarget> list = salesPerformanceAndTargetService.selectSalesPerformanceAndTargetList(salesPerformanceAndTarget);
        ExcelUtil<SalesPerformanceAndTarget> util = new ExcelUtil<SalesPerformanceAndTarget>(SalesPerformanceAndTarget.class);
        return util.exportExcel(list, "target");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:target:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(salesPerformanceAndTargetService.selectSalesPerformanceAndTargetById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:target:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        return toAjax(salesPerformanceAndTargetService.insertSalesPerformanceAndTarget(salesPerformanceAndTarget));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:target:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        return toAjax(salesPerformanceAndTargetService.updateSalesPerformanceAndTarget(salesPerformanceAndTarget));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:target:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(salesPerformanceAndTargetService.deleteSalesPerformanceAndTargetByIds(ids));
    }
}
