package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.CbFleetDetails;
import com.ruoyi.feike.service.ICbFleetDetailsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/details")
public class CbFleetDetailsController extends BaseController
{
    @Autowired
    private ICbFleetDetailsService cbFleetDetailsService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:details:list')")
    @GetMapping("/list")
    public TableDataInfo list(CbFleetDetails cbFleetDetails)
    {
        startPage();
        List<CbFleetDetails> list = cbFleetDetailsService.selectCbFleetDetailsList(cbFleetDetails);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:details:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CbFleetDetails cbFleetDetails)
    {
        List<CbFleetDetails> list = cbFleetDetailsService.selectCbFleetDetailsList(cbFleetDetails);
        ExcelUtil<CbFleetDetails> util = new ExcelUtil<CbFleetDetails>(CbFleetDetails.class);
        return util.exportExcel(list, "details");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:details:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(cbFleetDetailsService.selectCbFleetDetailsById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:details:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CbFleetDetails cbFleetDetails)
    {
        return toAjax(cbFleetDetailsService.insertCbFleetDetails(cbFleetDetails));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:details:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CbFleetDetails cbFleetDetails)
    {
        return toAjax(cbFleetDetailsService.updateCbFleetDetails(cbFleetDetails));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:details:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cbFleetDetailsService.deleteCbFleetDetailsByIds(ids));
    }
}
