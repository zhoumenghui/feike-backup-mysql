package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.vo.ContractRecordFour;
import com.ruoyi.feike.domain.vo.ContractRecordTow;
import com.ruoyi.feike.service.IContractRecordFourSerivce;
import com.ruoyi.feike.service.IContractRecordTowSerivce;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@RequestMapping("/contract/recordFour")
public class ContractRecordFourController extends BaseController
{
    @Autowired
    private IContractRecordFourSerivce contractRecordFourSerivce;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询【请填写功能名称】列表
     */
    //@PreAuthorize("@ss.hasPermi('system:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(ContractRecordFour contractRecordFour)
    {
        startPage();

        List<ContractRecordFour> list = contractRecordFourSerivce.selectContractRecordFourList(contractRecordFour);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    //@PreAuthorize("@ss.hasPermi('system:record:export')")
    @Log(title = "【导出报表】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ContractRecordFour contractRecordFour)
    {
        List<ContractRecordFour> list = contractRecordFourSerivce.selectContractRecordFourList(contractRecordFour);
        ExcelUtil<ContractRecordFour> util = new ExcelUtil<ContractRecordFour>(ContractRecordFour.class);
        return util.exportExcel(list, "recordFourInfo");
    }


    /**
     * 新增【请填写功能名称】
     */
    @Log(title = "【新增报表】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ContractRecordFour contractRecordFour)
    {
        return toAjax(contractRecordFourSerivce.insertContractRecordFour(contractRecordFour));
    }


}
