package com.ruoyi.feike.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.Arith;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.vo.ContractRecord;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IParkingLocationRegistrationService;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;
import com.spire.doc.documents.BookmarksNavigator;
import jdk.nashorn.internal.parser.JSONParser;
import com.alibaba.fastjson.JSONArray;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api/fileWork/replace")
public class WordLabelController {
    @Autowired
    private PdfController pdfController;
    @Autowired
    private DealercodeContractMapper dealercodeContractMapper;
    @Autowired
    private SecuritiesMapper securitiesMapper;
    @Autowired
    private ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper;
    @Autowired
    private CreditConditionMapper creditConditionMapper;
    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;
    @Autowired
    private DealerInformationMapper dealerInformationMapper;
    @Autowired
    private CbFleetDetailsMapper cbFleetDetailsMapper;
    @Autowired
    private IParkingLocationRegistrationService parkingLocationRegistrationService;
    @Autowired
    private DealerSectorContractgroupMapper dealerSectorContractgroupMapper;
    @Autowired
    private ContractRecordMapper contractRecordMapper;
    @Autowired
    private SysDictDataMapper dictDataMapper;

    /**
     * 独立于其他word合同，单独需要生成的合同头部，用来作为pdf拼接后的合同头
     */
    @PostMapping("/generateWordHeader")
    public Map generateWordHeader(Map<String, Object> map) {
        String dealerName = (String) map.get("dealerName");
        String sector = (String) map.get("sector");
        String limitType = (String) map.get("limitType");
        String agtNo = (String) map.get("agtNo");
        String contracts = (String) map.get("contracts");
        String instanceId = (String) map.get("instanceId");
        //返回一个对像，里面放额度金额，到期日，比例，用于其他报表之类的表取值
        boolean isPart = (Boolean) map.get("isPart");

        String amount = null;//金额
        String deposit = null;//比例
        String expiredDate = null;//到期日

        Map<String, Object> stringMap = new HashMap<>();
        try {
            String contractlocation ="/profile/template/Wholesale Checklist.docx";
            String contractname ="Wholesale Checklist";
            String contract ="";
            Map<String, Object> hashMap = new HashMap<>();
            hashMap.put("contractname",contractname);
            hashMap.put("instanceid",instanceId);
            //加载包含书签的Word文档
            Document doc = new Document();
            doc.loadFromFile(RuoYiConfig.getProfile() + contractlocation.substring(8));


            //定位到指定书签位置
            BookmarksNavigator bookmarksNavigator = new BookmarksNavigator(doc);

            //用文本内容替换原有书签位置的文本，新替换的内容与原文格式一致
            bookmarksNavigator.moveToBookmark("经销商名称");
            bookmarksNavigator.replaceBookmarkContent(dealerName,true);

            bookmarksNavigator.moveToBookmark("合同号");
            bookmarksNavigator.replaceBookmarkContent(agtNo,true);

            bookmarksNavigator.moveToBookmark("主机厂");
            bookmarksNavigator.replaceBookmarkContent(sector,true);

            bookmarksNavigator.moveToBookmark("合同名称");
            bookmarksNavigator.replaceBookmarkContent(contracts,true);

            bookmarksNavigator.moveToBookmark("流水号");
            bookmarksNavigator.replaceBookmarkContent(instanceId,true);

            ProposalByCommericalAndMarketing proposalByCommericalAndMarketing = new ProposalByCommericalAndMarketing();
            proposalByCommericalAndMarketing.setDealername(dealerName);
            proposalByCommericalAndMarketing.setSector(sector);
            proposalByCommericalAndMarketing.setLimitType(limitType);
            proposalByCommericalAndMarketing.setInstanceId(instanceId);
            List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing);
            if(StringUtils.isNotEmpty(proposalByCommericalAndMarketings)){
                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalLimit()) && !proposalByCommericalAndMarketings.get(0).getProposalLimit().equals("")) {
                    bookmarksNavigator.moveToBookmark("额度金额");
                    DecimalFormat df = new DecimalFormat("#,###");
                    String format1 = df.format(Long.valueOf(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString()));
                    bookmarksNavigator.replaceBookmarkContent(format1,true);
                    amount = proposalByCommericalAndMarketings.get(0).getProposalLimit().toString();
                }

                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals("")) {
                    bookmarksNavigator.moveToBookmark("保证金比例");
                    bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit(),true);
                    deposit = proposalByCommericalAndMarketings.get(0).getProposalCashDeposit();
                }

//                if(limitType.equals("CB")) {
//                    double formerdepo = 0;
//                    double newdepo = 0;
//                    ProposalByCommericalAndMarketing pbam = new ProposalByCommericalAndMarketing();
//                    pbam.setDealername(dealerName);
//                    pbam.setSector(sector);
//                    pbam.setLimitType("CB");
//                    pbam.setInstanceId(instanceId);
//                    List<ProposalByCommericalAndMarketing> pbam1 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(pbam);
//                    pbam.setLimitType("NORMAL");
//                    List<ProposalByCommericalAndMarketing> pbam2 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(pbam);
//                    if (StringUtils.isNotEmpty(pbam1) && StringUtils.isNotEmpty(pbam1.get(0).getProposalCashDeposit())) {
//                        if (pbam1.get(0).getProposalCashDeposit().equals(0)) {
//                            formerdepo = 0;
//                        } else {
//                            formerdepo = Double.parseDouble(pbam1.get(0).getProposalCashDeposit().replace("%", ""));
//                        }
//                    }
//                    if (StringUtils.isNotEmpty(pbam2) && StringUtils.isNotEmpty(pbam2.get(0).getProposalCashDeposit())) {
//                        if (pbam2.get(0).getProposalCashDeposit().equals(0)) {
//                            newdepo = 0;
//                        } else {
//                            newdepo = Double.parseDouble(pbam2.get(0).getProposalCashDeposit().replace("%", ""));
//                        }
//                    }
//                    //这个是CB保证金比例，CD+NORMAL的合
//                    double add1 = Arith.add(formerdepo, newdepo);
//                    if (StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals(" ")) {
//                        bookmarksNavigator.moveToBookmark("保证金比例");
//                        bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(add1)) + "%", true);
//                    } else {
//                        bookmarksNavigator.moveToBookmark("保证金比例");
//                        bookmarksNavigator.replaceBookmarkContent("0", true);
//
//                    }
//                }
            }

            if(limitType.equals("NORMAL") || limitType.equals("Normal") && sector.toUpperCase().equals("PEUGEOT")){
                ProposalByCommericalAndMarketing proposalByCommericalAndMarketingParts = new ProposalByCommericalAndMarketing();
                proposalByCommericalAndMarketingParts.setDealername(dealerName);
                proposalByCommericalAndMarketingParts.setSector(sector);
                proposalByCommericalAndMarketingParts.setLimitType("Part");
                proposalByCommericalAndMarketingParts.setInstanceId(instanceId);
                List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingsParts = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketingParts);
                if(StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts)){
                    if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().equals("")) {
                        bookmarksNavigator.moveToBookmark("parts额度金额");
                        bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().toString(),true);
                    }
                    if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit().equals("")) {
                        bookmarksNavigator.moveToBookmark("parts保证金比例");
                        bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit(), true);
                    }
                }
            }
            //判断当他是Naveco 品牌的时候
            if(sector.equals("Naveco Daily")){
                if (limitType.equals("Normal")) {
                    //对三方合同附件二里的书签进行取值操作
                    //原贷款额度要取CB + NORMAL的额度和
                    double cb = 0;
                    double normal = 0;
                    double cb2 = 0;
                    double normal2 = 0;
                    ProposalByCommericalAndMarketing proposalByCommericalAndMarketing1 = new ProposalByCommericalAndMarketing();
                    proposalByCommericalAndMarketing1.setDealername(dealerName);
                    proposalByCommericalAndMarketing1.setSector(sector);
                    proposalByCommericalAndMarketing1.setLimitType("CB");
                    proposalByCommericalAndMarketing1.setInstanceId(instanceId);
                    List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings1 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing1);
                    proposalByCommericalAndMarketing1.setLimitType("NORMAL");
                    List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings2 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing1);

                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings1) && StringUtils.isNotNull(proposalByCommericalAndMarketings1.get(0).getProposalLimit()) && !proposalByCommericalAndMarketings.get(0).getProposalLimit().equals("")) {
                        cb2 = proposalByCommericalAndMarketings1.get(0).getProposalLimit();
                    }
                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings2) && StringUtils.isNotNull(proposalByCommericalAndMarketings2.get(0).getProposalLimit()) && !proposalByCommericalAndMarketings.get(0).getProposalLimit().equals("")) {
                        normal2 = proposalByCommericalAndMarketings2.get(0).getProposalLimit();
                    }
                    String add2 = Convert.getDoubleString(Arith.add(cb2, normal2));
                    bookmarksNavigator.moveToBookmark("额度金额");
                    DecimalFormat df = new DecimalFormat("#,###");
                    String format2 = df.format(Long.valueOf(add2));
                    bookmarksNavigator.replaceBookmarkContent(format2, true);
                    amount = Convert.toStr(add2);
                }
            }

            CreditCondition creditCondition = new CreditCondition();
            creditCondition.setDealername(dealerName);
            creditCondition.setSector(sector);
            creditCondition.setLimittype(limitType);
            creditCondition.setInstanceid(instanceId);
            List<CreditCondition> creditConditions = creditConditionMapper.selectCreditConditionList(creditCondition);
            DealercodeContract dealercodeContract = new DealercodeContract();
            dealercodeContract.setDealerNameCN(dealerName);
            dealercodeContract.setSector(sector);
            dealercodeContract.setLimitType(limitType);
            dealercodeContract.setInstanceId(instanceId);
            dealercodeContract.setContractName("CB 大客户");
            List<DealercodeContract> dealercodeContracts = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
            if(StringUtils.isNotEmpty(creditConditions)){
                if(StringUtils.isNotNull(creditConditions.get(0).getExpireddate())) {
                    String dateToStr = DateUtils.parseDateToStr("yyyy年MM月dd日", creditConditions.get(0).getExpireddate());
                    if(StringUtils.isEmpty(dealercodeContracts)){
                        bookmarksNavigator.moveToBookmark("到期日");
                        bookmarksNavigator.replaceBookmarkContent(dateToStr, true);
                    }
                    expiredDate = DateUtils.dateTime(creditConditions.get(0).getExpireddate());
                }
            }



            //查该经销商下对应得担保人和担保机构
            Securities securities = new Securities();
            securities.setInstanceId(instanceId);
            securities.setDealername(dealerName);
            List<Securities> securitiesList = securitiesMapper.selectSecuritiesList(securities);
            StringBuilder stringBuilder = new StringBuilder();
            StringBuilder stringBuilder1 = new StringBuilder();
            for(Securities se: securitiesList){
                if(StringUtils.isNotEmpty(se.getGuaranteeType())){
                    if(se.getGuaranteeType().equals("personal")){
                        if(!StringUtils.isEmpty(se.getProposalNameCN())){
                            stringBuilder.append(se.getProposalNameCN()).append(",");
                        }
                    }
                    if(se.getGuaranteeType().equals("corporate")){
                        if(!StringUtils.isEmpty(se.getProposalNameCN())){
                            stringBuilder1.append(se.getProposalNameCN()).append(",");
                        }
                    }
                }
            }



            if(StringUtils.isNotEmpty(stringBuilder)) {
                String danbaoren = (stringBuilder.substring(0, stringBuilder.length() - 1)).toString();//担保人
                bookmarksNavigator.moveToBookmark("担保人");
                bookmarksNavigator.replaceBookmarkContent(danbaoren, true);
            }

            if(StringUtils.isNotEmpty(stringBuilder1)) {
                String danbaojigou = (stringBuilder1.substring(0, stringBuilder1.length() - 1)).toString();//担保机构
                bookmarksNavigator.moveToBookmark("担保公司");
                bookmarksNavigator.replaceBookmarkContent(danbaojigou, true);
            }

            stringMap.put("amount",amount);
            stringMap.put("deposit",deposit);
            stringMap.put("expiredDate",expiredDate);

            //保存文档FileFormat.Docx_2013
            doc.saveToFile("/"+RuoYiConfig.getProfile()+"/wordfill/word/"+instanceId+"/"+contractname+".docx",FileFormat.Docx_2013);
            doc.dispose();

            //重新读取生成的文档
            InputStream is = new FileInputStream("/"+RuoYiConfig.getProfile()+"/wordfill/word/"+instanceId+"/"+contractname+".docx");
            XWPFDocument document = new XWPFDocument(is);
            //以上Spire.Doc 生成的文件会自带警告信息，这里来删除Spire.Doc 的警告
            document.removeBodyElement(0);
            //输出word内容文件流，新输出路径位置
            OutputStream os=new FileOutputStream("/"+RuoYiConfig.getProfile()+"/wordfill/word/"+instanceId+"/"+contractname+".docx");
            document.write(os);
            os.flush();
            os.close();
            document.close();
            pdfController.generatePdf(hashMap);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException("头部word书签填充出错！！");
        }
        return stringMap;
    }

    public String generateContract(String name ,String dealerName){
        String contractPath = dictDataMapper.selectDictLabel("contract", "股东会决议");
        //加载包含书签的Word文档
        Document doc = new Document();
        doc.loadFromFile(RuoYiConfig.getProfile() + contractPath);
        //定位到指定书签位置
        BookmarksNavigator bookmarksNavigator = new BookmarksNavigator(doc);
        String[] split = name.split(",");

        for (int i = 0; i < split.length; i++) {
            System.out.println("担保公司名称" + (i + 1));
            bookmarksNavigator.moveToBookmark("担保公司名称" + (i + 1));
            bookmarksNavigator.replaceBookmarkContent(split[i], true);

            bookmarksNavigator.moveToBookmark("经销商名称" + (i + 1));
            bookmarksNavigator.replaceBookmarkContent(dealerName,true);

        }
        String fileName = IdUtils.simpleUUID();
        String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String filePath = "/"+RuoYiConfig.getProfile()+"/upload"+"/"+"Shareholders/"+format+"/"+fileName+"/"+"股东会决议.docx";
        File file1 = new File(filePath);
        File fileParent = file1.getParentFile();
        if (!fileParent.exists()) {
            // 创建父目录文件夹
            fileParent.mkdirs();
        }
        //保存文档FileFormat.Docx_2013
        doc.saveToFile(filePath, FileFormat.Docx_2013);
        doc.dispose();
        //重新读取生成的文档
        try{
            InputStream is = new FileInputStream(filePath);
            XWPFDocument document = new XWPFDocument(is);
            //以上Spire.Doc 生成的文件会自带警告信息，这里来删除Spire.Doc 的警告
            document.removeBodyElement(0);
            //输出word内容文件流，新输出路径位置
            OutputStream os=new FileOutputStream(filePath);
            document.write(os);
            os.flush();
            os.close();
            document.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return  "/profile/upload"+"/"+"Shareholders/"+format+"/"+fileName+"/"+"股东会决议.docx";
    }

    public void generateWor(@RequestBody Map parameters) {
        try {
            String contractlocation =(String)parameters.get("contractlocation");
            String contractname =(String)parameters.get("contractname");
            String contract =(String)parameters.get("contract");
            String priority =(String)parameters.get("priority");
            String dealername = (String) parameters.get("dealername");
            String sector = (String) parameters.get("sector");
            String limittype = (String) parameters.get("limittype");
            String instanceid = (String) parameters.get("instanceid");

            //加载包含书签的Word文档
            Document doc = new Document();
            doc.loadFromFile(RuoYiConfig.getProfile() + contractlocation.substring(8));
            //定位到指定书签位置
            BookmarksNavigator bookmarksNavigator = new BookmarksNavigator(doc);

            DealercodeContract dealercodeContract = new DealercodeContract();
            dealercodeContract.setInstanceId(instanceid);
            dealercodeContract.setDealerNameCN(dealername);
            dealercodeContract.setSector(sector);
            dealercodeContract.setLimitType(limittype);
            List<DealercodeContract> dealercodeContracts = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
            //出的合同可能包含其他合同号，而这一套其他合同号又没出，之前出过，所以去历史合同数据里查
            DealerSectorContractgroup dealerSectorContractgroup = new DealerSectorContractgroup();
            dealerSectorContractgroup.setDealername(dealername);
            dealerSectorContractgroup.setSector(sector);
            //查该经销商下对应得担保人和担保机构
            Securities securities = new Securities();
            securities.setInstanceId(instanceid);
            securities.setDealername(dealername);
            List<Securities> securitiesList = securitiesMapper.selectSecuritiesList(securities);
            StringBuilder stringBuilder = new StringBuilder();
            StringBuilder stringBuilder1 = new StringBuilder();
            for(Securities se: securitiesList){
                if(StringUtils.isNotEmpty(se.getGuaranteeType())){
                    if(se.getGuaranteeType().equals("personal") && StringUtils.isNotEmpty(se.getProposalNameCN())){
                        stringBuilder.append(se.getProposalNameCN()).append(",");
                    }
                    if(se.getGuaranteeType().equals("corporate") && StringUtils.isNotEmpty(se.getProposalNameCN())){
                        stringBuilder1.append(se.getProposalNameCN()).append(",");
                    }
                }
            }
//            if(StringUtils.isNotEmpty(stringBuilder)) {
//                String danbaoren = (stringBuilder.substring(0, stringBuilder.length() - 1)).toString();//担保人
//                bookmarksNavigator.moveToBookmark("担保人");
//                bookmarksNavigator.replaceBookmarkContent(danbaoren, true);
//
//                bookmarksNavigator.moveToBookmark("担保信息");
//                bookmarksNavigator.replaceBookmarkContent(danbaoren, true);
//
//            }
            if(StringUtils.isNotEmpty(stringBuilder1)) {
                String danbaojigou = (stringBuilder1.substring(0, stringBuilder1.length() - 1)).toString();//担保机构
                bookmarksNavigator.moveToBookmark("担保公司");
                bookmarksNavigator.replaceBookmarkContent(danbaojigou, true);

                bookmarksNavigator.moveToBookmark("担保公司1");
                bookmarksNavigator.replaceBookmarkContent(danbaojigou, true);
            } else {
                bookmarksNavigator.moveToBookmark("担保公司");
                bookmarksNavigator.replaceBookmarkContent(" ", true);

                bookmarksNavigator.moveToBookmark("担保公司1");
                bookmarksNavigator.replaceBookmarkContent(" ", true);
            }
            if(StringUtils.isNotEmpty(stringBuilder)){
                String danbaoren = (stringBuilder.substring(0, stringBuilder.length() - 1)).toString();
                bookmarksNavigator.moveToBookmark("担保人");
                bookmarksNavigator.replaceBookmarkContent(danbaoren,true);

                bookmarksNavigator.moveToBookmark("担保人1");
                bookmarksNavigator.replaceBookmarkContent(danbaoren, true);
            } else {
                bookmarksNavigator.moveToBookmark("担保人");
                bookmarksNavigator.replaceBookmarkContent(" ",true);

                bookmarksNavigator.moveToBookmark("担保人1");
                bookmarksNavigator.replaceBookmarkContent(" ", true);
            }
            //查经销商法人
            List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByDealerName(dealername);
            if(StringUtils.isNotEmpty(stringBuilder) || StringUtils.isNotEmpty(stringBuilder1)){
                StringBuilder stringBuilder2 = new StringBuilder();
                StringBuilder stringBuilder3 = new StringBuilder();
                if(StringUtils.isNotEmpty(stringBuilder)){
                    String danbaoren = (stringBuilder.substring(0, stringBuilder.length() - 1)).toString();//担保人
                    stringBuilder2.append(danbaoren).append(",");
                    stringBuilder3.append(danbaoren).append(",");
                }
                if(StringUtils.isNotEmpty(stringBuilder1)){
                    String danbaojigou = (stringBuilder1.substring(0, stringBuilder1.length() - 1)).toString();//担保机构
                    stringBuilder2.append(danbaojigou).append(",");
                    stringBuilder3.append(danbaojigou).append(",");
                }
                String danbaorenOrDanbaojigou = (stringBuilder2.substring(0, stringBuilder2.length() - 1)).toString();//担保人+担保机构
                String danbaorenOrDanbaojigouNew = (stringBuilder3.substring(0, stringBuilder3.length() - 1)).toString();//担保人+担保机构,这个是给表格用
//                bookmarksNavigator.moveToBookmark("担保人");
//                bookmarksNavigator.replaceBookmarkContent(danbaorenOrDanbaojigou, true);

                bookmarksNavigator.moveToBookmark("担保信息");
                bookmarksNavigator.replaceBookmarkContent(danbaorenOrDanbaojigou, true);

                bookmarksNavigator.moveToBookmark("担保信息1");
                bookmarksNavigator.replaceBookmarkContent(danbaorenOrDanbaojigou, true);

                bookmarksNavigator.moveToBookmark("担保信息2");
                bookmarksNavigator.replaceBookmarkContent(danbaorenOrDanbaojigou, true);

                bookmarksNavigator.moveToBookmark("担保信息3");
                bookmarksNavigator.replaceBookmarkContent(danbaorenOrDanbaojigou, true);

                if(contractname.equals("人民币循环贷款合同")) {
                    String[] split = danbaorenOrDanbaojigouNew.split(",");
                    for (int i = 0; i < split.length; i++) {
                        bookmarksNavigator.moveToBookmark("担保人B" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(split[i], true);

                        bookmarksNavigator.moveToBookmark("担保人A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(split[i], true);

                        bookmarksNavigator.moveToBookmark("经销商名称B" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        bookmarksNavigator.moveToBookmark("经销商名称A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        if(StringUtils.isNotEmpty(dealerInformations)){
                            if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeCN())) {
                                bookmarksNavigator.moveToBookmark("经销商法人A" + (i + 1));
                                bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);
                            }
                        }

                        dealercodeContract.setContractName("人民币循环贷款合同附件五");
                        DealercodeContract dealercodeContract1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件五合同号" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(contract, true);
                    }
                }

                if(contractname.equals("人民币循环贷款合同附件五")) {
                    String[] split = danbaorenOrDanbaojigouNew.split(",");
                    for (int i = 0; i < split.length; i++) {
                        bookmarksNavigator.moveToBookmark("担保人B" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(split[i], true);

                        bookmarksNavigator.moveToBookmark("担保人A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(split[i], true);

                        bookmarksNavigator.moveToBookmark("经销商名称B" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        bookmarksNavigator.moveToBookmark("经销商名称A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        if(StringUtils.isNotEmpty(dealerInformations)){
                            if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeCN())) {
                                bookmarksNavigator.moveToBookmark("经销商法人A" + (i + 1));
                                bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);
                            }
                        }

                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件五合同号" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(contract, true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                }

                if(contractname.equals("试乘试驾车贷款合同")) {
                    String[] split = danbaorenOrDanbaojigouNew.split(",");
                    if(split != null && split.length>0){
                        for (int i = 0; i < split.length; i++) {
                            bookmarksNavigator.moveToBookmark("担保信息B" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(split[i], true);

                            bookmarksNavigator.moveToBookmark("担保信息A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(split[i], true);

                            bookmarksNavigator.moveToBookmark("经销商名称B" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealername, true);

                            bookmarksNavigator.moveToBookmark("经销商名称A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealername, true);

                            if(StringUtils.isNotEmpty(dealerInformations)){
                                if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeCN())) {
                                    bookmarksNavigator.moveToBookmark("经销商法人A" + (i + 1));
                                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);
                                }
                            }

                            dealercodeContract.setContractName("试乘试驾车贷款合同");
                            DealercodeContract dealercodeContract1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract).get(0);

                            bookmarksNavigator.moveToBookmark("试驾车循环贷款附件三合同号A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract()+"-3-1", true);

                            bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract(), true);
                        }
                    }else{
                        bookmarksNavigator.moveToBookmark("经销商名称B1");
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        bookmarksNavigator.moveToBookmark("经销商名称A1");
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        dealercodeContract.setContractName("试乘试驾车贷款合同");
                        DealercodeContract dealercodeContract1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract).get(0);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款附件三合同号A1");
                        bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract()+"-3-1", true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号A1");
                        bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract(), true);
                    }

                }

                if(contractname.equals("试乘试驾车贷款合同附件三")) {
                    String[] split = danbaorenOrDanbaojigouNew.split(",");
                    if(split != null && split.length>0){
                        for (int i = 0; i < split.length; i++) {
                            bookmarksNavigator.moveToBookmark("担保信息B" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(split[i], true);

                            bookmarksNavigator.moveToBookmark("担保信息A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(split[i], true);

                            bookmarksNavigator.moveToBookmark("经销商名称B" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealername, true);

                            bookmarksNavigator.moveToBookmark("经销商名称A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealername, true);

                            if(StringUtils.isNotEmpty(dealerInformations)){
                                if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeCN())) {
                                    bookmarksNavigator.moveToBookmark("经销商法人A" + (i + 1));
                                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);
                                }
                            }

                            dealercodeContract.setContractName("试乘试驾车贷款合同附件三");
                            DealercodeContract dealercodeContract1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract).get(0);

                            bookmarksNavigator.moveToBookmark("试驾车循环贷款附件三合同号A" + (i + 1));
                            bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract(), true);

                            bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号A" + (i + 1));
                            String[] split1 = dealercodeContract1.getContract().split("-");
                            bookmarksNavigator.replaceBookmarkContent(split1[0], true);
                        }
                    }else{
                        bookmarksNavigator.moveToBookmark("经销商名称B1");
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        bookmarksNavigator.moveToBookmark("经销商名称A1");
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        dealercodeContract.setContractName("试乘试驾车贷款合同附件三");
                        DealercodeContract dealercodeContract1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract).get(0);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款附件三合同号A1");
                        bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号A1");
                        String[] split1 = dealercodeContract1.getContract().split("-");
                        bookmarksNavigator.replaceBookmarkContent(split1[0], true);
                    }

                }
            } else {
                bookmarksNavigator.moveToBookmark("担保信息");
                bookmarksNavigator.replaceBookmarkContent(" ", true);

                bookmarksNavigator.moveToBookmark("担保人1");
                bookmarksNavigator.replaceBookmarkContent(" ", true);

                bookmarksNavigator.moveToBookmark("担保信息1");
                bookmarksNavigator.replaceBookmarkContent(" ", true);

                bookmarksNavigator.moveToBookmark("担保信息2");
                bookmarksNavigator.replaceBookmarkContent(" ", true);

                bookmarksNavigator.moveToBookmark("担保信息3");
                bookmarksNavigator.replaceBookmarkContent(" ", true);

                if(contractname.equals("试乘试驾车贷款合同附件三")) {
                    bookmarksNavigator.moveToBookmark("经销商名称B1");
                    bookmarksNavigator.replaceBookmarkContent(dealername, true);

                    bookmarksNavigator.moveToBookmark("经销商名称A1");
                    bookmarksNavigator.replaceBookmarkContent(dealername, true);

                    dealercodeContract.setContractName("试乘试驾车贷款合同附件三");
                    DealercodeContract dealercodeContract1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract).get(0);

                    bookmarksNavigator.moveToBookmark("试驾车循环贷款附件三合同号A1");
                    bookmarksNavigator.replaceBookmarkContent(dealercodeContract1.getContract(), true);

                    bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号A1");
                    String[] split1 = dealercodeContract1.getContract().split("-");
                    bookmarksNavigator.replaceBookmarkContent(split1[0], true);
                }
                if((contractname.equals("人民币循环贷款合同") || contractname.equals("人民币循环贷款合同附件五")) && sector.toUpperCase().equals("PEUGEOT")) {
                    for (int i = 0; i < 1; i++) {
                        bookmarksNavigator.moveToBookmark("经销商名称B" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        bookmarksNavigator.moveToBookmark("经销商名称A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealername, true);

                        if(StringUtils.isNotEmpty(dealerInformations)){
                            if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeCN())) {
                                bookmarksNavigator.moveToBookmark("经销商法人A" + (i + 1));
                                bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);
                            }
                        }

                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件五合同号" + (i + 1));
                        if(contractname.equals("人民币循环贷款合同")){
                            bookmarksNavigator.replaceBookmarkContent(contract+"-5-1", true);
                        }else{
                            bookmarksNavigator.replaceBookmarkContent(contract, true);
                        }


                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号A" + (i + 1));
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                }
            }


            //每经历一次合同填充，就会循环该分组下所有的合同，把能出现的合同号都赋值
                for(DealercodeContract dc: dealercodeContracts){
                    if(dc.getContractName().equals("三方合同") || dc.getContractName().equals("三方-C") || dc.getContractName().equals("三方-E")){
                        bookmarksNavigator.moveToBookmark("三方合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(),true);
                        bookmarksNavigator.moveToBookmark("三方合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(),true);
                    }
                    if(dc.getContractName().equals("个人保函")){
                        bookmarksNavigator.moveToBookmark("个人保函合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(),true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(),true);

                        bookmarksNavigator.moveToBookmark("三方合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"W").toString(), true);

                        bookmarksNavigator.moveToBookmark("主信托合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"T").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号1");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);
                    }
                    if(dc.getContractName().equals("主信托")){
                        bookmarksNavigator.moveToBookmark("主信托合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(),true);
                    }
                    if(dc.getContractName().equals("企业保函")){
                        bookmarksNavigator.moveToBookmark("企业保函合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(),true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(),true);

                        bookmarksNavigator.moveToBookmark("三方合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"W").toString(), true);

                        bookmarksNavigator.moveToBookmark("主信托合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"T").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号1");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);
                    }
                    //用文本内容替换原有书签位置的文本，新替换的内容与原文格式一致
                    if(dc.getContractName().equals("浮动抵押")) {
                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                    }

                    if(dc.getContractName().equals("试乘试驾车贷款合同")) {

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("ContractNumberD");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        List<DealerSectorContractgroup> dealerSectorContractgroups = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                        if(dealerSectorContractgroups!=null && dealerSectorContractgroups.size()>0){
                            DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroups.get(0);
                            bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                        }


                    }
                    if(dc.getContractName().equals("试驾车个人保函")) {
                        bookmarksNavigator.moveToBookmark("试驾车个人保函合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                        bookmarksNavigator.moveToBookmark("个人保函合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                    }
                    if(dc.getContractName().equals("试驾车企业保函")) {
                        bookmarksNavigator.moveToBookmark("试驾车企业保函合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                        bookmarksNavigator.moveToBookmark("企业保函合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                    }

                    if(dc.getContractName().equals("人民币循环贷款合同")) {
                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号4");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                    }
                    if(dc.getContractName().equals("人民币循环贷款合同附件二")) {
                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件二合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号4");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("人民币循环贷款合同附件三")) {
                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件三合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号4");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("人民币循环贷款合同附件四")) {
                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件四合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号4");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("人民币循环贷款合同附件五")) {
                        bookmarksNavigator.moveToBookmark("人民币循环贷款协议附件五合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号4");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("试乘试驾车贷款合同附件一")) {
                        bookmarksNavigator.moveToBookmark("试驾车循环贷款附件一合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("试乘试驾车贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("ContractNumberD");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("试乘试驾车贷款合同附件二")) {
                        bookmarksNavigator.moveToBookmark("试驾车循环贷款附件二合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("试乘试驾车贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("ContractNumberD");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("试乘试驾车贷款合同附件三")) {
                        bookmarksNavigator.moveToBookmark("试驾车循环贷款附件三合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("试乘试驾车贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("试驾车循环贷款合同号3");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                    }
                    if(dc.getContractName().equals("试乘试驾车抵押合同")) {
                        bookmarksNavigator.moveToBookmark("DEMO抵押合同合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("DEMO抵押合同合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);
                    }
                    if(dc.getContractName().equals("CB 改装车大客户延期补充协议")) {
                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);


                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("三方合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"W").toString(), true);

                        bookmarksNavigator.moveToBookmark("主信托合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"T").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号1");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);


                        dealerSectorContractgroup.setContractname("企业保函");
                        List<DealerSectorContractgroup> dealerSectorContractgroup2 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                        if(StringUtils.isNotEmpty(dealerSectorContractgroup2)) {
                            bookmarksNavigator.moveToBookmark("企业保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup2.get(0).getContractnumber(), true);
                        } else {
                            bookmarksNavigator.moveToBookmark("企业保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(" ", true);
                        }
                        dealerSectorContractgroup.setContractname("个人保函");
                        List<DealerSectorContractgroup> dealerSectorContractgroup3 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                        if(StringUtils.isNotEmpty(dealerSectorContractgroup3)){
                            bookmarksNavigator.moveToBookmark("个人保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup3.get(0).getContractnumber(),true);
                        } else {
                            bookmarksNavigator.moveToBookmark("个人保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(" ",true);
                        }
                    }
                    if(dc.getContractName().equals("CB 改装车延期还款产品条款附件（循环）")) {
                        bookmarksNavigator.moveToBookmark("CB改装车延期还款产品条款附件合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("CB 改装车协议（循环）");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    //TODO 还有其他合同号没加,暂不确定，先出主合同，这一套没附件，那这附件号怎么去处理
                    if(dc.getContractName().equals("CB 大客户协议（一次性额度）")) {
                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup3 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup3.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup3.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("三方合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup3.getContractnumber()).replace(6,7,"W").toString(), true);

                        bookmarksNavigator.moveToBookmark("主信托合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup3.getContractnumber()).replace(6,7,"T").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup3.getContractnumber()).replace(6,7,"M").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号1");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup3.getContractnumber()).replace(6,7,"M").toString(), true);


                        dealerSectorContractgroup.setContractname("企业保函");
                        List<DealerSectorContractgroup> dealerSectorContractgroup2 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                        if(StringUtils.isNotEmpty(dealerSectorContractgroup2)) {
                            bookmarksNavigator.moveToBookmark("企业保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup2.get(0).getContractnumber(), true);
                        } else {
                            bookmarksNavigator.moveToBookmark("企业保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(" ", true);
                        }

                        dealerSectorContractgroup.setContractname("个人保函");
                        List<DealerSectorContractgroup> dealerSectorContractgroup4 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                        if(StringUtils.isNotEmpty(dealerSectorContractgroup4)){
                            bookmarksNavigator.moveToBookmark("个人保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup4.get(0).getContractnumber(),true);
                        } else {
                            bookmarksNavigator.moveToBookmark("个人保函合同号");
                            bookmarksNavigator.replaceBookmarkContent(" ",true);
                        }

//                        dealerSectorContractgroup.setContractname("CB 大客户延期还款产品担保附件一");
//                        List<DealerSectorContractgroup> dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
//
//                        if(StringUtils.isNotEmpty(dealerSectorContractgroup1)){
//                            bookmarksNavigator.moveToBookmark("CB大客户延期还款产品担保附件一合同号");
//                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.get(0).getContractnumber(), true);
//                        }
//
//                        dealerSectorContractgroup.setContractname("CB 大客户延期还款产品担保附件二");
//                        List<DealerSectorContractgroup> dealerSectorContractgroup2 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
//
//                        if(StringUtils.isNotEmpty(dealerSectorContractgroup2)){
//                            bookmarksNavigator.moveToBookmark("CB大客户延期还款产品担保附件二合同号");
//                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup2.get(0).getContractnumber(), true);
//                        }
                    }
                    if(dc.getContractName().equals("CB 大客户延期还款产品担保附件一")) {
                        bookmarksNavigator.moveToBookmark("CB大客户延期还款产品担保附件一合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("CB 大客户协议（一次性额度）");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("CB 大客户延期还款产品担保附件二")) {
                        bookmarksNavigator.moveToBookmark("CB大客户延期还款产品担保附件二合同号");
                        bookmarksNavigator.replaceBookmarkContent(dc.getContract(), true);

                        dealerSectorContractgroup.setContractname("CB 大客户协议（一次性额度）");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge主合同号2");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);
                    }
                    if(dc.getContractName().equals("面签承诺函")){
                        if(limittype.equals("NORMAL") || limittype.equals("Normal") || limittype.toUpperCase().equals("PART") ){
                            dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                            DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                            bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号或试乘试驾车贷款合同合同号系统自行判断");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                            if(contractname.equals("面签承诺函")) {
                                ContractRecord record = new ContractRecord();
                                record.setInstanceid(instanceid);
                                record.setDealername(dealername);
                                record.setSector(sector);
                                record.setLimitType(limittype);
                                record.setContracttype(contractname);
                                List<ContractRecord> contractRecords = contractRecordMapper.selectContractRecordList(record);
                                if (CollectionUtil.isNotEmpty(contractRecords)) {
                                    for (ContractRecord cr : contractRecords) {
                                        cr.setContractno(dealerSectorContractgroup1.getContractnumber());
                                        contractRecordMapper.updateContractRecord(cr);
                                    }
                                }
                                List<ContractRecord> contractRecordThrees = contractRecordMapper.selectContractRecordThreeList(record);
                                if (CollectionUtil.isNotEmpty(contractRecordThrees)) {
                                    for (ContractRecord cr : contractRecordThrees) {
                                        cr.setContractno(dealerSectorContractgroup1.getContractnumber());
                                        contractRecordMapper.updateContractRecordThree(cr);
                                    }
                                }
                                record.setLimitType("part");
                                List<ContractRecord> contractRecordThrees2 = contractRecordMapper.selectContractRecordThreeList(record);
                                if (CollectionUtil.isNotEmpty(contractRecordThrees2)) {
                                    for (ContractRecord cr : contractRecordThrees2) {
                                        cr.setContractno(dealerSectorContractgroup1.getContractnumber());
                                        contractRecordMapper.updateContractRecordThree(cr);
                                    }
                                }
                            }

                            dealercodeContract.setContractName("个人保函");
                            List<DealercodeContract> dealercodeContracts1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
                            if(StringUtils.isNotEmpty(dealercodeContracts1)){
                                bookmarksNavigator.moveToBookmark("个人保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(dealercodeContracts1.get(0).getContract(),true);
                            } else {
                                bookmarksNavigator.moveToBookmark("个人保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(" ",true);
                            }
                            dealercodeContract.setContractName("企业保函");
                            List<DealercodeContract> dealercodeContracts2 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
                            if(StringUtils.isNotEmpty(dealercodeContracts2)){
                                bookmarksNavigator.moveToBookmark("企业保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(dealercodeContracts2.get(0).getContract(),true);
                            } else {
                                bookmarksNavigator.moveToBookmark("企业保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(" ",true);
                            }
                        }
                        if(limittype.equals("DEMO") || limittype.equals("Demo")){
                            dealerSectorContractgroup.setContractname("试乘试驾车贷款合同");
                            DealerSectorContractgroup dealerSectorContractgroup2 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                            bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号或试乘试驾车贷款合同合同号系统自行判断");
                            bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup2.getContractnumber(), true);

                            if(contractname.equals("面签承诺函")) {
                                ContractRecord record = new ContractRecord();
                                record.setInstanceid(instanceid);
                                record.setDealername(dealername);
                                record.setSector(sector);
                                record.setLimitType(limittype);
                                record.setContracttype(contractname);
                                List<ContractRecord> contractRecords = contractRecordMapper.selectContractRecordList(record);
                                if (CollectionUtil.isNotEmpty(contractRecords)) {
                                    for (ContractRecord cr : contractRecords) {
                                        cr.setContractno(dealerSectorContractgroup2.getContractnumber());
                                        contractRecordMapper.updateContractRecord(cr);
                                    }
                                }
                                List<ContractRecord> contractRecordThrees = contractRecordMapper.selectContractRecordThreeList(record);
                                if (CollectionUtil.isNotEmpty(contractRecordThrees)) {
                                    for (ContractRecord cr : contractRecordThrees) {
                                        cr.setContractno(dealerSectorContractgroup2.getContractnumber());
                                        contractRecordMapper.updateContractRecordThree(cr);
                                    }
                                }
                                record.setLimitType("part");
                                List<ContractRecord> contractRecordThrees2 = contractRecordMapper.selectContractRecordThreeList(record);
                                if (CollectionUtil.isNotEmpty(contractRecordThrees2)) {
                                    for (ContractRecord cr : contractRecordThrees2) {
                                        cr.setContractno(dealerSectorContractgroup2.getContractnumber());
                                        contractRecordMapper.updateContractRecordThree(cr);
                                    }
                                }
                            }

                            dealercodeContract.setContractName("试驾车个人保函");
                            List<DealercodeContract> dealercodeContracts1 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
                            if(StringUtils.isNotEmpty(dealercodeContracts1)){
                                bookmarksNavigator.moveToBookmark("个人保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(dealercodeContracts1.get(0).getContract(),true);
                            } else {
                                bookmarksNavigator.moveToBookmark("个人保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(" ",true);
                            }
                            dealercodeContract.setContractName("试驾车企业保函");
                            List<DealercodeContract> dealercodeContracts2 = dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
                            if(StringUtils.isNotEmpty(dealercodeContracts2)){
                                bookmarksNavigator.moveToBookmark("企业保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(dealercodeContracts2.get(0).getContract(),true);
                            } else {
                                bookmarksNavigator.moveToBookmark("企业保函合同号");
                                bookmarksNavigator.replaceBookmarkContent(" ",true);
                            }
                        }



                    }
                    if(dc.getContractName().equals("人民币循环贷款协议补充协议-二网服务费")){
                        dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                        DealerSectorContractgroup dealerSectorContractgroup1 = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup).get(0);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("人民币循环贷款主合同号1");
                        bookmarksNavigator.replaceBookmarkContent(dealerSectorContractgroup1.getContractnumber(), true);

                        bookmarksNavigator.moveToBookmark("三方合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"W").toString(), true);

                        bookmarksNavigator.moveToBookmark("主信托合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"T").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);

                        bookmarksNavigator.moveToBookmark("浮动抵押合同号");
                        bookmarksNavigator.replaceBookmarkContent(new StringBuilder(dealerSectorContractgroup1.getContractnumber()).replace(6,7,"M").toString(), true);
                    }
                }

            //查签署合同日期
            AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId(instanceid);
            String signedDate = DateUtils.parseDateToStr("yyyy年MM月dd日", annualReviewy.getUpdateTime());
            String signedDate1 = DateUtils.parseDateToStr("yyyy/MM/dd", annualReviewy.getUpdateTime());
            bookmarksNavigator.moveToBookmark("签署合同日期");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("签署合同日期1");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("合同签署日期");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("合同签署日期1");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("签署合同日期2");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("签署合同日期3");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("临额起始日");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("临额起始日TEMP");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("SystemDate");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);

            bookmarksNavigator.moveToBookmark("SystemDate1");
            bookmarksNavigator.replaceBookmarkContent(signedDate1, true);


            //查到期日
            CreditCondition creditCondition = new CreditCondition();
            creditCondition.setDealername(dealername);
            creditCondition.setSector(sector);
            creditCondition.setLimittype(limittype);
            creditCondition.setInstanceid(instanceid);
            List<CreditCondition> creditConditions = creditConditionMapper.selectCreditConditionList(creditCondition);
            if(StringUtils.isNotEmpty(creditConditions)){
                if(StringUtils.isNotNull(creditConditions.get(0).getExpireddate())) {
                    String dateToStr = DateUtils.parseDateToStr("yyyy年MM月dd日", creditConditions.get(0).getExpireddate());
                    bookmarksNavigator.moveToBookmark("到期日");
                    bookmarksNavigator.replaceBookmarkContent(dateToStr, true);

                    bookmarksNavigator.moveToBookmark("CB循环额度到期日");
                    bookmarksNavigator.replaceBookmarkContent(dateToStr, true);

                    bookmarksNavigator.moveToBookmark("临额到期日");
                    bookmarksNavigator.replaceBookmarkContent(dateToStr, true);

                    bookmarksNavigator.moveToBookmark("临额到期日TEMP");
                    bookmarksNavigator.replaceBookmarkContent(dateToStr, true);

                    bookmarksNavigator.moveToBookmark("临额到期日1");
                    bookmarksNavigator.replaceBookmarkContent(dateToStr, true);
                }
            }
            if(sector.toUpperCase().equals("PEUGEOT") && (limittype.toUpperCase().equals("NORMAL") || limittype.toUpperCase().equals("PART"))){
                //查询
                ProposalByCommericalAndMarketing proposalByCommericalAndMarketingParts = new ProposalByCommericalAndMarketing();
                proposalByCommericalAndMarketingParts.setDealername(dealername);
                proposalByCommericalAndMarketingParts.setSector(sector);
                proposalByCommericalAndMarketingParts.setLimitType("Part");
                proposalByCommericalAndMarketingParts.setInstanceId(instanceid);
                List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingsParts = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketingParts);
                if(StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts)){
                    if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().equals("") && proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()>0) {
                        //查到期日
                        CreditCondition creditConditionParts = new CreditCondition();
                        creditConditionParts.setDealername(dealername);
                        creditConditionParts.setSector(sector);
                        creditConditionParts.setLimittype(limittype);
                        creditConditionParts.setInstanceid(instanceid);
                        List<CreditCondition> creditConditionsParts = creditConditionMapper.selectCreditConditionList(creditConditionParts);
                        if(StringUtils.isNotEmpty(creditConditionsParts)){
                            if(StringUtils.isNotNull(creditConditionsParts.get(0).getExpireddate())) {
                                String dateToStr = DateUtils.parseDateToStr("yyyy年MM月dd日", creditConditionsParts.get(0).getExpireddate());
                                bookmarksNavigator.moveToBookmark("parts到期日");
                                bookmarksNavigator.replaceBookmarkContent(dateToStr, true);

                            }
                        }
                    }
                }
            }

            //查经销商法人
//            List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByDealerName(dealername);
            if(StringUtils.isNotEmpty(dealerInformations)){
                if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeCN())) {
                    bookmarksNavigator.moveToBookmark("经销商法人");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);

                    bookmarksNavigator.moveToBookmark("经销商法人1");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);

                    bookmarksNavigator.moveToBookmark("经销商法人2");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);

                    bookmarksNavigator.moveToBookmark("经销商法人3");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);

                    bookmarksNavigator.moveToBookmark("经销商法人4");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeCN(), true);
                } else {
                    bookmarksNavigator.moveToBookmark("经销商法人");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                    bookmarksNavigator.moveToBookmark("经销商法人1");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                    bookmarksNavigator.moveToBookmark("经销商法人2");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                    bookmarksNavigator.moveToBookmark("经销商法人3");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                    bookmarksNavigator.moveToBookmark("经销商法人4");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getLegalRepresentativeEN())) {
                    bookmarksNavigator.moveToBookmark("DealerLegalrepresentative");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getLegalRepresentativeEN(), true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getRegisteredUrbanCn())){
                    bookmarksNavigator.moveToBookmark("注册地省市");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredUrbanCn(), true);
                }else{
                    bookmarksNavigator.moveToBookmark("注册地省市");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getRegisteredAddressCN())){
                    bookmarksNavigator.moveToBookmark("经销商注册地址");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredAddressCN(), true);

                    bookmarksNavigator.moveToBookmark("经销商注册地址1");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredAddressCN(), true);

                    bookmarksNavigator.moveToBookmark("经销商注册地址（中文）");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredAddressCN(), true);

                } else {
                    bookmarksNavigator.moveToBookmark("经销商注册地址");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                    bookmarksNavigator.moveToBookmark("经销商注册地址1");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                    bookmarksNavigator.moveToBookmark("经销商注册地址（中文）");
                    bookmarksNavigator.replaceBookmarkContent(" ", true);

                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getEnName())) {
                    bookmarksNavigator.moveToBookmark("DealerName");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getEnName(), true);

                    bookmarksNavigator.moveToBookmark("DealerName1");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getEnName(), true);

                    bookmarksNavigator.moveToBookmark("DealerName2");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getEnName(), true);

                    bookmarksNavigator.moveToBookmark("经销商名称（英文）");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getEnName(), true);

                    bookmarksNavigator.moveToBookmark("经销商名称（英文）1");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getEnName(), true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getRegisteredAddressEN())) {
                    bookmarksNavigator.moveToBookmark("DealerRegisteredaddress");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredAddressEN(), true);

                    bookmarksNavigator.moveToBookmark("DealerRegisteredaddress1");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredAddressEN(), true);

                    bookmarksNavigator.moveToBookmark("经销商注册地址（英文）");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredAddressEN(), true);

                }
                if(StringUtils.isNotEmpty(dealerInformations.get(0).getRegisteredUrbanEn())) {
                    bookmarksNavigator.moveToBookmark("注册地省市（英文）");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredUrbanEn(), true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getRegisteredCapitalCn())){
                    bookmarksNavigator.moveToBookmark("注册资本（中文）");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredCapitalCn(), true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getRegisteredCapitalEn())){
                    bookmarksNavigator.moveToBookmark("注册资本（英文）");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getRegisteredCapitalEn(), true);
                }

                if(StringUtils.isNotEmpty(dealerInformations.get(0).getUniformCreditCode())){
                    bookmarksNavigator.moveToBookmark("统一社会信用代码");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getUniformCreditCode(), true);

                    bookmarksNavigator.moveToBookmark("统一社会信用代码1");
                    bookmarksNavigator.replaceBookmarkContent(dealerInformations.get(0).getUniformCreditCode(), true);
                }
            }


            //查额度金额
            ProposalByCommericalAndMarketing proposalByCommericalAndMarketing = new ProposalByCommericalAndMarketing();
            proposalByCommericalAndMarketing.setDealername(dealername);
            proposalByCommericalAndMarketing.setSector(sector);
            proposalByCommericalAndMarketing.setLimitType(limittype);
            proposalByCommericalAndMarketing.setInstanceId(instanceid);
            List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing);
            if(StringUtils.isNotEmpty(proposalByCommericalAndMarketings)){
                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalLimit()) && !proposalByCommericalAndMarketings.get(0).getProposalLimit().equals("")) {
                    bookmarksNavigator.moveToBookmark("额度金额");
                    bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString(),true);

                    bookmarksNavigator.moveToBookmark("额度金额中文大写");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString()),true);

                    bookmarksNavigator.moveToBookmark("额度金额大写");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString()),true);

                    bookmarksNavigator.moveToBookmark("临时额度金额中文大写");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString()),true);

                    bookmarksNavigator.moveToBookmark("CB额度金额");
                    bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString(),true);

                    bookmarksNavigator.moveToBookmark("CB额度金额中文大写");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString()),true);


                    if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals("")) {
                        if (proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals("0%")) {

                            bookmarksNavigator.moveToBookmark("保证金比例TEMP");
                            bookmarksNavigator.replaceBookmarkContent(" / ", true);

                            bookmarksNavigator.moveToBookmark("保证金金额TEMP");
                            bookmarksNavigator.replaceBookmarkContent(" / ", true);

                            bookmarksNavigator.moveToBookmark("保证金金额大写");
                            bookmarksNavigator.replaceBookmarkContent(" / ", true);

                        } else {
                            bookmarksNavigator.moveToBookmark("保证金比例TEMP");
                            System.out.println(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit());
                            bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit(), true);

                            double deposit = Double.parseDouble(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().replace("%", ""));
                            double depositAmt = Arith.mul(proposalByCommericalAndMarketings.get(0).getProposalLimit(), deposit * 0.01);

                            bookmarksNavigator.moveToBookmark("保证金金额TEMP");
                            String str = Convert.toStr(Convert.getDoubleString(depositAmt));
                            System.out.println(str);
                            bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(depositAmt)), true);

                            bookmarksNavigator.moveToBookmark("保证金金额大写");
                            bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(Convert.getDoubleString(depositAmt))), true);

                        }
                    }

                }

                //保证金金额是额度金额X额度比例
                //保证金金额or现质押保证金金额
                double mul = 0;
                //原质押保证金金额
                double mulUsed = 0;
                //CB现保证金金额
                double mulCB = 0;
                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalLimit()) && StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals("")){
                    mul = Arith.mul(proposalByCommericalAndMarketings.get(0).getProposalLimit(), Double.parseDouble(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().replace("%", "")) * 0.01);
                }
                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getApprovedLimit()) && StringUtils.isNotEmpty(proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit().equals("")){
                    mulUsed = Arith.mul(proposalByCommericalAndMarketings.get(0).getApprovedLimit(), Double.parseDouble(proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit().replace("%", "")) * 0.01);
                }

                double formerdepo = 0;
                double newdepo = 0;
                ProposalByCommericalAndMarketing pbam = new ProposalByCommericalAndMarketing();
                pbam.setDealername(dealername);
                pbam.setSector(sector);
                pbam.setLimitType("CB");
                pbam.setInstanceId(instanceid);
                List<ProposalByCommericalAndMarketing> pbam1 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(pbam);
                pbam.setLimitType("NORMAL");
                List<ProposalByCommericalAndMarketing> pbam2 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(pbam);
                if(StringUtils.isNotEmpty(pbam1) && StringUtils.isNotEmpty(pbam1.get(0).getProposalCashDeposit())){
                    if(pbam1.get(0).getProposalCashDeposit().equals(0)){
                        formerdepo = 0;
                    }else {
                        formerdepo = Double.parseDouble(pbam1.get(0).getProposalCashDeposit().replace("%", ""));
                    }
                }
                if(StringUtils.isNotEmpty(pbam2) && StringUtils.isNotEmpty(pbam2.get(0).getProposalCashDeposit())){
                    if(pbam2.get(0).getProposalCashDeposit().equals(0)){
                        newdepo = 0;
                    }else {
                        newdepo = Double.parseDouble(pbam2.get(0).getProposalCashDeposit().replace("%", ""));
                    }
                }
                //这个是CB保证金比例，CD+NORMAL的合
                double add1 = Arith.add(formerdepo, newdepo);
                //这个是CB保证金金额。由现金额额度XCD+NORMAL的比例和
                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalLimit()) && StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals("")){
                    mulCB = Arith.mul(proposalByCommericalAndMarketings.get(0).getProposalLimit(),add1*0.01);
                }

                bookmarksNavigator.moveToBookmark("保证金金额");
                bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(mul)),true);

                bookmarksNavigator.moveToBookmark("保证金金额1");
                bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(mul)),true);

                bookmarksNavigator.moveToBookmark("保证金金额中文大写");
                bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(Convert.getDoubleString(mul))),true);

                bookmarksNavigator.moveToBookmark("CB保证金金额");
                bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(mul)),true);

                bookmarksNavigator.moveToBookmark("CB保证金金额中文大写");
                bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(Convert.getDoubleString(mul))),true);

                bookmarksNavigator.moveToBookmark("CB保证金金额大写");
                bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(Convert.getDoubleString(mul))),true);

                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getProposalCashDeposit().equals("")) {
                    bookmarksNavigator.moveToBookmark("保证金比例");
                    bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit(), true);

                    bookmarksNavigator.moveToBookmark("CB保证金比例");
                    bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalCashDeposit(), true);
                } else {
                    bookmarksNavigator.moveToBookmark("保证金比例");
                    bookmarksNavigator.replaceBookmarkContent("0", true);

                    bookmarksNavigator.moveToBookmark("CB保证金比例");
                    bookmarksNavigator.replaceBookmarkContent("0", true);
                }

                if(StringUtils.isNotNull(proposalByCommericalAndMarketings.get(0).getApprovedLimit()) && !proposalByCommericalAndMarketings.get(0).getApprovedLimit().equals("")) {
                    bookmarksNavigator.moveToBookmark("原质押保证金金额");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(mulUsed)), true);
                } else {
                    bookmarksNavigator.moveToBookmark("原质押保证金金额");
                    bookmarksNavigator.replaceBookmarkContent("0", true);
                }
                if(StringUtils.isNotEmpty(proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit().equals("")) {
                    bookmarksNavigator.moveToBookmark("原质押保证金比例");
                    bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit().toString(), true);
                } else {
                    bookmarksNavigator.moveToBookmark("原质押保证金比例");
                    bookmarksNavigator.replaceBookmarkContent("0", true);
                }

                //判断当他是Naveco 品牌的时候
                if(sector.equals("Naveco Daily")) {
                    if (limittype.equals("Normal")) {
                    //对三方合同附件二里的书签进行取值操作
                    //原贷款额度要取CB + NORMAL的额度和
                    double cb = 0;
                    double normal = 0;
                    double cb2 = 0;
                    double normal2 = 0;
                    //保证金金额，为CB+Normal的额度*Normal的比例
                    double normal3 = 0;
                    //原质押保证金金额
                    double normal4 = 0;
                    ProposalByCommericalAndMarketing proposalByCommericalAndMarketing1 = new ProposalByCommericalAndMarketing();
                    proposalByCommericalAndMarketing1.setDealername(dealername);
                    proposalByCommericalAndMarketing1.setSector(sector);
                    proposalByCommericalAndMarketing1.setLimitType("CB");
                    proposalByCommericalAndMarketing1.setInstanceId(instanceid);
                    List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings1 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing1);
                    proposalByCommericalAndMarketing1.setLimitType("NORMAL");
                    List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings2 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing1);
                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings1) && StringUtils.isNotNull(proposalByCommericalAndMarketings1.get(0).getApprovedLimit()) && !proposalByCommericalAndMarketings.get(0).getApprovedLimit().equals("")) {
                        cb = proposalByCommericalAndMarketings1.get(0).getApprovedLimit();
                    }
                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings2) && StringUtils.isNotNull(proposalByCommericalAndMarketings2.get(0).getApprovedLimit()) && !proposalByCommericalAndMarketings.get(0).getApprovedLimit().equals("")) {
                        normal = proposalByCommericalAndMarketings2.get(0).getApprovedLimit();
                    }
                    String add = Convert.getDoubleString(Arith.add(cb, normal));
                    bookmarksNavigator.moveToBookmark("原贷款额度（元）");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toStr(add), true);

                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings1) && StringUtils.isNotNull(proposalByCommericalAndMarketings1.get(0).getProposalLimit()) && !proposalByCommericalAndMarketings.get(0).getProposalLimit().equals("")) {
                        cb2 = proposalByCommericalAndMarketings1.get(0).getProposalLimit();
                    }
                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings2) && StringUtils.isNotNull(proposalByCommericalAndMarketings2.get(0).getProposalLimit()) && !proposalByCommericalAndMarketings.get(0).getProposalLimit().equals("")) {
                        normal2 = proposalByCommericalAndMarketings2.get(0).getProposalLimit();
                    }
                    String add2 = Convert.getDoubleString(Arith.add(cb2, normal2));
                    bookmarksNavigator.moveToBookmark("额度金额");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toStr(add2), true);

                    bookmarksNavigator.moveToBookmark("额度金额中文大写");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(add2)), true);

                    if(StringUtils.isNotEmpty(proposalByCommericalAndMarketings2)) {
                        if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings2.get(0).getApprovedCashDeposit()) && !proposalByCommericalAndMarketings2.get(0).getApprovedCashDeposit().equals("")) {
                            normal4 = Arith.mul(Arith.add(cb, normal), Double.parseDouble(proposalByCommericalAndMarketings2.get(0).getApprovedCashDeposit().replace("%", "")) * 0.01);
                        }
                    }
                    if(StringUtils.isNotEmpty(proposalByCommericalAndMarketings2)) {
                        if (StringUtils.isNotNull(proposalByCommericalAndMarketings2.get(0).getProposalLimit()) && StringUtils.isNotNull(proposalByCommericalAndMarketings2.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketings2.get(0).getProposalCashDeposit().equals("")) {
                            normal3 = Arith.mul(Arith.add(cb2, normal2), Double.parseDouble(proposalByCommericalAndMarketings2.get(0).getProposalCashDeposit().replace("%", "")) * 0.01);
                        }
                    }


                    bookmarksNavigator.moveToBookmark("原质押保证金金额");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(normal4)), true);

                    bookmarksNavigator.moveToBookmark("保证金金额");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(normal3)),true);

                    bookmarksNavigator.moveToBookmark("保证金金额1");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(normal3)),true);

                    bookmarksNavigator.moveToBookmark("保证金金额中文大写");
                    bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(Convert.getDoubleString(normal3))),true);


                    if (add.equals(add2)) {
                        bookmarksNavigator.moveToBookmark("贷款额度调整有无");
                        bookmarksNavigator.replaceBookmarkContent("无", true);
                    } else {
                        bookmarksNavigator.moveToBookmark("贷款额度调整有无");
                        bookmarksNavigator.replaceBookmarkContent("有", true);
                    }
                    proposalByCommericalAndMarketing1.setLimitType(limittype);
                    List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings3 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing1);
                    //看保证金比例跟保证金有无变动
                    String formerDeposit = "0";
                    String newDeposit = "0";
                    if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings3)) {
                        if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings3.get(0).getApprovedCashDeposit())) {
                            formerDeposit = proposalByCommericalAndMarketings3.get(0).getApprovedCashDeposit();
                        }
                        if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings3.get(0).getProposalCashDeposit())) {
                            newDeposit = proposalByCommericalAndMarketings3.get(0).getProposalCashDeposit();
                        }
                    }
                    if (mul == mulUsed && formerDeposit.equals(newDeposit)) {
                        bookmarksNavigator.moveToBookmark("保证金比例及金额调整有无");
                        bookmarksNavigator.replaceBookmarkContent("无", true);
                    } else {
                        bookmarksNavigator.moveToBookmark("保证金比例及金额调整有无");
                        bookmarksNavigator.replaceBookmarkContent("有", true);
                    }
                    //判断当他为templimt流程的时候才给下面的数据填值
                    if (annualReviewy.getType().equals("tempLimit")) {

                        bookmarksNavigator.moveToBookmark("短期临时贷款额度有无");
                        bookmarksNavigator.replaceBookmarkContent("无", true);

                        bookmarksNavigator.moveToBookmark("临时额度金额");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("临额起始日");
                        bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);

                        bookmarksNavigator.moveToBookmark("临额到期日1");
                        bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);
                    } else {
                        bookmarksNavigator.moveToBookmark("短期临时贷款额度有无");
                        bookmarksNavigator.replaceBookmarkContent("无", true);

                        bookmarksNavigator.moveToBookmark("临时额度金额");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("临额起始日");
                        bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);

                        bookmarksNavigator.moveToBookmark("临额到期日1");
                        bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);
                    }
                  }
                    if(limittype.equals("Temp")){
                        //对三方合同附件二里的书签进行取值操作
                        //原贷款额度要取CB + NORMAL的额度和
                        double temp = 0;
                        double temp2 = 0;
                        //保证金金额，为CB+Normal的额度*Normal的比例
                        double temp3 = 0;
                        //原质押保证金金额
                        double temp4 = 0;

                        bookmarksNavigator.moveToBookmark("原贷款额度（元）");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("到期日");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("额度金额");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("额度金额中文大写");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("原质押保证金金额");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("原质押保证金比例");
                        bookmarksNavigator.replaceBookmarkContent(" ", true);

                        bookmarksNavigator.moveToBookmark("保证金金额");
                        bookmarksNavigator.replaceBookmarkContent(" ",true);

                        bookmarksNavigator.moveToBookmark("保证金金额1");
                        bookmarksNavigator.replaceBookmarkContent(" ",true);

                        bookmarksNavigator.moveToBookmark("保证金比例");
                        bookmarksNavigator.replaceBookmarkContent(" ",true);

                        bookmarksNavigator.moveToBookmark("保证金金额中文大写");
                        bookmarksNavigator.replaceBookmarkContent(" ",true);



                        bookmarksNavigator.moveToBookmark("贷款额度调整有无");
                        bookmarksNavigator.replaceBookmarkContent("无", true);


                        bookmarksNavigator.moveToBookmark("保证金比例及金额调整有无");
                        bookmarksNavigator.replaceBookmarkContent("无", true);

                        //判断当他为templimt流程的时候才给下面的数据填值
                        if (annualReviewy.getType().equals("tempLimit")) {
                            if (StringUtils.isNotEmpty(proposalByCommericalAndMarketings)) {
                                bookmarksNavigator.moveToBookmark("临时额度金额");
                                bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketings.get(0).getProposalLimit().toString(), true);

                                bookmarksNavigator.moveToBookmark("短期临时贷款额度有无");
                                bookmarksNavigator.replaceBookmarkContent("有", true);
                            } else {
                                bookmarksNavigator.moveToBookmark("短期临时贷款额度有无");
                                bookmarksNavigator.replaceBookmarkContent("无", true);

                                bookmarksNavigator.moveToBookmark("临时额度金额");
                                bookmarksNavigator.replaceBookmarkContent(" ", true);

                                bookmarksNavigator.moveToBookmark("临额起始日");
                                bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);

                                bookmarksNavigator.moveToBookmark("临额到期日1");
                                bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);
                            }
                        } else {
                            bookmarksNavigator.moveToBookmark("短期临时贷款额度有无");
                            bookmarksNavigator.replaceBookmarkContent("无", true);

                            bookmarksNavigator.moveToBookmark("临时额度金额");
                            bookmarksNavigator.replaceBookmarkContent(" ", true);

                            bookmarksNavigator.moveToBookmark("临额起始日");
                            bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);

                            bookmarksNavigator.moveToBookmark("临额到期日1");
                            bookmarksNavigator.replaceBookmarkContent("___年___月___日", true);
                        }
                    }
                }
            }


            //查询Parts额度金额
            if(limittype.equals("NORMAL") || limittype.equals("Normal") ||limittype.toUpperCase().equals("PART") ){
                ProposalByCommericalAndMarketing proposalByCommericalAndMarketingParts = new ProposalByCommericalAndMarketing();
                proposalByCommericalAndMarketingParts.setDealername(dealername);
                proposalByCommericalAndMarketingParts.setSector(sector);
                proposalByCommericalAndMarketingParts.setLimitType("PART");
                proposalByCommericalAndMarketingParts.setInstanceId(instanceid);
                List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingsParts = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketingParts);
                if(StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts)){
                    if(StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts.get(0).getApprovedCashDeposit()) && !proposalByCommericalAndMarketingsParts.get(0).getApprovedCashDeposit().equals("")) {
                        bookmarksNavigator.moveToBookmark("parts原质押保证金比例");
                        bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketingsParts.get(0).getApprovedCashDeposit().toString(), true);
                    } else {
                        bookmarksNavigator.moveToBookmark("parts原质押保证金比例");
                        bookmarksNavigator.replaceBookmarkContent("0", true);
                    }

                    if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getApprovedLimit()) && StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts.get(0).getApprovedCashDeposit()) && !proposalByCommericalAndMarketings.get(0).getApprovedCashDeposit().equals("")){
                        double  mulUsedParts = Arith.mul(proposalByCommericalAndMarketingsParts.get(0).getApprovedLimit(), Double.parseDouble(proposalByCommericalAndMarketingsParts.get(0).getApprovedCashDeposit().replace("%", "")) * 0.01);
                        bookmarksNavigator.moveToBookmark("parts原质押保证金金额");
                        bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(mulUsedParts)), true);
                    }else{
                        bookmarksNavigator.moveToBookmark("parts原质押保证金金额");
                        bookmarksNavigator.replaceBookmarkContent("0", true);
                    }

                    if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().equals("") && proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()>0) {
                        bookmarksNavigator.moveToBookmark("parts额度金额");
                        bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().toString(),true);

                        bookmarksNavigator.moveToBookmark("parts额度金额大写");
                        bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().toString()),true);

                        if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit().equals("")) {
                            bookmarksNavigator.moveToBookmark("parts保证金比例");
                            bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit(), true);

                            double deposit = Double.parseDouble(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit().replace("%", ""));
                            double depositAmt = Arith.mul(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit(), deposit * 0.01);

                            bookmarksNavigator.moveToBookmark("parts保证金金额");
                            String str = Convert.toStr(Convert.getDoubleString(depositAmt));
                            bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(depositAmt)), true);

                            bookmarksNavigator.moveToBookmark("parts保证金金额1");
                            bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(depositAmt)), true);

                            bookmarksNavigator.moveToBookmark("parts保证金金额大写");
                            bookmarksNavigator.replaceBookmarkContent(Convert.toChinese(Convert.toStr(Convert.getDoubleString(depositAmt))), true);

                        }

                    }

                    if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit().equals("")) {
                        bookmarksNavigator.moveToBookmark("parts保证金比例");
                        bookmarksNavigator.replaceBookmarkContent(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit(), true);

                        double deposit = Double.parseDouble(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit().replace("%", ""));
                        double depositAmt = Arith.mul(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit(), deposit * 0.01);

                        bookmarksNavigator.moveToBookmark("parts保证金金额1");
                        bookmarksNavigator.replaceBookmarkContent(Convert.toStr(Convert.getDoubleString(depositAmt)), true);

                    }
                }
            }


            bookmarksNavigator.moveToBookmark("CreditBridge一次性额度延期还款起始日");
            bookmarksNavigator.replaceBookmarkContent(signedDate, true);



            bookmarksNavigator.moveToBookmark("经销商名称");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称1");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称2");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称3");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称4");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称5");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称6");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称7");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称8");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称9");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称（中文）");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);

            bookmarksNavigator.moveToBookmark("经销商名称（中文）1");
            bookmarksNavigator.replaceBookmarkContent(dealername, true);
            //TODO 书签表格替换未完
            //查三方WFS取贷款放款日，本金金额，填充表格数据
            if(contractname.equals("CB 大客户协议（一次性额度）") || contractname.equals("CB 大客户延期还款产品担保附件二")) {
                if(StringUtils.isNotEmpty(contract)) {
                    String substring = contract.substring(0, 6);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("dealerName", dealername);
                    List<Map<String, Object>> vinlist = parkingLocationRegistrationService.vinlist(map);
                    StringBuilder stringBuilderTable1 = new StringBuilder();
                    StringBuilder stringBuilderTable2 = new StringBuilder();
                    StringBuilder stringBuilderTable3 = new StringBuilder();
                    StringBuilder stringBuilderTable4 = new StringBuilder();
                    StringBuilder stringBuilderTable5 = new StringBuilder();
                    StringBuilder stringBuilderTable6 = new StringBuilder();
                    StringBuilder stringBuilderTable7 = new StringBuilder();
                    BigDecimal bigDecimal = new BigDecimal(0);
                    CbFleetDetails cbFleetDetails = new CbFleetDetails();
                    cbFleetDetails.setDealerName(dealername);
                    cbFleetDetails.setInstanceId(instanceid);
                    List<CbFleetDetails> cbFleetDetails1 = cbFleetDetailsMapper.selectCbFleetDetailsList(cbFleetDetails);
                    int zongsu = 0;
                    if(StringUtils.isNotEmpty(cbFleetDetails1)){
                        for(int i = 0; i <cbFleetDetails1.size() ; i++){
                            if(StringUtils.isNotEmpty(cbFleetDetails1.get(i).getVehicle())) {
                                String vehicle = cbFleetDetails1.get(i).getVehicle();
                                List<Map<String, Object>> collections = JSON.parseObject(vehicle, List.class);
                                //TODO CB表格书签暂定
                                for (Map<String, Object> a : collections) {
                                    if (StringUtils.isNotEmpty(a.get("VinNo").toString()) && !a.get("VinNo").toString().equals("")) {
                                        String vinNo = a.get("VinNo").toString();//车架号
                                        String originalBalance = a.get("originalBalance").toString().replace(",", "");;//金额
                                        String paymentDate = a.get("paymentDate").toString();//付款日;
                                        String startDay = DateUtils.parseDateToStr("yyyy-MM-dd", annualReviewy.getUpdateTime());
                                        String maturityDate = a.get("maturityDate").toString();
                                        //一次性车架号
                                        stringBuilderTable1.append(vinNo).append("\n");
                                        //一次性额度贷款放款日
                                        stringBuilderTable2.append(paymentDate).append("\n");
                                        //一次性额度贷款本金金额。
                                        stringBuilderTable3.append(originalBalance).append("\n");
                                        //一次性延期还款起始日
                                        stringBuilderTable4.append(startDay).append("\n");
                                        //查Credit Bridge一次性额度延期还款还款日
                                        if (StringUtils.isNotNull(cbFleetDetails1.get(i).getPaymentDays())) {
                                            Date endTime = DateUtils.parseDate(DateUtils.getEndTime(annualReviewy.getUpdateTime(), (int) cbFleetDetails1.get(i).getPaymentDays().longValue()));
                                            String dateToStr = DateUtils.parseDateToStr("yyyy-MM-dd", endTime);
                                            Date date = DateUtils.parseDate(maturityDate);
                                            String dateToStrnew = DateUtils.parseDateToStr("yyyy-MM-dd", date);
                                            int i1 = endTime.compareTo(date);
                                            //还款日这个地方，判断他跟MaturityDate日期，哪个更早就用哪个
                                            if (i1 > 0) {
                                                stringBuilderTable5.append(dateToStrnew).append("\n");
                                            }
                                            if (i1 < 0) {
                                                stringBuilderTable5.append(dateToStr).append("\n");
                                            }
                                            if (i1 == 0) {
                                                stringBuilderTable5.append(dateToStr).append("\n");
                                            }

                                        }
                                        //总数或者编号
                                        zongsu += 1;
                                        //编号
                                        stringBuilderTable6.append(zongsu).append("\n");
                                        //总金额
                                        bigDecimal = bigDecimal.add(Convert.toBigDecimal(originalBalance));
                                    }
                                }
                                if (StringUtils.isNotEmpty(cbFleetDetails1.get(i).getFleetCustomer())) {
                                    stringBuilderTable7.append(cbFleetDetails1.get(i).getFleetCustomer()).append(",");
                                }
                            }
                        }
                        String table1="";
                        String table2="";
                        String table3="";
                        String table4="";
                        String table5="";
                        String table6="";
                        String table7="";
                        if(StringUtils.isNotEmpty(stringBuilderTable1)) {
                            table1 = (stringBuilderTable1.substring(0, stringBuilderTable1.length() - 1)).toString();
                        }
                        if(StringUtils.isNotEmpty(stringBuilderTable2)) {
                            table2 = (stringBuilderTable2.substring(0, stringBuilderTable2.length() - 1)).toString();
                        }
                        if(StringUtils.isNotEmpty(stringBuilderTable3)) {
                            table3 = (stringBuilderTable3.substring(0, stringBuilderTable3.length() - 1)).toString();
                        }
                        if(StringUtils.isNotEmpty(stringBuilderTable4)) {
                            table4 = (stringBuilderTable4.substring(0, stringBuilderTable4.length() - 1)).toString();
                        }
                        if(StringUtils.isNotEmpty(stringBuilderTable5)) {
                            table5 = (stringBuilderTable5.substring(0, stringBuilderTable5.length() - 1)).toString();
                        }
                        if(StringUtils.isNotEmpty(stringBuilderTable6)) {
                            table6 = (stringBuilderTable6.substring(0, stringBuilderTable6.length() - 1)).toString();
                        }
                        if(StringUtils.isNotEmpty(stringBuilderTable7)) {
                            table7 = (stringBuilderTable7.substring(0, stringBuilderTable7.length() - 1)).toString();
                        }
                        //CB表格第一列车架号
                        bookmarksNavigator.moveToBookmark("第一列车架号");
                        bookmarksNavigator.replaceBookmarkContent(table1, true);
                        //CB表格第二列贷款放款日
                        bookmarksNavigator.moveToBookmark("第二列贷款放款日");
                        bookmarksNavigator.replaceBookmarkContent(table2, true);
                        //CB表格第三列贷款本金
                        bookmarksNavigator.moveToBookmark("第三列贷款本金");
                        bookmarksNavigator.replaceBookmarkContent(table3, true);
                        //CB表格第四列延期还款起始日
                        bookmarksNavigator.moveToBookmark("第四列延期还款起始日");
                        bookmarksNavigator.replaceBookmarkContent(table4, true);
                        //CB表格第五列延期还款还款日
                        bookmarksNavigator.moveToBookmark("第五列延期还款还款日");
                        bookmarksNavigator.replaceBookmarkContent(table5, true);
                        //编号
                        bookmarksNavigator.moveToBookmark("编号");
                        bookmarksNavigator.replaceBookmarkContent(table6, true);
                        //总数
                        bookmarksNavigator.moveToBookmark("总数");
                        bookmarksNavigator.replaceBookmarkContent(Convert.toStr(zongsu), true);
                        //总本金金额
                        bookmarksNavigator.moveToBookmark("总贷款本金金额");
                        bookmarksNavigator.replaceBookmarkContent(bigDecimal.toString(), true);

                        bookmarksNavigator.moveToBookmark("CreditBridge一次性额度购买者名称全称");
                        bookmarksNavigator.replaceBookmarkContent(table7, true);
                    }
//                    for (Map<String, Object> a : vinlist) {
//                        if(substring.equals(a.get("DealerCode"))){
//                            List<Map<String, Object>> loanInfo = (List<Map<String, Object>>)a.get("LoanInfo");
//                            for (int i = 0; i <loanInfo.size() ; i++) {
//                                //一次性车架号
//                                String vin = Convert.toStr(loanInfo.get(i).get("VIN"));
//                                stringBuilderTable1.append("\n").append(vin).append("\n");
//                                //一次性额度贷款放款日
//                                String paymentDate = Convert.toStr(loanInfo.get(i).get("PaymentDate"));
//                                stringBuilderTable2.append("\n").append(paymentDate).append("\n");
//                                //一次性额度贷款本金金额。
//                                String loanOSAmount = Convert.toStr(loanInfo.get(i).get("LoanOSAmount"));
//                                stringBuilderTable3.append("\n").append(loanOSAmount).append("\n");
//                                //一次性延期还款起始日
//                                stringBuilderTable4.append(signedDate).append("\n");
//                                //编号
//                                stringBuilderTable6.append("\n").append(i+1).append("\n");
//                                //总金额
//                                bigDecimal = bigDecimal.add(Convert.toBigDecimal(loanInfo.get(i).get("LoanOSAmount")));
//                                //查Credit Bridge一次性额度延期还款还款日
//                                if(StringUtils.isNotEmpty(cbFleetDetails1)) {
//                                    Date endTime = DateUtils.parseDate(DateUtils.getEndTime(annualReviewy.getUpdateTime(), (int) cbFleetDetails1.get(0).getPaymentDays().longValue()));
//                                    String dateToStr = DateUtils.parseDateToStr("yyyy年MM月dd日", endTime);
//                                    stringBuilderTable5.append(dateToStr).append("\n");
//                                }
//                                zongsu +=1;
//                            }
//                        }
//                    }
                }
            }



            File file2 = new File(RuoYiConfig.getProfile()+"/wordfill/word/"+instanceid);
            if (file2.mkdir()) {
                System.out.println("单文件夹"+instanceid+"创建成功！创建后的文件目录为：" + file2.getPath() + ",上级文件为:" + file2.getParent());
            }
            //保存文档FileFormat.Docx_2013
            doc.saveToFile("/"+RuoYiConfig.getProfile()+"/wordfill/word/"+instanceid+"/"+contractname+".docx", FileFormat.Docx_2013);
            doc.dispose();

            //重新读取生成的文档
            InputStream is = new FileInputStream("/"+RuoYiConfig.getProfile()+"/wordfill/word/"+instanceid+"/"+contractname+".docx");
            XWPFDocument document = new XWPFDocument(is);
            //以上Spire.Doc 生成的文件会自带警告信息，这里来删除Spire.Doc 的警告
            document.removeBodyElement(0);
            //输出word内容文件流，新输出路径位置
            OutputStream os=new FileOutputStream("/"+RuoYiConfig.getProfile()+"/wordfill/word/"+instanceid+"/"+contractname+".docx");
            document.write(os);
            os.flush();
            os.close();
            document.close();

        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException("word书签填充出错!!");
        }
    }
}
