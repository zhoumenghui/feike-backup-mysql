package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.HAml;
import com.ruoyi.feike.domain.vo.DealerInformationVO;
import com.ruoyi.feike.domain.vo.DealerformationVO;
import com.ruoyi.feike.mapper.DealerInformationMapper;
import com.ruoyi.feike.mapper.DealerSectorLimitflagMapper;
import com.ruoyi.feike.service.IDealerInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * feikeController
 *
 * @author ybw
 * @date 2022-07-12
 */
@RestController
@RequestMapping("/feike/informationMaintain")
public class DealerInformationController extends BaseController
{
    @Autowired
    private IDealerInformationService dealerInformationService;
    @Autowired
    private DealerSectorLimitflagMapper dealerSectorLimitflagMapper;
    @Autowired
    private DealerInformationMapper dealerInformationMapper;

    /**
     * 查询feike列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DealerInformation dealerInformation)
    {
        startPage();
        List<DealerInformation> list = dealerInformationService.selectDealerInformationList3(dealerInformation);
        for (DealerInformation information : list) {
            if(StringUtils.isNotEmpty(information.getShareholderStructure())) {
                String shareholderStructure = information.getShareholderStructure();
                if (StringUtils.isNotEmpty(shareholderStructure)) {
                    String[] split = shareholderStructure.split(";");
                    StringBuffer gudong = new StringBuffer();
                    for (String sp : split) {
                        Map<String, Object> objectObjectHashMap = new HashMap<>();
                        ArrayList<String> objects = new ArrayList<>();
                        StringTokenizer stringTokenizer = new StringTokenizer(sp, "#:");
                        while (stringTokenizer.hasMoreElements()) {
                            objects.add(stringTokenizer.nextToken());
                        }
                        if (objects.size() == 3) {
                            gudong.append(objects.get(1));
                            gudong.append("/");
                        }
                    }
                    if (gudong.length() > 0) {
                        gudong.deleteCharAt(gudong.length() - 1);
                        gudong.append(",");
                        for (String sp : split) {
                            Map<String, Object> objectObjectHashMap = new HashMap<>();
                            ArrayList<String> objects = new ArrayList<>();
                            StringTokenizer stringTokenizer = new StringTokenizer(sp, "#:");
                            while (stringTokenizer.hasMoreElements()) {
                                objects.add(stringTokenizer.nextToken());
                            }
                            if (objects.size() == 3) {
                                gudong.append(objects.get(2));
                                gudong.append("/");
                            }
                        }
                    }
                    if (gudong.length() > 0) {
                        gudong.deleteCharAt(gudong.length() - 1);
                        information.setShareholderStructure(gudong.toString());
                    }else{
                        information.setShareholderStructure("");
                    }
                }
            }
        }
        return getDataTable(list);
    }

    /**
     * 查询根据dealer分组后的列表
     */
    @GetMapping("/listnew")
    public TableDataInfo listnew(DealerInformation dealerInformation)
    {
        startPage();
        List<DealerInformation> list = dealerInformationService.selectDealerInformationListnew(dealerInformation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealerInformation dealerInformation)
    {
        List<DealerInformation> list = dealerInformationService.selectDealerInformationList3(dealerInformation);
        for (DealerInformation information : list) {
            if(StringUtils.isNotEmpty(information.getShareholderStructure())) {
                String shareholderStructure = information.getShareholderStructure();
                if (StringUtils.isNotEmpty(shareholderStructure)) {
                    String[] split = shareholderStructure.split(";");
                    StringBuffer gudong = new StringBuffer();
                    for (String sp : split) {
                        Map<String, Object> objectObjectHashMap = new HashMap<>();
                        ArrayList<String> objects = new ArrayList<>();
                        StringTokenizer stringTokenizer = new StringTokenizer(sp, "#:");
                        while (stringTokenizer.hasMoreElements()) {
                            objects.add(stringTokenizer.nextToken());
                        }
                        if (objects.size() == 3) {
                            gudong.append(objects.get(1));
                            gudong.append("/");
                        }
                    }
                    if (gudong.length() > 0) {
                        gudong.deleteCharAt(gudong.length() - 1);
                        gudong.append(",");
                        for (String sp : split) {
                            Map<String, Object> objectObjectHashMap = new HashMap<>();
                            ArrayList<String> objects = new ArrayList<>();
                            StringTokenizer stringTokenizer = new StringTokenizer(sp, "#:");
                            while (stringTokenizer.hasMoreElements()) {
                                objects.add(stringTokenizer.nextToken());
                            }
                            if (objects.size() == 3) {
                                gudong.append(objects.get(2));
                                gudong.append("/");
                            }
                        }
                    }
                    if (gudong.length() > 0) {
                        gudong.deleteCharAt(gudong.length() - 1);
                        information.setShareholderStructure(gudong.toString());
                    }else{
                        information.setShareholderStructure("");
                    }
                }
                if(StringUtils.isNotEmpty(information.getCorporateGuarantorCn())){
                    String corporateGuarantorCn = information.getCorporateGuarantorCn();
                    String str = corporateGuarantorCn.replace('#', '/');
                    information.setCorporateGuarantorCn(str);
                }
                if(StringUtils.isNotEmpty(information.getCorporateGuarantorJson())){
                    String corporateGuarantorJson = information.getCorporateGuarantorJson();
                    String str = corporateGuarantorJson.replace('#', '/');
                    String replace = str.replace(";", ",");
                    information.setCorporateGuarantorJson(replace);
                }
            }
        }
        ExcelUtil<DealerInformation> util = new ExcelUtil<DealerInformation>(DealerInformation.class);
        return util.exportExcel(list, "InformationMaintain");
    }

    /**
     * 获取feike详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dealerInformationService.selectDealerInformationById(id));
    }

    /**
     * 根据dealername获取feike详细信息
     */
    @PostMapping(value = "/list")
    public AjaxResult list(@RequestBody Map<String,Object> map)
    {
        DealerInformationVO dealerformationVO = dealerInformationService.selectDealerInformationByname(map);
        return AjaxResult.success(dealerformationVO);
    }

    @PostMapping(value = "/listByName")
    public TableDataInfo listByName(@RequestBody List<DealerInformation> params)
    {
        List<DealerformationVO> list = dealerInformationService.listByName(params);
        return getDataTable(list);
    }

    /**
     * 新增feike
     */
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealerInformationVO dealerInformation)
    {
        return toAjax(dealerInformationService.insertDealerInformation(dealerInformation));
    }

    /**
     * 修改feike
     */
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealerInformationVO dealerInformation)
    {
        return toAjax(dealerInformationService.updateDealerInformationnew(dealerInformation));
    }

    /**
     * 删除feike
     */
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dealerInformationService.deleteDealerInformationByIds(ids));
    }

    @GetMapping(value = "/getGroups")
    public AjaxResult getGroups()
    {
        return AjaxResult.success(dealerInformationService.getGroups());
    }

    @GetMapping(value = "/getDealers")
    public AjaxResult getDealers(String groupName)
    {
        if("-".equals(groupName)){
            return AjaxResult.success(dealerInformationService.getDealersbynull(groupName));
        }
        return AjaxResult.success(dealerInformationService.getDealers(groupName));
    }

    @GetMapping(value = "/getSectors")
    public AjaxResult getSectors(String dealerName)
    {
        return AjaxResult.success(dealerInformationService.getSectors(dealerName));
    }


    @PostMapping(value = "/getDealerInfo")
    public AjaxResult getDealerInfo(@RequestBody Map<String,Object> map)
    {
        String groupName = (String)map.get("groupName");
        String dealerName = (String)map.get("dealerName");
        if(StringUtils.isNotEmpty(groupName)){
            return AjaxResult.success(dealerInformationService.getDealerInfo(groupName));
        }else {
            return AjaxResult.success(dealerInformationService.getDealerInfoByDealerName(dealerName));
        }
    }

    @GetMapping(value = "/getDealerInfoByALL")
    public AjaxResult getDealerInfoByALL() {
        return AjaxResult.success(dealerInformationService.getDealerInfoByALL());
    }

    @GetMapping(value = "/updateFca")
    public AjaxResult updateFca()
    {
        dealerInformationMapper.updateDealerInformationOEM();
        return toAjax(dealerSectorLimitflagMapper.updateDealerSectorLimitflagOwnFlag());
    }

    @GetMapping(value = "/updateOem")
    public AjaxResult updateOem()
    {
        return toAjax(dealerSectorLimitflagMapper.updateDealerSectorLimitflagThreePartyFlag());
    }
}
