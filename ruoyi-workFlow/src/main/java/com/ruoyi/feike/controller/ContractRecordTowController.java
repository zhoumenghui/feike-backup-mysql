package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.vo.ContractRecordTow;
import com.ruoyi.feike.service.IContractRecordTowSerivce;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 *
 * @author ruoyi
 * @date 2022-10-27
 */
@RestController
@RequestMapping("/contract/recordTow")
public class ContractRecordTowController extends BaseController
{
    @Autowired
    private IContractRecordTowSerivce contractRecordTowSerivce;

    @Autowired
    private TokenService tokenService;

    /**
     * 查询【请填写功能名称】列表
     */
    //@PreAuthorize("@ss.hasPermi('system:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(ContractRecordTow contractRecordTow)
    {
        startPage();
        List<ContractRecordTow> list = contractRecordTowSerivce.selectContractRecordTowList(contractRecordTow);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    //@PreAuthorize("@ss.hasPermi('system:record:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ContractRecordTow contractRecordTow)
    {
        List<ContractRecordTow> list = contractRecordTowSerivce.selectContractRecordTowList(contractRecordTow);
        ExcelUtil<ContractRecordTow> util = new ExcelUtil<ContractRecordTow>(ContractRecordTow.class);
        return util.exportExcel(list, "recordTowInfo");
    }


    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:record:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ContractRecordTow contractRecordTow)
    {
        return toAjax(contractRecordTowSerivce.insertContractRecordTow(contractRecordTow));
    }


}
