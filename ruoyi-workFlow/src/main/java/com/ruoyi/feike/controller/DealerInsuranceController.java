package com.ruoyi.feike.controller;

import com.ruoyi.common.core.controller.BaseController;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.feike.domain.BillingInformation;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.InsuranceAddress;
import com.ruoyi.feike.domain.SelfplanInformation;
import com.ruoyi.feike.mapper.BillingInformationMapper;
import com.ruoyi.feike.mapper.InsuranceAddressMapper;
import com.ruoyi.feike.service.IBillingInformationService;
import com.ruoyi.feike.service.IInsuranceAddressService;
import com.ruoyi.feike.service.IReportInsuranceThreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("/feike/dealerInsurance")
public class DealerInsuranceController extends BaseController
{

    @Autowired
    private IReportInsuranceThreeService reportInsuranceThreeService;

    @Autowired
    private InsuranceAddressMapper insuranceAddressMapper;

    @Autowired
    private BillingInformationMapper billingInformationMapper;

    @GetMapping("/list")
    public TableDataInfo list(DealerInformation dealerInformation)
    {
        startPage();
        List<Map<String, Object>> list = reportInsuranceThreeService.getDealerInsuranceByDealerName(dealerInformation.getDealerName());
        return getDataTable(list);
    }

    @GetMapping("/getInfoByDealerName/{dealerName}")
    public AjaxResult getInfoByDealerName(@PathVariable("dealerName")String dealerName){
        if(StringUtils.isNotEmpty(dealerName)){
            String id = dealerName.replace("\"", "");
            return AjaxResult.success(reportInsuranceThreeService.getInfoByDealerName(id));
        }else {
            return AjaxResult.error("经销商名称不能为空");
        }
    }

    @PostMapping("/add")
    public AjaxResult addDealerInsurance(@RequestBody Map<String, Object> map)
    {
        System.out.println("新增接收参数:"+map);
        List<Map<String, Object>> declarationAddress = (List<Map<String, Object>>) map.get("declarationAddress");
        String dealerName = (String)map.get("dealerName");
        String receiverCompanyAddress = (String)map.get("receiverCompanyAddress");
        String receiverTelephone = (String)map.get("receiverTelephone");
        String receiver = (String)map.get("receiver");
        Integer isGeneral = (Integer)map.get("isGeneral");
        String address2 = (String)map.get("address");
        String  isGeneralStr = "是";
        if(isGeneral == 2){
            isGeneralStr = "否";
        }
        String bankAccount = (String)map.get("bankAccount");
        String openingBank = (String)map.get("openingBank");
        String telephone = (String)map.get("telephone");
        String emailAddress = (String)map.get("emailAddress");
        String registrationNumber = (String)map.get("registrationNumber");
        for (Map<String, Object> addressList : declarationAddress) {
            String address = (String) addressList.get("address");
            String mainShopName = (String) addressList.get("mainShopName");
            InsuranceAddress insuranceAddress = new InsuranceAddress();
            insuranceAddress.setDealerName(dealerName);
            insuranceAddress.setAddress(address);
            insuranceAddress.setMainShopName(mainShopName);
            insuranceAddressMapper.insertInsuranceAddressByInfo(insuranceAddress);
        }
        BillingInformation billingInformation = new BillingInformation();
        billingInformation.setCompanyName(dealerName);
        billingInformation.setIsGeneral(isGeneralStr);
        billingInformation.setBankAccount(bankAccount);
        billingInformation.setAddress(address2);
        billingInformation.setOpeningBank(openingBank);
        billingInformation.setTelephone(telephone);
        billingInformation.setRegistrationNumber(registrationNumber);
        billingInformationMapper.insertBillingInformationByInfo(billingInformation);

        BillingInformation billingInformationByInfo = new BillingInformation();
        billingInformationByInfo.setReceiverCompanyName(dealerName);
        billingInformationByInfo.setReceiver(receiver);
        billingInformationByInfo.setReceiverTelephone(receiverTelephone);
        billingInformationByInfo.setReceiverCompanyAddress(receiverCompanyAddress);
        billingInformation.setEmailAddress(emailAddress);
        billingInformationMapper.insertBillingInformationByReceiver(billingInformationByInfo);

        return AjaxResult.success();
    }

    /**
     * 修改feike
     */
    @PostMapping("/edit")
    public AjaxResult editDealerInsurance(@RequestBody Map<String, Object> map)
    {
        System.out.println("修改接收参数:"+map);
        List<Map<String, Object>> declarationAddress = (List<Map<String, Object>>) map.get("declarationAddress");
        String dealerName = (String)map.get("dealerName");
        String receiverCompanyAddress = (String)map.get("receiverCompanyAddress");
        String receiverTelephone = (String)map.get("receiverTelephone");
        String receiver = (String)map.get("receiver");
        String address2 = (String)map.get("address");
        Integer isGeneral = (Integer)map.get("isGeneral");
        String  isGeneralStr = "是";
        if(isGeneral == 2){
            isGeneralStr = "否";
        }
        String bankAccount = (String)map.get("bankAccount");
        String openingBank = (String)map.get("openingBank");
        String telephone = (String)map.get("telephone");
        String emailAddress = (String)map.get("emailAddress");
        String registrationNumber = (String)map.get("registrationNumber");
        if(StringUtils.isNotEmpty(dealerName)){
            insuranceAddressMapper.deleteInsuranceAddressByInfo(dealerName);
            billingInformationMapper.deleteBillingInformationByInfo(dealerName);
            billingInformationMapper.deleteBillingInformationByReceiver(dealerName);

            for (Map<String, Object> addressList : declarationAddress) {
                String address = (String) addressList.get("address");
                String mainShopName = (String) addressList.get("mainShopName");
                InsuranceAddress insuranceAddress = new InsuranceAddress();
                insuranceAddress.setDealerName(dealerName);
                insuranceAddress.setAddress(address);
                insuranceAddress.setMainShopName(mainShopName);
                insuranceAddressMapper.insertInsuranceAddressByInfo(insuranceAddress);
            }

            BillingInformation billingInformation = new BillingInformation();
            billingInformation.setCompanyName(dealerName);
            billingInformation.setIsGeneral(isGeneralStr);
            billingInformation.setBankAccount(bankAccount);
            billingInformation.setAddress(address2);
            billingInformation.setOpeningBank(openingBank);
            billingInformation.setTelephone(telephone);
            billingInformation.setRegistrationNumber(registrationNumber);
            billingInformationMapper.insertBillingInformationByInfo(billingInformation);

            BillingInformation billingInformationByInfo = new BillingInformation();
            billingInformationByInfo.setReceiverCompanyName(dealerName);
            billingInformationByInfo.setReceiver(receiver);
            billingInformationByInfo.setReceiverTelephone(receiverTelephone);
            billingInformationByInfo.setReceiverCompanyAddress(receiverCompanyAddress);
            billingInformationByInfo.setEmailAddress(emailAddress);
            billingInformationMapper.insertBillingInformationByReceiver(billingInformationByInfo);
        }

        return AjaxResult.success();
    }


}
