package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.DealercodeContract;
import com.ruoyi.feike.service.IDealercodeContractService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生成的合同详细信息Controller
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
@RestController
@RequestMapping("/feike/producecontract")
public class DealercodeContractController extends BaseController
{
    @Autowired
    private IDealercodeContractService dealercodeContractService;

    /**
     * 查询生成的合同详细信息列表
     */
    @PreAuthorize("@ss.hasPermi('feike:producecontract:list')")
    @GetMapping("/list")
    public TableDataInfo list(DealercodeContract dealercodeContract)
    {
        startPage();
        List<DealercodeContract> list = dealercodeContractService.selectDealercodeContractList(dealercodeContract);
        return getDataTable(list);
    }

    /**
     * 导出生成的合同详细信息列表
     */
    @PreAuthorize("@ss.hasPermi('feike:producecontract:export')")
    @Log(title = "生成的合同详细信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealercodeContract dealercodeContract)
    {
        List<DealercodeContract> list = dealercodeContractService.selectDealercodeContractList(dealercodeContract);
        ExcelUtil<DealercodeContract> util = new ExcelUtil<DealercodeContract>(DealercodeContract.class);
        return util.exportExcel(list, "producecontract");
    }

    /**
     * 获取生成的合同详细信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:producecontract:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dealercodeContractService.selectDealercodeContractById(id));
    }

    /**
     * 新增生成的合同详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:producecontract:add')")
    @Log(title = "生成的合同详细信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealercodeContract dealercodeContract)
    {
        return toAjax(dealercodeContractService.insertDealercodeContract(dealercodeContract));
    }

    /**
     * 修改生成的合同详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:producecontract:edit')")
    @Log(title = "生成的合同详细信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealercodeContract dealercodeContract)
    {
        return toAjax(dealercodeContractService.updateDealercodeContract(dealercodeContract));
    }

    /**
     * 删除生成的合同详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:producecontract:remove')")
    @Log(title = "生成的合同详细信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dealercodeContractService.deleteDealercodeContractByIds(ids));
    }
}
