package com.ruoyi.feike.controller;

import java.util.List;

import com.ruoyi.feike.domain.ReportDepositTow;
import com.ruoyi.feike.mapper.ReportDepositTowMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ReportDeposit;
import com.ruoyi.feike.service.IReportDepositService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
@RestController
@RequestMapping("/feike/reportDeposit")
public class ReportDepositController extends BaseController
{
    @Autowired
    private IReportDepositService reportDepositService;

    @Autowired
    private ReportDepositTowMapper reportDepositTowMapper;

    /**
     * 查询feike列表
     */
//    @PreAuthorize("@ss.hasPermi('feike:reportDeposit:list')")
    @GetMapping("/list")
    public TableDataInfo list(ReportDeposit reportDeposit)
    {
        startPage();
        List<ReportDeposit> list = reportDepositService.selectReportDepositList(reportDeposit);
        return getDataTable(list);
    }

    @GetMapping("/listTow")
    public TableDataInfo listTow(ReportDepositTow reportDepositTow)
    {
        startPage();
        List<ReportDepositTow> list = reportDepositTowMapper.selectReportDepositTowList(reportDepositTow);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
//    @PreAuthorize("@ss.hasPermi('feike:reportDeposit:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ReportDeposit reportDeposit)
    {
        List<ReportDeposit> list = reportDepositService.selectReportDepositList(reportDeposit);
        ExcelUtil<ReportDeposit> util = new ExcelUtil<ReportDeposit>(ReportDeposit.class);
        return util.exportExcel(list, "reportDeposit");
    }

    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/exportTow")
    public AjaxResult exportTow(ReportDepositTow reportDepositTow)
    {
        List<ReportDepositTow> list = reportDepositTowMapper.selectReportDepositTowList(reportDepositTow);
        ExcelUtil<ReportDepositTow> util = new ExcelUtil<ReportDepositTow>(ReportDepositTow.class);
        return util.exportExcel(list, "reportDepositTow");
    }
    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:reportDeposit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(reportDepositService.selectReportDepositById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:reportDeposit:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ReportDeposit reportDeposit)
    {
        return toAjax(reportDepositService.insertReportDeposit(reportDeposit));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:reportDeposit:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ReportDeposit reportDeposit)
    {
        return toAjax(reportDepositService.updateReportDeposit(reportDeposit));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:reportDeposit:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(reportDepositService.deleteReportDepositByIds(ids));
    }
}
