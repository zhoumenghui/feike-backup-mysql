package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.Securities;
import com.ruoyi.feike.service.ISecuritiesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-11
 */
@RestController
@RequestMapping("/feike/securities")
public class SecuritiesController extends BaseController
{
    @Autowired
    private ISecuritiesService securitiesService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:securities:list')")
    @GetMapping("/list")
    public TableDataInfo list(Securities securities)
    {
        startPage();
        List<Securities> list = securitiesService.selectSecuritiesList(securities);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:securities:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Securities securities)
    {
        List<Securities> list = securitiesService.selectSecuritiesList(securities);
        ExcelUtil<Securities> util = new ExcelUtil<Securities>(Securities.class);
        return util.exportExcel(list, "securities");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:securities:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(securitiesService.selectSecuritiesById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:securities:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Securities securities)
    {
        return toAjax(securitiesService.insertSecurities(securities));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:securities:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Securities securities)
    {
        return toAjax(securitiesService.updateSecurities(securities));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:securities:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(securitiesService.deleteSecuritiesByIds(ids));
    }
}
