package com.ruoyi.feike.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ExcelSheet;
import com.ruoyi.common.utils.ExportSheetUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.mapper.AnnualReviewyMapper;
import com.ruoyi.feike.mapper.InsuranceDeclarationInfoMapper;
import com.ruoyi.feike.service.IReportInsuranceThreeService;
import com.ruoyi.feike.service.IWfsLimitSetUpService;
import com.sun.jmx.snmp.BerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/feike/reportTwo")
public class ReportTwoController extends BaseController {

    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;

    @Autowired
    private IReportInsuranceThreeService reportInsuranceThreeService;

    @Autowired
    private IWfsLimitSetUpService wfsLimitSetUpService;

    @Autowired
    private InsuranceDeclarationInfoMapper infoMapper;

    /**
     * 查询报表h_report-insurance-2列表 流程9已完结数据
     */
    @GetMapping("/list")
    public TableDataInfo list(AnnualReviewy annualReviewy) {
        startPage();
        List<AnnualReviewy> annualReviewies = annualReviewyMapper.listWFSDo(annualReviewy);
        return getDataTable(annualReviewies);
    }


    /**
     * 获取报表h_report-insurance-2详细信息
     */
    @GetMapping(value = "/{instanceId}")
    public AjaxResult getInfo(@PathVariable("instanceId") String instanceId)
    {
        return AjaxResult.success(reportInsuranceThreeService.getTwo(instanceId));
    }

    /**
     * 根据月份进行导出
     */
    @GetMapping(value = "/export/{startDate}/{endDate}")
    public void getInfo(@PathVariable String startDate,@PathVariable String endDate,HttpServletResponse response, HttpServletRequest request)
    {
//        String startDate = "2022-12-01";
//        String endDate = "2023-01-31";
        Map<String,Object> map = new HashMap<>();
        map.put("startDate",startDate);
        map.put("endDate",endDate);
        Map<Object, Object> objectObjectMap = reportInsuranceThreeService.exportByDate(map);
//        hashMap.put("sumKeeps",sumKeeps);NewSumKeep
//        hashMap.put("addresses",addresses);InsuranceAddress
//        hashMap.put("informations",informations);BillingInformation


        List<NewSumKeep> sumKeeps = BeanUtils.castList(objectObjectMap.get("sumKeeps"), NewSumKeep.class);
        List<InsuranceAddress> addresses = BeanUtils.castList(objectObjectMap.get("addresses"), InsuranceAddress.class);
        List<InvoiceInformation> informations = BeanUtils.castList(objectObjectMap.get("informations"), InvoiceInformation.class);

        List<String[]> sumKeepsStr = new ArrayList<>();

        List<String[]> addressesStr = new ArrayList<>();

        List<String[]> informationsStr = new ArrayList<>();

        for (InvoiceInformation information : informations) {
            informationsStr.add(BeanUtils.removeString(BeanUtils.ObjectToStringArr(information)));
        }

        for (InsuranceAddress address : addresses) {
            addressesStr.add(BeanUtils.removeString(BeanUtils.ObjectToStringArr(address)));
        }

        if(CollectionUtil.isNotEmpty(sumKeeps)){
            for (NewSumKeep sumKeep : sumKeeps) {
                String[] strings =new String[9];
                strings[0] = sumKeep.getInstanceId();
                strings[1] = sumKeep.getDealerName();
                strings[2] = sumKeep.getSector();
                String effectiveDate = DateUtils.geLinToDate(sumKeep.getEffectiveDate().toString());
                strings[3] =effectiveDate ;
                String expiryDate = DateUtils.geLinToDate(sumKeep.getExpiryDate().toString());
                strings[4] = expiryDate;
                strings[5] = sumKeep.getSum().toString();
                strings[6] = sumKeep.getPlanRate();
                strings[7] = sumKeep.getPremium();
                String substring = sumKeep.getPremiumReceivedDate().substring(0, 10);
                strings[8] = substring;
                sumKeepsStr.add(strings);
            }
        }

        //设置sheet的表头与表名
        String[] invoiceSheetHead = {"Process No","Dealer Name", "Sector", "Effective Date", "Expiry Date", "Coverage", "Rate", "Premium", "Premium Received date"};
        String[] billSheetHead = {"Process No" , "Dealer Name", "Main Shop / 2nd Tier Dealership Name", "Adress"};
        String[] informationsHead = {"Process No", "Dealer Name", "Taxpayer identification number", "地址","电话","开户行","银行账号","是否为增值税一般纳税人"};
        ExcelSheet invoiceExcel = new ExcelSheet("Declared Information", invoiceSheetHead,sumKeepsStr);
        ExcelSheet addressExcel = new ExcelSheet("Declared address", billSheetHead,addressesStr);
        ExcelSheet informationsExcel = new ExcelSheet("Invoice Information", informationsHead,informationsStr);List<ExcelSheet> mysheet = new ArrayList<>();
        mysheet.add(invoiceExcel);
        mysheet.add(addressExcel);
        mysheet.add(informationsExcel);
        String fileName = "MonthlyInsuranceDeclaration";
        ExportSheetUtil.exportManySheetExcel(fileName, mysheet,response,request);

//        if(CollectionUtil.isEmpty(sumKeeps) && CollectionUtil.isEmpty(addresses) && CollectionUtil.isEmpty(informations)){
//            throw  new BaseException("所选月份无数据");
//        }else {
//
//
//        }


        //return AjaxResult.success();
    }


    @GetMapping("/getInfoByDealerName/{dealerName}")
    public AjaxResult getInfoByDealerName(@PathVariable("dealerName")String dealerName){
        if(StringUtils.isNotEmpty(dealerName)){
            String id = dealerName.replace("\"", "");
            return AjaxResult.success(reportInsuranceThreeService.getInfoByDealerName(id));
        }else {
            return AjaxResult.error("经销商名称不能为空");
        }
    }

    @GetMapping(value = "/getInfoByDealerNameByHis/{dealerName}/{instanceId}")
    public AjaxResult getInfoByDealerNameByHis(@PathVariable("dealerName")String dealerName ,@PathVariable("instanceId")String instanceId){
        if(StringUtils.isNotEmpty(dealerName)){
            String id = dealerName.replace("\"", "");
            return AjaxResult.success(reportInsuranceThreeService.getInfoByDealerNameByHis(id,instanceId));
        }else {
            return AjaxResult.error("经销商名称不能为空");
        }
    }
}
