package com.ruoyi.feike.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.constant.StatusReturnCodeConstant;
import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.feike.domain.vo.ThirdCreditInfoSimpleVO;
import com.ruoyi.feike.domain.vo.ThirdCreditInfoVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.CreditCondition;
import com.ruoyi.feike.service.ICreditConditionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author ybw
 * @date 2022-07-12
 */
@Api(value = "credit condition",tags = "credit condition")
@RestController
@RequestMapping("/feike/CreditCondition")
public class CreditConditionController extends BaseController
{
    @Autowired
    private ICreditConditionService creditConditionService;

    /**
     * 查询feike列表
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:list')")
    @ApiOperation("信息条件列表")
    @GetMapping("/list")
    public TableDataInfo list(@RequestBody List<String> dealerNames)
    {
        startPage();
        List<CreditCondition> list = creditConditionService.selectCreditConditionByList(dealerNames);
        return getDataTable(list);
    }

    @ApiOperation("获取第三方的..信息")
    @PostMapping("/thirdCredit")
    public List<ThirdCreditInfoSimpleVO> thirdCreditResult(@RequestBody List<String> dealerNames){
        return creditConditionService.thirdCreditResult(dealerNames);
    }


    /**
     * 导出feike列表
     */
/*    @PreAuthorize("@ss.hasPermi('feike:resource:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CreditCondition creditCondition)
    {
        List<CreditCondition> list = creditConditionService.selectCreditConditionList(creditCondition);
        ExcelUtil<CreditCondition> util = new ExcelUtil<CreditCondition>(CreditCondition.class);
        return util.exportExcel(list, "resource");
    }*/

    /**
     * 获取feike详细信息
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:query')")
    @ApiOperation("信息条件详情")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(creditConditionService.selectCreditConditionById(id));
    }
    /**
     * 新增feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:add')")
    @ApiOperation("信息条件添加")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CreditCondition creditCondition)
    {
        return toAjax(creditConditionService.insertCreditCondition(creditCondition));
    }

    /**
     * 修改feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:edit')")
    @ApiOperation("信息条件编辑")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CreditCondition creditCondition)
    {
        return toAjax(creditConditionService.updateCreditCondition(creditCondition));
    }

    /**
     * 删除feike
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:remove')")
    @ApiOperation("信息条件删除")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(creditConditionService.deleteCreditConditionByIds(ids));
    }

    /**
     * 新增经销商明细表
     */
    //@PreAuthorize("@ss.hasPermi('feike:resource:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(@RequestBody Map<String, Object> resultMap) {
        try {
            return toAjax(creditConditionService.saveCreditcs(resultMap));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("新增失败,原因是:"+e.toString());
        }
    }
}
