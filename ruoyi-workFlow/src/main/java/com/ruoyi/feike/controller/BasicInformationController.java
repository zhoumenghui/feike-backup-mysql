package com.ruoyi.feike.controller;

import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.service.IBasicInformationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/information")
public class BasicInformationController extends BaseController
{
    @Autowired
    private IBasicInformationService basicInformationService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:information:list')")
    @GetMapping("/list")
    public TableDataInfo list(BasicInformation basicInformation)
    {
        startPage();
        List<BasicInformation> list = basicInformationService.selectBasicInformationList(basicInformation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:information:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicInformation basicInformation)
    {
        List<BasicInformation> list = basicInformationService.selectBasicInformationList(basicInformation);
        ExcelUtil<BasicInformation> util = new ExcelUtil<BasicInformation>(BasicInformation.class);
        return util.exportExcel(list, "information");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:information:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(basicInformationService.selectBasicInformationById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:information:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasicInformation basicInformation)
    {
        return toAjax(basicInformationService.insertBasicInformation(basicInformation));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:information:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasicInformation basicInformation)
    {
        return toAjax(basicInformationService.updateBasicInformation(basicInformation));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:information:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(basicInformationService.deleteBasicInformationByIds(ids));
    }

    /**
     * @Description: 根据传入的dealerid(经销商id)修改PriorityId()Priority(优先级)
     * @Author: YBW
     * @param  resultMap
     * @Date:
     */
    @PreAuthorize("@ss.hasPermi('feike:information:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @RequestMapping(value = "/updateBasicInformationRules",method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult updateBasicInformationRules(@RequestBody Map<String, Object> resultMap){
        return toAjax(basicInformationService.updateBasicInformationRules(resultMap));
    }

    /**
     * 测试合同生成规则
     */
    @RequestMapping(value = "/listcontract",method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult listcontract(@RequestBody Map<String, Object> resultMap){
        return AjaxResult.success(basicInformationService.matchTheContractNew(resultMap));
    }

    /**
     * 测试分组是否成功
     */
    @RequestMapping(value = "/listcontractgroupby",method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult listcontractgroupby(@RequestBody Map<String, Object> resultMap){
        return AjaxResult.success(basicInformationService.contracts(resultMap));
    }

    /**
     * 在返回生成的合同数据前，将数据插入到h_dealercode_contract表里
     */
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("/insertcontract")
    public AjaxResult insertcontract(@RequestBody Map<String, Object> resultMap)
    {
        return toAjax(basicInformationService.contractnew(resultMap));
    }
}
