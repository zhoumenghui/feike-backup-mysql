package com.ruoyi.feike.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.service.IDealerInformationService;
import com.ruoyi.feike.service.IDepositDecreaseService;
import com.ruoyi.feike.service.ILimitCalculationForCommericalNeedsService;
import com.ruoyi.feike.service.IRefreshService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.Collator;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author lss
 * @version 1.0
 * @description: 外部数据源控制器
 * @date 2022/8/3 13:23
 */
@RestController
@RequestMapping("/feike")
@Slf4j
public class DepositDecreaseController {

    @Autowired
    private IDepositDecreaseService depositDecreaseService;

    @Autowired
    private IDealerInformationService dealerInformationService;

    @Autowired
    private ILimitCalculationForCommericalNeedsService limitCalculationForCommericalNeedsService;
    @Value("${wfs.url}")
    private String wfsUrl;


    /**
     * @description: 降低保证金比例申请审批流程接口
     * @param:
     * @return:
     * @author
     * @date: 2022/8/3 13:26
     */

    @PostMapping("/depositDecrease")
    public AjaxResult list(@RequestBody NewApplicationDTO newApplicationDTO) {
        if (newApplicationDTO == null) {
            return AjaxResult.error("数据错误!");
        }
        depositDecreaseService.insertActApprove(newApplicationDTO);
        return AjaxResult.success();
    }


    /**
     * @description: ODS数据查询接口
     * @param:
     * @return:
     * @author lss
     * @date: 2022/8/11 15:20
     */
    @PostMapping("/getForeignInfo")
    public AjaxResult getForeignInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) {
        return depositDecreaseService.getForignList(depositDecreaseDTO);
    }

    @PostMapping("/getCreditInfo")
    public AjaxResult getCreditInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("DealerCode", depositDecreaseDTO.getDealerCodeArr());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//        HttpPost httpPost = new HttpPost("http://10.226.186.86:8081/CreditLoanSuspenseAPIUAT7/CreditInfo");
        HttpPost httpPost = new HttpPost(wfsUrl + "CreditInfo");
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost.setHeader("Connection", "keep-alive");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String, Object> objectList = new HashMap<>();
        JSONArray data1 =null;
        JSONArray objects =null;
        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseStr = EntityUtils.toString(response.getEntity());
            log.info("WFS外部接口数据返回结果=[{}]", responseStr);
            objectList = JSONObject.parseObject(responseStr, Map.class);
            log.info("WFS外部接口数据返回JSON结果=[{}]", JSON.toJSONString(responseStr));
            Object data = objectList.get("Data");
            objects = new JSONArray();
            data1 = JSONObject.parseArray(JSONObject.toJSONString(objectList.get("Data")));
            if(data1!=null && data1.size()>0){
                for (Object o : data1) {
                    System.out.println(o);
                    JSONArray data2 = JSONObject.parseArray(JSONObject.toJSONString(o));
                    if(data2!=null && data2.size()>0){
                        Map<String, Object> stringObjectMap = new TreeMap<String, Object>(
                                new Comparator<String>() {
                                    public int compare(String obj1, String obj2) {
                                        // 升序排序
                                        return obj1.compareTo(obj2);
                                    }
                                });
                        JSONArray objects1 = new JSONArray();
                        for (Object o1 : data2) {
                            Map<String, Object>  stringObjectHashMap =  JSONObject.parseObject(JSONObject.toJSONString(o1),Map.class);
                            if(stringObjectHashMap != null && stringObjectHashMap.get("DealerCode")!=null && stringObjectHashMap.get("CreditInfo")!=null ){
                                JSONArray data3 = JSONObject.parseArray(JSONObject.toJSONString(stringObjectHashMap.get("CreditInfo")));
                                if(data3 !=null && data3.size()>0){
                                    for (Object o2 : data3) {
                                        if(o2!=null){
                                            Map<String, Object>  CreditInfo = JSONObject.parseObject(JSONObject.toJSONString(o2), Map.class);
                                            if(CreditInfo!=null && CreditInfo.get("Sector")!=null){
                                                stringObjectMap.put(CreditInfo.get("Sector").toString(),o1);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        //重新set
                        Set<String> keySet = stringObjectMap.keySet();
                        Iterator<String> iter = keySet.iterator();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            objects1.add(stringObjectMap.get(key));
                        }
                        objects.add(objects1);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(objectList);
        objectList.put("Data",objects);
        return AjaxResult.success(objectList);
    }

    @PostMapping("/getDeposit")
    public AjaxResult getDeposit(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException {
        JSONObject jsonObject = new JSONObject();
        List<String[]> dealerCodeArr = depositDecreaseDTO.getDealerCodeArr();
        jsonObject.put("DealerCode", depositDecreaseDTO.getDealerCodeArr());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//        HttpPost httpPost = new HttpPost("http://10.226.186.86:8081/CreditLoanSuspenseAPIUAT7/CreditInfo");
        HttpPost httpPost = new HttpPost(wfsUrl + "SuspenseInfo");
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost.setHeader("Connection", "keep-alive");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String, Object> objectList = new HashMap<>();
        List<Map<String, String>> maps = new ArrayList<>();
        ArrayList<String> stringNew = new ArrayList<>();
        ArrayList<String> stringOld = new ArrayList<>();
        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseStr = EntityUtils.toString(response.getEntity());
            log.info("WFS外部接口数据返回结果=[{}]", responseStr);
            objectList = JSONObject.parseObject(responseStr, Map.class);
            log.info("WFS外部接口数据返回JSON结果=[{}]", JSON.toJSONString(responseStr));
            try{
              /*  stringObjectHashMap = JSONObject.parseObject(String.valueOf(JSONObject.parseObject(String.valueOf(JSONObject.parseArray(JSONObject.toJSONString(JSONObject.parseArray(JSONObject.toJSONString(objectList.get("Data"))).get(0))).get(0)))), Map.class);
                System.out.println(stringObjectHashMap);*/
                Object data = objectList.get("Data");
                JSONArray data1 = JSONObject.parseArray(JSONObject.toJSONString(objectList.get("Data")));
                if(data1!=null && data1.size()>0){
                    for (Object o : data1) {
                        System.out.println(o);
                        JSONArray data2 = JSONObject.parseArray(JSONObject.toJSONString(o));
                        if(data2!=null && data2.size()>0){
                            for (Object o1 : data2) {
                                Map<String, Object>  stringObjectHashMap =  JSONObject.parseObject(JSONObject.toJSONString(o1),Map.class);
                                if(stringObjectHashMap != null && stringObjectHashMap.get("DealerCode")!=null && stringObjectHashMap.get("SuspenseInfo")!=null ){
                                    Map<String, Object>  suspenseInfo = JSONObject.parseObject(JSONObject.toJSONString(stringObjectHashMap.get("SuspenseInfo")), Map.class);
                                    if(suspenseInfo!=null && suspenseInfo.get("Deposit")!=null){
                                        HashMap<String, String> stringStringHashMap = new HashMap<>();
                                        stringStringHashMap.put("DealerCode",stringObjectHashMap.get("DealerCode").toString());
                                        stringNew.add(stringObjectHashMap.get("DealerCode").toString());
                                        stringStringHashMap.put("Deposit",suspenseInfo.get("Deposit").toString());
                                        if(suspenseInfo.get("CBDeposit")!=null){
                                            stringStringHashMap.put("CBDeposit",suspenseInfo.get("CBDeposit").toString());
                                        }
                                        if(suspenseInfo.get("PartDeposit")!=null){
                                            stringStringHashMap.put("PartDeposit",suspenseInfo.get("PartDeposit").toString());
                                        }
                                        if(suspenseInfo.get("UsedDeposit")!=null){
                                            stringStringHashMap.put("UsedDeposit",suspenseInfo.get("UsedDeposit").toString());
                                        }
                                        stringStringHashMap.put("TEMPDeposit","-");
                                        maps.add(stringStringHashMap);
                                    }
                                }
                            }
                        }
                    }
                }
            }catch (NullPointerException e){
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String[]> dealerCodeArr1 = depositDecreaseDTO.getDealerCodeArr();
        for (String[] strings : dealerCodeArr1) {
            if(strings!=null && strings.length>0){
                for (String string : strings) {
                    stringOld.add(string);
                }
            }
        }
        if(stringOld.size()>stringNew.size()){
            stringOld.removeAll(stringNew);
            if(stringOld.size()>0){
                for (String s : stringOld) {
                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                    stringStringHashMap.put("DealerCode",s);
                    stringStringHashMap.put("Deposit","");
                    stringStringHashMap.put("CBDeposit","");
                    stringStringHashMap.put("PartDeposit","");
                    stringStringHashMap.put("TEMPDeposit","-");
                    stringStringHashMap.put("UsedDeposit","");
                    maps.add(stringStringHashMap);
                }
            }
        }
        return AjaxResult.success(maps);
    }

    @PostMapping("/getLoanInfo")
    public AjaxResult LoanInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException {
        List<Object> resultList = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        JSONObject jsonObject = new JSONObject();
        List<String[]> dealerCodes = depositDecreaseDTO.getDealerCodeArr();
        List<String> dealerCodelist = new ArrayList<>();
        for (String[] dealerCode : dealerCodes) {
            for (String s : dealerCode) {
                dealerCodelist.add(s);
            }
        }

        List<DealerInformation> dealerInformations = dealerInformationService.listByCodes(dealerCodelist);
        List<Map<String, Map<String, String>>> vinNoList = new ArrayList<>();
        jsonObject.put("DealerCode", depositDecreaseDTO.getDealerCodeArr());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//        HttpPost httpPost = new HttpPost("http://10.226.186.86:8081/CreditLoanSuspenseAPIUAT7/CreditInfo");
        HttpPost httpPost = new HttpPost(wfsUrl + "LoanInfo");
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost.setHeader("Connection", "keep-alive");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String, Object> objectList = new HashMap<>();
        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseStr = EntityUtils.toString(response.getEntity());
            log.info("WFS外部接口数据返回结果=[{}]", responseStr);
            objectList = JSONObject.parseObject(responseStr, Map.class);
            log.info("WFS外部接口数据返回JSON结果=[{}]", JSON.toJSONString(responseStr));
            if (Integer.valueOf(String.valueOf(objectList.get("Code"))) == 200 || Integer.valueOf(String.valueOf(objectList.get("Code"))) == 205) {
                String data = String.valueOf(objectList.get("Data"));
                Map dateMap = new HashMap();
                JSONArray array = JSONObject.parseArray(data);
                for (int i = 0; i < array.size(); i++) {
                    JSONArray jsonArray = array.getJSONArray(i);
                    for (int i1 = 0; i1 < jsonArray.size(); i1++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i1);
                        if (null == jsonObject1) {
                            continue;
                        }
                        String dealerCode = jsonObject1.getString("DealerCode");
                        JSONArray loanInfo = jsonObject1.getJSONArray("LoanInfo");
                        List<Map<String, String>> selectMap = new ArrayList<>();
                        for (int i2 = 0; i2 < loanInfo.size(); i2++) {
                            Map<String, String> valueMap = new HashMap<>();
                            JSONObject jsonObject2 = loanInfo.getJSONObject(i2);
                            valueMap.put("vin", jsonObject2.getString("VIN"));
                            valueMap.put("loanOSAmount", jsonObject2.getString("LoanOSAmount"));
                            valueMap.put("maturityDate", jsonObject2.getString("MaturityDate"));
                            valueMap.put("paymentDate", jsonObject2.getString("PaymentDate"));
                            selectMap.add(valueMap);
                        }
                        dateMap.put(dealerCode, selectMap);
                    }
                    vinNoList.add(dateMap);
                }
            }

            List<DealerInformation> dealerInfo =  dealerInformations.stream().filter(BeanUtils.distinctByKey(DealerInformation::getDealerName)).collect(Collectors.toList());
            map.put("date", dealerInfo);
            map.put("select", vinNoList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AjaxResult.success(map);
    }


    @PostMapping("/getSuspenseInfo")
    public AjaxResult getSuspenseInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException {
      return dealerInformationService.getSuspenseInfo(depositDecreaseDTO);
    }



    /**
     * @description: 授信额度预测查询接口
     * @param:
     * @return:
     * @author wlw
     * @date:
     */
    @PostMapping("/getlimitCalculation")
    public AjaxResult getForeignInfo(@RequestBody LimitCalculationSearchDTO dto) {
        Map<String, List<LimitCalculationForCommericalNeeds>> map = limitCalculationForCommericalNeedsService.getForeignInfo(dto);
        return AjaxResult.success(map);
    }

    @PostMapping("/salesPerformanceAndTarget")
    public AjaxResult salesPerformanceAndTarget(@RequestBody SalesPerformanceAndTargetDTO dto) {
        String currentYear = DateUtils.getCurrentYear();
        String lastYear = DateUtils.getLastYear();
        dto.setLastYear(lastYear);
        dto.setCurrentYear(currentYear);
        List<String> titleList = new ArrayList<>();
        titleList.add("Item");
        titleList.add("Sector");
        titleList.add("YR" + lastYear + "-Actual");
        titleList.add("YTD" + currentYear + "-Actual");
        titleList.add("YR" + currentYear + "-Target");
        List<Map<String, Object>> list = depositDecreaseService.salesPerformanceAndTarget(dto);
        Map<String, Object> map = new HashMap<>();
        map.put("title", titleList);
        map.put("result", list);
        return AjaxResult.success(map);
    }

    @PostMapping("/wholesalePerformanceRecentMonthsOs")
    public AjaxResult wholesalePerformanceRecentMonthsOs(@RequestBody WholesalePerformanceRecentMonthsOsDTO dto) {
        String[] preYearMonth = DateUtils.getPreYearMonth();
        dto.setMonths(preYearMonth);
        dto.setMonthsStr(StringUtils.join(preYearMonth, ","));
        List<String> titleList = new ArrayList<>();
        titleList.add("Dealer");
        titleList.add("Sector");
        titleList.add("Limit type");
        StringBuffer buffer = new StringBuffer(preYearMonth[0]);
        buffer.insert(4, "-");
        titleList.add(buffer.toString());
        buffer.setLength(0);
        buffer.append(preYearMonth[1]);
        buffer.insert(4, "-");
        titleList.add(buffer.toString());
        buffer.setLength(0);
        buffer.append(preYearMonth[2]);
        buffer.insert(4, "-");
        titleList.add(buffer.toString());
        List<Map<String, Object>> list = depositDecreaseService.wholesalePerformanceRecentMonthsOs(dto);
        Map<String, Object> map = new HashMap<>();
        map.put("title", titleList);
        map.put("result", list);
        return AjaxResult.success(map);
    }

    @PostMapping("/loyaltyRecentQuarters")
    public AjaxResult loyaltyPerformanceRecentQuarters(@RequestBody LoyaltyPerformanceRecentDTO dto) {
        List<String> titleList = new ArrayList<>();
        dto.transformRecentQuarters();
        titleList.add("Item");
        titleList.add("Dealer");
        titleList.add("Sector");
        for (String s : dto.getLastYearTimeText()) {
            titleList.add(s);
        }
        for (String s : dto.getCurrentYearTimeText()) {
            titleList.add(s);
        }
        List<Map<String, Object>> list = depositDecreaseService.loyaltyPerformanceRecentQuarters(dto);
        Map<String, Object> map = new HashMap<>();
        map.put("title", titleList);
        map.put("result", list);
        return AjaxResult.success(map);
    }

    @PostMapping("/loyaltyRecentMonths")
    public AjaxResult loyaltyPerformanceRecentMonths(@RequestBody LoyaltyPerformanceRecentDTO dto) {
        dto.transformRecentMonths();
        List<String> titleList = new ArrayList<>();
        titleList.add("Item");
        titleList.add("Dealer");
        titleList.add("Sector");

        for (String s : dto.getLastYearTimeText()) {
            titleList.add(s);
        }
        for (String s : dto.getCurrentYearTimeText()) {
            titleList.add(s);
        }
        List<LoyaltyPerformance6month> list2 = new ArrayList<>();
        List<LoyaltyPerformance6month> list = depositDecreaseService.loyaltyPerformanceRecentMonths(dto);
        List<LoyaltyPerformance6month> list1 = depositDecreaseService.loyaltyPerformanceRecentMonthsRePenContract(dto);

        Collections.sort(list1, new Comparator<LoyaltyPerformance6month>() {
            @Override
            public int compare(LoyaltyPerformance6month o1, LoyaltyPerformance6month o2) {
                return o1.getSector().substring(0).compareTo(o2.getSector().substring(0));
            }

            @Override
            public boolean equals(Object obj) {
                return false;
            }
        });
        System.out.println(list1);
        list.addAll(list1);
        ArrayList<String> items = new ArrayList<>();
        items.add("WS Pen%");
        items.add("RT Pen%");
        items.add("Contract");
        Set<String> set = new HashSet<>();
        for (int i = 0; i < titleList.size(); i++) {
            for (int k = 0; k < items.size(); k++) {
                boolean flag = false;
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getMonth().equals(titleList.get(i))) {
                        String value = list.get(j).getValue();
                        flag = true;
                    }

                }
                if (!flag) {
                    for (int j = 0; j < dto.getDealerName().length; j++) {
                        LoyaltyPerformance6month loyaltyPerformance6month = new LoyaltyPerformance6month();
                        loyaltyPerformance6month.setDealerName(dto.getDealerName()[j]);
                        loyaltyPerformance6month.setItem(items.get(k));
                        loyaltyPerformance6month.setMonth(titleList.get(i));
                        // loyaltyPerformance6month.setSector();
                        loyaltyPerformance6month.setValue("0");
                    }
                }
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("title", titleList);
        for (LoyaltyPerformance6month loyaltyPerformance6month : list) {
            String value = loyaltyPerformance6month.getValue();
            if (StringUtils.equals(loyaltyPerformance6month.getItem(), "Contract")) {
                loyaltyPerformance6month.setValue(value.replace("%", ""));
            }
        }
        Collections.sort(list, new Comparator<LoyaltyPerformance6month>() {
            @Override
            public int compare(LoyaltyPerformance6month o1, LoyaltyPerformance6month o2) {
                return o1.getDealerName().compareTo(o2.getDealerName());
            }

            @Override
            public boolean equals(Object obj) {
                return false;
            }
        });
        Collections.sort(list, new Comparator<LoyaltyPerformance6month>() {
            @Override
            public int compare(LoyaltyPerformance6month o1, LoyaltyPerformance6month o2) {
                return o1.getItem().compareTo(o2.getItem());
            }

            @Override
            public boolean equals(Object obj) {
                return false;
            }
        });
        if(list!=null && list.size()>0){
            for (LoyaltyPerformance6month loyaltyPerformance6month : list) {
                if(loyaltyPerformance6month.getSector()!=null && !loyaltyPerformance6month.getSector().equals("SLB")){
                    list2.add(loyaltyPerformance6month);
                }
            }
        }
        map.put("result", list);
        if(list2!=null && list2.size()>0){
            map.put("result", list2);
        }
        return AjaxResult.success(map);
    }

    private List<Map<String, Object>> transResultList(List<Map<String, Object>> list) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        for (Map map : list) {

        }
        return null;
    }

    /**
     * @description: 获取WHOLESALE PERFORMANCE-recent 3 months O/S模块外部数据源
     * @param: LimitCalculationSearchDTO
     * @return:
     * @author lss
     * @date: 2022/8/18 15:05
     */
    @PostMapping("/performanceRecentThreemonth")
    public AjaxResult performanceRecentThreemonth(@RequestBody LimitCalculationSearchDTO dto) {
        Map<String, List<LimitCalculationForCommericalNeeds>> map = limitCalculationForCommericalNeedsService.getForeignInfo(dto);
        return AjaxResult.success(map);
    }



}
