package com.ruoyi.feike.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.LoyaltyPerformance4q;
import com.ruoyi.feike.service.IOracleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * feikeController
 *
 * @author ruoyi
 * @date 2022-07-07
 */
@RestController
@RequestMapping("/oracleData")
public class OracleController extends BaseController {

	@Autowired
	private IOracleService oracleService;

	@ApiOperation("机构信息列表")
	@PostMapping("/getLoyaltyPerformance4Q")
	 public TableDataInfo getLoyaltyPerformance4Q(@RequestBody Map<String,Object> map) {
		List<String> dealerNames = (List<String>) map.get("dealerNames");
		List<LoyaltyPerformance4q> list = oracleService.getLoyaltyPerformance4Q(dealerNames);
		return getDataTable(list);
	}
}
