package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.ActiveLimitAdjustmentProposal;
import com.ruoyi.feike.domain.HTempContract;

import java.util.List;


public interface HTempContractMapper
{

    public List<HTempContract> selectHTempContractList(HTempContract hTempContract);


    public List<HTempContract> selectHTempContractListGroup(HTempContract hTempContract);



    public int updateHTempContract(HTempContract hTempContract);

}
