package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.HDealerDm;

import java.util.List;
import java.util.Map;

/**
 * 经销商与dm岗位用户映射Mapper接口
 *
 * @author ruoyi
 * @date 2022-12-23
 */
public interface HDealerDmMapper
{


    /**
     * 查询【经销商与dm岗位用户映射表】
     *
     * @param dealerName 【请填写功能名称】dealerName
     * @return 【请填写功能名称】
     */
    public HDealerDm selectHDealerDmByDealerName(String dealerName);

    /**
     * 查询经销商与dm岗位用户映射
     *
     * @param id 经销商与dm岗位用户映射ID
     * @return 经销商与dm岗位用户映射
     */
    public HDealerDm selectHDealerDmById(Long id);

    /**
     * 查询经销商与dm岗位用户映射列表
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 经销商与dm岗位用户映射集合
     */
    public List<HDealerDm> selectHDealerDmList(HDealerDm hDealerDm);


    public List<HDealerDm> selectHDealerDmListGroupByUW(HDealerDm hDealerDm);


    /**
     * 新增经销商与dm岗位用户映射
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 结果
     */
    public int insertHDealerDm(HDealerDm hDealerDm);

    /**
     * 修改经销商与dm岗位用户映射
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 结果
     */
    public int updateHDealerDm(HDealerDm hDealerDm);

    /**
     * 删除经销商与dm岗位用户映射
     *
     * @param id 经销商与dm岗位用户映射ID
     * @return 结果
     */
    public int deleteHDealerDmById(Long id);

    /**
     * 批量删除经销商与dm岗位用户映射
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHDealerDmByIds(Long[] ids);


    /**
     * 根据dealername修改dealername
     *
     * @param  【经销商与dm岗位用户映射表】
     * @return 结果
     */
    public int updateHDealerDmDealername(Map map);
}
