package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.vo.DealerInformationVO;
import com.ruoyi.feike.domain.vo.DealerformationVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * feikeMapper接口
 *
 * @author ybw
 * @date 2022-07-12
 */
@Repository
public interface DealerInformationMapper
{

    DealerInformation selectInfoBySectorAndDealerName(@Param("sector")String sector,@Param("dealerName")String dealerName);


    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DealerInformation selectDealerInformationById(Long id);

    /**
     * 查询feike列表
     *
     * @param dealerInformation feike
     * @return feike集合
     */
    public List<DealerInformation> selectDealerInformationList(DealerInformation dealerInformation);

    public List<DealerInformation> selectDealerInformationList2(DealerInformation dealerInformation);

    public List<DealerInformation> selectDealerInformationList3(DealerInformation dealerInformation);


    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int insertDealerInformation(DealerInformation dealerInformation);

    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int insertDealerInformationnew(DealerInformationVO dealerInformation);

    /**
     * 修改feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int updateDealerInformation(DealerInformation dealerInformation);

    /**
     * 修改dealername相同的全部数据feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int updateDealerInformationnew(DealerInformation dealerInformation);

    public int updateDealerInformationDealername(DealerInformation dealerInformation);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerInformationById(Long id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerInformationByIds(Long[] ids);

    public List<DealerInformation> getGroups();
    public List<DealerInformation> getDealers(String groupName);

    public List<DealerInformation> getDealersbynull(String groupName);
    public List<DealerInformation> getSectors(String dealerName);

    /**
     * 根据dealername查询feike
     *
     * @param  dealerName
     * @return feike
     */
    public List<DealerInformation> selectDealerInformationByname(String dealerName);

    public List<DealerInformation> selectDealerInformationByCode(String dealerCode);

    public List<DealerInformation> selectDealerInformationListnew(DealerInformation dealerInformation);

    DealerInformation selectDealerInformationByDealerNameAndType(String dealernamecn);

    List<Map> selectDealerInformationDistinctALL();

    List<DealerInformation> selectDealerInformationByDealerName(String dealernamecn);

    List<DealerInformation> listByName(List<DealerInformation> params);

    List getInfoListByDealerName(@Param("dealerName") String dealerName);

    @Select("select * from h_dealer_information where dealerName in (#{dealerName})")
    List<DealerInformation> codesByDealerNames(@Param("dealerName") String dealerName);


    List<DealerInformation> listByCodes(@Param("dealerCodes") List<String> dealerCodes);

    int maxSerialNumber();

    int maxid();

    List<DealerInformation> getDealerNameByCode(@Param("dealerCode") String dealerCode);

    List<DealerInformation> getDealerNameByName(@Param("dealerName") String dealerName);
    //删除经销商全部信息
    public int deleteDealerInformationByName(String dealername);
    //当group发生改变的时候，修改全部group对应得数据
    int updateDealerInformationGroup(Map map);

    List<String> getDealerInfo(@Param("groupName") String groupName);

    List<String> getDealerInfoByDealerName(@Param("dealerName") String dealerName);


    List<String> getDealerInfoByALL();

    public int updateDealerInformationOEM();

}
