package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.HChangeLegalPerson;
import com.ruoyi.feike.domain.SysDbOperLog;

import java.util.List;


public interface SysDbOperLogMapper
{
    public int insertSysDbOperLog(SysDbOperLog sysDbOperLog);

}
