package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.vo.WholesalePerformanceRecentVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WholesalePerformanceRecentMapper {

    /**
     * 查询feike列表
     *
     * @param dealer
     * @return feike集合
     */
    public List<Map<String,Object>> selectBalanceInformationList(WholesalePerformanceRecentVO dealer);
}
