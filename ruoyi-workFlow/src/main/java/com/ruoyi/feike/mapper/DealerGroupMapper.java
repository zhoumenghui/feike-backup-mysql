package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.DealerGroup;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-07-05
 */
public interface DealerGroupMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DealerGroup selectDealerGroupById(String id);

    /**
     * 查询feike列表
     *
     * @param dealerGroup feike
     * @return feike集合
     */
    public List<DealerGroup> selectDealerGroupList(DealerGroup dealerGroup);

    /**
     * 新增feike
     *
     * @param dealerGroup feike
     * @return 结果
     */
    public int insertDealerGroup(DealerGroup dealerGroup);

    /**
     * 修改feike
     *
     * @param dealerGroup feike
     * @return 结果
     */
    public int updateDealerGroup(DealerGroup dealerGroup);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerGroupById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerGroupByIds(String[] ids);
}
