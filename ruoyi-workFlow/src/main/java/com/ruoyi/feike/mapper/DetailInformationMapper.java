package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.DetailInformation;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-06
 */
public interface DetailInformationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public DetailInformation selectDetailInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param detailInformation feike
     * @return feike集合
     */
    public List<DetailInformation> selectDetailInformationList(DetailInformation detailInformation);

    /**
     * 新增feike
     * 
     * @param detailInformation feike
     * @return 结果
     */
    public int insertDetailInformation(DetailInformation detailInformation);

    /**
     * 修改feike
     * 
     * @param detailInformation feike
     * @return 结果
     */
    public int updateDetailInformation(DetailInformation detailInformation);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteDetailInformationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDetailInformationByIds(String[] ids);

    int deleteDetailInformationByInstanceIds(String[] ids);

    int deleteDetailInformationByInstanceId(String instanceId);
}
