package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.vo.ContractRecord;
import com.ruoyi.feike.domain.vo.ContractRecordTow;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ContractRecordTowMapper {


    public List<ContractRecordTow> selectContractRecordTowList(ContractRecordTow contractRecordTow);

    public int insertContractRecordTow(ContractRecordTow contractRecordTow);

}
