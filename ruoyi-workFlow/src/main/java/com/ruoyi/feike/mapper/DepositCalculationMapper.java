package com.ruoyi.feike.mapper;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.feike.domain.DepositCalculation;
import com.ruoyi.feike.domain.ReportDeposit;
import io.lettuce.core.dynamic.annotation.Param;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-08-25
 */
public interface DepositCalculationMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DepositCalculation selectDepositCalculationById(String id);

    /**
     * 查询feike列表
     *
     * @param depositCalculation feike
     * @return feike集合
     */
    @DataSource(DataSourceType.MASTER)
    public List<DepositCalculation> selectDepositCalculationList(DepositCalculation depositCalculation);

    /**
     * 新增feike
     *
     * @param depositCalculation feike
     * @return 结果
     */
    public int insertDepositCalculation(DepositCalculation depositCalculation);

    /**
     * 修改feike
     *
     * @param depositCalculation feike
     * @return 结果
     */
    public int updateDepositCalculation(DepositCalculation depositCalculation);


    public int updateDepositCalculationByDeposit(DepositCalculation depositCalculation);


    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDepositCalculationById(String id);

    /**
     * 删除feike
     *
     * @param instanceId feikeID
     * @return 结果
     */
    public int deleteDepositCalculationByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDepositCalculationByIds(String[] ids);

    /**
     * 根据instanceId获取审批list
     * @param instanceId
     * @return
     */
    List<DepositCalculation> getListByInstanceId(@Param("instanceId") String instanceId);
}
