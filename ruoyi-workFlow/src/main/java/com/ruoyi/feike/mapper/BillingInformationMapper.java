package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.BillingInformation;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface BillingInformationMapper 
{
    /**
     * 查询feike
     * 
     * @param instanceId feikeID
     * @return feike
     */
    public BillingInformation selectBillingInformationById(String instanceId);

    /**
     * 查询feike列表
     * 
     * @param billingInformation feike
     * @return feike集合
     */
    public List<BillingInformation> selectBillingInformationList(BillingInformation billingInformation);

    /**
     * 新增feike
     * 
     * @param billingInformation feike
     * @return 结果
     */
    public int insertBillingInformation(BillingInformation billingInformation);

    public int insertBillingInformationByInfo(BillingInformation billingInformation);

    public int insertBillingInformationByReceiver(BillingInformation billingInformation);


    public int insertBillingInformationHisByInfo(BillingInformation billingInformation);

    public int insertBillingInformationHisByReceiver(BillingInformation billingInformation);

    public int deleteBillingInformationByInfo(String name);

    public int deleteBillingInformationByReceiver(String name);

    public int deleteBillingInformationHisByInfo(String instanceId);

    public int deleteBillingInformationHisByReceiver(String instanceId);

    /**
     * 修改feike
     * 
     * @param billingInformation feike
     * @return 结果
     */
    public int updateBillingInformation(BillingInformation billingInformation);

    /**
     * 删除feike
     * 
     * @param instanceId feikeID
     * @return 结果
     */
    public int deleteBillingInformationById(String instanceId);

    /**
     * 批量删除feike
     * 
     * @param instanceIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillingInformationByIds(String[] instanceIds);
}
