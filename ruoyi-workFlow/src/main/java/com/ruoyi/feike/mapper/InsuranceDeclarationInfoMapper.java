package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.InsuranceDeclarationInfo;
import com.ruoyi.feike.domain.ReportInsuranceThree;
import com.ruoyi.feike.domain.dto.DeclaredInformationDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface InsuranceDeclarationInfoMapper
{

    public List<InsuranceDeclarationInfo> selectInsuranceDeclarationInfoList(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public List<InsuranceDeclarationInfo> selectDepositDeclarationInfoList(InsuranceDeclarationInfo insuranceDeclarationInfo);


    public int insertInsuranceDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public int insertDepositDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public int updateInsuranceDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo);


    List<InsuranceDeclarationInfo> getInsuranceDeclarationInfoByInstanceId(@Param("instanceId") String instanceId);

    InsuranceDeclarationInfo selectInsuranceDeclarationInfoById(@Param("id") String id);


    List<DeclaredInformationDTO> getInstanceIdList(@Param("startDate") String startDate, @Param("endDate") String endDate);





}
