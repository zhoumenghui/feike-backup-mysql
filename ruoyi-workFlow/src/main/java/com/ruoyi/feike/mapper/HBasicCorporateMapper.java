package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.HBasicCorporate;
import com.ruoyi.feike.domain.HBasicCorporateGuarantee;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface HBasicCorporateMapper
{
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public HBasicCorporate selectHBasicCorporateById(Long id);

    public List<HBasicCorporate> selectCorporateByName(String dealername);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param hBasicCorporate 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HBasicCorporate> selectHBasicCorporateList(HBasicCorporate hBasicCorporate);


    public List<HBasicCorporate> selectHBasicCorporateListByName(HBasicCorporate hBasicCorporate);

    /**
     * 新增【请填写功能名称】
     *
     * @param hBasicCorporate 【请填写功能名称】
     * @return 结果
     */
    public int insertHBasicCorporate(HBasicCorporate hBasicCorporate);

    public int insertCorporate(HBasicCorporate hBasicCorporate);

    /**
     * 修改【请填写功能名称】
     *
     * @param hBasicCorporate 【请填写功能名称】
     * @return 结果
     */
    public int updateHBasicCorporate(HBasicCorporate hBasicCorporate);

    public int updateCorporate(HBasicCorporate hBasicCorporate);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteHBasicCorporateById(Long id);

    public int deleteHBasicCorporateByInstanceId(String instanceId);

    public int deleteHBasicCorporateByInstanceIdAndName(@Param("instanceId") String instanceId , @Param("dealername") String dealername);

    public int deleteCorporateByName(String dealername);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHBasicCorporateByIds(Long[] ids);
}