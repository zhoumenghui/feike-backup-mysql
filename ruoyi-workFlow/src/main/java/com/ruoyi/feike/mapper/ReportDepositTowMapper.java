package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.ReportDepositTow;

import java.util.List;


public interface ReportDepositTowMapper
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ReportDepositTow selectReportDepositTowById(String id);

    /**
     * 查询feike列表
     * 
     * @param reportDepositTow feike
     * @return feike集合
     */
    public List<ReportDepositTow> selectReportDepositTowList(ReportDepositTow reportDepositTow);

    /**
     * 新增feike
     * 
     * @param reportDepositTow feike
     * @return 结果
     */
    public int insertReportDepositTow(ReportDepositTow reportDepositTow);

    /**
     * 修改feike
     * 
     * @param reportDepositTow feike
     * @return 结果
     */
    public int updateReportDepositTow(ReportDepositTow reportDepositTow);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteReportDepositTowById(String id);


}
