package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * feikeMapper接口
 *
 * @author ruoyi
 * @date 2022-07-07
 */
public interface AnnualReviewyMapper
{

    /**
     * 通过instanceId获取title申请类型
     * @param instanceId
     * @return
     */
    public String selectByInstanceId(@Param("instanceId") String instanceId);

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public AnnualReviewy selectAnnualReviewyById(String id);

    /**
     * 查询feike列表
     *
     * @param annualReviewy feike
     * @return feike集合
     */
    public List<AnnualReviewy> selectAnnualReviewyList(AnnualReviewy annualReviewy);


    public List<AnnualReviewy> selectAnnualReviewyListByDate(AnnualReviewy annualReviewy);

    public List<AnnualReviewy> selectAnnualReviewyListByInstanceIds(Collection<String> instanceIds);



    /**
     * 新增feike
     *
     * @param annualReviewy feike
     * @return 结果
     */
    public int insertAnnualReviewy(AnnualReviewy annualReviewy);

    /**
     * 修改feike
     *
     * @param annualReviewy feike
     * @return 结果
     */
    public int updateAnnualReviewy(AnnualReviewy annualReviewy);
    public int updateAnnualReviewyByInstanceId(AnnualReviewy annualReviewy);


    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteAnnualReviewyById(String id);


    public int deleteProposalById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAnnualReviewyByIds(String[] ids);

    AnnualReviewy selectAnnualReviewyByInstanceId(String instanceId);

    List<AnnualReviewy> selectAnnualReviewyByInstanceIdList(String instanceId);

    AnnualReviewy selectAnnualReviewyByInstanceId1(String instanceId);

    List<AnnualReviewy> selectAnnualReviewyListByWorkflowLeaveAndDeptId(@Param("annualReviewy") AnnualReviewy annualReviewy,@Param("deptId") Long deptId);

    List<AnnualReviewy> selectAnnualReviewyByIds(String[] ids);

    int selectCountByCreatenameAndStatus(@Param("username")String username,@Param("status")String status);

    List<AnnualReviewy> selectAnnualReviewyByInstanceIds(List<String> instanceIds);

	int closeUpdateAnnualReviewyObj(@Param("instanceId")String instanceId,@Param("lockname")String lockname);

    int openUpdateAnnualReviewyObj(String lockname);

    List<AnnualReviewyHistoryVO> selectAnnualReviewyListBySelf(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("basicInformation")BasicInformation basicInformation, @Param("username")String username);

    List<AnnualReviewyHistoryVO> selectAnnualReviewyListBySelf1(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("basicInformation")BasicInformation basicInformation, @Param("username")String username);

    List<AnnualReviewyHistoryVO> selectAnnualReviewyListBySelf2(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("basicInformation")BasicInformation basicInformation, @Param("username")String username);

    List<AnnualReviewyHistoryVO> selectAnnualReviewyListBySelf3(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("basicInformation")BasicInformation basicInformation, @Param("username")String username);


    List<Map<String,Object>> getForeignDsoList(DepositDecreaseDTO depositDecreaseDTO);

    List<Map<String,Object>> getForeignAingList(DepositDecreaseDTO depositDecreaseDTO);

    List<Map<String,Object>> salesPerformanceAndTarget(SalesPerformanceAndTargetDTO dto);

    List<Map<String,Object>> performanceRecentThreemonth(DepositDecreaseDTO depositDecreaseDTO);
    List<Map<String,Object>> salesPerformanceAndTargetSellOut(SalesPerformanceAndTargetDTO dto);

    List<Map<String,Object>> salesPerformanceAndTargetSellIn(SalesPerformanceAndTargetDTO dto);

    List<Map<String, Object>> wholesalePerformanceRecentMonthsOs(WholesalePerformanceRecentMonthsOsDTO dto);



    List<Map<String, Object>> loyaltyPerformanceRecentQuarters(LoyaltyPerformanceRecentDTO dto);

    List<LoyaltyPerformance6month> loyaltyPerformanceRecentMonths(LoyaltyPerformanceRecentDTO dto);

    List<AnnualReviewy> listMineDo(@Param("userName")String userName,@Param("state") Integer status);

    List<LoyaltyPerformance6month> loyaltyPerformanceRecentMonthsRePenContract(LoyaltyPerformanceRecentDTO dto);

    List<String> getWorkName();

    List<String> getWorkflowFormData(@Param("instanceId")String instanceId);

    /**
     * 获取流程九已完成流程
     */
    List<AnnualReviewy> listWFSDo(AnnualReviewy annualReviewy);

    List<AnnualReviewy> listDmInstanceId(@Param("dm")String dm);
}
