package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.LoyaltyPerformance6month;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-08-24
 */
public interface LoyaltyPerformance6monthMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public LoyaltyPerformance6month selectLoyaltyPerformance6monthById(String id);

    /**
     * 查询feike列表
     *
     * @param loyaltyPerformance6month feike
     * @return feike集合
     */
    public List<LoyaltyPerformance6month> selectLoyaltyPerformance6monthList(LoyaltyPerformance6month loyaltyPerformance6month);

    /**
     * 新增feike
     *
     * @param loyaltyPerformance6month feike
     * @return 结果
     */
    public int insertLoyaltyPerformance6month(LoyaltyPerformance6month loyaltyPerformance6month);

    /**
     * 修改feike
     *
     * @param loyaltyPerformance6month feike
     * @return 结果
     */
    public int updateLoyaltyPerformance6month(LoyaltyPerformance6month loyaltyPerformance6month);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteLoyaltyPerformance6monthById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLoyaltyPerformance6monthByIds(String[] ids);

    List<LoyaltyPerformance6month> selectLoyaltyPerformance6monthByInstanceId(String instanceId);

    List<String> selectLoyaltyPerformance6monthHeardersByInstanceId(String instanceId);
}
