package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.SectorInformation;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-08-17
 */
public interface SectorInformationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public SectorInformation selectSectorInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param sectorInformation feike
     * @return feike集合
     */
    public List<SectorInformation> selectSectorInformationList(SectorInformation sectorInformation);

    /**
     * 新增feike
     * 
     * @param sectorInformation feike
     * @return 结果
     */
    public int insertSectorInformation(SectorInformation sectorInformation);

    /**
     * 修改feike
     * 
     * @param sectorInformation feike
     * @return 结果
     */
    public int updateSectorInformation(SectorInformation sectorInformation);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteSectorInformationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSectorInformationByIds(String[] ids);
}
