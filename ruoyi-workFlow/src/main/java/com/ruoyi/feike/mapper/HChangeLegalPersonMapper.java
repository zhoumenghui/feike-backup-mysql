package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.HChangeLegalPerson;


import java.util.List;


public interface HChangeLegalPersonMapper
{
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public HChangeLegalPerson selectHChangeLegalPersonById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param hChangeLegalPerson 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HChangeLegalPerson> selectHChangeLegalPersonList(HChangeLegalPerson hChangeLegalPerson);

    /**
     * 新增【请填写功能名称】
     *
     * @param hChangeLegalPerson 【请填写功能名称】
     * @return 结果
     */
    public int insertHChangeLegalPerson(HChangeLegalPerson hChangeLegalPerson);

    /**
     * 修改【请填写功能名称】
     *
     * @param hChangeLegalPerson 【请填写功能名称】
     * @return 结果
     */
    public int updateHChangeLegalPerson(HChangeLegalPerson hChangeLegalPerson);

}
