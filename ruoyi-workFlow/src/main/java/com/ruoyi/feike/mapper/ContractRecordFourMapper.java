package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.vo.ContractRecordFour;
import com.ruoyi.feike.domain.vo.ContractRecordTow;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ContractRecordFourMapper {


    public List<ContractRecordFour> selectContractRecordFourList(ContractRecordFour contractRecordFour);

    public int insertContractRecordFour(ContractRecordFour contractRecordFour);

}
