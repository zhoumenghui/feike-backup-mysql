package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.DealercodeContract;
import com.ruoyi.feike.domain.ProposalByCommericalAndMarketing;

/**
 * 生成的合同详细信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
public interface DealercodeContractMapper 
{
    /**
     * 查询生成的合同详细信息
     * 
     * @param id 生成的合同详细信息ID
     * @return 生成的合同详细信息
     */
    public DealercodeContract selectDealercodeContractById(Long id);

    /**
     * 查询生成的合同详细信息列表
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 生成的合同详细信息集合
     */
    public List<DealercodeContract> selectDealercodeContractList(DealercodeContract dealercodeContract);


    public List<DealercodeContract> selectDealercodeContractByCancel(DealercodeContract dealercodeContract);

    /**
     * 根据instanceId查询合同详细信息列表
     * @param instanceId
     * @return
     */
    public List<DealercodeContract> getAllByInstanceId(String instanceId);

    /**
     * 新增生成的合同详细信息
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 结果
     */
    public int insertDealercodeContract(DealercodeContract dealercodeContract);

    /**
     * 修改生成的合同详细信息
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 结果
     */
    public int updateDealercodeContract(DealercodeContract dealercodeContract);

    /**
     * 删除生成的合同详细信息
     * 
     * @param id 生成的合同详细信息ID
     * @return 结果
     */
    public int deleteDealercodeContractById(Long id);

    /**
     * 批量删除生成的合同详细信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealercodeContractByIds(Long[] ids);

    /**
     * 根据dealer(经销商)和sector(品牌)为一组进行分组，去除重复的字段
     *
     * @param dealercodeContract feike
     * @return feike集合
     */
    public List<DealercodeContract> dealercodeContractListgroupby(DealercodeContract dealercodeContract);

    /**
     * 多个合同转pdf并合并的时候要用
     * 根据dealer(经销商)和sector(品牌)和limittype(授信类型)为一组进行分组，去除重复的字段
     *
     * @param dealercodeContract feike
     * @return feike集合
     */
    public List<DealercodeContract> dealercodeContractListgroupbypdf(DealercodeContract dealercodeContract);

    public List<DealercodeContract> dealercodeContractListgroupbypdf1(DealercodeContract dealercodeContract);

    public List<DealercodeContract> dealercodeContractListgroupbypdf2(DealercodeContract dealercodeContract);

    public List<DealercodeContract> dealercodeContractListgroupbypdf3(DealercodeContract dealercodeContract);

    /**
     * 查询生成的合同合同地址不为null的详细信息列表
     *
     * @param dealercodeContract 生成的合同详细信息
     * @return 生成的合同详细信息集合
     */
    public List<DealercodeContract> selectDealercodeContractListnotnull(DealercodeContract dealercodeContract);

    /**
     * 根据ID进行倒序查最新的一条对应几率
     * @param dealercodeContract
     * @return
     */
    public List<DealercodeContract> selectDealercodeContractListOrderById(DealercodeContract dealercodeContract);
}
