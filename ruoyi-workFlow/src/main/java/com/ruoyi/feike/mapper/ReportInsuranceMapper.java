package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.ReportInsurance;
import org.apache.ibatis.annotations.Param;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
public interface ReportInsuranceMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ReportInsurance selectReportInsuranceById(String id);

    /**
     * 查询feike列表
     * 
     * @param reportInsurance feike
     * @return feike集合
     */
    public List<ReportInsurance> selectReportInsuranceList(ReportInsurance reportInsurance);

    /**
     * 新增feike
     * 
     * @param reportInsurance feike
     * @return 结果
     */
    public int insertReportInsurance(ReportInsurance reportInsurance);

    /**
     * 修改feike
     * 
     * @param reportInsurance feike
     * @return 结果
     */
    public int updateReportInsurance(ReportInsurance reportInsurance);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteReportInsuranceById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteReportInsuranceByIds(String[] ids);

    List<ReportInsurance> selectReportInsuranceExcelList(@Param("type") String type);
}
