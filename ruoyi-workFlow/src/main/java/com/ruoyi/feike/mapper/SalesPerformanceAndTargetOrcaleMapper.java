package com.ruoyi.feike.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.feike.domain.vo.SalesPerformanceAndTargetOrcaleVO;
import com.ruoyi.feike.domain.vo.WholesalePerformanceRecentVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface SalesPerformanceAndTargetOrcaleMapper
{

    /**
     * 查询feike列表
     * 
     * @param dealerName
     * @return feike集合
     */
    @DataSource(value = DataSourceType.SLAVE)
    public List<Map<String,Object>> selectSalesPerformanceAndTargetOrcaleListLs(SalesPerformanceAndTargetOrcaleVO dealerName);

    /**
     * 查询feike列表
     *
     * @param dealerName
     * @return feike集合
     */
    @DataSource(value = DataSourceType.SLAVE)
    public List<Map<String,Object>> selectSalesPerformanceAndTargetOrcaleListPs(SalesPerformanceAndTargetOrcaleVO  dealerName);


}
