package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.LoyaltyPerformance4q;
import com.ruoyi.feike.domain.LoyaltyPerformance6month;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author lss
 * @version 1.0
 * @description: TODO
 * @date 2022/8/28 16:59
 */
@Mapper
public interface RefreshMapper {
    /**
     * recent6month数据更新接口
     * @param loyaltyPerformance6month
     */
    void updateRecent6Month(LoyaltyPerformance6month loyaltyPerformance6month);

    /**
     * FOR RECENT 4 QUARTERS数据更新接口
     * @param loyaltyPerformance4q
     */
    void updateLoyaltyPerformance4q(LoyaltyPerformance4q loyaltyPerformance4q);

    /**
     * PERFORMANCE-RECENT 3 MONTHS OS数据更新接口
     * @param paramMap
     */
    void updateThreeMonths(Map<String,String> paramMap);

    /**
     * SALES PERFORMANCE AND TARGET (DATA FROM OEM)获取数据更新接口
      * @param paramMap
     */
    void updatePerformanceAndTarget(Map<String,Object> paramMap);

    void updateLimitCaculationForCommercialNeeds(Map<String,Object> paramMap);

}
