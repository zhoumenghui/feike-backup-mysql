package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.ActWorkflowLock;
import org.apache.ibatis.annotations.Mapper;

/**
 * feikeMapper接口
 *
 * @author ruoyi
 * @date 2022-07-07
 */
@Mapper
public interface ActWorkflowLockMapper
{


    public int insertActWorkflowLock(ActWorkflowLock actWorkflowLock);


    public int deleteActWorkflowLockById(String id);


}
