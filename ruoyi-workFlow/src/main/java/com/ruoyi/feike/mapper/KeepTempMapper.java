package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.vo.KeepVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface KeepTempMapper {

    public int insertKeeps(@Param("list")List<KeepVO> keep);

    public List<Map<String,Object>> selectKeep();

    public List<KeepVO> selectKeep1();

    public int deleteKeep();
}
