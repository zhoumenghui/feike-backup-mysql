package com.ruoyi.feike.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.feike.domain.LoyaltyPerformance4q;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OracleMapper {

	@DataSource(value = DataSourceType.SLAVE)
	public List<LoyaltyPerformance4q> getLoyaltyPerformance4Q(@Param("dealerNames") List<String> dealerNames, @Param("currentYear") String currentYear, @Param("lastYear") String lastYear, @Param("thisYearQ") List<String> thisYearQ, @Param("lastYearQ") List<String> lastYearQ);
	@DataSource(value = DataSourceType.SLAVE)
	public List<LoyaltyPerformance4q> getLoyaltyPerformance6Month(@Param("dealerNames") List<String> dealerNames, @Param("currentYear") String currentYear, @Param("lastYear") String lastYear, @Param("thisYearQ") List<String> thisYearM, @Param("lastYearQ") List<String> lastYearM);
}
