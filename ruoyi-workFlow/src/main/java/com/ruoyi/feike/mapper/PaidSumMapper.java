package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.PaidSum;
import com.ruoyi.feike.domain.ReportInsurance;
import org.apache.ibatis.annotations.Param;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface PaidSumMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public PaidSum selectPaidSumById(String id);

    /**
     * 查询feike列表
     * 
     * @param paidSum feike
     * @return feike集合
     */
    public List<PaidSum> selectPaidSumList(PaidSum paidSum);

    /**
     * 新增feike
     * 
     * @param paidSum feike
     * @return 结果
     */
    public int insertPaidSum(PaidSum paidSum);

    /**
     * 修改feike
     * 
     * @param paidSum feike
     * @return 结果
     */
    public int updatePaidSum(PaidSum paidSum);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deletePaidSumById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePaidSumByIds(String[] ids);

    List<ReportInsurance> selectInformationByInstanceId(@Param("instanceId") String instanceId);
}
