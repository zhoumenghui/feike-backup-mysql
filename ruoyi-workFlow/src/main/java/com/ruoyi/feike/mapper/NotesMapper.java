package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.Notes;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author lsn
 * @date 2022-07-06
 */
public interface NotesMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public Notes selectNotesById(String id);

    /**
     * 查询feike列表
     * 
     * @param notes feike
     * @return feike集合
     */
    public List<Notes> selectNotesList(Notes notes);

    /**
     * 新增feike
     * 
     * @param notes feike
     * @return 结果
     */
    public int insertNotes(Notes notes);

    /**
     * 修改feike
     * 
     * @param notes feike
     * @return 结果
     */
    public int updateNotes(Notes notes);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteNotesById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNotesByIds(String[] ids);

    Notes selectNotesByInstanceId(String instanceId);
}
