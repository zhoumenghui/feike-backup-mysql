package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.HExpiryDateHis;

import java.util.List;


/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2023-02-07
 */
public interface HExpiryDateHisMapper {
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public HExpiryDateHis selectHExpiryDateHisById(Long id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param hExpiryDateHis 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HExpiryDateHis> selectHExpiryDateHisList(HExpiryDateHis hExpiryDateHis);

    /**
     * 新增【请填写功能名称】
     *
     * @param hExpiryDateHis 【请填写功能名称】
     * @return 结果
     */
    public int insertHExpiryDateHis(HExpiryDateHis hExpiryDateHis);

    /**
     * 修改【请填写功能名称】
     *
     * @param hExpiryDateHis 【请填写功能名称】
     * @return 结果
     */
    public int updateHExpiryDateHis(HExpiryDateHis hExpiryDateHis);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteHExpiryDateHisById(Long id);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHExpiryDateHisByIds(Long[] ids);
}
