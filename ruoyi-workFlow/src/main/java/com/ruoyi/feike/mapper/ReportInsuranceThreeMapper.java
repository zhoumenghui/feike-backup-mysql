package com.ruoyi.feike.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.ReportInsuranceThree;
import org.apache.ibatis.annotations.Param;

/**
 * 报表h_report-insurance-3Mapper接口
 *
 * @author ruoyi
 * @date 2022-12-27
 */
public interface ReportInsuranceThreeMapper {
    /**
     * 查询报表h_report-insurance-3
     *
     * @param id 报表h_report-insurance-3ID
     * @return 报表h_report-insurance-3
     */
    public ReportInsuranceThree selectReportInsuranceThreeById(Long id);

    /**
     * 查询报表h_report-insurance-3列表
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 报表h_report-insurance-3集合
     */
    public List<ReportInsuranceThree> selectReportInsuranceThreeList(ReportInsuranceThree reportInsuranceThree);

    /**
     * 新增报表h_report-insurance-3
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 结果
     */
    public int insertReportInsuranceThree(ReportInsuranceThree reportInsuranceThree);

    /**
     * 修改报表h_report-insurance-3
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 结果
     */
    public int updateReportInsuranceThree(ReportInsuranceThree reportInsuranceThree);

    /**
     * 删除报表h_report-insurance-3
     *
     * @param id 报表h_report-insurance-3ID
     * @return 结果
     */
    public int deleteReportInsuranceThreeById(Long id);

    /**
     * 批量删除报表h_report-insurance-3
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteReportInsuranceThreeByIds(Long[] ids);

    List<Map<String, Object>> getInstanceIdList2(@Param("startDate") String startDate, @Param("endDate") String endDate);

    List<Map<String, Object>> getInstanceIdList3(@Param("startDate") String startDate, @Param("endDate") String endDate);

    List<Map<String, Object>> getInfoDeclaration(@Param("dealerName") String dealerName);

    List<Map<String, Object>> getInfoInformation(@Param("dealerName") String dealerName);

    List<Map<String, Object>> getInfoAddress(@Param("dealerName") String dealerName);


    List<Map<String, Object>> getInfoDeclarationHis(@Param("instanceId") String instanceId);

    List<Map<String, Object>> getInfoInformationHis(@Param("instanceId") String instanceId);

    List<Map<String, Object>> getInfoAddressHis(@Param("instanceId") String instanceId);

    List<Map<String,Object>>  getDealerInsuranceByDealerName(@Param("dealerName") String dealerName);
}
