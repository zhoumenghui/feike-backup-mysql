package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.CbFleetDetails;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface CbFleetDetailsMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public CbFleetDetails selectCbFleetDetailsById(String id);

    /**
     * 查询feike列表
     * 
     * @param cbFleetDetails feike
     * @return feike集合
     */
    public List<CbFleetDetails> selectCbFleetDetailsList(CbFleetDetails cbFleetDetails);

    /**
     * 新增feike
     * 
     * @param cbFleetDetails feike
     * @return 结果
     */
    public int insertCbFleetDetails(CbFleetDetails cbFleetDetails);

    /**
     * 修改feike
     * 
     * @param cbFleetDetails feike
     * @return 结果
     */
    public int updateCbFleetDetails(CbFleetDetails cbFleetDetails);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteCbFleetDetailsById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCbFleetDetailsByIds(String[] ids);
}
