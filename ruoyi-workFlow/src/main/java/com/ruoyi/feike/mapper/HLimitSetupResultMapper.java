package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.HLimitSetupResult;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
public interface HLimitSetupResultMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public HLimitSetupResult selectHLimitSetupResultById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hLimitSetupResult 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HLimitSetupResult> selectHLimitSetupResultList(HLimitSetupResult hLimitSetupResult);

    /**
     * 新增【请填写功能名称】
     * 
     * @param hLimitSetupResult 【请填写功能名称】
     * @return 结果
     */
    public int insertHLimitSetupResult(HLimitSetupResult hLimitSetupResult);

    /**
     * 修改【请填写功能名称】
     * 
     * @param hLimitSetupResult 【请填写功能名称】
     * @return 结果
     */
    public int updateHLimitSetupResult(HLimitSetupResult hLimitSetupResult);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteHLimitSetupResultById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHLimitSetupResultByIds(Long[] ids);
}
