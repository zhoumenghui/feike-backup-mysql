package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.HNewSumKeepModify;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2023-01-11
 */
public interface HNewSumKeepModifyMapper
{
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public HNewSumKeepModify selectHNewSumKeepModifyById(String id);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param hNewSumKeepModify 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HNewSumKeepModify> selectHNewSumKeepModifyList(HNewSumKeepModify hNewSumKeepModify);

    /**
     * 新增【请填写功能名称】
     *
     * @param hNewSumKeepModify 【请填写功能名称】
     * @return 结果
     */
    public int insertHNewSumKeepModify(HNewSumKeepModify hNewSumKeepModify);

    /**
     * 修改【请填写功能名称】
     *
     * @param hNewSumKeepModify 【请填写功能名称】
     * @return 结果
     */
    public int updateHNewSumKeepModify(HNewSumKeepModify hNewSumKeepModify);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteHNewSumKeepModifyById(String id);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHNewSumKeepModifyByIds(String[] ids);

    List<HNewSumKeepModify> selectHNewSumKeepModifyListByInstanceId(String instanceId);

}
