package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.SalesPerformanceAndTarget;

import java.util.List;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-07-11
 */
public interface SalesPerformanceAndTargetMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public SalesPerformanceAndTarget selectSalesPerformanceAndTargetById(String id);

    /**
     * 查询feike列表
     *
     * @param salesPerformanceAndTarget feike
     * @return feike集合
     */
    public List<SalesPerformanceAndTarget> selectSalesPerformanceAndTargetList(SalesPerformanceAndTarget salesPerformanceAndTarget);

    /**
     * 新增feike
     *
     * @param salesPerformanceAndTarget feike
     * @return 结果
     */
    public int insertSalesPerformanceAndTarget(SalesPerformanceAndTarget salesPerformanceAndTarget);

    /**
     * 修改feike
     *
     * @param salesPerformanceAndTarget feike
     * @return 结果
     */
    public int updateSalesPerformanceAndTarget(SalesPerformanceAndTarget salesPerformanceAndTarget);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteSalesPerformanceAndTargetById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSalesPerformanceAndTargetByIds(String[] ids);

    List<SalesPerformanceAndTarget> selectSalesPerformanceAndTargetByInstanceId(String instanceId);

    int deleteSalesPerformanceAndTargetByInstanceIds(String[] ids);

    int deleteSalesPerformanceAndTargetByInstanceId(String instanceId);
}
