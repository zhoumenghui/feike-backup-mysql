package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.ReportDeposit;
import org.apache.ibatis.annotations.Param;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
public interface ReportDepositMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ReportDeposit selectReportDepositById(String id);

    /**
     * 查询feike列表
     * 
     * @param reportDeposit feike
     * @return feike集合
     */
    public List<ReportDeposit> selectReportDepositList(ReportDeposit reportDeposit);

    /**
     * 新增feike
     * 
     * @param reportDeposit feike
     * @return 结果
     */
    public int insertReportDeposit(ReportDeposit reportDeposit);

    /**
     * 修改feike
     * 
     * @param reportDeposit feike
     * @return 结果
     */
    public int updateReportDeposit(ReportDeposit reportDeposit);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteReportDepositById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteReportDepositByIds(String[] ids);

    /**
     * 更具instanceId修改值
     * @param reportDeposit
     */
    void updateReportDepositByInstanceId(ReportDeposit reportDeposit);

    List<ReportDeposit> getMsgByInstanceId(@Param("instanceId") String instanceId);

    void deleteByInstanceId(@Param("instanceId") String instanceId);
}
