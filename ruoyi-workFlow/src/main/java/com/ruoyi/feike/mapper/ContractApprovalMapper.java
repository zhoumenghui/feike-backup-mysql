package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.ContractApproval;
import org.apache.ibatis.annotations.Param;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-09-06
 */
public interface ContractApprovalMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ContractApproval selectContractApprovalById(String id);

    /**
     * 查询feike列表
     * 
     * @param contractApproval feike
     * @return feike集合
     */
    public List<ContractApproval> selectContractApprovalList(ContractApproval contractApproval);

    /**
     * 新增feike
     * 
     * @param contractApproval feike
     * @return 结果
     */
    public int insertContractApproval(ContractApproval contractApproval);

    /**
     * 修改feike
     * 
     * @param contractApproval feike
     * @return 结果
     */
    public int updateContractApproval(ContractApproval contractApproval);

    /**
     * 根据instanceId 修改feike
     *
     * @param contractApproval feike
     * @return 结果
     */
    int updateContractApprovalByInstanceId(ContractApproval contractApproval);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteContractApprovalById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteContractApprovalByIds(String[] ids);

    /**
     *
     * @param instanceId
     * @return
     */
    ContractApproval getInfoByInstanceId(@Param("instanceId") String instanceId);
}
