package com.ruoyi.feike.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.DealerSectorContractgroup;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 合同Mapper接口
 *
 * @author ruoyi
 * @date 2022-07-25
 */
@Repository
public interface DealerSectorContractgroupMapper
{
    /**
     * 查询合同
     *
     * @param id 合同ID
     * @return 合同
     */
    public DealerSectorContractgroup selectDealerSectorContractgroupById(String id);

    /**
     * 查询合同列表
     *
     * @param dealerSectorContractgroup 合同
     * @return 合同集合
     */
    public List<DealerSectorContractgroup> selectDealerSectorContractgroupList(DealerSectorContractgroup dealerSectorContractgroup);


    /**
     * 根据dealerName sector contractname 查询
     * @param contractGroup
     * @return
     */
    List<DealerSectorContractgroup>  getContractGroup(DealerSectorContractgroup contractGroup);

    /**
     * 获取合同号 最大次数记录
     * @param contractGroup
     * @return
     */
    DealerSectorContractgroup getMaxContractNumber(DealerSectorContractgroup contractGroup);

    /**
     * 新增合同
     *
     * @param dealerSectorContractgroup 合同
     * @return 结果
     */
    public int insertDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 修改合同
     *
     * @param dealerSectorContractgroup 合同
     * @return 结果
     */
    public int updateDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 修改合同
     *
     * @param dealerSectorContractGroup 合同
     * @return 结果
     */
    public int updateContractGroup(DealerSectorContractgroup dealerSectorContractGroup);

    /**
     * 删除合同
     *
     * @param id 合同ID
     * @return 结果
     */
    public int deleteDealerSectorContractgroupById(String id);

    /**
     * 批量删除合同
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerSectorContractgroupByIds(String[] ids);

    /**
     * 根据dealername,sector,contractname删除合同
     */
    public int deleteDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 根据dealername,sector,contractname修改合同
     */
    public int updateDealerSectorContractgroupWhere(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 查出主合同编号最大年份那一类数据的最大编号值
     */
    public String selectMaxNumber(DealerSectorContractgroup dealerSectorContractgroup);

    public String selectNumberByDealerCode(@Param("dealerCode") String dealerCode);

    public String selectMaxNumberByDealerCode();

    public int insertDealerSectorContractMax(Map map);

    public int updateDealerSectorContract(Map map);

    /**
     * 查出主合同数据里的最大年份
     */
    public String selectMaxNian(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 查出对应合同取他的第几次出合同序列号，并+1
     */
    public String selectMaxSerialNumber(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 根据经销商名称修改经销商名称
     */
    public int updateDealerSectorContractgroupWhereDealername(Map map);

    /**
     * 查询根据dealercode分组后的数据
     *
     * @param dealerSectorContractgroup 合同
     * @return 合同集合
     */
    public List<DealerSectorContractgroup> selectDealerSectorContractgroupListgroupbyDealercode(DealerSectorContractgroup dealerSectorContractgroup);
}
