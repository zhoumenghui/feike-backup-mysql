package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.HAml;
import com.ruoyi.feike.domain.HBasicCorporate;
import com.ruoyi.feike.domain.HBasicCorporateGuarantee;
import com.ruoyi.feike.domain.vo.KeepVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface HAmlMapper
{


    public HAml selectHAmlById(Long id);
    public List<HAml> selectHAmlList(HAml hAml);


    public List<HAml> selectHAmlInfoList(@Param("amlName") String amlName);

    public String getAmlName(@Param("amlName") String amlName);

    public int insertHAmlInfo(@Param("list")List<HAml> aml);


    public int insertAmlHis(HAml aml);

    public int deleteAml();


    public int updateHAml(HAml hAml);


    public int deleteHAmlById(Long id);


}