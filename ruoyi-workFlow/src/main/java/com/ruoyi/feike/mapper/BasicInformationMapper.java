package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.BasicInformationDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-07-05
 */
@Repository
public interface BasicInformationMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public BasicInformation selectBasicInformationById(String id);

    public List<BasicInformation> selectDealerGroupByByInstanceId(String instanceId);

    public List<BasicInformation> selectDealerGroupByDealerName(String instanceId);


    public List<BasicInformation> selectDealerGroupByByInstanceIds(Collection<String> instanceIds);


    /**
     * 查询feike列表
     *
     * @param basicInformation feike
     * @return feike集合
     */
    public List<BasicInformation> selectBasicInformationList(BasicInformation basicInformation);

    /**
     * 新增feike
     *
     * @param basicInformation feike
     * @return 结果
     */
    public int insertBasicInformation(BasicInformation basicInformation);
    public int insertBasicInformationByDto(BasicInformationDTO basicInformation);

    /**
     * 修改feike
     *
     * @param basicInformation feike
     * @return 结果
     */
    public int updateBasicInformation(BasicInformation basicInformation);

    public int updateBasicInformationAndDealerinfor(BasicInformation basicInformation);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteBasicInformationById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasicInformationByIds(String[] ids);

    int deleteBasicInformationByInstanceIds(String[] ids);

    int deleteBasicInformationByInstanceId(String instanceId);

    //查询对应经销商名称流程里是不是存在进行中的
    public int deleteBasicInformationAnnualDealername(String dealername);

    public int deleteBasicInformationAnnualDealername2(@Param("dealername") String dealername ,@Param("sector") String sector);
    public int deleteBasicInformationAnnualDealername3(@Param("dealername") String dealername ,@Param("sector") String sector);

    //查经销商名称正在进行中的流程，
    public List<BasicInformation> deleteBasicInformationAnnualDealernameList(String dealername);
    //查经销商名称正在进行中的流程和草稿，
    public List<BasicInformation> deleteBasicInformationAnnualDealernameListAndcaogao(String dealername);
    //查集团名称正在进行中的流程，
    public List<BasicInformation> deleteBasicInformationAnnualGroupnameList(String groupname);
}
