package com.ruoyi.feike.mapper;

import java.util.List;

import com.ruoyi.feike.domain.ReportInsurance;
import com.ruoyi.feike.domain.SelfplanInformation;
import io.lettuce.core.dynamic.annotation.Param;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface SelfplanInformationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public SelfplanInformation selectSelfplanInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param selfplanInformation feike
     * @return feike集合
     */
    public List<SelfplanInformation> selectSelfplanInformationList(SelfplanInformation selfplanInformation);

    /**
     * 新增feike
     * 
     * @param selfplanInformation feike
     * @return 结果
     */
    public int insertSelfplanInformation(SelfplanInformation selfplanInformation);

    /**
     * 修改feike
     * 
     * @param selfplanInformation feike
     * @return 结果
     */
    public int updateSelfplanInformation(SelfplanInformation selfplanInformation);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteSelfplanInformationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSelfplanInformationByIds(String[] ids);

    /**
     * 根据instanceId查询数据
     * @param instanceId
     * @return
     */
    List<ReportInsurance> selectInformationByInstanceId(@Param("instanceId") String instanceId);
}
