package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.vo.ContractRecord;
import com.ruoyi.feike.domain.vo.ContractRecordThree;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ContractRecordMapper {

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public ContractRecord selectHContractRecordById(Long id);


    public ContractRecord selectContractRecordThreeListById(Long id);


    /**
     * 查询【请填写功能名称】列表
     *
     * @param contractRecord 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ContractRecord> selectContractRecordList(ContractRecord contractRecord);


    public List<ContractRecord> selectContractRecordList2(ContractRecord contractRecord);


    public List<ContractRecord> selectContractRecordList3(ContractRecord contractRecord);


    public List<ContractRecord> selectContractRecordThreeList(ContractRecord contractRecord);


    public List<ContractRecord> selectContractRecordListBylimit(ContractRecord contractRecord);

    public List<ContractRecord> selectContractRecordListInstanceidNull(ContractRecord contractRecord);

    public List<ContractRecordThree> selectContractRecordThreeListEffDateisNotNull(ContractRecordThree contractRecordThree);

    public List<ContractRecordThree> selectContractRecordThreeListEffDateisNotNull2(ContractRecordThree contractRecordThree);


    /**
     * 按签回日期存在，RC批复日期取最近一条
     * @param contractRecord
     * @return
     */
    public List<ContractRecord> selectContractRecordListRecentlyEffectiveDate(ContractRecord contractRecord);


    public List<ContractRecord> selectContractRecordListRecentlyEffectiveDate2(ContractRecord contractRecord);


    public List<ContractRecord> selectContractRecordListRecentlyEffectiveDateByPart(ContractRecord contractRecord);

    public List<ContractRecord> selectContractRecordListRecentlyEffectiveDate2ByPart(ContractRecord contractRecord);

    /**
     * 新增【请填写功能名称】
     *
     * @param contractRecord 【请填写功能名称】
     * @return 结果
     */
    public int insertContractRecord(ContractRecord contractRecord);

    public int insertContractRecordThree(ContractRecord contractRecord);


    /**
     * 修改【请填写功能名称】
     *
     * @param contractRecord 【请填写功能名称】
     * @return 结果
     */
    public int updateContractRecord(ContractRecord contractRecord);

    public int updateContractRecordThree(ContractRecord contractRecord);

    public int updateContractRecordThree2(ContractRecord contractRecord);


    public int updateContractRecord2(ContractRecord contractRecord);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteContractRecordByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteContractRecordById(Long id);

    public int batchInsert(List<ContractRecord> list);

    public int updateBatchById(List<ContractRecord> list);

    void updateOriginal(@Param("record") ContractRecord record);

}
