package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.HBasicCorporateGuarantee;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface HBasicCorporateGuaranteeMapper
{
    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public HBasicCorporateGuarantee selectHBasicCorporateGuaranteeById(Long id);

    public List<HBasicCorporateGuarantee> selectCorporateGuaranteeByName(String dealername);

    /**
     * 查询【请填写功能名称】列表
     *
     * @param hBasicCorporateGuarantee 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<HBasicCorporateGuarantee> selectHBasicCorporateGuaranteeList(HBasicCorporateGuarantee hBasicCorporateGuarantee);

    public List<HBasicCorporateGuarantee> selectHBasicCorporateGuaranteeListByName(HBasicCorporateGuarantee hBasicCorporateGuarantee);


    /**
     * 新增【请填写功能名称】
     *
     * @param hBasicCorporateGuarantee 【请填写功能名称】
     * @return 结果
     */
    public int insertHBasicCorporateGuarantee(HBasicCorporateGuarantee hBasicCorporateGuarantee);

    public int insertCorporateGuarantee(HBasicCorporateGuarantee hBasicCorporateGuarantee);

    /**
     * 修改【请填写功能名称】
     *
     * @param hBasicCorporateGuarantee 【请填写功能名称】
     * @return 结果
     */
    public int updateHBasicCorporateGuarantee(HBasicCorporateGuarantee hBasicCorporateGuarantee);

    public int updateCorporateGuarantee(HBasicCorporateGuarantee hBasicCorporateGuarantee);

    /**
     * 删除【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteHBasicCorporateGuaranteeById(Long id);

    public int deleteHBasicCorporateGuaranteeByInstanceId(String instanceId);

    public int deleteHBasicCorporateGuaranteeByInstanceIdAndName(@Param("instanceId") String instanceId , @Param("dealername") String dealername);

    public int deleteCorporateGuaranteeByName(String dealername);

    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHBasicCorporateGuaranteeByIds(Long[] ids);
}