package com.ruoyi.feike.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.NewSumKeep;
import com.ruoyi.feike.domain.NewSumKeepFour;
import org.apache.ibatis.annotations.Param;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-08-31
 */
public interface NewSumKeepMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public NewSumKeep selectNewSumKeepById(String id);

    /**
     * 查询feike列表
     *
     * @param newSumKeep feike
     * @return feike集合
     */
    public List<NewSumKeep> selectNewSumKeepList(NewSumKeep newSumKeep);


    public List<NewSumKeep> selectNewSumKeepHisList(NewSumKeep newSumKeep);

    /**
     * 查询feike列表
     *
     * @param newSumKeep feike
     * @return feike集合
     */
    public List<NewSumKeep> selectNewSumKeepList2(NewSumKeep newSumKeep);

    public List<NewSumKeep> selectNewSumKeepHisList2(NewSumKeep newSumKeep);

    public List<Map<String,Object>> selectDeclarationInfo(@Param("instanceId") String instanceId);

    public List<NewSumKeep> selectNewSumKeepList3(NewSumKeep newSumKeep);

    public List<NewSumKeep> selectNewSumKeepList4(NewSumKeep newSumKeep);

    public List<NewSumKeep> selectInsuranceHistory(NewSumKeep newSumKeep);

    public List<NewSumKeepFour> selectNewSumKeepListFour(NewSumKeep newSumKeep);

    public List<NewSumKeep> selectNewSumKeepList5(NewSumKeep newSumKeep);

    public List<NewSumKeep> selectNewSumKeepList6(NewSumKeep newSumKeep);

    /**
     * 新增feike
     *
     * @param newSumKeep feike
     * @return 结果
     */
    public int insertNewSumKeep(NewSumKeep newSumKeep);


    public int insertNewSumKeepHis(NewSumKeep newSumKeep);


    public int insertInsuranceHistory(NewSumKeep newSumKeep);


    public int deleteInsuranceHistoryById(String id);


    /**
     * 修改feike
     *
     * @param newSumKeep feike
     * @return 结果
     */
    public int updateNewSumKeep(NewSumKeep newSumKeep);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteNewSumKeepById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNewSumKeepByIds(String[] ids);

    List<NewSumKeep> newSumSelect(NewSumKeep newSumKeep);

    public Map<String,Object> getRate(@Param("sector") String sector);
}
