package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.Grade;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-08-22
 */
public interface GradeMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public Grade selectGradeById(String id);

    /**
     * 查询feike列表
     *
     * @param grade feike
     * @return feike集合
     */
    public List<Grade> selectGradeList(Grade grade);

    /**
     * 新增feike
     *
     * @param grade feike
     * @return 结果
     */
    public int insertGrade(Grade grade);

    /**
     * 修改feike
     *
     * @param grade feike
     * @return 结果
     */
    public int updateGrade(Grade grade);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteGradeById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGradeByIds(String[] ids);

    List<Grade> selectGradeByInstanceId(String instanceId);
}
