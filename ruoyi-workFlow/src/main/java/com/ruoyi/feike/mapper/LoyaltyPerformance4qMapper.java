package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.LoyaltyPerformance4q;

/**
 * feikeMapper接口
 *
 * @author zmh
 * @date 2022-08-19
 */
public interface LoyaltyPerformance4qMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public LoyaltyPerformance4q selectLoyaltyPerformance4qById(String id);

    /**
     * 查询feike列表
     *
     * @param loyaltyPerformance4q feike
     * @return feike集合
     */
    public List<LoyaltyPerformance4q> selectLoyaltyPerformance4qList(LoyaltyPerformance4q loyaltyPerformance4q);
    public List<LoyaltyPerformance4q> selectLoyaltyPerformance4qByInstanceId(String instanceId);

    /**
     * 新增feike
     *
     * @param loyaltyPerformance4q feike
     * @return 结果
     */
    public int insertLoyaltyPerformance4q(LoyaltyPerformance4q loyaltyPerformance4q);

    /**
     * 修改feike
     *
     * @param loyaltyPerformance4q feike
     * @return 结果
     */
    public int updateLoyaltyPerformance4q(LoyaltyPerformance4q loyaltyPerformance4q);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteLoyaltyPerformance4qById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLoyaltyPerformance4qByIds(String[] ids);
}
