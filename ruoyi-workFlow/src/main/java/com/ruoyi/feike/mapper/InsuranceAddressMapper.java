package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.InsuranceAddress;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface InsuranceAddressMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public InsuranceAddress selectInsuranceAddressById(String id);

    /**
     * 查询feike列表
     * 
     * @param insuranceAddress feike
     * @return feike集合
     */
    public List<InsuranceAddress> selectInsuranceAddressList(InsuranceAddress insuranceAddress);

    /**
     * 新增feike
     * 
     * @param insuranceAddress feike
     * @return 结果
     */
    public int insertInsuranceAddress(InsuranceAddress insuranceAddress);


    public int insertInsuranceAddressByInfo(InsuranceAddress insuranceAddress);

    public int insertInsuranceAddressHisByInfo(InsuranceAddress insuranceAddress);

    public int deleteInsuranceAddressByInfo(String name);

    public int deleteInsuranceAddressHisByInfo(String instanceId);


    /**
     * 修改feike
     * 
     * @param insuranceAddress feike
     * @return 结果
     */
    public int updateInsuranceAddress(InsuranceAddress insuranceAddress);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteInsuranceAddressById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteInsuranceAddressByIds(String[] ids);
}
