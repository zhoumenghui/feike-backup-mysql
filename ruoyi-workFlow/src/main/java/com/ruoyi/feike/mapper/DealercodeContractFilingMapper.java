package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.DealercodeContractFiling;
import org.apache.ibatis.annotations.Param;

/**
 * 合同归档表Mapper接口
 *
 * @author ruoyi
 * @date 2022-08-23
 */
public interface DealercodeContractFilingMapper
{
    /**
     * 查询合同归档表
     *
     * @param id 合同归档表ID
     * @return 合同归档表
     */
    public DealercodeContractFiling selectDealercodeContractFilingById(Long id);

    /**
     * 查询合同归档表列表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 合同归档表集合
     */
    public List<DealercodeContractFiling> selectDealercodeContractFilingList(DealercodeContractFiling dealercodeContractFiling);

    public List<DealercodeContractFiling> selectDealercodeContractFilingList2(DealercodeContractFiling dealercodeContractFiling);

    public List<DealercodeContractFiling> selectDealercodeContractFilingList3(DealercodeContractFiling dealercodeContractFiling);

    public List<DealercodeContractFiling> selectDealercodeContractFilingList4(DealercodeContractFiling dealercodeContractFiling);



    /**
     * 根据合同签回日期进行排序
     * @param dealercodeContractFiling
     * @return
     */
    public List<DealercodeContractFiling> selectDealercodeContractFilingListRecentlyEffectiveDate(DealercodeContractFiling dealercodeContractFiling);

    public List<DealercodeContractFiling> selectDealercodeContractFilingByMax(DealercodeContractFiling dealercodeContractFiling);

    /**
     * 新增合同归档表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 结果
     */
    public int insertDealercodeContractFiling(DealercodeContractFiling dealercodeContractFiling);

    /**
     * 修改合同归档表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 结果
     */
    public int updateDealercodeContractFiling(DealercodeContractFiling dealercodeContractFiling);

    public int updateDealercodeContractFiling2(DealercodeContractFiling dealercodeContractFiling);


    public int updateDealercodeContractFilingInstanceId(DealercodeContractFiling dealercodeContractFiling);

    /**
     * 删除合同归档表
     *
     * @param id 合同归档表ID
     * @return 结果
     */
    public int deleteDealercodeContractFilingById(Long id);

    /**
     * 批量删除合同归档表
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealercodeContractFilingByIds(Long[] ids);

    public List<DealercodeContractFiling> selectDealercodeContractFilingBySubProcessId(@Param("subProcessId") String instanceId);

}
