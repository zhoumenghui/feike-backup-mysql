package com.ruoyi.feike.mapper;


import com.ruoyi.feike.domain.HAml;
import com.ruoyi.feike.domain.HRpt;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface HRptMapper
{



    public List<HRpt> selectHRptInfoList(@Param("rptName") String rptName);


    public String getRptName(@Param("rptName") String rptName);


    public int insertHRptInfo(@Param("list")List<HRpt> rpt);


    public int deleteRpt();





}