package com.ruoyi.feike.instener;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.activiti.domain.ActWorkflowFormData;
import com.ruoyi.activiti.mapper.ActWorkflowFormDataMapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.vo.ContractRecord;
import com.ruoyi.feike.domain.vo.ContractRecordFour;
import com.ruoyi.feike.domain.vo.ContractRecordTow;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.*;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class StartDMEndStateListener implements ExecutionListener {

    @Autowired
    private ParkingLocationRegistrationMapper parkingLocationRegistrationMapper;

    @Autowired
    private ParkingLocationMapper parkingLocationMapper;

    @Autowired
    private ReportInsuranceMapper reportInsuranceMapper;

    @Autowired
    private ReportInsuranceThreeMapper reportInsuranceThreeMapper;


    // @Autowired
    // private RepositoryService repositoryService;

    private Expression state;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        AnnualReviewy annualReviewy = new AnnualReviewy();
        String instanceId = delegateExecution.getProcessInstanceBusinessKey();
        String processInstanceBusinessKey = delegateExecution.getProcessInstanceBusinessKey();
        String processDefinitionId = delegateExecution.getProcessDefinitionId();
        // String processInstanceId = delegateExecution.getProcessInstanceId();
        // ProcessInstance processInstance = SpringUtils.getBean(RuntimeService.class).createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        AnnualReviewyMapper annualReviewyMapper = SpringUtils.getBean(AnnualReviewyMapper.class);
        AnnualReviewy annualReviewy1 = annualReviewyMapper.selectAnnualReviewyByInstanceId1(instanceId);
        if(annualReviewy1 !=null){

            HAmlMapper amlMapper = SpringUtils.getBean(HAmlMapper.class);
            HAml hAml = new HAml();
            hAml.setInstanceId(instanceId);
            List<HAml> amls = amlMapper.selectHAmlList(hAml);
            if(amls!=null && amls.size()>0){
                //先删除标记
                for (HAml aml : amls) {
                    amlMapper.deleteHAmlById(aml.getId());
                }
                IAnnualReviewyService annualReviewyService = SpringUtils.getBean(IAnnualReviewyService.class);
                List<HAml> hAmlList = annualReviewyService.selectAmlInfoList(instanceId);
                for (HAml Aml : hAmlList) {
                    amlMapper.insertAmlHis(Aml);
                }
            }

            if(annualReviewy1.getType()!=null && annualReviewy1.getType().equals("contractApproval")){
                ContractApprovalMapper contractApprovalMapper = SpringUtils.getBean(ContractApprovalMapper.class);
                ContractApproval contractApproval = contractApprovalMapper.getInfoByInstanceId(instanceId);
                if(contractApproval!=null){
                    DealercodeContractFiling dealercodeContractFiling2 = new DealercodeContractFiling();
                    dealercodeContractFiling2.setSubProcessId(instanceId);
                    dealercodeContractFiling2.setInstanceId(instanceId);
                    DealercodeContractFilingMapper dealercodeContractFilingMapper = SpringUtils.getBean(DealercodeContractFilingMapper.class);
                    List<DealercodeContractFiling> dealercodeContractFilings = dealercodeContractFilingMapper.selectDealercodeContractFilingList(dealercodeContractFiling2);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    String res = simpleDateFormat.format(new Date());
                    if(dealercodeContractFilings!=null && dealercodeContractFilings.size()>0){
                        for (DealercodeContractFiling contractFiling : dealercodeContractFilings) {
                            contractFiling.setContractNameHistory(contractApproval.getSector()+"Demo"+contractApproval.getDealerName()+res);
                            dealercodeContractFilingMapper.updateDealercodeContractFiling(contractFiling);
                        }
                    }
                    ContractRecord contractRecord = new ContractRecord();
                    contractRecord.setInstanceid(instanceId);
                    contractRecord.setDealername(annualReviewy1.getDealerName());
                    contractRecord.setLimitType(annualReviewy1.getLimitType());
                    contractRecord.setSector(annualReviewy1.getSector() );
                    ContractRecordMapper contractRecordMapper = SpringUtils.getBean(ContractRecordMapper.class);
                    List<ContractRecord> contractRecords = contractRecordMapper.selectContractRecordList(contractRecord);
                    for (ContractRecord record : contractRecords) {
                        record.setEffectivedate(new Date());
                        contractRecordMapper.updateContractRecord(record);
                    }
                    List<ContractRecord> contractRecords1 = contractRecordMapper.selectContractRecordThreeList(contractRecord);
                    for (ContractRecord contractRecord1 : contractRecords1) {
                        contractRecord1.setEffectivedate(new Date());
                        contractRecordMapper.updateContractRecordThree(contractRecord1);
                    }
                }
            }
            if(annualReviewy1.getDealerName() !=null && annualReviewy1.getSector() !=null && annualReviewy1.getLimitType()!=null){
                String[] split = instanceId.split("-");
                ContractRecordMapper contractRecordMapper = SpringUtils.getBean(ContractRecordMapper.class);
                ContractRecord contractRecord = new ContractRecord();
                contractRecord.setInstanceid(split[0]);
                contractRecord.setDealername(annualReviewy1.getDealerName());
                contractRecord.setLimitType(annualReviewy1.getLimitType());
                contractRecord.setSector(annualReviewy1.getSector() );
                List<ContractRecord> contractRecords = contractRecordMapper.selectContractRecordList(contractRecord);
                DealerSectorContractgroup dealerSectorContractgroup = new DealerSectorContractgroup();
                dealerSectorContractgroup.setDealername(annualReviewy1.getDealerName());
                dealerSectorContractgroup.setSector(annualReviewy1.getSector());

                DealerSectorContractgroupMapper bean = SpringUtils.getBean(DealerSectorContractgroupMapper.class);

                String contractnumber = "";
                String note = null;
                dealerSectorContractgroup.setContractname("人民币循环贷款合同");
                List<DealerSectorContractgroup> dealerSectorContractgroups = bean.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                if(dealerSectorContractgroups!=null && dealerSectorContractgroups.size()>0){
                    String contractnumber1 = dealerSectorContractgroups.get(0).getContractnumber();
                    StringBuilder stringBuilder = new StringBuilder(contractnumber1);
                    if(annualReviewy1.getLimitType().equals("Demo")){
                        contractnumber = stringBuilder.replace(6,7,String.valueOf("DEMO")).toString();
                    } else if(annualReviewy1.getLimitType().equals("CB")){
                        contractnumber = stringBuilder.replace(6,7,String.valueOf("CB")).toString();
                    }else {
                        contractnumber = contractnumber1;
                    }
                }else{
                    if(annualReviewy1.getLimitType().equals("Demo")){
                        dealerSectorContractgroup.setContractname("试乘试驾车贷款合同");
                        List<DealerSectorContractgroup> dealerSectorContractgroups1 = bean.selectDealerSectorContractgroupList(dealerSectorContractgroup);
                        if(dealerSectorContractgroups1!=null && dealerSectorContractgroups1.size()>0){
                            String contractnumber1 = dealerSectorContractgroups1.get(0).getContractnumber();
                            contractnumber = contractnumber1;
                        }
                    }
                }

                for (ContractRecord record : contractRecords) {
                    record.setEffectivedate(new Date());
                    contractRecordMapper.updateContractRecord(record);
                    if(note == null ){
                        note  =  record.getScenario();
                    }
                }
                List<ContractRecord> contractRecords1 = contractRecordMapper.selectContractRecordThreeList(contractRecord);
                for (ContractRecord contractRecord1 : contractRecords1) {
                    contractRecord1.setEffectivedate(new Date());
                    contractRecordMapper.updateContractRecordThree(contractRecord1);
                }
                if(annualReviewy1.getLimitType().equals("Normal")){
                    contractRecord.setLimitType("part");
                    List<ContractRecord> contractRecords2 = contractRecordMapper.selectContractRecordThreeList(contractRecord);
                    for (ContractRecord contractRecord3 : contractRecords2) {
                        contractRecord3.setEffectivedate(new Date());
                        contractRecordMapper.updateContractRecordThree(contractRecord3);
                    }
                }

                DealercodeContractFilingMapper dealercodeContractFilingMapper = SpringUtils.getBean(DealercodeContractFilingMapper.class);
                DealercodeContractFiling dealercodeContractFiling = new DealercodeContractFiling();
                dealercodeContractFiling.setInstanceId(split[0]);
                dealercodeContractFiling.setDealerNameCN(annualReviewy1.getDealerName());
                dealercodeContractFiling.setLimitType(annualReviewy1.getLimitType());
                dealercodeContractFiling.setSector(annualReviewy1.getSector());
                List<DealercodeContractFiling> dealercodeContractFilings = dealercodeContractFilingMapper.selectDealercodeContractFilingList(dealercodeContractFiling);
                for(DealercodeContractFiling decodeCf : dealercodeContractFilings){
                    decodeCf.setEffectiveDate(new Date());
                    decodeCf.setContractNameHistory(annualReviewy1.getSector()+annualReviewy1.getLimitType()+annualReviewy1.getDealerName()+DateUtils.getDateyyyymmdd());
                    dealercodeContractFilingMapper.updateDealercodeContractFiling(decodeCf);
                    ContractRecordTowMapper contractRecordTowMapper = SpringUtils.getBean(ContractRecordTowMapper.class);
                    ContractRecordFourMapper contractRecordFourMapper = SpringUtils.getBean(ContractRecordFourMapper.class);
                    IDealerInformationService dealerInformationService = SpringUtils.getBean(IDealerInformationService.class);

                    ContractRecordTow contractRecordTow = new ContractRecordTow();
                    contractRecordTow.setInstanceid(split[0]);
                    contractRecordTow.setSector(annualReviewy1.getSector());
                    contractRecordTow.setDealername(annualReviewy1.getDealerName());
                    contractRecordTow.setContractno(contractnumber);
                    BigDecimal bigDecimal = new BigDecimal(decodeCf.getFacilityAmount());
                    BigDecimal bigDecimal1 = bigDecimal.divide(new BigDecimal(1000000));
                    contractRecordTow.setFacilityamount(bigDecimal1.toString()+"M");
                    contractRecordTow.setDeposit(decodeCf.getDeposit());
                    contractRecordTow.setExpirydate(decodeCf.getExpiredDate());
                    contractRecordTow.setEffectivedate(new Date());
                    contractRecordTow.setNote(note);
                    contractRecordTow.setLimitType(decodeCf.getLimitType());
                    contractRecordTowMapper.insertContractRecordTow(contractRecordTow);

                    IAnnualReviewyService bean1 = SpringUtils.getBean(IAnnualReviewyService.class);
                    bean1.saveDbLog("1","新增报表",null,split[0],null,contractRecordTow,"流程审批结束");
                    //查询是不是包含Part插入报表2
                    if(annualReviewy1.getSector().toUpperCase().equals("PEUGEOT") && (decodeCf.getLimitType().toUpperCase().equals("NORMAL"))){
                        //查询
                        ProposalByCommericalAndMarketing proposalByCommericalAndMarketingParts = new ProposalByCommericalAndMarketing();
                        proposalByCommericalAndMarketingParts.setDealername(annualReviewy1.getDealerName());
                        proposalByCommericalAndMarketingParts.setSector(annualReviewy1.getSector());
                        proposalByCommericalAndMarketingParts.setLimitType("Part");
                        proposalByCommericalAndMarketingParts.setInstanceId(split[0]);
                        ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper = SpringUtils.getBean(ProposalByCommericalAndMarketingMapper.class);
                        List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingsParts = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketingParts);
                        if(StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts)){
                            if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().equals("") && proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()>0) {
                                BigDecimal partAmt = new BigDecimal(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit());
                                BigDecimal partAmt1 = partAmt.divide(new BigDecimal(1000000));
                                contractRecordTow.setFacilityamount(partAmt1.toString()+"M");
                                contractRecordTow.setDeposit(proposalByCommericalAndMarketingsParts.get(0).getProposalCashDeposit());
                                contractRecordTow.setLimitType("Part");
                                contractRecordTowMapper.insertContractRecordTow(contractRecordTow);
                            }
                        }
                    }

                    DealerInformation dealerInformation = new DealerInformation();
                    dealerInformation.setDealerName(annualReviewy1.getDealerName());
                    dealerInformation.setMake(annualReviewy1.getSector());
                    List<DealerInformation> dealerInformations = dealerInformationService.selectDealerInformationList2(dealerInformation);
                    String dealerCode = dealerInformations.get(0).getDealerCode() == null ? " " : dealerInformations.get(0).getDealerCode();
                    ContractRecordFour contractRecordFour = new ContractRecordFour();
                    contractRecordFour.setInstanceid(split[0]);
                    contractRecordFour.setSector(annualReviewy1.getSector());
                    contractRecordFour.setDealerName(annualReviewy1.getDealerName());
                    contractRecordFour.setContractNo(contractnumber);
                    contractRecordFour.setDealerCode(dealerCode);
                    contractRecordFour.setLimitType(decodeCf.getLimitType());
                    DecimalFormat df = new DecimalFormat("#,###");
                    String amt = df.format(Long.valueOf(decodeCf.getFacilityAmount()));
                    if(decodeCf.getLimitType().equals("Temp")){
                        contractRecordFour.setFacilityAmount(amt);
                        contractRecordFour.setTempLimitEffectiveDate(new Date());
                        contractRecordFour.setTempLimitExpiryDate(decodeCf.getExpiredDate());
                    }else{
                        contractRecordFour.setFacilityAmount(amt);
                        contractRecordFour.setExpiryDate(decodeCf.getExpiredDate());
                        contractRecordFour.setEffectiveDate(new Date());
                    }
                    Securities guaranteeInformation = new Securities();
                    guaranteeInformation.setInstanceId(split[0]);
                    guaranteeInformation.setDealername(annualReviewy1.getDealerName());

                    ContractRecord contractRecordTemp = new ContractRecord();
                    contractRecordTemp.setInstanceid(split[0]);
                    contractRecordTemp.setDealercode(dealerCode);
                    contractRecordTemp.setLimitType(decodeCf.getLimitType());
                    List<ContractRecord> contractRecordsTemp = contractRecordMapper.selectContractRecordListBylimit(contractRecordTemp);

                    if(contractRecordsTemp==null || contractRecordsTemp.size() ==0 ){
                        contractRecordFour.setIfNewContract("Y");
                    }
                    boolean sense = false;
                    SecuritiesMapper securitiesMapper = SpringUtils.getBean(SecuritiesMapper.class);
                    List<Securities> guaranteeInformations = securitiesMapper.selectSecuritiesListTypeIsnotnull(guaranteeInformation);
                    if (StringUtils.isNotEmpty(guaranteeInformations)) {
                        for (Securities gif : guaranteeInformations) {
                            if (StringUtils.isEmpty(gif.getCurrentNameCN())) {
                                gif.setCurrentNameCN("");
                            }
                            if (StringUtils.isEmpty(gif.getProposalNameCN())) {
                                gif.setProposalNameCN("");
                            }
                            String currentNameCN = gif.getCurrentNameCN();
                            String proposalNameCN = gif.getProposalNameCN();
                            String currentNameCNStr = currentNameCN.replace("（", "(").replace("）", ")").replace(" ", "").toLowerCase();
                            String proposalNameCNStr = proposalNameCN.replace("（", "(").replace("）", ")").replace(" ", "").toLowerCase();
                            if ((!currentNameCNStr.equals(proposalNameCNStr))) {
                                sense = true;
                                break;
                            }
                        }
                        if(sense){
                            StringBuilder stringBuilder1 = new StringBuilder();
                            for (Securities gif : guaranteeInformations) {
                                if (!StringUtils.isEmpty(gif.getProposalNameCN())) {
                                    stringBuilder1.append(gif.getProposalNameCN()+",");
                                }
                                if (!StringUtils.isEmpty(gif.getProposalNameEN())) {
                                    stringBuilder1.append(gif.getProposalNameEN()+",");
                                }
                            }
                            if(stringBuilder1.length()>0){
                                stringBuilder1.deleteCharAt(stringBuilder1.length() - 1);
                                contractRecordFour.setIfGuaranteeChange(stringBuilder1.toString());
                            }
                        }
                    }
                    contractRecordFourMapper.insertContractRecordFour(contractRecordFour);



                    IAnnualReviewyService bean2 = SpringUtils.getBean(IAnnualReviewyService.class);
                    bean2.saveDbLog("1","新增报表",null,split[0],null,contractRecordFour,"流程审批结束");

                    //查询是不是包含Part插入报表4
                    if(annualReviewy1.getSector().toUpperCase().equals("PEUGEOT") && (decodeCf.getLimitType().toUpperCase().equals("NORMAL"))){
                        //查询
                        ProposalByCommericalAndMarketing proposalByCommericalAndMarketingParts = new ProposalByCommericalAndMarketing();
                        proposalByCommericalAndMarketingParts.setDealername(annualReviewy1.getDealerName());
                        proposalByCommericalAndMarketingParts.setSector(annualReviewy1.getSector());
                        proposalByCommericalAndMarketingParts.setLimitType("Part");
                        proposalByCommericalAndMarketingParts.setInstanceId(split[0]);
                        ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper = SpringUtils.getBean(ProposalByCommericalAndMarketingMapper.class);
                        List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingsParts = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketingParts);
                        if(StringUtils.isNotEmpty(proposalByCommericalAndMarketingsParts)){
                            if(StringUtils.isNotNull(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()) && !proposalByCommericalAndMarketingsParts.get(0).getProposalLimit().equals("") && proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()>0) {
                                DecimalFormat df1 = new DecimalFormat("#,###");
                                String partAmt = df1.format(Long.valueOf(proposalByCommericalAndMarketingsParts.get(0).getProposalLimit()));
                                contractRecordFour.setFacilityAmount(partAmt);
                                contractRecordFour.setLimitType("Part");
                                contractRecordFourMapper.insertContractRecordFour(contractRecordFour);
                            }
                        }
                    }

                }

            }

            NewSumKeepMapper newSumKeepMapper = SpringUtils.getBean(NewSumKeepMapper.class);

            HNewSumKeepModifyMapper bean = SpringUtils.getBean(HNewSumKeepModifyMapper.class);

            List<HNewSumKeepModify> hNewSumKeepModifies = bean.selectHNewSumKeepModifyListByInstanceId(instanceId);

            for (HNewSumKeepModify newSumKeepModify : hNewSumKeepModifies) {
                NewSumKeep newSum = new NewSumKeep();
                newSum.setId(newSumKeepModify.getOldid());
                if(ObjectUtil.isNotEmpty(newSumKeepModify.getNewsum())){
                    newSum.setSum(newSumKeepModify.getNewsum());
                }
                if(StringUtils.isNotEmpty(newSumKeepModify.getNewplanrate())){
                    newSum.setPlanRate(newSumKeepModify.getNewplanrate());
                }
                if(ObjectUtil.isNotEmpty(newSumKeepModify.getNeweffectivedate())){
                    newSum.setEffectiveDate(newSumKeepModify.getNeweffectivedate());

                }
                if(ObjectUtil.isNotEmpty(newSumKeepModify.getNewexpirydate())){
                    newSum.setExpiryDate(newSumKeepModify.getNewexpirydate());
                }
       /*         if(ObjectUtil.isNotEmpty(newSumKeepModify.getNewPremium())){
                    newSum.setPremium(newSumKeepModify.getNewPremium());
                }*/
                if(ObjectUtil.isNotEmpty(newSumKeepModify.getNewNote())){
                    newSum.setNote(newSumKeepModify.getNewNote());
                }
                newSum.setInTransition(1);
                NewSumKeep newSumKeep1 = newSumKeepMapper.selectNewSumKeepById(newSumKeepModify.getOldid());
                newSumKeepMapper.updateNewSumKeep(newSum);
                NewSumKeep newSumKeep2 = newSumKeepMapper.selectNewSumKeepById(newSumKeepModify.getOldid());
                IAnnualReviewyService bean3 = SpringUtils.getBean(IAnnualReviewyService.class);
                bean3.saveDbLog("3","修改历史保险信息",null ,instanceId,newSumKeep1,newSumKeep2,"保险变更流程审批通过");

                ReportInsurance reportInsurance = new ReportInsurance();
                reportInsurance.setInstanceId(instanceId);
                reportInsurance.setDealerName(newSumKeep2.getDealerName());
                reportInsurance.setSector(newSumKeep2.getSector());
                reportInsurance.setEffectiveDate(newSumKeep2.getEffectiveDate());
                reportInsurance.setExpiryDate(newSumKeep2.getExpiryDate());
                reportInsurance.setCoverage(newSumKeep2.getSum().toString());
                reportInsurance.setRate(newSumKeep2.getPlanRate());
                reportInsurance.setNote("保险信息变更");
                ReportInsuranceMapper bean2 = SpringUtils.getBean(ReportInsuranceMapper.class);
                bean2.insertReportInsurance(reportInsurance);
                ReportInsuranceMapper insuranceMapper = SpringUtils.getBean(ReportInsuranceMapper.class);
            }

        }
        //更新报表
        ApprovalStatusReportMapper approvalStatusReportMapper = SpringUtils.getBean(ApprovalStatusReportMapper.class);
        ApprovalStatusReport approvalStatusReport = new ApprovalStatusReport();
        approvalStatusReport.setInstanceId(instanceId);
        List<ApprovalStatusReport> approvalStatusReports = approvalStatusReportMapper.selectApprovalStatusReportList1(approvalStatusReport);
        if(approvalStatusReports!=null && approvalStatusReports.size()>0){
            //更新RC批复的到期日
            CreditConditionMapper creditConditionMapper = SpringUtils.getBean(CreditConditionMapper.class);
            ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper = SpringUtils.getBean(ProposalByCommericalAndMarketingMapper.class);
            for (ApprovalStatusReport statusReport : approvalStatusReports) {
                if(!StringUtils.isEmpty(statusReport.getSector())){
                    String[] split = statusReport.getSector().split("/");
                    StringBuilder expiryDateStr = new StringBuilder();
                    StringBuilder depositStr = new StringBuilder();
                    StringBuilder depositStr1 = new StringBuilder();
                    for (String s : split) {
                        CreditCondition creditCondition = new CreditCondition();
                        creditCondition.setDealername(statusReport.getDealerNameCn());
                        creditCondition.setInstanceid(instanceId);
                        creditCondition.setSector(statusReport.getSector());
                        creditCondition.setLimittype("NORMAL");
                        List<CreditCondition> creditConditions = creditConditionMapper.selectCreditConditionList(creditCondition);
                        if(creditConditions!=null && creditConditions.size()>0){
                            CreditCondition creditCondition1 = creditConditions.get(0);
                            if(creditCondition1!=null && creditCondition1.getExpireddate()!=null){
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                String format = sdf.format(creditCondition1.getExpireddate());
                                expiryDateStr.append(format).append("/");
                            }
                        }
                        ProposalByCommericalAndMarketing proposalByCommericalAndMarketing = new ProposalByCommericalAndMarketing();
                        proposalByCommericalAndMarketing.setDealername(statusReport.getDealerNameCn());
                        proposalByCommericalAndMarketing.setInstanceId(instanceId);
                        proposalByCommericalAndMarketing.setSector(s);
                        List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings2 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList1(proposalByCommericalAndMarketing);
                        if(proposalByCommericalAndMarketings2!=null && proposalByCommericalAndMarketings2.size()>0){
                            StringBuilder deposit = new StringBuilder();
                            StringBuilder deposit2 = new StringBuilder();
                            for (ProposalByCommericalAndMarketing byCommericalAndMarketing1 : proposalByCommericalAndMarketings2) {
                                if(!StringUtils.isEmpty(byCommericalAndMarketing1.getApprovedCashDeposit())){
                                    deposit.append(byCommericalAndMarketing1.getApprovedCashDeposit()).append(",");
                                }
                                if(!StringUtils.isEmpty(byCommericalAndMarketing1.getProposalCashDeposit())){
                                    deposit2.append(byCommericalAndMarketing1.getProposalCashDeposit()).append(",");
                                }
                            }
                            if(!StringUtils.isEmpty(deposit.toString())){
                                deposit.deleteCharAt(deposit.length() - 1);
                                depositStr.append(deposit).append("/");
                            }
                            if(!StringUtils.isEmpty(deposit2.toString())){
                                deposit2.deleteCharAt(deposit2.length() - 1);
                                depositStr1.append(deposit2).append("/");

                            }
                        }

                    }
                    if(!StringUtils.isEmpty(depositStr.toString())){
                        depositStr.deleteCharAt(depositStr.length() - 1);
                        statusReport.setOriginalSecurityRatio(depositStr.toString());
                    }
                    if(!StringUtils.isEmpty(depositStr1.toString())){
                        depositStr1.deleteCharAt(depositStr1.length() - 1);
                        statusReport.setUpdatedSecurityRatio(depositStr1.toString());
                    }

                    if(!StringUtils.isEmpty(expiryDateStr.toString())){
                        expiryDateStr.deleteCharAt(expiryDateStr.length() - 1);
                        statusReport.setExpiryDate(expiryDateStr.toString());
                    }
                    //更新额度信息
                    ProposalByCommericalAndMarketing proposalByCommericalAndMarketingTemp = new ProposalByCommericalAndMarketing();
                    proposalByCommericalAndMarketingTemp.setDealername(statusReport.getDealerNameCn());
                    proposalByCommericalAndMarketingTemp.setInstanceId(instanceId);
                    List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings1 = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketingTemp);
                    Long currentLimitSum = 0l;
                    Long proposedLimitSum = 0l;
                    if(proposalByCommericalAndMarketings1!=null && proposalByCommericalAndMarketings1.size()>0){
                        for (ProposalByCommericalAndMarketing byCommericalAndMarketing : proposalByCommericalAndMarketings1) {
                            if(byCommericalAndMarketing.getApprovedLimit()!=null){
                                currentLimitSum = currentLimitSum+byCommericalAndMarketing.getApprovedLimit();
                            }
                            if(byCommericalAndMarketing.getProposalLimit()!=null){
                                proposedLimitSum =proposedLimitSum+byCommericalAndMarketing.getProposalLimit();
                            }
                        }
                        statusReport.setOriginalLimit(currentLimitSum.toString());
                        statusReport.setUpdatedLimit(proposedLimitSum.toString());
                    }else {
                        //查询
                        ActiveLimitAdjustmentProposal adjustmentProposal = new ActiveLimitAdjustmentProposal();
                        adjustmentProposal.setInstanceId(instanceId);
                        adjustmentProposal.setDealerName(statusReport.getDealerNameCn());
                        ActiveLimitAdjustmentProposalMapper activeLimitAdjustmentProposalMapper = SpringUtils.getBean(ActiveLimitAdjustmentProposalMapper.class);
                        List<ActiveLimitAdjustmentProposal> activeLimitAdjustmentProposals1 = activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalList(adjustmentProposal);
                        if(activeLimitAdjustmentProposals1!=null && activeLimitAdjustmentProposals1.size()>0){
                            for (ActiveLimitAdjustmentProposal proposal : activeLimitAdjustmentProposals1) {
                                if(proposal.getActiveLimit()!=null){
                                    currentLimitSum = currentLimitSum+proposal.getActiveLimit();
                                }
                                if(proposal.getProposedActiveLimit()!=null){
                                    String s = proposal.getProposedActiveLimit();
                                    String replace = s.replace(",", "");
                                    proposedLimitSum =proposedLimitSum+Long.valueOf(replace);
                                }

                            }
                            statusReport.setOriginalLimit(currentLimitSum.toString());
                            statusReport.setUpdatedLimit(proposedLimitSum.toString());
                        }
                    }

                }
                statusReport.setSituation("Approved");
                statusReport.setCompletedDate(new Date());
                if(annualReviewy1!=null){
                    statusReport.setDelegation(annualReviewy1.getApprovalName());
                }
                //查询最后uw审批人
                ActWorkflowFormDataMapper ActWorkflowFormDataMapper = SpringUtils.getBean(ActWorkflowFormDataMapper.class);
                ActWorkflowFormData actWorkflowFormData = new ActWorkflowFormData();
                actWorkflowFormData.setBusinessKey(statusReport.getInstanceId());
                actWorkflowFormData.setTaskNodeName("Underwriter");
                List<ActWorkflowFormData> actWorkflowFormData1 = ActWorkflowFormDataMapper.selectActWorkflowFormDataList(actWorkflowFormData);
                if(actWorkflowFormData1!=null && actWorkflowFormData1.size()>0){
                    statusReport.setResponsibleUw(actWorkflowFormData1.get(0).getCreateName());
                }
                approvalStatusReportMapper.updateApprovalStatusReport(statusReport);
            }

        }
        if(annualReviewy1.getType()!=null && annualReviewy1.getType().equals("parkingLocationRegistration")){
            System.out.println("进入监听器新增");
            List<ParkingLocationRegistration> parkingLocationRegistrations = SpringUtils.getBean(ParkingLocationRegistrationMapper.class).selectParkingLocationRegistrationByInstancsId(instanceId);
            for (ParkingLocationRegistration parkingLocationRegistration : parkingLocationRegistrations) {
                ParkingLocation parkingLocation = new ParkingLocation();
                parkingLocation.setDealernamecn(parkingLocationRegistration.getDealerName());
                // parkingLocation.setParkinglocation(parkingLocationRegistration.getParkinglocation());
                parkingLocation.setParkinglocation(parkingLocationRegistration.getProvinceCN());
                parkingLocation.setGroupnameen(parkingLocationRegistration.getGroupNameCn());
                parkingLocation.setType(parkingLocationRegistration.getType());
                parkingLocation.setEffectivedate(parkingLocationRegistration.getStartDate());
                parkingLocation.setDuedate(parkingLocationRegistration.getEndDate());
                parkingLocation.setVin(parkingLocationRegistration.getVin());
                parkingLocation.setDistancefrom4s(parkingLocationRegistration.getDistanceFrom4s());
                parkingLocation.setTel(parkingLocationRegistration.getTel());
                parkingLocation.setTwondtierdeposit(new BigDecimal(0));
                parkingLocation.setNotes(parkingLocationRegistration.getNotes());
                parkingLocation.setStatus("0"); //开启
                parkingLocation.setInstanceId(parkingLocationRegistration.getInstanceId());
                parkingLocation.setId(IdUtils.simpleUUID());
                SpringUtils.getBean(ParkingLocationMapper.class).insertParkingLocation(parkingLocation);
                IAnnualReviewyService bean3 = SpringUtils.getBean(IAnnualReviewyService.class);
                bean3.saveDbLog("1","新增记录","新增 parkingLocation",instanceId,null,parkingLocation,"流程审批结束");

            }
        }
        if(annualReviewy1.getType()!=null && annualReviewy1.getType().equals("tierCashDeposit")){

            // 对 ParkingLocation表 的 Dealer Code和2nd-tier deposit gap 做更新操作      根据经销商名称和位置做查询
            TierCashDeposit tierCashDeposit = SpringUtils.getBean(TierCashDepositMapper.class).selectTierCashDepositById(instanceId);
            if(tierCashDeposit.getTierDepositReceivable() != null && tierCashDeposit.getTierDepositReceivable() > 0){
                ReportDeposit reportDeposit = new ReportDeposit();
                reportDeposit.setId(IdUtils.simpleUUID());
                reportDeposit.setInstanceId(instanceId);
                reportDeposit.setRequestAmount(tierCashDeposit.getTierDepositReceivable().toString());
                reportDeposit.setDealerName(tierCashDeposit.getDealerNameCn());
                reportDeposit.setDealerCode(tierCashDeposit.getDealerCode());
                reportDeposit.setDepositType("tierCashDeposit");
                if(!StringUtils.isEmpty(tierCashDeposit.getRefundType())){
                    reportDeposit.setRequestType(tierCashDeposit.getRefundType());
                }else{
                    reportDeposit.setRequestType("Deposit increase");
                }
                reportDeposit.setRequestDate(new Date());

                SpringUtils.getBean(ReportDepositMapper.class).insertReportDeposit(reportDeposit);
                //新增报表
                IAnnualReviewyService bean3 = SpringUtils.getBean(IAnnualReviewyService.class);
                bean3.saveDbLog("1","新增报表",null,instanceId,null,reportDeposit,"流程审批结束");

                //tierCashDeposit.getTierDepositReceivable() 大于0
                //insertReportDeposit
            }

            ParkingLocation parkingLocation = SpringUtils.getBean(ParkingLocationMapper.class).selectParkingLocationByDealerAndParkingLocation(tierCashDeposit.getDealerNameCn(), tierCashDeposit.getParkingLocation());
            ParkingLocation parkingLocation2 = SpringUtils.getBean(ParkingLocationMapper.class).selectParkingLocationByDealerAndParkingLocation(tierCashDeposit.getDealerNameCn(), tierCashDeposit.getParkingLocation());
            if(parkingLocation!=null){
                parkingLocation.setDealercode(tierCashDeposit.getDealerCode());
                parkingLocation.setTierDepositGap(tierCashDeposit.getTierDepositGap());
                if(tierCashDeposit.getTierDepositReceivable() !=null){
                    parkingLocation.setTwondtierdeposit(new BigDecimal(tierCashDeposit.getTierDepositReceivable()));
                }
                IAnnualReviewyService bean3 = SpringUtils.getBean(IAnnualReviewyService.class);
                SpringUtils.getBean(ParkingLocationMapper.class).updateParkingLocation(parkingLocation);
                bean3.saveDbLog("3","修改ParkingLocation信息",null ,instanceId,parkingLocation2,parkingLocation,"流程审批通过");

            }
        }
        if(annualReviewy1.getState().equals("4")){
            return;
        }
        annualReviewy.setCompletedDate(new Date());
        annualReviewy.setInstanceId(delegateExecution.getProcessInstanceBusinessKey());
        annualReviewy.setState("1");
        SpringUtils.getBean(IAnnualReviewyService.class).updateAnnualReviewyByInstanceId(annualReviewy);
    }
}
