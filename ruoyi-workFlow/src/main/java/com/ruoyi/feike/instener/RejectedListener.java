package com.ruoyi.feike.instener;

import com.github.pagehelper.util.StringUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.EmailUtil;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.ApprovalStatusReport;
import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.DealercodeContractFiling;
import com.ruoyi.feike.domain.vo.ContractRecord;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IAnnualReviewyService;
import com.ruoyi.feike.service.IApprovalStatusReportService;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

@Slf4j
@Component
public class RejectedListener implements ExecutionListener , ApplicationContextAware {

    private Expression state;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private BasicInformationMapper basicInformationMapper;

    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;

    private static ApplicationContext context = null;

    @Override
    public void notify(DelegateExecution delegateExecution) {

        //需要更新状态
        String businessKey =null ;
        String instanceId = delegateExecution.getProcessInstanceId();
        if(null !=instanceId){
            HistoryService historyService = SpringUtils.getBean(HistoryService.class);
            ISysUserService userService = SpringUtils.getBean(ISysUserService.class);
            List<HistoricTaskInstance> taskInstance = historyService.createHistoricTaskInstanceQuery().processInstanceId(instanceId).list();
            HistoricProcessInstance procInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(instanceId).singleResult();
            businessKey = procInstance.getBusinessKey();

            ContractRecordMapper contractRecordMapper = SpringUtils.getBean(ContractRecordMapper.class);
            DealercodeContractFilingMapper dealercodeContractFilingMapper = SpringUtils.getBean(DealercodeContractFilingMapper.class);
            IAnnualReviewyService annualReviewyService = SpringUtils.getBean(IAnnualReviewyService.class);
            AnnualReviewyMapper mapper = SpringUtils.getBean(AnnualReviewyMapper.class);

            AnnualReviewy annualReviewy = mapper.selectAnnualReviewyByInstanceId(businessKey);

            if(annualReviewy !=null) {
                annualReviewy.setState("6");
                mapper.updateAnnualReviewy(annualReviewy);
                //更新状态
                ApprovalStatusReportMapper approvalStatusReportMapper = SpringUtils.getBean(ApprovalStatusReportMapper.class);
                ApprovalStatusReport approvalStatusReport = new ApprovalStatusReport();
                approvalStatusReport.setInstanceId(businessKey);
                List<ApprovalStatusReport> approvalStatusReports = approvalStatusReportMapper.selectApprovalStatusReportList1(approvalStatusReport);
                if(approvalStatusReports!=null && approvalStatusReports.size()>0){
                    for (ApprovalStatusReport statusReport : approvalStatusReports) {
                        statusReport.setSituation("Rejected");
                        approvalStatusReportMapper.updateApprovalStatusReport(statusReport);
                    }
                }
            }

        }
    }
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
