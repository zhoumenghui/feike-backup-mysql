package com.ruoyi.feike.instener;

import com.github.pagehelper.util.StringUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.EmailUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.LimitActiveProposal;
import com.ruoyi.feike.mapper.AnnualReviewyMapper;
import com.ruoyi.feike.mapper.BasicInformationMapper;
import com.ruoyi.feike.service.ILimitActiveProposalService;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.HistoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class SetUpListener implements ExecutionListener {

    private Expression state;

    @Override
    public void notify(DelegateExecution delegateExecution) {

        String instanceId = delegateExecution.getProcessInstanceId();
        ILimitActiveProposalService bean = SpringUtils.getBean(ILimitActiveProposalService.class);
        LimitActiveProposal limitActiveProposal = new LimitActiveProposal();
        limitActiveProposal.setInstanceId(instanceId);
        List<LimitActiveProposal> limitActiveProposals = bean.selectLimitActiveProposalList(limitActiveProposal);
        for (LimitActiveProposal activeProposal : limitActiveProposals) {
            activeProposal.setIsRefundOrder(1);
            bean.updateLimitActiveProposal(activeProposal);
        }
    }
}
