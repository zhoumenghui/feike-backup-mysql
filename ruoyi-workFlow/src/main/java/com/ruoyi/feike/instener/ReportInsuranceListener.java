package com.ruoyi.feike.instener;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.LimitActiveProposal;
import com.ruoyi.feike.domain.ProposalByCommericalAndMarketing;
import com.ruoyi.feike.domain.ReportDeposit;
import com.ruoyi.feike.domain.ReportInsurance;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IProposalByCommericalAndMarketingService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Slf4j
public class ReportInsuranceListener implements ExecutionListener {

    @Value("${wfs.url}")
    private String wfsUrl;
    @Autowired
    private SelfplanInformationMapper selfplanInformationMapper;
    @Autowired
    private NewSumMapper newSumMapper;
    @Autowired
    private PaidSumMapper paidSumMapper;
    @Autowired
    private ReportInsuranceMapper reportInsuranceMapper;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        String instanceId = delegateExecution.getProcessInstanceBusinessKey();
        // 根据instanceId 查询dealerCode 取首个dealercode
        ProposalByCommericalAndMarketing proposalByCommericalAndMarketing = new ProposalByCommericalAndMarketing();
        proposalByCommericalAndMarketing.setInstanceId(instanceId);
        List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings = SpringUtils.getBean(IProposalByCommericalAndMarketingService.class).selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing);
        if(CollectionUtils.isEmpty(proposalByCommericalAndMarketings)){
            log.info("当前实例对应节点数据不存在,instanceId={}",instanceId);
            return;
        }
        ProposalByCommericalAndMarketing proposalByCommericalAndMarketing1 = proposalByCommericalAndMarketings.get(0);
        if(Objects.isNull(proposalByCommericalAndMarketing1)){
            log.info("当前实例对应节点数据不存在,instanceId={},proposalByCommericalAndMarketing1={}",instanceId,proposalByCommericalAndMarketing1);
        }
        String dealerCode = proposalByCommericalAndMarketing.getDealerCode();
        //  获取数据 填充数据
        List<ReportInsurance> list = selfplanInformationMapper.selectInformationByInstanceId(instanceId);
        List<ReportInsurance> list2 = newSumMapper.selectInformationByInstanceId(instanceId);
        List<ReportInsurance> list3 = paidSumMapper.selectInformationByInstanceId(instanceId);
        list.addAll(list2);
        list.addAll(list3);
        if(CollectionUtils.isEmpty(list)){
            log.info("Report insurance 数据为空");
            return;
        }
        // 填充数据
        list.forEach(data->{
            data.setCreateTime(new Date());
            data.setUpdateTime(new Date());
            reportInsuranceMapper.insertReportInsurance(data);
        });
    }
}
