package com.ruoyi.feike.service.impl;

import com.ruoyi.feike.domain.ActWorkflowLock;
import com.ruoyi.feike.mapper.ActWorkflowLockMapper;
import com.ruoyi.feike.service.IActWorkflowLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActWorkflowLockServiceImpl implements IActWorkflowLockService {


    @Autowired
    ActWorkflowLockMapper   actWorkflowLockMapper;

    @Override
    public int insertActWorkflowLock(ActWorkflowLock actWorkflowLock) {
        return actWorkflowLockMapper.insertActWorkflowLock(actWorkflowLock);
    }

    @Override
    public int deleteActWorkflowLockById(String id) {
        return actWorkflowLockMapper.deleteActWorkflowLockById(id);
    }
}
