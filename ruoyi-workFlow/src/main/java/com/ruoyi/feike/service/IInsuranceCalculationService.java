package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.InsuranceCalculation;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface IInsuranceCalculationService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public InsuranceCalculation selectInsuranceCalculationById(String id);

    /**
     * 查询feike列表
     * 
     * @param insuranceCalculation feike
     * @return feike集合
     */
    public List<InsuranceCalculation> selectInsuranceCalculationList(InsuranceCalculation insuranceCalculation);

    /**
     * 新增feike
     * 
     * @param insuranceCalculation feike
     * @return 结果
     */
    public int insertInsuranceCalculation(InsuranceCalculation insuranceCalculation);

    /**
     * 修改feike
     * 
     * @param insuranceCalculation feike
     * @return 结果
     */
    public int updateInsuranceCalculation(InsuranceCalculation insuranceCalculation);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteInsuranceCalculationByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteInsuranceCalculationById(String id);
}
