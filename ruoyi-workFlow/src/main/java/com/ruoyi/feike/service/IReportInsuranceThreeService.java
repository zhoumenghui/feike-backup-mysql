package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.ReportInsuranceThree;

/**
 * 报表h_report-insurance-3Service接口
 *
 * @author ruoyi
 * @date 2022-12-27
 */
public interface IReportInsuranceThreeService
{
    /**
     * 查询报表h_report-insurance-3
     *
     * @param id 报表h_report-insurance-3ID
     * @return 报表h_report-insurance-3
     */
    public ReportInsuranceThree selectReportInsuranceThreeById(Long id);

    /**
     * 查询报表h_report-insurance-3列表
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 报表h_report-insurance-3集合
     */
    public List<ReportInsuranceThree> selectReportInsuranceThreeList(ReportInsuranceThree reportInsuranceThree);

    /**
     * 新增报表h_report-insurance-3
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 结果
     */
    public int insertReportInsuranceThree(ReportInsuranceThree reportInsuranceThree);

    /**
     * 修改报表h_report-insurance-3
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 结果
     */
    public int updateReportInsuranceThree(ReportInsuranceThree reportInsuranceThree);

    /**
     * 批量删除报表h_report-insurance-3
     *
     * @param ids 需要删除的报表h_report-insurance-3ID
     * @return 结果
     */
    public int deleteReportInsuranceThreeByIds(Long[] ids);

    /**
     * 删除报表h_report-insurance-3信息
     *
     * @param id 报表h_report-insurance-3ID
     * @return 结果
     */
    public int deleteReportInsuranceThreeById(Long id);

    Map<String,Object> getTwo(String instanceId);

    Map<Object, Object> exportByDate(Map<String, Object> map);

    Map<String,Object> getInfoByDealerName(String dealerName);

    Map<String,Object> getInfoByDealerNameByHis(String dealerName,String instanceId);

    List<Map<String, Object>> getDealerInsuranceByDealerName(String dealerName);
}
