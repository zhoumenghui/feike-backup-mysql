package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.ReportInsurance;

/**
 * feikeService接口
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
public interface IReportInsuranceService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ReportInsurance selectReportInsuranceById(String id);

    /**
     * 查询feike列表
     * 
     * @param reportInsurance feike
     * @return feike集合
     */
    public List<ReportInsurance> selectReportInsuranceList(ReportInsurance reportInsurance);

    /**
     * 新增feike
     * 
     * @param reportInsurance feike
     * @return 结果
     */
    public int insertReportInsurance(ReportInsurance reportInsurance);

    /**
     * 修改feike
     * 
     * @param reportInsurance feike
     * @return 结果
     */
    public int updateReportInsurance(ReportInsurance reportInsurance);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteReportInsuranceByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteReportInsuranceById(String id);

    /**
     * 导出部分或全量数据
     * @param type
     * @return
     */
    List<ReportInsurance> selectReportInsuranceExcelList(String type);
}
