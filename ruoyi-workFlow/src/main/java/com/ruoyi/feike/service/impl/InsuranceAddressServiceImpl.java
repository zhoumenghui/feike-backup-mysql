package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.InsuranceAddressMapper;
import com.ruoyi.feike.domain.InsuranceAddress;
import com.ruoyi.feike.service.IInsuranceAddressService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class InsuranceAddressServiceImpl implements IInsuranceAddressService 
{
    @Autowired
    private InsuranceAddressMapper insuranceAddressMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public InsuranceAddress selectInsuranceAddressById(String id)
    {
        return insuranceAddressMapper.selectInsuranceAddressById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param insuranceAddress feike
     * @return feike
     */
    @Override
    public List<InsuranceAddress> selectInsuranceAddressList(InsuranceAddress insuranceAddress)
    {
        return insuranceAddressMapper.selectInsuranceAddressList(insuranceAddress);
    }

    /**
     * 新增feike
     * 
     * @param insuranceAddress feike
     * @return 结果
     */
    @Override
    public int insertInsuranceAddress(InsuranceAddress insuranceAddress)
    {
        return insuranceAddressMapper.insertInsuranceAddress(insuranceAddress);
    }

    /**
     * 修改feike
     * 
     * @param insuranceAddress feike
     * @return 结果
     */
    @Override
    public int updateInsuranceAddress(InsuranceAddress insuranceAddress)
    {
        return insuranceAddressMapper.updateInsuranceAddress(insuranceAddress);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteInsuranceAddressByIds(String[] ids)
    {
        return insuranceAddressMapper.deleteInsuranceAddressByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteInsuranceAddressById(String id)
    {
        return insuranceAddressMapper.deleteInsuranceAddressById(id);
    }
}
