package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealercodeCountMapper;
import com.ruoyi.feike.domain.DealercodeCount;
import com.ruoyi.feike.service.IDealercodeCountService;

/**
 * countService业务层处理
 * 
 * @author ybw
 * @date 2022-08-09
 */
@Service
public class DealercodeCountServiceImpl implements IDealercodeCountService 
{
    @Autowired
    private DealercodeCountMapper dealercodeCountMapper;

    /**
     * 查询count
     * 
     * @param id countID
     * @return count
     */
    @Override
    public DealercodeCount selectDealercodeCountById(Long id)
    {
        return dealercodeCountMapper.selectDealercodeCountById(id);
    }

    /**
     * 查询count列表
     * 
     * @param dealercodeCount count
     * @return count
     */
    @Override
    public List<DealercodeCount> selectDealercodeCountList(DealercodeCount dealercodeCount)
    {
        return dealercodeCountMapper.selectDealercodeCountList(dealercodeCount);
    }

    /**
     * 新增count
     * 
     * @param dealercodeCount count
     * @return 结果
     */
    @Override
    public int insertDealercodeCount(DealercodeCount dealercodeCount)
    {
        return dealercodeCountMapper.insertDealercodeCount(dealercodeCount);
    }

    /**
     * 修改count
     * 
     * @param dealercodeCount count
     * @return 结果
     */
    @Override
    public int updateDealercodeCount(DealercodeCount dealercodeCount)
    {
        return dealercodeCountMapper.updateDealercodeCount(dealercodeCount);
    }

    /**
     * 批量删除count
     * 
     * @param ids 需要删除的countID
     * @return 结果
     */
    @Override
    public int deleteDealercodeCountByIds(Long[] ids)
    {
        return dealercodeCountMapper.deleteDealercodeCountByIds(ids);
    }

    /**
     * 删除count信息
     * 
     * @param id countID
     * @return 结果
     */
    @Override
    public int deleteDealercodeCountById(Long id)
    {
        return dealercodeCountMapper.deleteDealercodeCountById(id);
    }
}
