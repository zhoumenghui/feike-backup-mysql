package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.vo.ContractRecordTow;

import java.util.List;

public interface IContractRecordTowSerivce {

    public List<ContractRecordTow> selectContractRecordTowList(ContractRecordTow contractRecordTow);


    public int insertContractRecordTow(ContractRecordTow contractRecordTow);


}
