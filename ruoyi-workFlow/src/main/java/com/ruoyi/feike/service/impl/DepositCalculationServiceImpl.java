package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DepositCalculationMapper;
import com.ruoyi.feike.domain.DepositCalculation;
import com.ruoyi.feike.service.IDepositCalculationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class DepositCalculationServiceImpl implements IDepositCalculationService 
{
    @Autowired
    private DepositCalculationMapper depositCalculationMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public DepositCalculation selectDepositCalculationById(String id)
    {
        return depositCalculationMapper.selectDepositCalculationById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param depositCalculation feike
     * @return feike
     */
    @Override
    public List<DepositCalculation> selectDepositCalculationList(DepositCalculation depositCalculation)
    {
        return depositCalculationMapper.selectDepositCalculationList(depositCalculation);
    }

    /**
     * 新增feike
     * 
     * @param depositCalculation feike
     * @return 结果
     */
    @Override
    public int insertDepositCalculation(DepositCalculation depositCalculation)
    {
        return depositCalculationMapper.insertDepositCalculation(depositCalculation);
    }

    /**
     * 修改feike
     * 
     * @param depositCalculation feike
     * @return 结果
     */
    @Override
    public int updateDepositCalculation(DepositCalculation depositCalculation)
    {
        return depositCalculationMapper.updateDepositCalculation(depositCalculation);
    }

    @Override
    public int updateDepositCalculationByDeposit(DepositCalculation depositCalculation)
    {
        return depositCalculationMapper.updateDepositCalculationByDeposit(depositCalculation);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteDepositCalculationByIds(String[] ids)
    {
        return depositCalculationMapper.deleteDepositCalculationByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteDepositCalculationById(String id)
    {
        return depositCalculationMapper.deleteDepositCalculationById(id);
    }
}
