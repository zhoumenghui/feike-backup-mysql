package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.BillingInformation;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface IBillingInformationService 
{
    /**
     * 查询feike
     * 
     * @param instanceId feikeID
     * @return feike
     */
    public BillingInformation selectBillingInformationById(String instanceId);

    /**
     * 查询feike列表
     * 
     * @param billingInformation feike
     * @return feike集合
     */
    public List<BillingInformation> selectBillingInformationList(BillingInformation billingInformation);

    /**
     * 新增feike
     * 
     * @param billingInformation feike
     * @return 结果
     */
    public int insertBillingInformation(BillingInformation billingInformation);

    /**
     * 修改feike
     * 
     * @param billingInformation feike
     * @return 结果
     */
    public int updateBillingInformation(BillingInformation billingInformation);

    /**
     * 批量删除feike
     * 
     * @param instanceIds 需要删除的feikeID
     * @return 结果
     */
    public int deleteBillingInformationByIds(String[] instanceIds);

    /**
     * 删除feike信息
     * 
     * @param instanceId feikeID
     * @return 结果
     */
    public int deleteBillingInformationById(String instanceId);
}
