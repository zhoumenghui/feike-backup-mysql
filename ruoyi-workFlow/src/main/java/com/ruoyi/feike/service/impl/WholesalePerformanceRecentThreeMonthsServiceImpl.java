package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.WholesalePerformanceRecentThreeMonthsMapper;
import com.ruoyi.feike.domain.WholesalePerformanceRecentThreeMonths;
import com.ruoyi.feike.service.IWholesalePerformanceRecentThreeMonthsService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class WholesalePerformanceRecentThreeMonthsServiceImpl implements IWholesalePerformanceRecentThreeMonthsService 
{
    @Autowired
    private WholesalePerformanceRecentThreeMonthsMapper wholesalePerformanceRecentThreeMonthsMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public WholesalePerformanceRecentThreeMonths selectWholesalePerformanceRecentThreeMonthsById(String id)
    {
        return wholesalePerformanceRecentThreeMonthsMapper.selectWholesalePerformanceRecentThreeMonthsById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return feike
     */
    @Override
    public List<WholesalePerformanceRecentThreeMonths> selectWholesalePerformanceRecentThreeMonthsList(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        return wholesalePerformanceRecentThreeMonthsMapper.selectWholesalePerformanceRecentThreeMonthsList(wholesalePerformanceRecentThreeMonths);
    }

    /**
     * 新增feike
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return 结果
     */
    @Override
    public int insertWholesalePerformanceRecentThreeMonths(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        return wholesalePerformanceRecentThreeMonthsMapper.insertWholesalePerformanceRecentThreeMonths(wholesalePerformanceRecentThreeMonths);
    }

    /**
     * 修改feike
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return 结果
     */
    @Override
    public int updateWholesalePerformanceRecentThreeMonths(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        return wholesalePerformanceRecentThreeMonthsMapper.updateWholesalePerformanceRecentThreeMonths(wholesalePerformanceRecentThreeMonths);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteWholesalePerformanceRecentThreeMonthsByIds(String[] ids)
    {
        return wholesalePerformanceRecentThreeMonthsMapper.deleteWholesalePerformanceRecentThreeMonthsByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteWholesalePerformanceRecentThreeMonthsById(String id)
    {
        return wholesalePerformanceRecentThreeMonthsMapper.deleteWholesalePerformanceRecentThreeMonthsById(id);
    }
}
