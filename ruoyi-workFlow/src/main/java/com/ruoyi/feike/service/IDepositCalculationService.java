package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.DepositCalculation;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface IDepositCalculationService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public DepositCalculation selectDepositCalculationById(String id);

    /**
     * 查询feike列表
     * 
     * @param depositCalculation feike
     * @return feike集合
     */
    public List<DepositCalculation> selectDepositCalculationList(DepositCalculation depositCalculation);

    /**
     * 新增feike
     * 
     * @param depositCalculation feike
     * @return 结果
     */
    public int insertDepositCalculation(DepositCalculation depositCalculation);

    /**
     * 修改feike
     * 
     * @param depositCalculation feike
     * @return 结果
     */
    public int updateDepositCalculation(DepositCalculation depositCalculation);

    public int updateDepositCalculationByDeposit(DepositCalculation depositCalculation);


    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteDepositCalculationByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteDepositCalculationById(String id);
}
