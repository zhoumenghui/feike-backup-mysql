package com.ruoyi.feike.service.impl;

import com.ruoyi.common.enums.Quarter;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.feike.domain.ActiveLimitAdjustmentProposal;
import com.ruoyi.feike.domain.LoyaltyPerformance4q;
import com.ruoyi.feike.domain.LoyaltyPerformance6month;
import com.ruoyi.feike.domain.OtherFinancingResource;
import com.ruoyi.feike.mapper.ActiveLimitAdjustmentProposalMapper;
import com.ruoyi.feike.mapper.OracleMapper;
import com.ruoyi.feike.service.IActiveLimitAdjustmentProposalService;
import com.ruoyi.feike.service.IOracleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * feikeService业务层处理
 *
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class OracleServiceImpl implements IOracleService
{

	@Autowired
	private OracleMapper oracleMapper;

	@Override
	public List<LoyaltyPerformance4q> getLoyaltyPerformance4Q(List<String> dealerNames)
	{
		String currentYear = DateUtils.getCurrentYear();
		String currentMonth = DateUtils.getCurrentMonth();
		String currentDay = DateUtils.getCurrentDay();
		String lastMonth = DateUtils.getLastMonth();
		List<String> lastYearQ = new ArrayList<>();
		List<String> thisYearQ = new ArrayList<>();
		String lastYear = String.valueOf(Math.subtractExact(Integer.valueOf(currentYear), 1));
		Map<String, Integer> sort = new HashMap<>();
		if(Quarter.Q1.contains(currentMonth)){
			if(currentMonth.equals("03") && !Quarter.DAY.contains(currentDay)){
				thisYearQ.add(currentYear+"Q1");
				lastYearQ.add(lastYear+"Q2");
				lastYearQ.add(lastYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(currentYear+"Q1",2);
				sort.put(lastYear+"Q2",3);
				sort.put(lastYear+"Q3",4);
				sort.put(lastYear+"Q4",1);
			}else{
				lastYearQ.add(lastYear+"Q1");
				lastYearQ.add(lastYear+"Q2");
				lastYearQ.add(lastYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(lastYear+"Q1",1);
				sort.put(lastYear+"Q2",2);
				sort.put(lastYear+"Q3",3);
				sort.put(lastYear+"Q4",4);
			}
		}else if(Quarter.Q2.contains(currentMonth)){

			if(currentMonth.equals("06") && !Quarter.DAY.contains(currentDay)){
				thisYearQ.add(currentYear+"Q1");
				thisYearQ.add(currentYear+"Q2");
				lastYearQ.add(lastYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(currentYear+"Q1",3);
				sort.put(currentYear+"Q2",4);
				sort.put(lastYear+"Q3",1);
				sort.put(lastYear+"Q4",2);
			}else{
				thisYearQ.add(currentYear+"Q1");
				lastYearQ.add(lastYear+"Q2");
				lastYearQ.add(lastYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(currentYear+"Q1",4);
				sort.put(lastYear+"Q2",1);
				sort.put(lastYear+"Q3",2);
				sort.put(lastYear+"Q4",3);
			}

		}else if(Quarter.Q3.contains(currentMonth)){
			if(currentMonth.equals("09") && !Quarter.DAY.contains(currentDay)){
				thisYearQ.add(currentYear+"Q1");
				thisYearQ.add(currentYear+"Q2");
				thisYearQ.add(currentYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(currentYear+"Q1",2);
				sort.put(currentYear+"Q2",3);
				sort.put(currentYear+"Q3",4);
				sort.put(lastYear+"Q4",1);
			}else{
				thisYearQ.add(currentYear+"Q1");
				thisYearQ.add(currentYear+"Q2");
				lastYearQ.add(lastYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(currentYear+"Q1",3);
				sort.put(currentYear+"Q2",4);
				sort.put(lastYear+"Q3",1);
				sort.put(lastYear+"Q4",2);
			}

		}else if(Quarter.Q4.contains(currentMonth)){

			if(currentMonth.equals("12") && !Quarter.DAY.contains(currentDay)){
				thisYearQ.add(currentYear+"Q1");
				thisYearQ.add(currentYear+"Q2");
				thisYearQ.add(currentYear+"Q3");
				thisYearQ.add(currentYear+"Q4");
				sort.put(currentYear+"Q1",1);
				sort.put(currentYear+"Q2",2);
				sort.put(currentYear+"Q3",3);
				sort.put(currentYear+"Q4",4);
			}else{
				thisYearQ.add(currentYear+"Q1");
				thisYearQ.add(currentYear+"Q2");
				thisYearQ.add(currentYear+"Q3");
				lastYearQ.add(lastYear+"Q4");
				sort.put(currentYear+"Q1",2);
				sort.put(currentYear+"Q2",3);
				sort.put(currentYear+"Q3",4);
				sort.put(lastYear+"Q4",1);
			}

		}

		List<LoyaltyPerformance4q> loyaltyPerformance4Q = oracleMapper.getLoyaltyPerformance4Q(dealerNames, currentYear, lastYear, thisYearQ, lastYearQ);
		for (int i = 0; i < loyaltyPerformance4Q.size(); i++) {
			LoyaltyPerformance4q loyaltyPerformance4q = loyaltyPerformance4Q.get(i);
			loyaltyPerformance4q.setLabel(loyaltyPerformance4q.getYear()+"-"+loyaltyPerformance4q.getType());
			loyaltyPerformance4q.setSort(sort.get(loyaltyPerformance4q.getType()));
			loyaltyPerformance4q.setYear(currentYear);
		}
		Collections.sort(loyaltyPerformance4Q, new Comparator<LoyaltyPerformance4q>() {
			@Override
			public int compare(LoyaltyPerformance4q o1, LoyaltyPerformance4q o2) {
				return o1.getSector().substring(0).compareTo(o2.getSector().substring(0));
			}

			@Override
			public boolean equals(Object obj) {
				return false;
			}
		});

		Collections.sort(loyaltyPerformance4Q, new Comparator<LoyaltyPerformance4q>() {
			@Override
			public int compare(LoyaltyPerformance4q o1, LoyaltyPerformance4q o2) {
				return o1.getDealerName().compareTo(o2.getDealerName());
			}

			@Override
			public boolean equals(Object obj) {
				return false;
			}
		});
		Collections.sort(loyaltyPerformance4Q, new Comparator<LoyaltyPerformance4q>() {
			@Override
			public int compare(LoyaltyPerformance4q o1, LoyaltyPerformance4q o2) {
				return o1.getItem().compareTo(o2.getItem());
			}

			@Override
			public boolean equals(Object obj) {
				return false;
			}
		});
		return loyaltyPerformance4Q;

	}

	@Override
	public List<LoyaltyPerformance4q> getLoyaltyPerformance6Month(List<String> dealerNames)
	{
		String currentYear = DateUtils.getCurrentYear();
		String currentMonth = DateUtils.getCurrentMonth();
		List<String> lastYearQ = new ArrayList<>();
		List<String> thisYearQ = new ArrayList<>();
		String lastYear = String.valueOf(Math.subtractExact(Integer.valueOf(currentYear), 1));
		Map<String, Integer> sort = new HashMap<>();
		if(Quarter.Q1.contains(currentMonth)){
			thisYearQ.add("Q1");
			lastYearQ.add("Q2");
			lastYearQ.add("Q3");
			lastYearQ.add("Q4");
			sort.put("Q1",4);
			sort.put("Q2",1);
			sort.put("Q3",2);
			sort.put("Q4",3);
		}else if(Quarter.Q2.contains(currentMonth)){
			thisYearQ.add("Q1");
			thisYearQ.add("Q2");
			lastYearQ.add("Q3");
			lastYearQ.add("Q4");
			sort.put("Q1",3);
			sort.put("Q2",4);
			sort.put("Q3",1);
			sort.put("Q4",2);
		}else if(Quarter.Q3.contains(currentMonth)){
			thisYearQ.add("Q1");
			thisYearQ.add("Q2");
			thisYearQ.add("Q3");
			lastYearQ.add("Q4");
			sort.put("Q1",2);
			sort.put("Q2",3);
			sort.put("Q3",4);
			sort.put("Q4",1);
		}else if(Quarter.Q4.contains(currentMonth)){
			thisYearQ.add("Q1");
			thisYearQ.add("Q2");
			thisYearQ.add("Q3");
			thisYearQ.add("Q4");
			sort.put("Q1",1);
			sort.put("Q2",2);
			sort.put("Q3",3);
			sort.put("Q4",4);
		}

		List<LoyaltyPerformance4q> loyaltyPerformance4Q = oracleMapper.getLoyaltyPerformance4Q(dealerNames, currentYear, lastYear, thisYearQ, lastYearQ);
		for (int i = 0; i < loyaltyPerformance4Q.size(); i++) {
			LoyaltyPerformance4q loyaltyPerformance4q = loyaltyPerformance4Q.get(i);
			loyaltyPerformance4q.setLabel(loyaltyPerformance4q.getYear()+"-"+loyaltyPerformance4q.getType());
			loyaltyPerformance4q.setSort(sort.get(loyaltyPerformance4q.getType()));
		}
		return loyaltyPerformance4Q;

	}
}
