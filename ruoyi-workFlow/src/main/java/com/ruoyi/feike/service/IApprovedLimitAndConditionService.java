package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.feike.domain.ApprovedLimitAndCondition;

/**
 * feikeService接口
 * 
 * @author ybw
 * @date 2022-07-12
 */
public interface IApprovedLimitAndConditionService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ApprovedLimitAndCondition selectApprovedLimitAndConditionById(String id);

    /**
     * 查询feike列表
     * 
     * @param approvedLimitAndCondition feike
     * @return feike集合
     */
    public List<ApprovedLimitAndCondition> selectApprovedLimitAndConditionList(ApprovedLimitAndCondition approvedLimitAndCondition);

    /**
     * 新增feike
     * 
     * @param approvedLimitAndCondition feike
     * @return 结果
     */
    public int insertApprovedLimitAndCondition(ApprovedLimitAndCondition approvedLimitAndCondition);

    /**
     * 修改feike
     * 
     * @param approvedLimitAndCondition feike
     * @return 结果
     */
    public int updateApprovedLimitAndCondition(ApprovedLimitAndCondition approvedLimitAndCondition);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteApprovedLimitAndConditionByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteApprovedLimitAndConditionById(String id);

    /**
     * 新增feike
     *
     * @param resultMap
     * @return 结果
     */
    public int saveAlac(Map<String, Object> resultMap);
}
