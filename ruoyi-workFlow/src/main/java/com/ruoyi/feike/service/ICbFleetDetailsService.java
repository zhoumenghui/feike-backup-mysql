package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.CbFleetDetails;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface ICbFleetDetailsService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public CbFleetDetails selectCbFleetDetailsById(String id);

    /**
     * 查询feike列表
     * 
     * @param cbFleetDetails feike
     * @return feike集合
     */
    public List<CbFleetDetails> selectCbFleetDetailsList(CbFleetDetails cbFleetDetails);

    /**
     * 新增feike
     * 
     * @param cbFleetDetails feike
     * @return 结果
     */
    public int insertCbFleetDetails(CbFleetDetails cbFleetDetails);

    /**
     * 修改feike
     * 
     * @param cbFleetDetails feike
     * @return 结果
     */
    public int updateCbFleetDetails(CbFleetDetails cbFleetDetails);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteCbFleetDetailsByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteCbFleetDetailsById(String id);
}
