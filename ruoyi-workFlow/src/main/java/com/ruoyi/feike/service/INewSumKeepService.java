package com.ruoyi.feike.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.feike.domain.*;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-08-31
 */
public interface INewSumKeepService {
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public NewSumKeep selectNewSumKeepById(String id);

    /**
     * 查询feike列表
     *
     * @param newSumKeep feike
     * @return feike集合
     */
    public List<PaidSum> selectNewSumKeepList(NewSumKeep newSumKeep);

    List<NewSumKeep> selectNewSumKeepList2(NewSumKeep newSumKeep);


    List<NewSumKeep> selectInsuranceHistory(NewSumKeep newSumKeep);

    List<NewSumKeep> selectNewSumKeepList3(NewSumKeep newSumKeep);

    List<NewSumKeep> selectNewSumKeepList4(NewSumKeep newSumKeep);

    List<NewSumKeepFour> selectNewSumKeepListFour(NewSumKeep newSumKeep);

    /**
     * 新增feike
     *
     * @param newSumKeep feike
     * @return 结果
     */
    public int insertNewSumKeep(NewSumKeep newSumKeep);


    public int insertNewSumKeepHis(NewSumKeep newSumKeep);

    public int insertInsuranceHistory(NewSumKeep newSumKeep);

    /**
     * 修改feike
     *
     * @param newSumKeep feike
     * @return 结果
     */
    public int updateNewSumKeep(NewSumKeep newSumKeep);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteNewSumKeepByIds(String[] ids);


    public int deleteInsuranceHistoryById(String id);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteNewSumKeepById(String id);

    List<NewSumKeep> newSumSelect(NewSumKeep newSumKeep);

    int addACt(Map<String, Object> map);

    List<HNewSumKeepModify> getByInstanceId(String instanceId);

    void update(Map<String, Object> map);

    public List<NewSum> newSums(List<LimitActiveProposal> limitActiveProposals, BigDecimal rate, String dealerName, String sector, boolean flag ,boolean typeFlag,String planMethod);

    public List<NewSum> newSumsTemp(List<LimitActiveProposal> limitActiveProposals, BigDecimal rate, String dealerName, String sector, boolean flag);

}