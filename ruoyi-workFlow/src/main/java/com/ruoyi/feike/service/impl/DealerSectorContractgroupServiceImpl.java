package com.ruoyi.feike.service.impl;

import java.util.List;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.enums.ContractName;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.mapper.DealerInformationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealerSectorContractgroupMapper;
import com.ruoyi.feike.domain.DealerSectorContractgroup;
import com.ruoyi.feike.service.IDealerSectorContractgroupService;

/**
 * 合同Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@Service
public class DealerSectorContractgroupServiceImpl implements IDealerSectorContractgroupService 
{
    @Autowired
    private DealerSectorContractgroupMapper dealerSectorContractgroupMapper;

    @Autowired
    private DealerInformationMapper dealerInformationMapper;

    /**
     * 查询合同
     * 
     * @param id 合同ID
     * @return 合同
     */
    @Override
    public DealerSectorContractgroup selectDealerSectorContractgroupById(String id)
    {
        return dealerSectorContractgroupMapper.selectDealerSectorContractgroupById(id);
    }

    /**
     * 查询合同列表
     * 
     * @param dealerSectorContractgroup 合同
     * @return 合同
     */
    @Override
    public List<DealerSectorContractgroup> selectDealerSectorContractgroupList(DealerSectorContractgroup dealerSectorContractgroup)
    {
        return dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
    }

    /**
     * 新增合同
     * 
     * @param dealerSectorContractgroup 合同
     * @return 结果
     */
    @Override
    public int insertDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup)
    {
        return dealerSectorContractgroupMapper.insertDealerSectorContractgroup(dealerSectorContractgroup);
    }

    /**
     * 修改合同
     * 
     * @param dealerSectorContractgroup 合同
     * @return 结果
     */
    @Override
    public int updateDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup)
    {
        return dealerSectorContractgroupMapper.updateDealerSectorContractgroup(dealerSectorContractgroup);
    }

    @Override
    public int updateOrAdd(DealerSectorContractgroup dealerSectorContractgroup) {
        System.out.println(dealerSectorContractgroup.getDealername());
        System.out.println(dealerSectorContractgroup.getSector());
        if(StringUtils.isEmpty(dealerSectorContractgroup.getDealername())|| StringUtils.isEmpty(dealerSectorContractgroup.getSector())){
            return  1;
        }
        DealerSectorContractgroup contractGroup = new DealerSectorContractgroup();
        contractGroup.setDealername(dealerSectorContractgroup.getDealername());
        contractGroup.setSector(dealerSectorContractgroup.getSector());
        contractGroup.setContractname(ContractName.SJCDY.getInfo());

        DealerInformation dealerInformation = dealerInformationMapper.selectInfoBySectorAndDealerName(dealerSectorContractgroup.getSector(), dealerSectorContractgroup.getDealername());
        dealerSectorContractgroup.setDealerCode(dealerInformation.getDealerCode());
        dealerSectorContractgroup.setExpDate(DateUtils.getDate());
        dealerSectorContractgroup.setContractname(ContractName.SJCDY.getInfo());


        List<DealerSectorContractgroup> contractGroup1 = dealerSectorContractgroupMapper.getContractGroup(contractGroup);
        //是否已存在
        if(CollectionUtil.isNotEmpty(contractGroup1)){
            return dealerSectorContractgroupMapper.updateContractGroup(dealerSectorContractgroup);
        }else {
            return dealerSectorContractgroupMapper.insertDealerSectorContractgroup(dealerSectorContractgroup);
        }
    }

    /**
     * 批量删除合同
     * 
     * @param ids 需要删除的合同ID
     * @return 结果
     */
    @Override
    public int deleteDealerSectorContractgroupByIds(String[] ids)
    {
        return dealerSectorContractgroupMapper.deleteDealerSectorContractgroupByIds(ids);
    }

    /**
     * 删除合同信息
     * 
     * @param id 合同ID
     * @return 结果
     */
    @Override
    public int deleteDealerSectorContractgroupById(String id)
    {
        return dealerSectorContractgroupMapper.deleteDealerSectorContractgroupById(id);
    }
}
