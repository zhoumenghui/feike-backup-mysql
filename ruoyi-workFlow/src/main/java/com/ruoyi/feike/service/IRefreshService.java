package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.dto.RefreshDTO;

import java.io.IOException;
import java.util.Map;

/**
 * @author lss
 * @version 1.0
 * @description: TODO
 * @date 2022/8/28 16:39
 */
public interface IRefreshService {

    /**
     * @description: 申请单刷新接口
     * @param: instanceId
     * @return:
     * @author lss
     * @date: 2022/8/28 13:35
     */
    Map<String, Object> refreshInfoMation(RefreshDTO refreshDTO) throws IOException;
}
