package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.ActiveLimitAdjustmentProposalMapper;
import com.ruoyi.feike.domain.ActiveLimitAdjustmentProposal;
import com.ruoyi.feike.service.IActiveLimitAdjustmentProposalService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class ActiveLimitAdjustmentProposalServiceImpl implements IActiveLimitAdjustmentProposalService 
{
    @Autowired
    private ActiveLimitAdjustmentProposalMapper activeLimitAdjustmentProposalMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public ActiveLimitAdjustmentProposal selectActiveLimitAdjustmentProposalById(String id)
    {
        return activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param activeLimitAdjustmentProposal feike
     * @return feike
     */
    @Override
    public List<ActiveLimitAdjustmentProposal> selectActiveLimitAdjustmentProposalList(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        return activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalList(activeLimitAdjustmentProposal);
    }

    /**
     * 新增feike
     * 
     * @param activeLimitAdjustmentProposal feike
     * @return 结果
     */
    @Override
    public int insertActiveLimitAdjustmentProposal(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        return activeLimitAdjustmentProposalMapper.insertActiveLimitAdjustmentProposal(activeLimitAdjustmentProposal);
    }

    /**
     * 修改feike
     * 
     * @param activeLimitAdjustmentProposal feike
     * @return 结果
     */
    @Override
    public int updateActiveLimitAdjustmentProposal(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal)
    {
        return activeLimitAdjustmentProposalMapper.updateActiveLimitAdjustmentProposal(activeLimitAdjustmentProposal);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteActiveLimitAdjustmentProposalByIds(String[] ids)
    {
        return activeLimitAdjustmentProposalMapper.deleteActiveLimitAdjustmentProposalByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteActiveLimitAdjustmentProposalById(String id)
    {
        return activeLimitAdjustmentProposalMapper.deleteActiveLimitAdjustmentProposalById(id);
    }
}
