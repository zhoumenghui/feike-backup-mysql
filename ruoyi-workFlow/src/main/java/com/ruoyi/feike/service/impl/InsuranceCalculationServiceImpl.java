package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.InsuranceCalculationMapper;
import com.ruoyi.feike.domain.InsuranceCalculation;
import com.ruoyi.feike.service.IInsuranceCalculationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class InsuranceCalculationServiceImpl implements IInsuranceCalculationService 
{
    @Autowired
    private InsuranceCalculationMapper insuranceCalculationMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public InsuranceCalculation selectInsuranceCalculationById(String id)
    {
        return insuranceCalculationMapper.selectInsuranceCalculationById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param insuranceCalculation feike
     * @return feike
     */
    @Override
    public List<InsuranceCalculation> selectInsuranceCalculationList(InsuranceCalculation insuranceCalculation)
    {
        return insuranceCalculationMapper.selectInsuranceCalculationList(insuranceCalculation);
    }

    /**
     * 新增feike
     * 
     * @param insuranceCalculation feike
     * @return 结果
     */
    @Override
    public int insertInsuranceCalculation(InsuranceCalculation insuranceCalculation)
    {
        return insuranceCalculationMapper.insertInsuranceCalculation(insuranceCalculation);
    }

    /**
     * 修改feike
     * 
     * @param insuranceCalculation feike
     * @return 结果
     */
    @Override
    public int updateInsuranceCalculation(InsuranceCalculation insuranceCalculation)
    {
        return insuranceCalculationMapper.updateInsuranceCalculation(insuranceCalculation);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteInsuranceCalculationByIds(String[] ids)
    {
        return insuranceCalculationMapper.deleteInsuranceCalculationByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteInsuranceCalculationById(String id)
    {
        return insuranceCalculationMapper.deleteInsuranceCalculationById(id);
    }
}
