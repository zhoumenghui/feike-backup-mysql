package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.LimitActiveProposalMapper;
import com.ruoyi.feike.domain.LimitActiveProposal;
import com.ruoyi.feike.service.ILimitActiveProposalService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class LimitActiveProposalServiceImpl implements ILimitActiveProposalService 
{
    @Autowired
    private LimitActiveProposalMapper limitActiveProposalMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public LimitActiveProposal selectLimitActiveProposalById(String id)
    {
        return limitActiveProposalMapper.selectLimitActiveProposalById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param limitActiveProposal feike
     * @return feike
     */
    @Override
    public List<LimitActiveProposal> selectLimitActiveProposalList(LimitActiveProposal limitActiveProposal)
    {
        return limitActiveProposalMapper.selectLimitActiveProposalList(limitActiveProposal);
    }

    @Override
    public List<LimitActiveProposal> selectCurrentActiveLimitExpiryDate(LimitActiveProposal limitActiveProposal)
    {
        return limitActiveProposalMapper.selectCurrentActiveLimitExpiryDate(limitActiveProposal);
    }

    /**
     * 新增feike
     * 
     * @param limitActiveProposal feike
     * @return 结果
     */
    @Override
    public int insertLimitActiveProposal(LimitActiveProposal limitActiveProposal)
    {
        return limitActiveProposalMapper.insertLimitActiveProposal(limitActiveProposal);
    }

    /**
     * 修改feike
     * 
     * @param limitActiveProposal feike
     * @return 结果
     */
    @Override
    public int updateLimitActiveProposal(LimitActiveProposal limitActiveProposal)
    {
        return limitActiveProposalMapper.updateLimitActiveProposal(limitActiveProposal);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteLimitActiveProposalByIds(String[] ids)
    {
        return limitActiveProposalMapper.deleteLimitActiveProposalByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteLimitActiveProposalById(String id)
    {
        return limitActiveProposalMapper.deleteLimitActiveProposalById(id);
    }
}
