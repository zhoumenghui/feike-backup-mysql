package com.ruoyi.feike.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.feike.domain.HDealerDm;
import com.ruoyi.feike.mapper.HDealerDmMapper;
import com.ruoyi.feike.service.IHDealerDmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 经销商与dm岗位用户映射Service业务层处理
 *
 * @author ruoyi
 * @date 2022-12-23
 */
@Service
public class HDealerDmServiceImpl implements IHDealerDmService
{
    @Autowired
    private HDealerDmMapper hDealerDmMapper;

    /**
     * 查询经销商与dm岗位用户映射
     *
     * @param id 经销商与dm岗位用户映射ID
     * @return 经销商与dm岗位用户映射
     */
    @Override
    public HDealerDm selectHDealerDmById(Long id)
    {
        return hDealerDmMapper.selectHDealerDmById(id);
    }

    /**
     * 查询经销商与dm岗位用户映射列表
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 经销商与dm岗位用户映射
     */
    @Override
    public List<HDealerDm> selectHDealerDmList(HDealerDm hDealerDm)
    {
        return hDealerDmMapper.selectHDealerDmList(hDealerDm);
    }

    /**
     * 新增经销商与dm岗位用户映射
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 结果
     */
    @Override
    public int insertHDealerDm(HDealerDm hDealerDm)
    {
        hDealerDm.setCreateTime(DateUtils.getNowDate());
        return hDealerDmMapper.insertHDealerDm(hDealerDm);
    }

    /**
     * 修改经销商与dm岗位用户映射
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 结果
     */
    @Override
    public int updateHDealerDm(HDealerDm hDealerDm)
    {
        hDealerDm.setUpdateTime(DateUtils.getNowDate());
        return hDealerDmMapper.updateHDealerDm(hDealerDm);
    }

    /**
     * 批量删除经销商与dm岗位用户映射
     *
     * @param ids 需要删除的经销商与dm岗位用户映射ID
     * @return 结果
     */
    @Override
    public int deleteHDealerDmByIds(Long[] ids)
    {
        return hDealerDmMapper.deleteHDealerDmByIds(ids);
    }

    /**
     * 删除经销商与dm岗位用户映射信息
     *
     * @param id 经销商与dm岗位用户映射ID
     * @return 结果
     */
    @Override
    public int deleteHDealerDmById(Long id)
    {
        return hDealerDmMapper.deleteHDealerDmById(id);
    }
}
