package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealercodeContractMapper;
import com.ruoyi.feike.domain.DealercodeContract;
import com.ruoyi.feike.service.IDealercodeContractService;

/**
 * 生成的合同详细信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
@Service
public class DealercodeContractServiceImpl implements IDealercodeContractService 
{
    @Autowired
    private DealercodeContractMapper dealercodeContractMapper;

    /**
     * 查询生成的合同详细信息
     * 
     * @param id 生成的合同详细信息ID
     * @return 生成的合同详细信息
     */
    @Override
    public DealercodeContract selectDealercodeContractById(Long id)
    {
        return dealercodeContractMapper.selectDealercodeContractById(id);
    }

    /**
     * 查询生成的合同详细信息列表
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 生成的合同详细信息
     */
    @Override
    public List<DealercodeContract> selectDealercodeContractList(DealercodeContract dealercodeContract)
    {
        return dealercodeContractMapper.selectDealercodeContractList(dealercodeContract);
    }

    /**
     * 新增生成的合同详细信息
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 结果
     */
    @Override
    public int insertDealercodeContract(DealercodeContract dealercodeContract)
    {
        return dealercodeContractMapper.insertDealercodeContract(dealercodeContract);
    }

    /**
     * 修改生成的合同详细信息
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 结果
     */
    @Override
    public int updateDealercodeContract(DealercodeContract dealercodeContract)
    {
        return dealercodeContractMapper.updateDealercodeContract(dealercodeContract);
    }

    /**
     * 批量删除生成的合同详细信息
     * 
     * @param ids 需要删除的生成的合同详细信息ID
     * @return 结果
     */
    @Override
    public int deleteDealercodeContractByIds(Long[] ids)
    {
        return dealercodeContractMapper.deleteDealercodeContractByIds(ids);
    }

    /**
     * 删除生成的合同详细信息信息
     * 
     * @param id 生成的合同详细信息ID
     * @return 结果
     */
    @Override
    public int deleteDealercodeContractById(Long id)
    {
        return dealercodeContractMapper.deleteDealercodeContractById(id);
    }
}
