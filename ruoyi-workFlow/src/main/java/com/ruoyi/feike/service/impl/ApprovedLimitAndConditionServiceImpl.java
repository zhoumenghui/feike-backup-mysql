package com.ruoyi.feike.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.Priority;
import com.ruoyi.common.constant.StatusReturnCodeConstant;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.DealerNegativeInformationCheck;
import com.ruoyi.feike.domain.vo.ApprovedLimitAndConditionVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.ApprovedLimitAndConditionMapper;
import com.ruoyi.feike.domain.ApprovedLimitAndCondition;
import com.ruoyi.feike.service.IApprovedLimitAndConditionService;

/**
 * feikeService业务层处理
 *
 * @author ybw
 * @date 2022-07-12
 */
@Service
public class ApprovedLimitAndConditionServiceImpl implements IApprovedLimitAndConditionService
{
    @Autowired
    private ApprovedLimitAndConditionMapper approvedLimitAndConditionMapper;

    @Autowired
    private DealerInformationServiceImpl dealerInformationService;

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public ApprovedLimitAndCondition selectApprovedLimitAndConditionById(String id)
    {
        return approvedLimitAndConditionMapper.selectApprovedLimitAndConditionById(id);
    }

    /**
     * 查询feike列表
     *
     * @param approvedLimitAndCondition feike
     * @return feike
     */
    @Override
    public List<ApprovedLimitAndCondition> selectApprovedLimitAndConditionList(ApprovedLimitAndCondition approvedLimitAndCondition)
    {
        return approvedLimitAndConditionMapper.selectApprovedLimitAndConditionList(approvedLimitAndCondition);
    }

    /**
     * 新增feike
     *
     * @param approvedLimitAndCondition feike
     * @return 结果
     */
    @Override
    public int insertApprovedLimitAndCondition(ApprovedLimitAndCondition approvedLimitAndCondition)
    {
        return approvedLimitAndConditionMapper.insertApprovedLimitAndCondition(approvedLimitAndCondition);
    }

    /**
     * 修改feike
     *
     * @param approvedLimitAndCondition feike
     * @return 结果
     */
    @Override
    public int updateApprovedLimitAndCondition(ApprovedLimitAndCondition approvedLimitAndCondition)
    {
        return approvedLimitAndConditionMapper.updateApprovedLimitAndCondition(approvedLimitAndCondition);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteApprovedLimitAndConditionByIds(String[] ids)
    {
        return approvedLimitAndConditionMapper.deleteApprovedLimitAndConditionByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteApprovedLimitAndConditionById(String id)
    {
        return approvedLimitAndConditionMapper.deleteApprovedLimitAndConditionById(id);
    }

    /**
     * 新增feike
     *
     * @param resultMap
     * @return 结果
     */
    @Override
    public int saveAlac(Map<String, Object> resultMap)
    {
        try {
            List<ApprovedLimitAndConditionVO> halac = JSONObject.parseArray(JSON.toJSONString(resultMap.get("halac")), ApprovedLimitAndConditionVO.class);
            String dealerId = (String) resultMap.get("dealerId");
            String dealerName = (String) resultMap.get("dealerName");
            if (StringUtils.isEmpty(halac)) {
                throw new BaseException("表数据不能为空！");
            }
            for(ApprovedLimitAndConditionVO alacs : halac ){

                ApprovedLimitAndCondition approvedLimitAndCondition = new ApprovedLimitAndCondition();
                String id = IdUtils.simpleUUID();
                alacs.setId(id);
                alacs.setDealerid(dealerId);
                alacs.setDealername(dealerName);
                BeanUtils.copyProperties(alacs,approvedLimitAndCondition);
                approvedLimitAndConditionMapper.insertApprovedLimitAndCondition(approvedLimitAndCondition);
            }
            return 1;
        }catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.toString());
        }
    }

}
