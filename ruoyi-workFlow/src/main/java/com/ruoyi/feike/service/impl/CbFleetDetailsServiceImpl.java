package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.CbFleetDetailsMapper;
import com.ruoyi.feike.domain.CbFleetDetails;
import com.ruoyi.feike.service.ICbFleetDetailsService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class CbFleetDetailsServiceImpl implements ICbFleetDetailsService 
{
    @Autowired
    private CbFleetDetailsMapper cbFleetDetailsMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public CbFleetDetails selectCbFleetDetailsById(String id)
    {
        return cbFleetDetailsMapper.selectCbFleetDetailsById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param cbFleetDetails feike
     * @return feike
     */
    @Override
    public List<CbFleetDetails> selectCbFleetDetailsList(CbFleetDetails cbFleetDetails)
    {
        return cbFleetDetailsMapper.selectCbFleetDetailsList(cbFleetDetails);
    }

    /**
     * 新增feike
     * 
     * @param cbFleetDetails feike
     * @return 结果
     */
    @Override
    public int insertCbFleetDetails(CbFleetDetails cbFleetDetails)
    {
        return cbFleetDetailsMapper.insertCbFleetDetails(cbFleetDetails);
    }

    /**
     * 修改feike
     * 
     * @param cbFleetDetails feike
     * @return 结果
     */
    @Override
    public int updateCbFleetDetails(CbFleetDetails cbFleetDetails)
    {
        return cbFleetDetailsMapper.updateCbFleetDetails(cbFleetDetails);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteCbFleetDetailsByIds(String[] ids)
    {
        return cbFleetDetailsMapper.deleteCbFleetDetailsByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteCbFleetDetailsById(String id)
    {
        return cbFleetDetailsMapper.deleteCbFleetDetailsById(id);
    }
}
