package com.ruoyi.feike.service.impl;

import java.util.List;

import com.github.pagehelper.util.StringUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.DealerSectorContractgroup;
import com.ruoyi.feike.mapper.DealerSectorContractgroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealerSectorLimitflagMapper;
import com.ruoyi.feike.domain.DealerSectorLimitflag;
import com.ruoyi.feike.service.IDealerSectorLimitflagService;

/**
 * FlagService业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@Service
public class DealerSectorLimitflagServiceImpl implements IDealerSectorLimitflagService 
{
    @Autowired
    private DealerSectorLimitflagMapper dealerSectorLimitflagMapper;
    @Autowired
    private DealerSectorContractgroupMapper dealerSectorContractgroupMapper;

    /**
     * 查询Flag
     * 
     * @param id FlagID
     * @return Flag
     */
    @Override
    public DealerSectorLimitflag selectDealerSectorLimitflagById(String id)
    {
        return dealerSectorLimitflagMapper.selectDealerSectorLimitflagById(id);
    }

    /**
     * 查询Flag列表
     * 
     * @param dealerSectorLimitflag Flag
     * @return Flag
     */
    @Override
    public List<DealerSectorLimitflag> selectDealerSectorLimitflagList(DealerSectorLimitflag dealerSectorLimitflag)
    {
        return dealerSectorLimitflagMapper.selectDealerSectorLimitflagList(dealerSectorLimitflag);
    }

    /**
     * 新增Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    @Override
    public int insertDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag)
    {
        return dealerSectorLimitflagMapper.insertDealerSectorLimitflag(dealerSectorLimitflag);
    }

    /**
     * 修改Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    @Override
    public int updateDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag)
    {
        return dealerSectorLimitflagMapper.updateDealerSectorLimitflag(dealerSectorLimitflag);
    }

    /**
     * 批量删除Flag
     * 
     * @param ids 需要删除的FlagID
     * @return 结果
     */
    @Override
    public int deleteDealerSectorLimitflagByIds(String[] ids)
    {
        return dealerSectorLimitflagMapper.deleteDealerSectorLimitflagByIds(ids);
    }

    /**
     * 删除Flag信息
     * 
     * @param id FlagID
     * @return 结果
     */
    @Override
    public int deleteDealerSectorLimitflagById(String id)
    {
        return dealerSectorLimitflagMapper.deleteDealerSectorLimitflagById(id);
    }

    /**
     * 筛选出flag表里正确的数据
     */
    @Override
    public void saixuanflag(){
        //添加经销商
/*        DealerSectorContractgroup dealerSectorContractgroup = new DealerSectorContractgroup();
        List<DealerSectorContractgroup> dealerSectorContractgroups = dealerSectorContractgroupMapper.selectDealerSectorContractgroupListgroupbyDealercode(dealerSectorContractgroup);
        for(DealerSectorContractgroup dealerSectorContractgroup1 : dealerSectorContractgroups){
            DealerSectorLimitflag dealerSectorLimitflag = new DealerSectorLimitflag();
            dealerSectorLimitflag.setId(IdUtils.simpleUUID());
            dealerSectorLimitflag.setDealername(dealerSectorContractgroup1.getDealername());
            dealerSectorLimitflag.setSector(dealerSectorContractgroup1.getSector());
            dealerSectorLimitflag.setNormalflag("true");
            dealerSectorLimitflag.setDemolflag("true");
            dealerSectorLimitflag.setTempflag("true");
            dealerSectorLimitflag.setMrgflag("true");
            dealerSectorLimitflag.setCbflag("true");
            dealerSectorLimitflag.setThreepartyflag("true");
            dealerSectorLimitflagMapper.insertDealerSectorLimitflag(dealerSectorLimitflag);
        }*/
        //修改flag
        DealerSectorLimitflag dealerSectorLimitflag = new DealerSectorLimitflag();
        List<DealerSectorLimitflag> dealerSectorLimitflags = dealerSectorLimitflagMapper.selectDealerSectorLimitflagList(dealerSectorLimitflag);
        for (DealerSectorLimitflag dlimt : dealerSectorLimitflags){
            DealerSectorContractgroup dealerSectorContractgroup = new DealerSectorContractgroup();
            dealerSectorContractgroup.setDealername(dlimt.getDealername());
            dealerSectorContractgroup.setSector(dlimt.getSector());
            dealerSectorContractgroup.setContractname("CB 改装车大客户延期补充协议");
            //dealerSectorContractgroup.setContractname("人民币循环贷款合同");
            List<DealerSectorContractgroup> dealerSectorContractgroups = dealerSectorContractgroupMapper.selectDealerSectorContractgroupList(dealerSectorContractgroup);
            if(StringUtils.isNotEmpty(dealerSectorContractgroups)){
                DealerSectorLimitflag dealerSectorLimitflag1 = new DealerSectorLimitflag();
                dealerSectorLimitflag1.setDealername(dlimt.getDealername());
                dealerSectorLimitflag1.setSector(dlimt.getSector());
                dealerSectorLimitflag1.setCbflag("false");
                //dealerSectorLimitflag1.setNormalflag("false");
                //dealerSectorLimitflag1.setThreepartyflag("false");
                //dealerSectorLimitflag1.setDemolflag("false");
                dealerSectorLimitflagMapper.updateDealerSectorLimitflag(dealerSectorLimitflag1);
            }
        }

    }

}
