package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.SelfplanInformationMapper;
import com.ruoyi.feike.domain.SelfplanInformation;
import com.ruoyi.feike.service.ISelfplanInformationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class SelfplanInformationServiceImpl implements ISelfplanInformationService 
{
    @Autowired
    private SelfplanInformationMapper selfplanInformationMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public SelfplanInformation selectSelfplanInformationById(String id)
    {
        return selfplanInformationMapper.selectSelfplanInformationById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param selfplanInformation feike
     * @return feike
     */
    @Override
    public List<SelfplanInformation> selectSelfplanInformationList(SelfplanInformation selfplanInformation)
    {
        return selfplanInformationMapper.selectSelfplanInformationList(selfplanInformation);
    }

    /**
     * 新增feike
     * 
     * @param selfplanInformation feike
     * @return 结果
     */
    @Override
    public int insertSelfplanInformation(SelfplanInformation selfplanInformation)
    {
        return selfplanInformationMapper.insertSelfplanInformation(selfplanInformation);
    }

    /**
     * 修改feike
     * 
     * @param selfplanInformation feike
     * @return 结果
     */
    @Override
    public int updateSelfplanInformation(SelfplanInformation selfplanInformation)
    {
        return selfplanInformationMapper.updateSelfplanInformation(selfplanInformation);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteSelfplanInformationByIds(String[] ids)
    {
        return selfplanInformationMapper.deleteSelfplanInformationByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteSelfplanInformationById(String id)
    {
        return selfplanInformationMapper.deleteSelfplanInformationById(id);
    }
}
