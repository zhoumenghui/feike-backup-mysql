package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.DealercodeContract;
import com.ruoyi.feike.domain.Plm;
import com.ruoyi.feike.domain.vo.ContractRecord;
import com.ruoyi.feike.mapper.ContractRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public interface IContractRecordSerivce {


    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public ContractRecord selectContractRecordById(Long id);


    public ContractRecord selectContractRecordThreeListById(Long id);


    /**
     * 查询【请填写功能名称】列表
     *
     * @param contractRecord 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<ContractRecord> selectContractRecordList(ContractRecord contractRecord);

    public List<ContractRecord> selectContractRecordList2(ContractRecord contractRecord);



    public List<ContractRecord> selectContractRecordThreeList(ContractRecord contractRecord);


    /**
     * 新增【请填写功能名称】
     *
     * @param contractRecord 【请填写功能名称】
     * @return 结果
     */
    public int insertContractRecord(ContractRecord contractRecord);


    public int insertContractRecordThree(ContractRecord contractRecord);

    /**
     * 修改【请填写功能名称】
     *
     * @param contractRecord 【请填写功能名称】
     * @return 结果
     */
    public int updateContractRecord(ContractRecord contractRecord);

    public int updateContractRecordThree(ContractRecord contractRecord);

    public int updateContractRecordThree2(ContractRecord contractRecord);


    public int updateContractRecord2(ContractRecord contractRecord);


    /**
     * 批量删除【请填写功能名称】
     *
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteContractRecordByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteContractRecordById(Long id);

    public int update(DealercodeContract dealercodeContract);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<ContractRecord> userList, Boolean isUpdateSupport, String operName);

}
