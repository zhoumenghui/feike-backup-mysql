package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.WholesalePerformanceCurrently;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface IWholesalePerformanceCurrentlyService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public WholesalePerformanceCurrently selectWholesalePerformanceCurrentlyById(String id);

    /**
     * 查询feike列表
     * 
     * @param wholesalePerformanceCurrently feike
     * @return feike集合
     */
    public List<WholesalePerformanceCurrently> selectWholesalePerformanceCurrentlyList(WholesalePerformanceCurrently wholesalePerformanceCurrently);

    /**
     * 新增feike
     * 
     * @param wholesalePerformanceCurrently feike
     * @return 结果
     */
    public int insertWholesalePerformanceCurrently(WholesalePerformanceCurrently wholesalePerformanceCurrently);

    /**
     * 修改feike
     * 
     * @param wholesalePerformanceCurrently feike
     * @return 结果
     */
    public int updateWholesalePerformanceCurrently(WholesalePerformanceCurrently wholesalePerformanceCurrently);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteWholesalePerformanceCurrentlyByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteWholesalePerformanceCurrentlyById(String id);
}
