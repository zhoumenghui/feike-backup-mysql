package com.ruoyi.feike.service;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.feike.domain.*;

import java.util.List;
import java.util.Map;


/**
 * @author lss
 * @version 1.0
 * @description: 新经销商申请接口类
 * @date 2022/8/2 15:07
 */
public interface IDepositDecreaseService {

    /**
    * @description: 新增降低保证金比例申请审批流程接口
    * @param: newApplicationDTO
    * @return: AjaxResult
    * @author lss
    * @date: 2022/8/2 15:15
    */
    AjaxResult insertActApprove(NewApplicationDTO newApplicationDTO);

    AjaxResult getForignList(DepositDecreaseDTO depositDecreaseDTO);

    Map<String, Object> getForeignInfo(LimitCalculationSearchDTO dto);

    List<Map<String,Object>> salesPerformanceAndTarget(SalesPerformanceAndTargetDTO dto);

    List<Map<String, Object>> wholesalePerformanceRecentMonthsOs(WholesalePerformanceRecentMonthsOsDTO dto);

    List<Map<String, Object>> loyaltyPerformanceRecentQuarters(LoyaltyPerformanceRecentDTO dto);



    /**
    * @description: 获取WHOLESALE PERFORMANCE-recent 3 months O/S外部数据源
    * @param:
    * @return:
    * @author
    * @date: 2022/8/18 17:44
    */

    List<Map<String,Object>> performanceRecentThreemonth(DepositDecreaseDTO depositDecreaseDTO);
    List<LoyaltyPerformance6month> loyaltyPerformanceRecentMonths(LoyaltyPerformanceRecentDTO dto);

    @DataSource(value = DataSourceType.SLAVE)
    List<LoyaltyPerformance6month> loyaltyPerformanceRecentMonthsRePenContract(LoyaltyPerformanceRecentDTO dto);
}
