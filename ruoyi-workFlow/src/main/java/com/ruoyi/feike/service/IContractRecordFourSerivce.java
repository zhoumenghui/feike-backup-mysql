package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.vo.ContractRecordFour;

import java.util.List;

public interface IContractRecordFourSerivce {

    public List<ContractRecordFour> selectContractRecordFourList(ContractRecordFour contractRecordFour);


    public int insertContractRecordFour(ContractRecordFour contractRecordFour);


}
