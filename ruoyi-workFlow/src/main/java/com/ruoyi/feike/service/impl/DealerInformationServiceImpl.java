package com.ruoyi.feike.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.InstanceIdUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;

import com.ruoyi.feike.domain.vo.DealerInformationVO;
import com.ruoyi.feike.domain.vo.DealerformationVO;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IAnnualReviewyService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.service.IDealerInformationService;
import org.springframework.transaction.annotation.Transactional;

/**
 * feikeService业务层处理
 *
 * @author ybw
 * @date 2022-07-12
 */
@Service
@Slf4j
public class DealerInformationServiceImpl implements IDealerInformationService
{
    @Autowired
    private DealerInformationMapper dealerInformationMapper;
    @Autowired
    private DealerSectorLimitflagMapper dealerSectorLimitflagMapper;

    @Autowired
    private InstanceIdUtil instanceIdUtil;

    @Autowired
    private IDealerInformationService dealerInformationService;
    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;
    @Autowired
    private BasicInformationMapper basicInformationMapper;
    @Autowired
    private DealerSectorContractgroupMapper dealerSectorContractgroupMapper;
    @Autowired
    private ParkingLocationMapper parkingLocationMapper;
    @Autowired
    private PlmMapper plmMapper;
    @Autowired
    private HDealerDmMapper dealerDmMapper;

    @Autowired
    private HChangeLegalPersonMapper changeLegalPersonMapper;

    @Autowired
    private IAnnualReviewyService annualReviewyService;


    @Value("${wfs.url}")
    private String wfsUrl;

    /**
     * 查询feike
     * @param id feikeID
     * @return feike
     */
    @Override
    public DealerInformation selectDealerInformationById(Long id)
    {
        return dealerInformationMapper.selectDealerInformationById(id);
    }

    /**
     * 根据dealername查
     *
     * @param map
     * @return feike
     */
    @Override
    public DealerInformationVO selectDealerInformationByname(Map<String, Object> map)
    {
        String dealerName = (String)map.get("dealername");
        DealerInformationVO dealerformationVO = new DealerInformationVO();
        List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByname(dealerName);
        HDealerDm hDealerDm = dealerDmMapper.selectHDealerDmByDealerName(dealerName);
        if(hDealerDm!=null){
            dealerformationVO.setDmName(hDealerDm.getDmName());
        }
        dealerformationVO.setId(dealerInformations.get(0).getId());
        dealerformationVO.setDealerNameEN(dealerInformations.get(0).getEnName());
        dealerformationVO.setDealerNameCN(dealerInformations.get(0).getDealerName());
        dealerformationVO.setGroupNameCN(dealerInformations.get(0).getGroupNameCN());
        dealerformationVO.setGroupNameEN(dealerInformations.get(0).getGroupName());
        dealerformationVO.setIndividualGuarantorCn(dealerInformations.get(0).getIndividualGuarantorCn());
        dealerformationVO.setIndividualGuarantorEn(dealerInformations.get(0).getIndividualGuarantorEn());
        dealerformationVO.setCorporateGuarantorEn(dealerInformations.get(0).getCorporateGuarantorEn());
        dealerformationVO.setCorporateGuarantorCn(dealerInformations.get(0).getCorporateGuarantorCn());
        dealerformationVO.setShareholderStructure(dealerInformations.get(0).getShareholderStructure());
        dealerformationVO.setRegisteredCapitalCn(dealerInformations.get(0).getRegisteredCapitalCn());
        dealerformationVO.setRegisteredCapitalEn(dealerInformations.get(0).getRegisteredCapitalEn());
        dealerformationVO.setRegisteredUrbanCn(dealerInformations.get(0).getRegisteredUrbanCn());
        dealerformationVO.setRegisteredUrbanEn(dealerInformations.get(0).getRegisteredUrbanEn());
        //TODO
        dealerformationVO.setRegisteredAddressCN(dealerInformations.get(0).getRegisteredAddressCN());
        dealerformationVO.setRegisteredAddressEN(dealerInformations.get(0).getRegisteredAddressEN());
        dealerformationVO.setLegalRepresentativeCN(dealerInformations.get(0).getLegalRepresentativeCN());
        dealerformationVO.setLegalRepresentativeEN(dealerInformations.get(0).getLegalRepresentativeEN());
        dealerformationVO.setIncorporationDate(dealerInformations.get(0).getIncorporationDate());
        dealerformationVO.setJoinSectorDealerNetworkDate(dealerInformations.get(0).getJoinSectorDealerNetworkDate());
        dealerformationVO.setUniformCreditCode(dealerInformations.get(0).getUniformCreditCode());
        dealerformationVO.setPaidUpCapital(dealerInformations.get(0).getPaidUpCapital());
        dealerformationVO.setGm(dealerInformations.get(0).getGm());
        dealerformationVO.setRelatedWorkingExperience(dealerInformations.get(0).getRelatedWorkingExperience());

        dealerformationVO.setProvince(dealerInformations.get(0).getProvince());
        dealerformationVO.setProvinceEN(dealerInformations.get(0).getProvinceEN());
        dealerformationVO.setCity(dealerInformations.get(0).getCity());
        dealerformationVO.setPeopleSum(dealerInformations.get(0).getPeopleSum());
        dealerformationVO.setEnterpriseType(dealerInformations.get(0).getEnterpriseType());
        dealerformationVO.setDownLoanAccount(dealerInformations.get(0).getDownLoanAccount());

        List<Map<String, Object>> setCorporateGuarantorJson = new ArrayList<>();
        String CorporateGuarantorJson = dealerInformations.get(0).getCorporateGuarantorJson();
        if(StringUtils.isNotEmpty(CorporateGuarantorJson)){
            String[] CorporateGuarantor = CorporateGuarantorJson.split(";");
            for(String co: CorporateGuarantor){
                Map<String, Object> CorporateGuarantorJsonMap = new HashMap<>();
                String[] CorporateGuarantorStr = co.split("#");
                if(CorporateGuarantorStr!=null){
                    int length = CorporateGuarantorStr.length;
                    if(length ==4){
                        CorporateGuarantorJsonMap.put("proposalNameCN",CorporateGuarantorStr[0]);
                        CorporateGuarantorJsonMap.put("guarantorIndustry",CorporateGuarantorStr[1]);
                        CorporateGuarantorJsonMap.put("guarantorRegionCode",CorporateGuarantorStr[2]);
                        CorporateGuarantorJsonMap.put("guarantorScale",CorporateGuarantorStr[3]);
                    }
                }
                setCorporateGuarantorJson.add(CorporateGuarantorJsonMap);
            }
        }else{
            //查询担保公司信息
            String corporateGuarantorCn = dealerInformations.get(0).getCorporateGuarantorCn();
            if(StringUtils.isNotEmpty(corporateGuarantorCn)){
                String[] corporate = corporateGuarantorCn.split("#");
                for (String s : corporate) {
                    Map<String, Object> CorporateGuarantorJsonMap = new HashMap<>();
                    CorporateGuarantorJsonMap.put("proposalNameCN",s);
                    setCorporateGuarantorJson.add(CorporateGuarantorJsonMap);
                }
            }

        }
        List<Map<String, Object>> cityJson = new ArrayList<>();
        Map<String, Object> cityJsonMap = new HashMap<>();
        if(dealerInformations.get(0).getProvince() != null ){
            cityJsonMap.put("province",dealerInformations.get(0).getProvince());
        }else{
            cityJsonMap.put("province","");
        }
        if(dealerInformations.get(0).getCity() != null ){
            cityJsonMap.put("city",dealerInformations.get(0).getCity());
        }else{
            cityJsonMap.put("city","");
        }
        if(dealerInformations.get(0).getPeopleSum() != null ){
            cityJsonMap.put("peopleSum",dealerInformations.get(0).getPeopleSum());
        }else{
            cityJsonMap.put("peopleSum","");
        }
        if(dealerInformations.get(0).getEnterpriseType() != null ){
            cityJsonMap.put("enterpriseType",dealerInformations.get(0).getEnterpriseType());
        }else{
            cityJsonMap.put("enterpriseType","");
        }
        if(dealerInformations.get(0).getDownLoanAccount() != null ){
            cityJsonMap.put("downLoanAccount",dealerInformations.get(0).getDownLoanAccount());
        }else{
            cityJsonMap.put("downLoanAccount","");
        }
        cityJson.add(cityJsonMap);
        dealerformationVO.setCorporateGuarantorJson(setCorporateGuarantorJson);
        dealerformationVO.setCityJson(cityJson);
        List<Map<String, Object>> setShareholdersAndShares = new ArrayList<>();
        String shareholderStructure = dealerInformations.get(0).getShareholderStructure();
        String[] split = shareholderStructure.split(";");
        for(String sp: split){
            Map<String, Object> objectObjectHashMap = new HashMap<>();
            ArrayList<String> objects = new ArrayList<>();
            StringTokenizer stringTokenizer = new StringTokenizer(sp, "#:");
            while(stringTokenizer.hasMoreElements()){
                objects.add(stringTokenizer.nextToken());
            }
            if(objects.size()==3) {
                objectObjectHashMap.put("shareholdersCn", objects.get(1));
                objectObjectHashMap.put("shareholdersEn", objects.get(0));
                objectObjectHashMap.put("share", objects.get(2));
            }

            setShareholdersAndShares.add(objectObjectHashMap);
        }

        dealerformationVO.setShareholdersAndShare(setShareholdersAndShares);
        ArrayList<String> sectors = new ArrayList<>();
        ArrayList<String> dealerCodes = new ArrayList<>();
        ArrayList<String> dealerCodeFromManufacturer = new ArrayList<>();
        for (int i1 = 0; i1 < dealerInformations.size(); i1++) {
            String make = dealerInformations.get(i1).getMake();
            String dealerCode = dealerInformations.get(i1).getDealerCode();
            String dealerCodetwo = dealerInformations.get(i1).getDealerCodeFromManufacturer();
            if(StringUtils.isNotEmpty(make)){
                sectors.add(make);
            }
            if(StringUtils.isNotEmpty(dealerCode)){
                dealerCodes.add(dealerCode);
            }
            if(StringUtils.isNotEmpty(dealerCodetwo)){
                dealerCodeFromManufacturer.add(dealerCodetwo);
            }
        }
        dealerformationVO.setSector(sectors);
        dealerformationVO.setDealerCodeFromWFS(dealerCodes);
        dealerformationVO.setDealerCodeFromManufacturer(dealerCodeFromManufacturer);
        return dealerformationVO;
    }

    /**
     * 查询feike列表
     *
     * @param dealerInformation feike
     * @return feike
     */
    @Override
    public List<DealerInformation> selectDealerInformationList(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.selectDealerInformationList(dealerInformation);
    }

    @Override
    public List<DealerInformation> selectDealerInformationList2(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.selectDealerInformationList2(dealerInformation);
    }

    @Override
    public List<DealerInformation> selectDealerInformationList3(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.selectDealerInformationList3(dealerInformation);
    }

    @Override
    public List<DealerInformation> selectDealerInformationListnew(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.selectDealerInformationListnew(dealerInformation);
    }

    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    @Override
    @Transactional
    public int insertDealerInformation(DealerInformationVO dealerInformation)
    {
        List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByname(dealerInformation.getDealerNameCN());
        if(StringUtils.isNotEmpty(dealerInformations)){
            throw new BaseException("经销商名称重复");
        }
        if(dealerInformation.getSector()!=null && dealerInformation.getDealerCodeFromManufacturer() !=null && dealerInformation.getDealerCodeFromWFS()!=null){

            if(dealerInformation.getSector().size() != dealerInformation.getDealerCodeFromManufacturer().size() || dealerInformation.getDealerCodeFromManufacturer().size() != dealerInformation.getDealerCodeFromWFS().size()){
                throw  new BaseException("Dealer Code From Manufacturer必须跟Dealer Code From WFS和Sector对应");
            }
            if(dealerInformation.getDealerCodeFromWFS().size() != dealerInformation.getDealerCodeFromWFS().stream().distinct().count() ){
                throw  new BaseException("Dealer Code From WFS里输入的数值不能重复");
            }
            List dealerCodeFromWFS1 = dealerInformation.getDealerCodeFromWFS();
            for(Object delaercode : dealerCodeFromWFS1){
                if(delaercode.toString().length() != 6){
                    throw  new BaseException("dealercode必须是六位");
                }
                List<DealerInformation> dealerInformationsTemp = dealerInformationMapper.selectDealerInformationByCode(delaercode.toString());
                if(StringUtils.isNotEmpty(dealerInformationsTemp)){
                    throw new BaseException("dealerCode:"+ delaercode+"重复");
                }
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        List<Map<String, Object>> shareholdersAndShare = dealerInformation.getShareholdersAndShare();
        for (Map<String, Object> map:shareholdersAndShare){
            stringBuilder.append(map.get("shareholdersEn")).append("#").append(map.get("shareholdersCn")).append(":").append(map.get("share")).append(";");
        }
        String shas = (stringBuilder.substring(0, stringBuilder.length() - 1)).toString();
        DealerInformation dealerInformation1 = new DealerInformation();
        BeanUtils.copyProperties(dealerInformation,dealerInformation1);
        dealerInformation1.setGroupName(dealerInformation.getGroupNameEN());
        dealerInformation1.setShareholderStructure(shas);
        dealerInformation1.setDealerName(dealerInformation.getDealerNameCN());
        dealerInformation1.setEnName(dealerInformation.getDealerNameEN());
        //多个相同的dealername对应一个同一个编号（编号用途，合同拼合同号的时候需要这个值来拼接）
//        Integer dealerInformation2= dealerInformationMapper.maxSerialNumber();
//        Integer dealerInformation2= dealerInformationMapper.maxid();
//        DealerInformation dealerInformation3 = dealerInformationMapper.selectDealerInformationById(dealerInformation2.longValue());
////        dealerInformation1.setSerialNumber(dealerInformation2.longValue()+1);
//        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
//        String dateNowStr = sdf.format(new Date());
//        if(dealerInformation3.getSerialNumber().longValue()+1>999 || dateNowStr.equals("01-01")){
//            dealerInformation1.setSerialNumber((long)1);
//        }else {
//            dealerInformation1.setSerialNumber(dealerInformation3.getSerialNumber().longValue()+1);
//        }

        //判断DM 是否有记录没有就新增
        HDealerDm hDealerDm = dealerDmMapper.selectHDealerDmByDealerName(dealerInformation.getDealerNameCN());
        if(hDealerDm==null){
            String uwName = instanceIdUtil.getRedisUW("uwName");
            String usetName ="";
            switch (uwName)
            {
                case "1":
                    usetName = "Evan Zhu";
                    break;
                case "2":
                    usetName = "Sheldon Liu";
                    break;
                case "3":
                    usetName = "Terry Zhou";
                    break;
                case "4":
                    usetName = "Lyn Xu";
                    break;
                default:
                    usetName = "Evan Zhu";
                    break;
            }
            HDealerDm dealerDm = new HDealerDm();
            dealerDm.setDmName(dealerInformation.getDmName());
            dealerDm.setCreateTime(DateUtils.getNowDate());
            dealerDm.setDealerName(dealerInformation.getDealerNameCN());
          //  dealerDm.setUwName(usetName);
            dealerDmMapper.insertHDealerDm(dealerDm);
        }


        List dealerCodeFromWFS = dealerInformation.getDealerCodeFromManufacturer();
        for (int i = 0; i <dealerCodeFromWFS.size() ; i++) {
//            dealerInformation1.setId(IdUtils.simpleUUID());
            dealerInformation1.setDealerName(dealerInformation.getDealerNameCN());
            dealerInformation1.setEnName(dealerInformation.getDealerNameEN());
            dealerInformation1.setDealerCodeFromManufacturer(dealerCodeFromWFS.get(i).toString());
            if(StringUtils.isNotEmpty(dealerInformation.getSector())) {
                dealerInformation1.setMake(dealerInformation.getSector().get(i).toString());
            }
            if(StringUtils.isNotEmpty(dealerInformation.getDealerCodeFromWFS())){
                dealerInformation1.setDealerCode(dealerInformation.getDealerCodeFromWFS().get(i).toString());
            }

            dealerInformationMapper.insertDealerInformation(dealerInformation1);
            annualReviewyService.saveDbLog("1","新增",null,null,null,dealerInformation1,"Dealer Info/新增");

        }
//        if(StringUtils.isNotEmpty(dealerInformation.getSector())) {
//            for (Object x : dealerInformation.getSector()) {
//                DealerSectorLimitflag dealerSectorLimitflag = new DealerSectorLimitflag();
//                dealerSectorLimitflag.setId(IdUtils.simpleUUID());
//                dealerSectorLimitflag.setDealername(dealerInformation.getDealerNameCN());
//                dealerSectorLimitflag.setSector(x.toString());
//                dealerSectorLimitflag.setNormalflag("true");
//                dealerSectorLimitflag.setDemolflag("true");
//                dealerSectorLimitflag.setTempflag("true");
//                dealerSectorLimitflag.setMrgflag("true");
//                dealerSectorLimitflag.setCbflag("true");
//                dealerSectorLimitflag.setThreepartyflag("true");
//                dealerSectorLimitflagMapper.insertDealerSectorLimitflag(dealerSectorLimitflag);
//            }
//        }

        return 1;
    }

    /**
     * 修改feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    @Override
    public int updateDealerInformation(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.updateDealerInformation(dealerInformation);
    }



    /**
     * 修改dealername相同的全部数据feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    @Override
    @Transactional
    public int updateDealerInformationnew(DealerInformationVO dealerInformation)
    {
        if(dealerInformation.getSector().size() != dealerInformation.getDealerCodeFromManufacturer().size() || dealerInformation.getDealerCodeFromManufacturer().size() != dealerInformation.getDealerCodeFromWFS().size()){
            throw  new BaseException("Dealer Code From Manufacturer必须跟Dealer Code From WFS和Sector对应");
        }
        if(dealerInformation.getDealerCodeFromWFS().size() != dealerInformation.getDealerCodeFromWFS().stream().distinct().count()){
            throw  new BaseException("Dealer Code From WFS里输入的数值不能重复");
        }
        List dealerCodeFromWFS = dealerInformation.getDealerCodeFromWFS();
        for(Object delaercode : dealerCodeFromWFS){
            if(delaercode.toString().length() != 6){
                throw  new BaseException("dealercode必须是六位");
            }
        }

        //根据旧经销商名称查之前的数据
        List<DealerInformation> dealerInformationsHistory = dealerInformationMapper.selectDealerInformationByDealerName(dealerInformation.getNewdealername());
        //法人中文变更
        if(dealerInformation !=null && dealerInformation.getLegalRepresentativeCN()!=null){
            if(!StringUtils.isEmpty(dealerInformation.getLegalRepresentativeCN())){
                //判断和历史是否相等
                String outLegalRepresentativeCN = null;
                for (DealerInformation information : dealerInformationsHistory) {
                    if(information.getLegalRepresentativeCN()!=null){
                        outLegalRepresentativeCN = information.getLegalRepresentativeCN();
                        break;
                    }
                }
                if(!dealerInformation.getLegalRepresentativeCN().equals(outLegalRepresentativeCN)){
                    //记录信息
                    HChangeLegalPerson hChangeLegalPerson = new HChangeLegalPerson();
                    hChangeLegalPerson.setDealerName(dealerInformation.getNewdealername());
                    hChangeLegalPerson.setLegalRepresentativeCN(outLegalRepresentativeCN);
                    hChangeLegalPerson.setIsSuccess(0);
                    List<HChangeLegalPerson> hChangeLegalPeople = changeLegalPersonMapper.selectHChangeLegalPersonList(hChangeLegalPerson);
                    if(hChangeLegalPeople == null || hChangeLegalPeople.size()==0){
                        hChangeLegalPerson.setCreateTime(DateUtils.getNowDate());
                        changeLegalPersonMapper.insertHChangeLegalPerson(hChangeLegalPerson);
                        annualReviewyService.saveDbLog("1","新增法人变更记录",null,null,null,hChangeLegalPerson,"Dealer Info/修改");

                    }
                }
            }
        }
        //查该经销商没有结束的流程数量
        int i1 = basicInformationMapper.deleteBasicInformationAnnualDealername(dealerInformation.getNewdealername());
        String formerGroupNameCn = "";
        String formerGroupNameEn = "";
        String groupNameCn = "";
        String groupNameEn = "";
        String newdealername = "";
        String newdealernameEN = "";
        if(StringUtils.isNotEmpty(dealerInformation.getNewgroupnameCn())){
            formerGroupNameCn = dealerInformation.getNewgroupnameCn();
        }
        if(StringUtils.isNotEmpty(dealerInformation.getNewgroupnameEn())){
            formerGroupNameEn = dealerInformation.getNewgroupnameEn();
        }
        if(StringUtils.isNotEmpty(dealerInformation.getGroupNameCN())){
            groupNameCn = dealerInformation.getGroupNameCN();
        }
        if(StringUtils.isNotEmpty(dealerInformation.getGroupNameEN())){
            groupNameEn = dealerInformation.getGroupNameEN();
        }
        if(StringUtils.isNotEmpty(dealerInformation.getNewdealername())){
            newdealername = dealerInformation.getNewdealername();
        }

        if(i1 > 0 && !newdealername.equals(dealerInformation.getDealerNameCN()) || i1 > 0 && !dealerInformation.getSector().equals(dealerInformation.getNewsector()) || i1 > 0 && !dealerInformation.getDealerCodeFromWFS().equals(dealerInformation.getNewdealerCodeFromWFS()) || i1 > 0 && (!formerGroupNameCn.equals(groupNameCn) || !formerGroupNameEn.equals(groupNameEn))){
            throw  new BaseException("该经销商有正在进行的流程，不准修改dealerName、dealerCode、sector、group");
        }
        //查该经销商没有结束的流程对应basicinfo表的信息
        List<BasicInformation> basicInformations = basicInformationMapper.deleteBasicInformationAnnualDealernameListAndcaogao(dealerInformation.getNewdealername());
        //当他修改了group的时候，就需要对pakinglocation进行修改，修改他们的group
        if(!formerGroupNameCn.equals(groupNameCn) || !formerGroupNameEn.equals(groupNameEn)) {
            Map<String, Object> map3 = new HashMap<>();
            map3.put("dealername", dealerInformation.getNewdealername());
            map3.put("groupNameCn", groupNameCn);
            map3.put("groupNameEn", groupNameEn);
            parkingLocationMapper.updateParkingLocationDealername(map3);
        }
        if(StringUtils.isNotEmpty(basicInformations)) {
            for (BasicInformation bis : basicInformations) {
                BasicInformation basicInformation = new BasicInformation();
                basicInformation.setId(bis.getId());
                basicInformation.setGroupNameEN(dealerInformation.getGroupNameEN());
                basicInformation.setGroupNameCN(dealerInformation.getGroupNameCN());
//                if(dealerInformation.getGroupNameCN()==null||dealerInformation.getGroupNameCN().equals("")){
//                    basicInformation.setGroupNameCN(null);
//                }
                basicInformation.setDealerNameCN(dealerInformation.getDealerNameCN());
                basicInformation.setDealerNameEN(dealerInformation.getDealerNameEN());
                basicInformation.setDealerCodeFromManufacturer(dealerInformation.getDealerCodeFromManufacturer().toString());
                StringBuilder stringBuilders = new StringBuilder();
                List<Map<String, Object>> shareholdersAndShare = dealerInformation.getShareholdersAndShare();
                for (Map<String, Object> map:shareholdersAndShare){
                    stringBuilders.append(map.get("shareholdersCn")).append("$|").append(map.get("shareholdersEn")).append("$|").append(map.get("share")).append("#");
                }
                String shas = (stringBuilders.substring(0, stringBuilders.length() - 1)).toString();
                basicInformation.setShareholders(shas);
                if(dealerInformation.getIncorporationDate() == null || dealerInformation.getIncorporationDate().equals("")){
                    basicInformation.setIncorporationDate(null);
                }else {
                    basicInformation.setIncorporationDate(DateUtils.parseDateToStr("yyyy-MM-dd",dealerInformation.getIncorporationDate()));
                }
                if(dealerInformation.getJoinSectorDealerNetworkDate() == null || dealerInformation.getJoinSectorDealerNetworkDate().equals("")){
                    basicInformation.setJoinSectorDealerNetworkDate(null);
                }else {
                    basicInformation.setJoinSectorDealerNetworkDate(DateUtils.parseDateToStr("yyyy-MM-dd",dealerInformation.getJoinSectorDealerNetworkDate()));
                }
                basicInformation.setRegisteredCapital(dealerInformation.getRegisteredCapitalEn());
                basicInformation.setPaidupCapital(dealerInformation.getPaidUpCapital());
                basicInformation.setGm(dealerInformation.getGm());
                basicInformation.setRelatedWorkingExperience(Convert.toInt(dealerInformation.getRelatedWorkingExperience()));
                basicInformation.setLegalRepresentativeCN(dealerInformation.getLegalRepresentativeCN());
                basicInformation.setLegalRepresentativeEN(dealerInformation.getLegalRepresentativeEN());
                basicInformation.setRegisteredAddressCN(dealerInformation.getRegisteredAddressCN());
                basicInformation.setRegisteredAddressEN(dealerInformation.getRegisteredAddressEN());
                basicInformationMapper.updateBasicInformationAndDealerinfor(basicInformation);
            }
        }

        //根据之前的经销商名称查dealerinfo数据
//        List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByDealerName(dealerInformation.getNewdealername());
//        for(DealerInformation di : dealerInformations){
//            //判断这个数据在flag表里是否存在
//            DealerSectorLimitflag dealerSectorLimitflag = new DealerSectorLimitflag();
//            dealerSectorLimitflag.setDealername(di.getDealerName());
//            dealerSectorLimitflag.setSector(di.getMake());
//            List<DealerSectorLimitflag> dealerSectorLimitflags = dealerSectorLimitflagMapper.selectDealerSectorLimitflagList(dealerSectorLimitflag);
//            if(StringUtils.isEmpty(dealerSectorLimitflags)){
//                //当他为空的时候，说明没在flag表里加过，那就需要给他加进去
//                DealerSectorLimitflag dealerSectorLimitflagnew = new DealerSectorLimitflag();
//                dealerSectorLimitflagnew.setId(IdUtils.simpleUUID());
//                dealerSectorLimitflagnew.setDealername(di.getDealerName());
//                dealerSectorLimitflagnew.setSector(di.getMake());
//                dealerSectorLimitflagnew.setNormalflag("true");
//                dealerSectorLimitflagnew.setDemolflag("true");
//                dealerSectorLimitflagnew.setTempflag("true");
//                dealerSectorLimitflagnew.setMrgflag("true");
//                dealerSectorLimitflagnew.setCbflag("true");
//                dealerSectorLimitflagnew.setCbonefleetflag("true");
//                dealerSectorLimitflagnew.setThreepartyflag("true");
//                dealerSectorLimitflagMapper.insertDealerSectorLimitflag(dealerSectorLimitflagnew);
//            }
//        }


        StringBuilder stringBuilder = new StringBuilder();
        List<Map<String, Object>> shareholdersAndShare = dealerInformation.getShareholdersAndShare();
        for (Map<String, Object> map:shareholdersAndShare){
            Object shareholdersEn = map.get("shareholdersEn") == null ? "" : map.get("shareholdersEn");
            Object shareholdersCn = map.get("shareholdersCn") == null ? "" : map.get("shareholdersCn");
            Object share = map.get("share") == null ? "" : map.get("share");
            stringBuilder.append(shareholdersEn).append("#").append(shareholdersCn).append(":").append(share).append(";");
        }
        String shas = (stringBuilder.substring(0, stringBuilder.length() - 1)).toString();

        DealerInformation dealerInformation1 = new DealerInformation();
        BeanUtils.copyProperties(dealerInformation,dealerInformation1);
        //先删后插
        for (DealerInformation information : dealerInformationsHistory) {
            annualReviewyService.saveDbLog("2","删除经销商信息",null,null,information,null,"Dealer Info/修改");
        }
        dealerInformationMapper.deleteDealerInformationByName(dealerInformation.getNewdealername());

        if(dealerInformation.getCorporateGuarantorJson()!=null){
            List<Map<String, Object>> corporateGuarantorJson = dealerInformation.getCorporateGuarantorJson();
            StringBuffer CorporateGuarantorJson = new StringBuffer();
            for (Map<String, Object> stringObjectMap : corporateGuarantorJson) {
                String proposalNameCN = (String)stringObjectMap.get("proposalNameCN");
                String guarantorIndustry = (String)stringObjectMap.get("guarantorIndustry");
                String guarantorRegionCode = (String)stringObjectMap.get("guarantorRegionCode");
                String guarantorScale = (String)stringObjectMap.get("guarantorScale");
                if(StringUtils.isNotEmpty(proposalNameCN)&& StringUtils.isNotEmpty(guarantorIndustry) && StringUtils.isNotEmpty(guarantorRegionCode) && StringUtils.isNotEmpty(guarantorScale)){
                    CorporateGuarantorJson.append(proposalNameCN).append("#");
                    CorporateGuarantorJson.append(guarantorIndustry).append("#");
                    CorporateGuarantorJson.append(guarantorRegionCode).append("#");
                    CorporateGuarantorJson.append(guarantorScale).append(";");
                }
            }
            if(StringUtils.isNotEmpty(CorporateGuarantorJson)){
                CorporateGuarantorJson.deleteCharAt(CorporateGuarantorJson.length()-1);
                dealerInformation1.setCorporateGuarantorJson(CorporateGuarantorJson.toString());
            }

        }

        if(dealerInformation.getCityJson()!=null){
            List<Map<String, Object>> cityJson = dealerInformation.getCityJson();
            StringBuffer CorporateGuarantorJson = new StringBuffer();
            for (Map<String, Object> stringObjectMap : cityJson) {
                String province = (String)stringObjectMap.get("province");
                if(StringUtils.isNotEmpty(province)){
                    dealerInformation1.setProvince(province);
                }else{
                    dealerInformation1.setProvince(null);
                }

                String city = (String)stringObjectMap.get("city");
                if(StringUtils.isNotEmpty(city)){
                    dealerInformation1.setCity(city);
                }else{
                    dealerInformation1.setCity(null);
                }
                String peopleSum = (String)stringObjectMap.get("peopleSum");
                if(StringUtils.isNotEmpty(peopleSum)){
                    dealerInformation1.setPeopleSum(peopleSum);
                }else{
                    dealerInformation1.setPeopleSum(null);
                }
                String enterpriseType = (String)stringObjectMap.get("enterpriseType");
                if(StringUtils.isNotEmpty(enterpriseType)){
                    dealerInformation1.setEnterpriseType(enterpriseType);
                }else{
                    dealerInformation1.setEnterpriseType(null);
                }
                String downLoanAccount = (String)stringObjectMap.get("downLoanAccount");
                if(StringUtils.isNotEmpty(downLoanAccount)){
                    dealerInformation1.setDownLoanAccount(downLoanAccount);
                }else{
                    dealerInformation1.setDownLoanAccount(null);
                }

            }
        }
        dealerInformation1.setId(null);
        if(dealerInformation.getGroupNameEN() == null || !dealerInformation.getGroupNameEN().equals("")){
            dealerInformation1.setGroupName(dealerInformation.getGroupNameEN());
        }
        if(dealerInformation.getGroupNameCN()==null || dealerInformation.getGroupNameCN().equals("")){
            dealerInformation1.setGroupNameCN(null);
        }
        dealerInformation1.setShareholderStructure(shas);
        if(dealerInformation.getDealerNameCN() == null || !dealerInformation.getDealerNameCN().equals("")){
            dealerInformation1.setDealerName(dealerInformation.getDealerNameCN());
        }
        if(dealerInformation.getDealerNameEN() == null || !dealerInformation.getDealerNameEN().equals("")){
            dealerInformation1.setEnName(dealerInformation.getDealerNameEN());
        }
        for (int i = 0; i < dealerInformation.getSector().size() ; i++) {
            if(StringUtils.isNotEmpty(dealerInformation.getDealerCodeFromManufacturer())){
                Object o = dealerInformation.getDealerCodeFromManufacturer().get(i);
                dealerInformation1.setDealerCodeFromManufacturer(o.toString());
            }else {
                dealerInformation1.setDealerCodeFromManufacturer("");
            }
            if(StringUtils.isNotEmpty(dealerInformation.getDealerCodeFromWFS())){
                Object o = dealerInformation.getDealerCodeFromWFS().get(i);
                dealerInformation1.setDealerCode(o.toString());
            }else {
                dealerInformation1.setDealerCodeFromManufacturer("");
            }
            if(StringUtils.isNotEmpty(dealerInformation.getSector())){
                Object o = dealerInformation.getSector().get(i);
                dealerInformation1.setMake(o.toString());
            }else {
                dealerInformation1.setMake("");
            }
            if(StringUtils.isNotEmpty(dealerInformationsHistory)) {
                if(dealerInformationsHistory.size() > i){
                    dealerInformation1.setFormerDealerName(dealerInformationsHistory.get(i).getFormerDealerName());
                    if(!dealerInformation.getNewdealername().equals(dealerInformation1.getDealerName())){
                        dealerInformation1.setFormerDealerName(dealerInformation.getNewdealername());
                    }
                }else {
                    dealerInformation1.setFormerDealerName(null);
                }
            }

//            DealerSectorLimitflag dealerSectorLimitflag1 = new DealerSectorLimitflag();
//            dealerSectorLimitflag1.setDealername(dealerInformation.getNewdealername());
//            dealerSectorLimitflag1.setSector(dealerInformation.getSector().get(i).toString());
//            List<DealerSectorLimitflag> dealerSectorLimitflags = dealerSectorLimitflagMapper.selectDealerSectorLimitflagListnew(dealerSectorLimitflag1);
//            if(StringUtils.isEmpty(dealerSectorLimitflags)) {
//                    DealerSectorLimitflag dealerSectorLimitflag = new DealerSectorLimitflag();
//                    dealerSectorLimitflag.setId(IdUtils.simpleUUID());
//                    dealerSectorLimitflag.setDealername(dealerInformation.getDealerNameCN());
//                    dealerSectorLimitflag.setSector(dealerInformation.getSector().get(i).toString());
//                    dealerSectorLimitflag.setNormalflag("true");
//                    dealerSectorLimitflag.setDemolflag("true");
//                    dealerSectorLimitflag.setTempflag("true");
//                    dealerSectorLimitflag.setMrgflag("true");
//                    dealerSectorLimitflag.setCbflag("true");
//                    dealerSectorLimitflag.setThreepartyflag("true");
//                    dealerSectorLimitflagMapper.insertDealerSectorLimitflag(dealerSectorLimitflag);
//            }


            dealerInformationMapper.insertDealerInformation(dealerInformation1);
            annualReviewyService.saveDbLog("1","新增经销商信息",null,null,null,dealerInformation1,"Dealer Info/修改");

        }
        if(dealerInformation.getDealerNameCN()!=null && dealerInformation.getDmName()!=null){
            HDealerDm hDealerDm = dealerDmMapper.selectHDealerDmByDealerName(dealerInformation.getDealerNameCN());
            if(hDealerDm!=null){
                hDealerDm.setDmName(dealerInformation.getDmName());
                hDealerDm.setUpdateTime(DateUtils.getNowDate());
                dealerDmMapper.updateHDealerDm(hDealerDm);
            }
        }
        //更改经销商名称也需要把其他的基础表里的经销商名称也更改掉
        //判断经销商名称是否发生变更
        if(!dealerInformation.getNewdealername().equals(dealerInformation.getDealerNameCN())) {
            Map<String, Object> map1 = new HashMap<>();
            map1.put("dealername1", dealerInformation.getDealerNameCN());
            map1.put("dealername", dealerInformation.getNewdealername());
            map1.put("dealernameEn1",dealerInformation.getDealerNameEN());
            map1.put("dealernameEn",dealerInformation.getNewdealernameEn());
            dealerSectorContractgroupMapper.updateDealerSectorContractgroupWhereDealername(map1);
            dealerSectorLimitflagMapper.updateDealerSectorLimitflagDealername(map1);
            //修改了经销商名称那就满足了修改flag表的条件
            DealerSectorLimitflag dealerSectorLimitflag1 = new DealerSectorLimitflag();
            dealerSectorLimitflag1.setDealername(dealerInformation.getDealerNameCN());
            dealerSectorLimitflag1.setNormalflag("true");
            dealerSectorLimitflag1.setPartsFlag("true");
            dealerSectorLimitflag1.setDemolflag("true");
            dealerSectorLimitflag1.setTempflag("true");
            dealerSectorLimitflag1.setMrgflag("true");
            dealerSectorLimitflag1.setCbflag("true");
            dealerSectorLimitflag1.setThreepartyflag("true");
            dealerSectorLimitflagMapper.updateDealerSectorLimitflagDealer(dealerSectorLimitflag1);
            parkingLocationMapper.updateParkingLocationDealername(map1);
            plmMapper.updatePlmDealername(map1);
            dealerDmMapper.updateHDealerDmDealername(map1);
        }
        return 1;
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerInformationByIds(Long[] ids)
    {
        return dealerInformationMapper.deleteDealerInformationByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerInformationById(Long id)
    {
        return dealerInformationMapper.deleteDealerInformationById(id);
    }

    @Override
    public List<DealerInformation> getGroups() {
        return dealerInformationMapper.getGroups();
    }
    @Override
    public List<DealerInformation> getDealers(String groupName) {
        return dealerInformationMapper.getDealers(groupName);
    }

    @Override
    public List<DealerInformation> getDealersbynull(String groupName) {
        return dealerInformationMapper.getDealersbynull(groupName);
    }
    @Override
    public List<DealerInformation> getSectors(String dealerName) {
        return dealerInformationMapper.getSectors(dealerName);
    }

    @Override
    public List<DealerInformation> listByCodes(List<String> dealerCodes) {
        List<DealerInformation> infos = dealerInformationMapper.listByCodes(dealerCodes);
        return infos;
    }

    /**
     * Balance InfoMation 外部数据源整合
     *
     * @param depositDecreaseDTO
     * @return
     */
    @Override
    public AjaxResult getSuspenseInfo(DepositDecreaseDTO depositDecreaseDTO) throws IOException {
        List<Object> resultList = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        List<String[]> dealerCodes = depositDecreaseDTO.getDealerCodeArr();
        List<String> dealerCodelist = new ArrayList<>();
        for (String[] dealerCode : dealerCodes) {
            for (String s : dealerCode) {
                dealerCodelist.add(s);
            }
        }

        //贷款余额、经销商数据匹配处理
        List<DealerInformation> dealerInformations = dealerInformationService.listByCodes(dealerCodelist);
        List<Map<String, Map<String, String>>> vinNoList = new ArrayList<>();
        String CreditInfodata = getPostForeignData("CreditInfo", depositDecreaseDTO.getDealerCodeArr());
        String SuspenseInfodata = getPostForeignData("SuspenseInfo", depositDecreaseDTO.getDealerCodeArr());
        JSONArray array = JSONObject.parseArray(CreditInfodata);
        Map<String,String> creditCountMap=new HashMap<>();
        Map<String,String> depositCashAmountMap=new HashMap<>();
        Map<String,String> currentSuspenseAmountMap=new HashMap<>();
        //O/S dealercode层面计算汇总
        if (array!=null&&array.size()>0){
            for (int i = 0; i < array.size(); i++) {
                JSONArray jsonArray = array.getJSONArray(i);

                for (int j = 0; j < jsonArray.size(); j++) {
                    BigDecimal creditSum= new BigDecimal(0);
                    JSONObject creditInfo = jsonArray.getJSONObject(j);
                    if (null == creditInfo) {
                        continue;
                    }
                    String dealerCode = creditInfo.getString("DealerCode");
                    String creditInfoStr=creditInfo.getString("CreditInfo");
                    JSONArray creditInfoObj = JSONObject.parseArray(creditInfoStr);
                    for (int k = 0; k < creditInfoObj.size(); k++) {
                        String creditDetStr=creditInfoObj.getJSONObject(k).getString("CreditDet");
                        if (creditDetStr==null){
                            continue;
                        }
                        JSONArray creditDetObj = JSONObject.parseArray(creditDetStr);
                        for (int l = 0; l < creditDetObj.size(); l++) {
                            String creditObject=creditDetObj.getString(l);
                            JSONObject creditMap= JSONObject.parseObject(creditObject);
                            String creditOs=creditMap.getString("ActiveOS");
                            if (creditOs==null||"null".equals(creditOs)){
                                creditOs="0";
                            }
                            creditSum = creditSum.add(new BigDecimal(creditOs));
                        }
                    }
                    creditCountMap.put(dealerCode,String.valueOf(creditSum));
                }
            }
        }

        //Deposit Cash Amount、Current Suspense Amount  在dealercode层面计算汇总
        JSONArray  suspenseArray= JSONObject.parseArray(SuspenseInfodata);
        if (suspenseArray!=null&&!"".equals(suspenseArray)&&suspenseArray.size()>0){
            for (int i = 0; i < suspenseArray.size(); i++) {
                JSONArray uspenseInfoObj=suspenseArray.getJSONArray(i);
                if (uspenseInfoObj==null){
                    continue;
                }
                for (int j = 0; j <uspenseInfoObj.size() ; j++) {
                    JSONObject suspenseObj=uspenseInfoObj.getJSONObject(j);
                    if (suspenseObj==null){
                        continue;
                    }
                    String dealerCode=suspenseObj.getString("DealerCode");
                    JSONObject SuspenseInfo=suspenseObj.getJSONObject("SuspenseInfo");
                    String suspenseAmount=SuspenseInfo.getString("SuspenseAmount");
                    String deposti=SuspenseInfo.getString("Deposit");
                    String nonRefundDeposit=SuspenseInfo.getString("NonRefundDeposit");
                    String ndTierCashDeposit=SuspenseInfo.getString("2ndTierCashDeposit");
                    String eXTDeposit=SuspenseInfo.getString("EXTDeposit");
                    String cBDeposit=SuspenseInfo.getString("CBDeposit");
//                1、Deposit Cash Amount = cBDeposit+nonRefundDeposit+deposti+ndTierCashDeposit
//                2、Current Suspense Amount = extdp+suspense amount
                    double depositCashAmount=Double.valueOf(cBDeposit)+Double.valueOf(nonRefundDeposit)+Double.valueOf(deposti)+Double.valueOf(ndTierCashDeposit);
                    double currentSuspenseAmount=Double.valueOf(eXTDeposit)+Double.valueOf(suspenseAmount);
                    depositCashAmountMap.put(dealerCode,String.valueOf(depositCashAmount));
                    currentSuspenseAmountMap.put(dealerCode,String.valueOf(currentSuspenseAmount));
                }

            }
        }


        //汇总最终结果返回
        List<DealerInformation> dealerInfoResult=dealerInformations.stream().collect(Collectors.groupingBy(DealerInformation::getDealerName))
                .entrySet().stream().map(stringListEntry -> {
                    String dealerNameKey=stringListEntry.getKey();
                    List<DealerInformation> value=stringListEntry.getValue();
                    for(DealerInformation dealerInformation:dealerInformations){
                        dealerInformation.setDepositCashAmount(depositCashAmountMap.get(dealerInformation.getDealerCode())==null?0.00:Double.valueOf(depositCashAmountMap.get(dealerInformation.getDealerCode())));
                        dealerInformation.setCurrentSuspenseAmount(currentSuspenseAmountMap.get(dealerInformation.getDealerCode())==null?0.00:Double.valueOf(currentSuspenseAmountMap.get(dealerInformation.getDealerCode())));
                        dealerInformation.setOs(creditCountMap.get(dealerInformation.getDealerCode())==null?0.00:Double.valueOf(creditCountMap.get(dealerInformation.getDealerCode())));
                    }
                    double depositAmout=value.stream().mapToDouble(DealerInformation::getDepositCashAmount).sum();
                    double currentSuspenseAmount=value.stream().mapToDouble(DealerInformation::getCurrentSuspenseAmount).sum();
                    double os=value.stream().mapToDouble(DealerInformation::getOs).sum();
                    DealerInformation dealer=new DealerInformation();
                    dealer.setDealerName(dealerNameKey);
                    dealer.setDepositCashAmount(new BigDecimal(depositAmout).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    dealer.setCurrentSuspenseAmount(new BigDecimal(currentSuspenseAmount).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    dealer.setOs(new BigDecimal(os).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                    //返回字段
                    dealer.setInterest("");
                    dealer.setMortgageAmount("");
                    dealer.setWriteOff("");
                    dealer.setNetBalance("");
                    return dealer;
                }).collect(Collectors.toList());

        map.put("balanceInformation", dealerInfoResult);
        return AjaxResult.success(map);
    }

    @Override
    public List<DealerformationVO> listByName(List<DealerInformation> params) {
        List<DealerformationVO> dealerformationVOS = new ArrayList<>();
        for (int i = 0; i < params.size(); i++) {
            DealerformationVO dealerformationVO = new DealerformationVO();
            String dealerName = params.get(i).getDealerName();
            List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByname(dealerName);
            dealerformationVO.setGroupNameCN(dealerInformations.get(0).getGroupNameCN());
            dealerformationVO.setGroupName(dealerInformations.get(0).getGroupName());
            dealerformationVO.setGroupNameEN(dealerInformations.get(0).getGroupName());
            dealerformationVO.setDealerNameCN(dealerInformations.get(0).getDealerName());
            dealerformationVO.setDealerNameEN(dealerInformations.get(0).getEnName());
            dealerformationVO.setDealerNameCN(dealerInformations.get(0).getDealerName());
            if(null !=dealerInformations.get(0).getIndividualGuarantorCn()){
                dealerformationVO.setIndividualGuarantor(dealerInformations.get(0).getIndividualGuarantorCn());
            }else{
                dealerformationVO.setIndividualGuarantor("");
            }
            if(null !=dealerInformations.get(0).getIndividualGuarantorEn()){
                dealerformationVO.setIndividualGuarantorEn(dealerInformations.get(0).getIndividualGuarantorEn());
            }else{
                dealerformationVO.setIndividualGuarantorEn("");
            }
            if(null !=dealerInformations.get(0).getCorporateGuarantorCn()){
                dealerformationVO.setCorporateGuarantor(dealerInformations.get(0).getCorporateGuarantorCn());
            }else{
                dealerformationVO.setCorporateGuarantor("");
            }
            if(null !=dealerInformations.get(0).getCorporateGuarantorEn()){
                dealerformationVO.setCorporateGuarantorEn(dealerInformations.get(0).getCorporateGuarantorEn());
            }else{
                dealerformationVO.setCorporateGuarantorEn("");
            }
            dealerformationVO.setRegisteredAddressEN(dealerInformations.get(0).getRegisteredAddressEN());
            dealerformationVO.setRegisteredAddressCN(dealerInformations.get(0).getRegisteredAddressCN());
            dealerformationVO.setLegalRepresentativeCN(dealerInformations.get(0).getLegalRepresentativeCN());
            dealerformationVO.setLegalRepresentativeEN(dealerInformations.get(0).getLegalRepresentativeEN());
            dealerformationVO.setPaidupCapital(dealerInformations.get(0).getPaidUpCapital());
            dealerformationVO.setRegisteredCapital(dealerInformations.get(0).getRegisteredCapitalEn());
            dealerformationVO.setRegisteredCapitalCn(dealerInformations.get(0).getRegisteredCapitalCn());
            dealerformationVO.setIncorporationDate(dealerInformations.get(0).getIncorporationDate());
            dealerformationVO.setJoinSectorDealerNetworkDate(dealerInformations.get(0).getJoinSectorDealerNetworkDate());
            dealerformationVO.setGm(dealerInformations.get(0).getGm());
            dealerformationVO.setRelatedWorkingExperience(dealerInformations.get(0).getRelatedWorkingExperience());

            List<Map<String, Object>> setShareholdersAndShares = new ArrayList<>();
            String shareholderStructure = dealerInformations.get(0).getShareholderStructure();
            String[] split = shareholderStructure.split(";");
            for(String sp: split){
                Map<String, Object> objectObjectHashMap = new HashMap<>();
                ArrayList<String> objects = new ArrayList<>();
                StringTokenizer stringTokenizer = new StringTokenizer(sp, "#:");
                while(stringTokenizer.hasMoreElements()){
                    objects.add(stringTokenizer.nextToken());
                }
                if(objects.size()==3) {
                    objectObjectHashMap.put("shareholdersCn", objects.get(1));
                    objectObjectHashMap.put("shareholdersEn", objects.get(0));
                    objectObjectHashMap.put("share", objects.get(2));
                }

                setShareholdersAndShares.add(objectObjectHashMap);
            }
            dealerformationVO.setShareholdersAndShare(setShareholdersAndShares);

            ArrayList<String> sectors = new ArrayList<>();
            ArrayList<String> dealerCodes = new ArrayList<>();
            ArrayList<String> dealerCodesFromManufacturers = new ArrayList<>();
            for (int i1 = 0; i1 < dealerInformations.size(); i1++) {
                String make = dealerInformations.get(i1).getMake();
                String dealerCode = dealerInformations.get(i1).getDealerCode();
                String dealerCodedealerCodesFromManufacturer = dealerInformations.get(i1).getDealerCodeFromManufacturer();
                if(StringUtils.isNotEmpty(make)){
                    sectors.add(make);
                }
                if(StringUtils.isNotEmpty(dealerCode)){
                    dealerCodes.add(dealerCode);
                }
                if(null !=dealerCodedealerCodesFromManufacturer ){
                    dealerCodesFromManufacturers.add(dealerCodedealerCodesFromManufacturer);
                }
            }
            dealerformationVO.setSector(sectors);
            dealerformationVO.setDealerCodeFromWFS(dealerCodes);
            dealerformationVO.setDealerCodeFromManufacturer(dealerCodesFromManufacturers);
            dealerformationVOS.add(dealerformationVO);
        }
        return dealerformationVOS;
    }

    private String getPostForeignData(String url, List<String[]> dealerArr) throws IOException {
        String resultStr = "";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("DealerCode", dealerArr);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(wfsUrl + url);
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost.setHeader("Connection", "keep-alive");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String, Object> objectList = new HashMap<>();
        HttpResponse response = httpClient.execute(httpPost);
        String responseStr = EntityUtils.toString(response.getEntity());
        log.info("WFS外部接口[{}]数据返回结果=[{}]", url, responseStr);
        objectList = JSONObject.parseObject(responseStr, Map.class);
        log.info("WFS外部接口[{}]数据返回JSON结果=[{}]",url, JSON.toJSONString(responseStr));
        if (Integer.valueOf(String.valueOf(objectList.get("Code"))) == 200 || Integer.valueOf(String.valueOf(objectList.get("Code"))) == 205) {
            resultStr = String.valueOf(objectList.get("Data"));
        }
        return resultStr;
    }

    @Override
    public List<String> getDealerInfo(String groupName) {
        return dealerInformationMapper.getDealerInfo(groupName);
    }

    @Override
    public List<String> getDealerInfoByDealerName(String dealerName) {
        return dealerInformationMapper.getDealerInfoByDealerName(dealerName);
    }

    @Override
    public List<String> getDealerInfoByALL() {
        return dealerInformationMapper.getDealerInfoByALL();
    }
}
