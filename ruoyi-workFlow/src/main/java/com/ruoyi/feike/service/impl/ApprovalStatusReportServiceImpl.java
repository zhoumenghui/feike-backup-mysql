package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.ApprovalStatusReportMapper;
import com.ruoyi.feike.domain.ApprovalStatusReport;
import com.ruoyi.feike.service.IApprovalStatusReportService;

/**
 * DM提交流程报表Service业务层处理
 * 
 * @author feike
 * @date 2023-01-18
 */
@Service
public class ApprovalStatusReportServiceImpl implements IApprovalStatusReportService 
{
    @Autowired
    private ApprovalStatusReportMapper approvalStatusReportMapper;

    /**
     * 查询DM提交流程报表
     * 
     * @param id DM提交流程报表ID
     * @return DM提交流程报表
     */
    @Override
    public ApprovalStatusReport selectApprovalStatusReportById(Long id)
    {
        return approvalStatusReportMapper.selectApprovalStatusReportById(id);
    }

    /**
     * 查询DM提交流程报表列表
     * 
     * @param approvalStatusReport DM提交流程报表
     * @return DM提交流程报表
     */
    @Override
    public List<ApprovalStatusReport> selectApprovalStatusReportList(ApprovalStatusReport approvalStatusReport)
    {
        return approvalStatusReportMapper.selectApprovalStatusReportList(approvalStatusReport);
    }

    /**
     * 新增DM提交流程报表
     * 
     * @param approvalStatusReport DM提交流程报表
     * @return 结果
     */
    @Override
    public int insertApprovalStatusReport(ApprovalStatusReport approvalStatusReport)
    {
        return approvalStatusReportMapper.insertApprovalStatusReport(approvalStatusReport);
    }

    /**
     * 修改DM提交流程报表
     * 
     * @param approvalStatusReport DM提交流程报表
     * @return 结果
     */
    @Override
    public int updateApprovalStatusReport(ApprovalStatusReport approvalStatusReport)
    {
        return approvalStatusReportMapper.updateApprovalStatusReport(approvalStatusReport);
    }

    /**
     * 批量删除DM提交流程报表
     * 
     * @param ids 需要删除的DM提交流程报表ID
     * @return 结果
     */
    @Override
    public int deleteApprovalStatusReportByIds(Long[] ids)
    {
        return approvalStatusReportMapper.deleteApprovalStatusReportByIds(ids);
    }

    /**
     * 删除DM提交流程报表信息
     * 
     * @param id DM提交流程报表ID
     * @return 结果
     */
    @Override
    public int deleteApprovalStatusReportById(Long id)
    {
        return approvalStatusReportMapper.deleteApprovalStatusReportById(id);
    }
}
