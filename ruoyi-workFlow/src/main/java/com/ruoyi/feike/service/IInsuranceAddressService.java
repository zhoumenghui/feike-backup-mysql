package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.InsuranceAddress;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface IInsuranceAddressService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public InsuranceAddress selectInsuranceAddressById(String id);

    /**
     * 查询feike列表
     * 
     * @param insuranceAddress feike
     * @return feike集合
     */
    public List<InsuranceAddress> selectInsuranceAddressList(InsuranceAddress insuranceAddress);

    /**
     * 新增feike
     * 
     * @param insuranceAddress feike
     * @return 结果
     */
    public int insertInsuranceAddress(InsuranceAddress insuranceAddress);

    /**
     * 修改feike
     * 
     * @param insuranceAddress feike
     * @return 结果
     */
    public int updateInsuranceAddress(InsuranceAddress insuranceAddress);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteInsuranceAddressByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteInsuranceAddressById(String id);
}
