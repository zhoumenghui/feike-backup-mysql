package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.PaidSum;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface IPaidSumService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public PaidSum selectPaidSumById(String id);

    /**
     * 查询feike列表
     * 
     * @param paidSum feike
     * @return feike集合
     */
    public List<PaidSum> selectPaidSumList(PaidSum paidSum);

    /**
     * 新增feike
     * 
     * @param paidSum feike
     * @return 结果
     */
    public int insertPaidSum(PaidSum paidSum);

    /**
     * 修改feike
     * 
     * @param paidSum feike
     * @return 结果
     */
    public int updatePaidSum(PaidSum paidSum);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deletePaidSumByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deletePaidSumById(String id);
}
