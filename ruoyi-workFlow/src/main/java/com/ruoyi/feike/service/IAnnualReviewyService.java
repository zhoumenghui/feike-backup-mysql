package com.ruoyi.feike.service;

import com.github.pagehelper.Page;
import com.ruoyi.activiti.domain.dto.ActTaskDTO;
import com.ruoyi.activiti.domain.vo.SearchVo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.CustomPage;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.dto.WaitForSaveNoteDTO;
import com.ruoyi.feike.domain.dto.YearAuditSaveDTO;
import com.ruoyi.feike.domain.vo.*;
import org.apache.poi.ss.formula.functions.T;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * feikeService接口
 *
 * @author ruoyi
 * @date 2022-07-07
 */
public interface IAnnualReviewyService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public Map<String,Object> selectAnnualReviewyById(String id);

    /**
     * 查询feike列表
     *
     * @param annualReviewy feike
     * @return feike集合
     */
    public List<AnnualReviewyHistoryVO> selectAnnualReviewyList(AnnualReviewy annualReviewy, BasicInformation basicInformation);

    public List<AnnualReviewyHistoryVO> selectAnnualReviewyList1(AnnualReviewy annualReviewy, BasicInformation basicInformation);

    public List<AnnualReviewyHistoryVO> selectAnnualReviewyList2(AnnualReviewy annualReviewy, BasicInformation basicInformation);

    public List<AnnualReviewyHistoryVO> selectAnnualReviewyList3(AnnualReviewy annualReviewy, BasicInformation basicInformation);




    /**
     * 新增feike
     *
     * @param map feike
     * @return 结果
     */
    public int insertAnnualReviewy(Map<String, Object> map);

    /**
     * 修改feike
     *
     * @param map feike
     * @return 结果
     */
    public int updateAnnualReviewy(Map<String, Object> map);


    public int updateAnnualReviewyByInstanceId(AnnualReviewy annualReviewy);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteAnnualReviewyByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteAnnualReviewyById(String id);

    List<AnnualReviewy> selectAnnualReviewyAndTaskNameList(AnnualReviewy annualReviewy);

    int startDM(Map<String, Object> map);

    int startPLM(Map<String, Object> map);

    int dmSelect(Map<String, Object> map);

    Map<String,Object> selectAnnualReviewyByInstanceId(String instanceId) throws Exception;

    Map<String,Object> selectAnnualReviewyinfo(String instanceId);

    Map<String,Object> getCorporateGuarantee(String instanceId);

    Map<String,Object> selectMailInfoById(String id);

    Map<String,Object> selectMailInfo(Map<String, Object> map);

    Map<String,Object> selectMailInfoByInstanceId(String instanceId);

    int updateMain(Map<String, Object> map);

    AjaxResult deleteMain(String id);

    DealerNegativeInformationCheck getMain(String taskId);

    List<AnnualReviewyVO> selectAnnualReviewyVOList(AnnualReviewy annualReviewy);

    Page<ActTaskDTO> selectProcessDefinitionSearchList(SearchVo searchVo, PageDomain pageDomain);

    CustomPage indexData();

    int closeUpdateAnnualReviewyObj(String instanceId,String username);

    int openUpdateAnnualReviewyObj(String username);

    List getInfoListByDealerName(String dealerName);

    List<Map<String, Object>> listWprvo(List<String> map);

    List<Map<String, Object>> listWprvoApproval(Map<String, Object> map);

    List<Map<String, Object>> listspat(List<String> map);

    List<Map<String, Object>> listspatApproval(Map<String, Object> map);

    List<Map<String, Object>> sectorinformationAndDealerinformation(List<String> map,String instanceId);

    public Map<String, Object> sectorinformationAndDealerinformation1(Map<String, Object> map);

    List<Map<String, Object>> approvalLimitCalculationForCommericalNeeds(Map<String, Object> map);

    List<Map<String, Object>> selectplmWhereDealerName(List<String> map);

    //年审信息保存
    AjaxResult yearAuditSave(YearAuditSaveDTO yearAuditSaveDTO);

	AjaxResult reminder(String instanceId);

    AjaxResult waitForSaveNote(WaitForSaveNoteDTO waitForSaveNoteDTO);

    List<AnnualReviewy> listMineDo();

    List<String> getWorkName();

    List<String> getWorkflowFormData(String instanceId);


    List<ApprovalStatusVO> selectAnnualReviewyVODMList(AnnualReviewy annualReviewy);


    List<HAml> selectAmlInfoList(String instanceId);


    String fileZip(String instanceId);


    public   <T> String saveDbLog(String oper_type, String oper_method, String title ,String instanceId, T oldBean, T newBean,String setScreenName);

}
