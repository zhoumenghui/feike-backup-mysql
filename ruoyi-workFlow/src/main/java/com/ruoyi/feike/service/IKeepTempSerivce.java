package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.vo.KeepVO;
import java.util.List;
import java.util.Map;

public interface IKeepTempSerivce {


    public int insertKeeps(List<KeepVO> Keep);

    public List<Map<String,Object>> selectKeep();

    public List<KeepVO> selectKeep1();

    public int deleteKeep();


}
