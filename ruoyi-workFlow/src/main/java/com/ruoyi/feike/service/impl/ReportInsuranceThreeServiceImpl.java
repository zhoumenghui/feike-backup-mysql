package com.ruoyi.feike.service.impl;

import java.util.*;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.dto.DeclaredInformationDTO;
import com.ruoyi.feike.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.service.IReportInsuranceThreeService;

/**
 * 报表h_report-insurance-3Service业务层处理
 *
 * @author ruoyi
 * @date 2022-12-27
 */
@Service
public class ReportInsuranceThreeServiceImpl implements IReportInsuranceThreeService
{
    @Autowired
    private ReportInsuranceThreeMapper reportInsuranceThreeMapper;


    @Autowired
    private NewSumMapper newSumMapper;


    @Autowired
    private NewSumKeepMapper newSumKeepMapper;


    @Autowired
    private InsuranceAddressMapper insuranceAddressMapper;

    @Autowired
    private BillingInformationMapper billingInformationMapper;

    @Autowired
    private InsuranceDeclarationInfoMapper infoMapper;

    /**
     * 查询报表h_report-insurance-3
     *
     * @param id 报表h_report-insurance-3ID
     * @return 报表h_report-insurance-3
     */
    @Override
    public ReportInsuranceThree selectReportInsuranceThreeById(Long id)
    {
        return reportInsuranceThreeMapper.selectReportInsuranceThreeById(id);
    }

    /**
     * 查询报表h_report-insurance-3列表
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 报表h_report-insurance-3
     */
    @Override
    public List<ReportInsuranceThree> selectReportInsuranceThreeList(ReportInsuranceThree reportInsuranceThree)
    {
        return reportInsuranceThreeMapper.selectReportInsuranceThreeList(reportInsuranceThree);
    }

    /**
     * 新增报表h_report-insurance-3
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 结果
     */
    @Override
    public int insertReportInsuranceThree(ReportInsuranceThree reportInsuranceThree)
    {
        return reportInsuranceThreeMapper.insertReportInsuranceThree(reportInsuranceThree);
    }

    /**
     * 修改报表h_report-insurance-3
     *
     * @param reportInsuranceThree 报表h_report-insurance-3
     * @return 结果
     */
    @Override
    public int updateReportInsuranceThree(ReportInsuranceThree reportInsuranceThree)
    {
        return reportInsuranceThreeMapper.updateReportInsuranceThree(reportInsuranceThree);
    }

    /**
     * 批量删除报表h_report-insurance-3
     *
     * @param ids 需要删除的报表h_report-insurance-3ID
     * @return 结果
     */
    @Override
    public int deleteReportInsuranceThreeByIds(Long[] ids)
    {
        return reportInsuranceThreeMapper.deleteReportInsuranceThreeByIds(ids);
    }

    /**
     * 删除报表h_report-insurance-3信息
     *
     * @param id 报表h_report-insurance-3ID
     * @return 结果
     */
    @Override
    public int deleteReportInsuranceThreeById(Long id)
    {
        return reportInsuranceThreeMapper.deleteReportInsuranceThreeById(id);
    }


    @Override
    public Map<String, Object> getTwo(String instanceId) {
        Map<String, Object> map = new HashMap<String, Object>();
        NewSumKeep newSumKeep = new NewSumKeep();
        newSumKeep.setInstanceId(instanceId);
        List<NewSumKeep> newSumKeeps = newSumKeepMapper.selectNewSumKeepHisList(newSumKeep);
        List<Map<String,Object>> declarationMap = null;
        List<Map<String,Object>> informationMap = null;
        List<InsuranceDeclarationInfo> insuranceDeclarationInfoByInstanceId = infoMapper.getInsuranceDeclarationInfoByInstanceId(instanceId);
        map.put("newSum",newSumKeeps);

        map.put("info",insuranceDeclarationInfoByInstanceId);
        //h_declaration_address
        declarationMap = reportInsuranceThreeMapper.getInfoDeclarationHis(instanceId);
        if(declarationMap == null || declarationMap.size() == 0){
            System.out.println("查最新");
            declarationMap = reportInsuranceThreeMapper.getInfoDeclaration(newSumKeeps.get(0).getDealerName());
        }
        //h_invoicing_information
        informationMap = reportInsuranceThreeMapper.getInfoInformationHis(instanceId);
        if(informationMap == null || informationMap.size() == 0){
            System.out.println("查最新");
            informationMap = reportInsuranceThreeMapper.getInfoInformation(newSumKeeps.get(0).getDealerName());
        }

        //h_invoice_address
        // List<Map<String,Object>> addressMap = reportInsuranceThreeMapper.getInfoAddress(newSumKeeps.get(0).getDealerName());

        map.put("insuranceAddresses",declarationMap);
        map.put("selectBillingInformationList",informationMap);

        return map;
    }

    @Override
    public Map<Object, Object> exportByDate(Map<String, Object> map) {
        String startDate = map.get("startDate").toString();
        String endDate = map.get("endDate").toString();
        List<DeclaredInformationDTO> instanceIdList = infoMapper.getInstanceIdList(startDate, endDate);

        Map<Object, Object> hashMap = new HashMap<>();

        ArrayList<NewSumKeep> sumKeeps = new ArrayList<NewSumKeep>();

        ArrayList<InsuranceAddress> addresses = new ArrayList<InsuranceAddress>();

        ArrayList<InvoiceInformation> informations = new ArrayList<>();


        for (DeclaredInformationDTO declaredInformationDTO : instanceIdList) {

            NewSumKeep newSumKeep = new NewSumKeep();
            newSumKeep.setInstanceId(declaredInformationDTO.getInstanceId());
            List<NewSumKeep> newSumKeeps = newSumKeepMapper.selectNewSumKeepHisList2(newSumKeep);
            if(CollectionUtil.isNotEmpty(newSumKeeps)){
                for (NewSumKeep sumKeep : newSumKeeps) {
                    sumKeep.setPremiumReceivedDate(declaredInformationDTO.getPremiumReceivedDate());

                }
                sumKeeps.addAll(newSumKeeps);



                List<Map<String, Object>> declarationMap  = null ;
                declarationMap = reportInsuranceThreeMapper.getInfoDeclarationHis(declaredInformationDTO.getInstanceId());
                if(declarationMap == null || declarationMap.size() == 0){
                    declarationMap = reportInsuranceThreeMapper.getInfoDeclaration(newSumKeeps.get(0).getDealerName());
                }
                if(declarationMap!=null && declarationMap.size()>0){
                    for (Map<String, Object> infoDeclaration : declarationMap) {
                        InsuranceAddress insuranceAddress = JSONObject.parseObject(JSONObject.toJSONString(infoDeclaration), InsuranceAddress.class);
                        addresses.add(insuranceAddress);
                    }

                }

                List<Map<String, Object>> informationMap  = null ;
                //h_invoicing_information
                informationMap = reportInsuranceThreeMapper.getInfoInformationHis(declaredInformationDTO.getInstanceId());
                if(informationMap == null || informationMap.size() == 0){
                    informationMap = reportInsuranceThreeMapper.getInfoInformation(newSumKeeps.get(0).getDealerName());
                }

                if(informationMap!=null && informationMap.size()>0){
                    for (Map<String, Object> information : informationMap) {
                        BillingInformation billingInformation = JSONObject.parseObject(JSONObject.toJSONString(information), BillingInformation.class);
                        InvoiceInformation invoiceInformation = new InvoiceInformation();
                        invoiceInformation.setReceiverCompanyName(billingInformation.getCompanyName());
                        invoiceInformation.setReceiverCompanyAddress(billingInformation.getAddress());
                        BeanUtil.copyProperties(information,invoiceInformation);
                        if(invoiceInformation.getIsGeneral()!=null){
                            if("2".equals(invoiceInformation.getIsGeneral())){
                                invoiceInformation.setIsGeneral("否");
                            }else{
                                invoiceInformation.setIsGeneral("是");
                            }
                        }
                        informations.add(invoiceInformation);
                    }

                }
            }

        }
        hashMap.put("sumKeeps",sumKeeps);
        hashMap.put("addresses",addresses);
        hashMap.put("informations",informations);
        return hashMap;
    }

    @Override
    public Map<String, Object> getInfoByDealerName(String dealerName) {
        //h_declaration_address
        List<Map<String,Object>> declarationMap = reportInsuranceThreeMapper.getInfoDeclaration(dealerName);
        //h_invoicing_information
        List<Map<String,Object>> informationMap = reportInsuranceThreeMapper.getInfoInformation(dealerName);
        //h_invoice_address
        List<Map<String,Object>> addressMap = reportInsuranceThreeMapper.getInfoAddress(dealerName);

        HashMap<String, Object> map = new HashMap<>();
        map.put("h_declaration_address",declarationMap);
        map.put("h_invoicing_information",informationMap);
        map.put("h_invoice_address",addressMap);
        map.put("uuid", IdUtils.get16UUID());
        return map;
    }

    @Override
    public Map<String, Object> getInfoByDealerNameByHis(String dealerName,String instanceId) {
        //h_declaration_address
        List<Map<String,Object>> declarationMap = null ;
        List<Map<String,Object>> informationMap = null ;
        List<Map<String,Object>> addressMap = null ;
        if(instanceId !=null ){
            System.out.println("有id");
            declarationMap = reportInsuranceThreeMapper.getInfoDeclarationHis(instanceId);
            if(declarationMap == null || declarationMap.size() == 0){
                System.out.println("查最新");
                declarationMap = reportInsuranceThreeMapper.getInfoDeclaration(dealerName);
            }
            informationMap = reportInsuranceThreeMapper.getInfoInformationHis(instanceId);
            if(informationMap == null || informationMap.size() == 0){
                System.out.println("查最新");
                informationMap = reportInsuranceThreeMapper.getInfoInformation(dealerName);
            }
            addressMap = reportInsuranceThreeMapper.getInfoAddressHis(instanceId);
            if(addressMap == null || addressMap.size() == 0){
                System.out.println("查最新");
                addressMap = reportInsuranceThreeMapper.getInfoAddress(dealerName);
            }

        }else{
            System.out.println("没有id");
            declarationMap = reportInsuranceThreeMapper.getInfoDeclaration(dealerName);
            //h_invoicing_information
            informationMap = reportInsuranceThreeMapper.getInfoInformation(dealerName);
            //h_invoice_address
            addressMap = reportInsuranceThreeMapper.getInfoAddress(dealerName);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("h_declaration_address",declarationMap);
        map.put("h_invoicing_information",informationMap);
        map.put("h_invoice_address",addressMap);
        map.put("uuid", IdUtils.get16UUID());
        return map;
    }

    @Override
    public List<Map<String, Object>> getDealerInsuranceByDealerName(String name) {
        List<Map<String, Object>> dealerInsuranceByDealerName = reportInsuranceThreeMapper.getDealerInsuranceByDealerName(name);
        return  dealerInsuranceByDealerName;
    }
}
