package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.NewSum;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-08-25
 */
public interface INewSumService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public NewSum selectNewSumById(String id);

    /**
     * 查询feike列表
     *
     * @param newSum feike
     * @return feike集合
     */
    public List<NewSum> selectNewSumList(NewSum newSum);

    /**
     * 新增feike
     *
     * @param newSum feike
     * @return 结果
     */
    public int insertNewSum(NewSum newSum);

    /**
     * 修改feike
     *
     * @param newSum feike
     * @return 结果
     */
    public int updateNewSum(NewSum newSum);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteNewSumByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteNewSumById(String id);

}
