package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.GradeMapper;
import com.ruoyi.feike.domain.Grade;
import com.ruoyi.feike.service.IGradeService;

/**
 * feikeService业务层处理
 *
 * @author zmh
 * @date 2022-08-22
 */
@Service
public class GradeServiceImpl implements IGradeService
{
    @Autowired
    private GradeMapper gradeMapper;

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public Grade selectGradeById(String id)
    {
        return gradeMapper.selectGradeById(id);
    }

    /**
     * 查询feike列表
     *
     * @param grade feike
     * @return feike
     */
    @Override
    public List<Grade> selectGradeList(Grade grade)
    {
        return gradeMapper.selectGradeList(grade);
    }

    /**
     * 新增feike
     *
     * @param grade feike
     * @return 结果
     */
    @Override
    public int insertGrade(Grade grade)
    {
        return gradeMapper.insertGrade(grade);
    }

    /**
     * 修改feike
     *
     * @param grade feike
     * @return 结果
     */
    @Override
    public int updateGrade(Grade grade)
    {
        return gradeMapper.updateGrade(grade);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteGradeByIds(String[] ids)
    {
        return gradeMapper.deleteGradeByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteGradeById(String id)
    {
        return gradeMapper.deleteGradeById(id);
    }

    @Override
    public List<Grade> selectGradeByInstanceId(String instanceId) {
        return gradeMapper.selectGradeByInstanceId(instanceId);
    }
}
