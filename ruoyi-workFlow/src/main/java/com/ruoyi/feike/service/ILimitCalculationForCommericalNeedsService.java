package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.LimitCalculationForCommericalNeeds;
import com.ruoyi.feike.domain.LimitCalculationSearchDTO;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-07-11
 */
public interface ILimitCalculationForCommericalNeedsService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public LimitCalculationForCommericalNeeds selectLimitCalculationForCommericalNeedsById(String id);

    /**
     * 查询feike列表
     *
     * @param limitCalculationForCommericalNeeds feike
     * @return feike集合
     */
    public List<LimitCalculationForCommericalNeeds> selectLimitCalculationForCommericalNeedsList(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds);

    /**
     * 新增feike
     *
     * @param limitCalculationForCommericalNeeds feike
     * @return 结果
     */
    public int insertLimitCalculationForCommericalNeeds(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds);

    /**
     * 修改feike
     *
     * @param limitCalculationForCommericalNeeds feike
     * @return 结果
     */
    public int updateLimitCalculationForCommericalNeeds(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteLimitCalculationForCommericalNeedsByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteLimitCalculationForCommericalNeedsById(String id);

    Map<String, List<LimitCalculationForCommericalNeeds>> getForeignInfo(LimitCalculationSearchDTO dto);
}
