package com.ruoyi.feike.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.DepositDecreaseDTO;
import com.ruoyi.feike.domain.vo.DealerInformationVO;
import com.ruoyi.feike.domain.vo.DealerformationVO;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * feikeService接口
 *
 * @author ybw
 * @date 2022-07-12
 */
public interface IDealerInformationService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DealerInformation selectDealerInformationById(Long id);

    DealerInformationVO selectDealerInformationByname(Map<String, Object> map);

    /**
     * 查询feike列表
     *
     * @param dealerInformation feike
     * @return feike集合
     */
    @DataSource(value = DataSourceType.MASTER)
    public List<DealerInformation> selectDealerInformationList(DealerInformation dealerInformation);

    @DataSource(value = DataSourceType.MASTER)
    public List<DealerInformation> selectDealerInformationList2(DealerInformation dealerInformation);


    @DataSource(value = DataSourceType.MASTER)
    public List<DealerInformation> selectDealerInformationList3(DealerInformation dealerInformation);


    List<DealerInformation> selectDealerInformationListnew(DealerInformation dealerInformation);

    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int insertDealerInformation(DealerInformationVO dealerInformation);

    /**
     * 修改feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int updateDealerInformation(DealerInformation dealerInformation);

    public int updateDealerInformationnew(DealerInformationVO dealerInformation);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteDealerInformationByIds(Long[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerInformationById(Long id);

    public List<DealerInformation> getGroups();
    public List<DealerInformation> getDealers(String groupName);

    List<DealerInformation> getDealersbynull(String groupName);

    List<DealerformationVO> listByName(List<DealerInformation> params);


    List<DealerInformation> getSectors(String dealerName);
    List<DealerInformation> listByCodes(List<String> dealerCodes);

    /**
     * Balance InfoMation 外部数据源整合
     * @param depositDecreaseDTO
     * @return
     */
    AjaxResult getSuspenseInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException;

    List<String> getDealerInfo(String groupName);

    List<String> getDealerInfoByDealerName(String dealerName);

    List<String> getDealerInfoByALL();
}
