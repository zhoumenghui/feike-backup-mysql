package com.ruoyi.feike.service.impl;

import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealercodeContractFilingMapper;
import com.ruoyi.feike.domain.DealercodeContractFiling;
import com.ruoyi.feike.service.IDealercodeContractFilingService;

/**
 * 合同归档表Service业务层处理
 *
 * @author ruoyi
 * @date 2022-08-23
 */
@Service
public class DealercodeContractFilingServiceImpl implements IDealercodeContractFilingService
{
    @Autowired
    private DealercodeContractFilingMapper dealercodeContractFilingMapper;

    /**
     * 查询合同归档表
     *
     * @param id 合同归档表ID
     * @return 合同归档表
     */
    @Override
    public DealercodeContractFiling selectDealercodeContractFilingById(Long id)
    {
        return dealercodeContractFilingMapper.selectDealercodeContractFilingById(id);
    }

    /**
     * 查询合同归档表列表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 合同归档表
     */
    @Override
    public List<DealercodeContractFiling> selectDealercodeContractFilingList(DealercodeContractFiling dealercodeContractFiling)
    {
        return dealercodeContractFilingMapper.selectDealercodeContractFilingList2(dealercodeContractFiling);
    }

    @Override
    public List<DealercodeContractFiling> selectDealercodeContractFilingListExport(DealercodeContractFiling dealercodeContractFiling)
    {
        return dealercodeContractFilingMapper.selectDealercodeContractFilingList4(dealercodeContractFiling);
    }

    /**
     * 新增合同归档表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 结果
     */
    @Override
    public int insertDealercodeContractFiling(DealercodeContractFiling dealercodeContractFiling)
    {
        return dealercodeContractFilingMapper.insertDealercodeContractFiling(dealercodeContractFiling);
    }

    /**
     * 修改合同归档表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 结果
     */
    @Override
    public int updateDealercodeContractFiling(DealercodeContractFiling dealercodeContractFiling)
    {
        return dealercodeContractFilingMapper.updateDealercodeContractFiling(dealercodeContractFiling);
    }

    @Override
    public int updateDealercodeContractFiling2(DealercodeContractFiling dealercodeContractFiling)
    {
        return dealercodeContractFilingMapper.updateDealercodeContractFiling2(dealercodeContractFiling);
    }


    /**
     * 删除合同归档表下的附件不删除数据库信息
     *
     * @param ids 需要删除的合同归档表ID
     * @return 结果
     */
    @Override
    public int deleteDealercodeContractFilingByIds(Long[] ids)
    {
        for(Long a : ids){
            DealercodeContractFiling dealercodeContractFiling = dealercodeContractFilingMapper.selectDealercodeContractFilingById(a);
            String contractLocation = dealercodeContractFiling.getContractLocation();
            FileUtils.deleteFile(RuoYiConfig.getProfile() + contractLocation.substring(8));
        }

        return 1;
    }

    /**
     * 删除合同归档表信息
     *
     * @param id 合同归档表ID
     * @return 结果
     */
    @Override
    public int deleteDealercodeContractFilingById(Long id)
    {
        return dealercodeContractFilingMapper.deleteDealercodeContractFilingById(id);
    }
}
