package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.DealercodeContractFiling;

/**
 * 合同归档表Service接口
 *
 * @author ruoyi
 * @date 2022-08-23
 */
public interface IDealercodeContractFilingService
{
    /**
     * 查询合同归档表
     *
     * @param id 合同归档表ID
     * @return 合同归档表
     */
    public DealercodeContractFiling selectDealercodeContractFilingById(Long id);

    /**
     * 查询合同归档表列表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 合同归档表集合
     */
    public List<DealercodeContractFiling> selectDealercodeContractFilingList(DealercodeContractFiling dealercodeContractFiling);



    public List<DealercodeContractFiling> selectDealercodeContractFilingListExport(DealercodeContractFiling dealercodeContractFiling);



    /**
     * 新增合同归档表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 结果
     */
    public int insertDealercodeContractFiling(DealercodeContractFiling dealercodeContractFiling);

    /**
     * 修改合同归档表
     *
     * @param dealercodeContractFiling 合同归档表
     * @return 结果
     */
    public int updateDealercodeContractFiling(DealercodeContractFiling dealercodeContractFiling);

    public int updateDealercodeContractFiling2(DealercodeContractFiling dealercodeContractFiling);


    /**
     * 批量删除合同归档表
     *
     * @param ids 需要删除的合同归档表ID
     * @return 结果
     */
    public int deleteDealercodeContractFilingByIds(Long[] ids);

    /**
     * 删除合同归档表信息
     *
     * @param id 合同归档表ID
     * @return 结果
     */
    public int deleteDealercodeContractFilingById(Long id);
}
