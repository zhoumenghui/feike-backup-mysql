package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.NotesMapper;
import com.ruoyi.feike.domain.Notes;
import com.ruoyi.feike.service.INotesService;

/**
 * feikeService业务层处理
 * 
 * @author lsn
 * @date 2022-07-06
 */
@Service
public class NotesServiceImpl implements INotesService 
{
    @Autowired
    private NotesMapper notesMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public Notes selectNotesById(String id)
    {
        return notesMapper.selectNotesById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param notes feike
     * @return feike
     */
    @Override
    public List<Notes> selectNotesList(Notes notes)
    {
        return notesMapper.selectNotesList(notes);
    }

    /**
     * 新增feike
     * 
     * @param notes feike
     * @return 结果
     */
    @Override
    public int insertNotes(Notes notes)
    {
        return notesMapper.insertNotes(notes);
    }

    /**
     * 修改feike
     * 
     * @param notes feike
     * @return 结果
     */
    @Override
    public int updateNotes(Notes notes)
    {
        return notesMapper.updateNotes(notes);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteNotesByIds(String[] ids)
    {
        return notesMapper.deleteNotesByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteNotesById(String id)
    {
        return notesMapper.deleteNotesById(id);
    }
}
