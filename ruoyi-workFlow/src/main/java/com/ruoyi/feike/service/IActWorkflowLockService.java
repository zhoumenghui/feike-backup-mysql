package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.ActWorkflowLock;


/**
 * feikeService接口
 *
 * @author ruoyi
 * @date 2022-07-07
 */
public interface IActWorkflowLockService


{
    public int insertActWorkflowLock(ActWorkflowLock actWorkflowLock);


    public int deleteActWorkflowLockById(String id);


}
