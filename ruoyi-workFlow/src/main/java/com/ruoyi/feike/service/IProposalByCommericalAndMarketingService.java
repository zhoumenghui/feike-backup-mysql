package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.ProposalByCommericalAndMarketing;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-18
 */
public interface IProposalByCommericalAndMarketingService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ProposalByCommericalAndMarketing selectProposalByCommericalAndMarketingById(String id);

    /**
     * 查询feike列表
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return feike集合
     */
    public List<ProposalByCommericalAndMarketing> selectProposalByCommericalAndMarketingList(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    /**
     * 新增feike
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return 结果
     */
    public int insertProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    /**
     * 修改feike
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return 结果
     */
    public int updateProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteProposalByCommericalAndMarketingByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteProposalByCommericalAndMarketingById(String id);
}
