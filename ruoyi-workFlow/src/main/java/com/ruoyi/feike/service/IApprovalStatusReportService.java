package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.ApprovalStatusReport;

/**
 * DM提交流程报表Service接口
 * 
 * @author feike
 * @date 2023-01-18
 */
public interface IApprovalStatusReportService 
{
    /**
     * 查询DM提交流程报表
     * 
     * @param id DM提交流程报表ID
     * @return DM提交流程报表
     */
    public ApprovalStatusReport selectApprovalStatusReportById(Long id);

    /**
     * 查询DM提交流程报表列表
     * 
     * @param approvalStatusReport DM提交流程报表
     * @return DM提交流程报表集合
     */
    public List<ApprovalStatusReport> selectApprovalStatusReportList(ApprovalStatusReport approvalStatusReport);

    /**
     * 新增DM提交流程报表
     * 
     * @param approvalStatusReport DM提交流程报表
     * @return 结果
     */
    public int insertApprovalStatusReport(ApprovalStatusReport approvalStatusReport);

    /**
     * 修改DM提交流程报表
     * 
     * @param approvalStatusReport DM提交流程报表
     * @return 结果
     */
    public int updateApprovalStatusReport(ApprovalStatusReport approvalStatusReport);

    /**
     * 批量删除DM提交流程报表
     * 
     * @param ids 需要删除的DM提交流程报表ID
     * @return 结果
     */
    public int deleteApprovalStatusReportByIds(Long[] ids);

    /**
     * 删除DM提交流程报表信息
     * 
     * @param id DM提交流程报表ID
     * @return 结果
     */
    public int deleteApprovalStatusReportById(Long id);
}
