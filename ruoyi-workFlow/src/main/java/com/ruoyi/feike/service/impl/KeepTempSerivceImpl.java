package com.ruoyi.feike.service.impl;

import com.ruoyi.feike.domain.vo.KeepVO;
import com.ruoyi.feike.mapper.KeepTempMapper;
import com.ruoyi.feike.service.IKeepTempSerivce;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class KeepTempSerivceImpl implements IKeepTempSerivce {

    @Autowired
    private KeepTempMapper keepTempMapper;


    @Override
    public int insertKeeps(List<KeepVO> Keep) {
        int i = keepTempMapper.insertKeeps(Keep);
        return  i ;
    }

    @Override
    public List<Map<String,Object>> selectKeep() {
       return  keepTempMapper.selectKeep();
    }

    @Override
    public List<KeepVO> selectKeep1() {
        return  keepTempMapper.selectKeep1();
    }

    @Override
    public int deleteKeep() {
        return  keepTempMapper.deleteKeep();
    }
}
