package com.ruoyi.feike.service.impl;


import com.ruoyi.feike.domain.vo.ContractRecordFour;
import com.ruoyi.feike.domain.vo.ContractRecordTow;
import com.ruoyi.feike.mapper.ContractRecordFourMapper;
import com.ruoyi.feike.mapper.ContractRecordTowMapper;
import com.ruoyi.feike.service.IContractRecordFourSerivce;
import com.ruoyi.feike.service.IContractRecordTowSerivce;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ContractRecordFourSerivceImpl implements IContractRecordFourSerivce {


    @Autowired
    private ContractRecordFourMapper contractRecordFourMapper;


    @Override
    public List<ContractRecordFour> selectContractRecordFourList(ContractRecordFour contractRecordFour)
    {
        return contractRecordFourMapper.selectContractRecordFourList(contractRecordFour);
    }

    @Override
    public int insertContractRecordFour(ContractRecordFour contractRecordFour)
    {
        return contractRecordFourMapper.insertContractRecordFour(contractRecordFour);
    }




}
