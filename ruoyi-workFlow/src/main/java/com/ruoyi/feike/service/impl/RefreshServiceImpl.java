package com.ruoyi.feike.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.stream.CollectorUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.DayUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.dto.RefreshDTO;
import com.ruoyi.feike.domain.vo.*;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotEmpty;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lss
 * @version 1.0
 * @description: TODO
 * @date 2022/8/28 16:39
 */
@Service
@Slf4j
public class RefreshServiceImpl implements IRefreshService {

	@Autowired
	private AnnualReviewyMapper annualReviewyMapper;

	@Autowired
	private BasicInformationMapper basicInformationMapper;

	@Autowired
	private WholesalePerformanceCurrentlyMapper wholesalePerformanceCurrentlyMapper;

	@Autowired
	private LimitCalculationForCommericalNeedsMapper limitCalculationForCommericalNeedsMapper;

	@Autowired
	private OtherFinancingResourceMapper otherFinancingResourceMapper;

	@Autowired
	private TemporaryConditionFollowUpMapper temporaryConditionFollowUpMapper;

	@Autowired
	private ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper;

	@Autowired
	private WholesalePerformanceRecentThreeMonthsMapper wholesalePerformanceRecentThreeMonthsMapper;

	@Autowired
	private SecuritiesMapper securitiesMapper;

	@Autowired
	private NotesMapper notesMapper;

	@Autowired
	private CreditConditionMapper creditConditionMapper;

	@Autowired
	private IDealerNegativeInformationCheckService dealerNegativeInformationCheckService;

	@Autowired
	private ICreditConditionService creditConditionService;

	@Autowired
	private IFileuploadService fileuploadService;

	@Autowired
	private IGradeService gradeService;

	@Autowired
	private LoyaltyPerformance4qMapper loyaltyPerformance4qMapper;

	@Autowired
	private LoyaltyPerformance6monthMapper loyaltyPerformance6monthMapper;

	@Autowired
	private IAnnualReviewyService annualReviewyService;

	@Autowired
	private IDepositDecreaseService depositDecreaseService;

	@Autowired
	private RefreshMapper refreshMapper;

	@Autowired
	private IOracleService oracleService;

	@Autowired
	private IOtherFinancingResourceService otherFinancingResourceService;

	@Autowired
	private BalanceInformationMapper balanceInformationMapper;

	@Autowired
	private IDealerInformationService dealerInformationService;

	@Autowired
	private DealerInformationMapper dealerInformationMapper;

	@Autowired
	private ActiveLimitAdjustmentProposalMapper activeLimitAdjustmentProposalMapper;

	@Autowired
	private HBasicCorporateGuaranteeMapper basicCorporateGuaranteeMapper;

	@Autowired
	private HBasicCorporateMapper basicCorporateMapper;

	/**
	 * @param refreshDTO
	 * @description: 申请单刷新接口
	 * @param: instanceId
	 * @return:
	 * @author lss
	 * @date: 2022/8/28 13:35
	 */
	@Override
	public Map<String, Object> refreshInfoMation(RefreshDTO refreshDTO) throws IOException {
		log.info("刷新按钮开始");
		String[] dealerName1 = refreshDTO.getDealerName();
		String dealerNameStr = dealerName1[0];
		String[] split = dealerNameStr.split(",");
		List<String> dealerList = Arrays.asList(split);
		String instanceId = refreshDTO.getInstanceId();
		List<String> dealerNames = new ArrayList<>();
		List<String> dealerCodes = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();  //一级
		if(instanceId.contains("WLS")) {
			throw  new BaseException("数据异常，请退账号，重新登录");
		}
		AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId(instanceId);
		if (annualReviewy != null) {
			map.put("id", annualReviewy.getId()); // id
			map.put("instanceId", annualReviewy.getInstanceId()); // instanceId
			map.put("title", annualReviewy.getTitle()); // 标题
			map.put("createTime", annualReviewy.getCreateTime()); // 创建时间
			map.put("createName", annualReviewy.getCreateName()); // 发起人中午名称
			map.put("createBy", annualReviewy.getCreateBy()); // 发起人用户名称
			map.put("username", annualReviewy.getLockname()); // 任务拾取人
		}
		Map<String, Object> data = new HashMap<>();  // 二级
		List<Map<String, Object>> childrenList = new ArrayList();

		Map<String, Object> groupName = new HashMap<>();
		List<BasicInformation> basicInformationList = basicInformationMapper.selectDealerGroupByByInstanceId(instanceId);
		if (basicInformationList != null && basicInformationList.size() > 0) {
			List<DealerInformation> names = new ArrayList<>();
			for (int i = 0; i < basicInformationList.size(); i++) {
				DealerInformation dealerInformation = new DealerInformation();
				dealerInformation.setDealerName(basicInformationList.get(i).getDealerNameCN());
				names.add(dealerInformation);
				dealerNames.add(basicInformationList.get(i).getDealerNameCN());
			}
			basicInformationMapper.deleteBasicInformationByInstanceId(instanceId);
			List<DealerformationVO> dealerformationVOS = dealerInformationService.listByName(names);
			if (dealerformationVOS != null && dealerformationVOS.size() > 0) {
				for (DealerformationVO dealerformationVO : dealerformationVOS) {
					Map<String, Object> children = new HashMap<>();

					groupName.put("groupNameEN", dealerformationVO.getGroupNameEN()); // groupNameEN
					groupName.put("groupNameCN", dealerformationVO.getGroupNameCN()); // groupNameCN
					dealerCodes.addAll(dealerformationVO.getDealerCodeFromWFS());
					children.put("basicInformationId", dealerformationVO.getId());
					children.put("instanceId", instanceId);
					children.put("gm", dealerformationVO.getGm());
					children.put("dealerCodeFromManufacturer", dealerformationVO.getDealerCodeFromManufacturer());
					children.put("dealerCodeFromWFS", dealerformationVO.getDealerCodeFromWFS());
					children.put("dealerNameCN", dealerformationVO.getDealerNameCN());
					children.put("dealerNameEN", dealerformationVO.getDealerNameEN());
					children.put("dealername", dealerformationVO.getDealerNameCN());
					children.put("registeredAddressCN", dealerformationVO.getRegisteredAddressCN());
					children.put("registeredAddressEN", dealerformationVO.getRegisteredAddressEN());
					children.put("legalRepresentativeCN", dealerformationVO.getLegalRepresentativeCN());
					children.put("legalRepresentativeEN", dealerformationVO.getLegalRepresentativeEN());
					children.put("incorporationDate", dealerformationVO.getIncorporationDate());
					children.put("joinSectorDealerNetworkDate", dealerformationVO.getJoinSectorDealerNetworkDate());
					children.put("paidupCapital", dealerformationVO.getPaidupCapital());
					children.put("registeredCapital", dealerformationVO.getRegisteredCapital());
					children.put("province", dealerformationVO.getProvince());
					children.put("relatedWorkingExperience", dealerformationVO.getRelatedWorkingExperience());
					children.put("value", dealerformationVO.getDealerValue());
					if (null != dealerformationVO.getSector()) {
						children.put("sector", dealerformationVO.getSector());
					}
					if (null != dealerformationVO.getShareholdersAndShare()) {
						children.put("shareholdersAndShare", dealerformationVO.getShareholdersAndShare());
					}
					childrenList.add(children);

					String gm = children.get("gm") != null ? "" + children.get("gm") : "";
					String dealerCodeFromManufacturer = children.get("dealerCodeFromManufacturer") != null ? "" + children.get("dealerCodeFromManufacturer") : "";
					String dealerCodeFromWFS = children.get("dealerCodeFromWFS") != null ? "" + children.get("dealerCodeFromWFS") : "";
					String dealerNameCN = children.get("dealerNameCN") != null ? "" + children.get("dealerNameCN") : "";
					String dealerNameEN = children.get("dealerNameEN") != null ? "" + children.get("dealerNameEN") : "";
					String dealername = children.get("dealername") != null ? "" + children.get("dealername") : "";
					Date incorporationDate = children.get("incorporationDate") != null ? (Date) children.get("incorporationDate") : null;
					String individualGuarantor = children.get("individualGuarantor") != null ? "" + children.get("individualGuarantor") : "";
					Date joinSectorDealerNetworkDate = children.get("joinSectorDealerNetworkDate") != null ? (Date) children.get("joinSectorDealerNetworkDate") : null;
					String paidupCapital = children.get("paidupCapital") != null ? "" + children.get("paidupCapital") : "";
					String registeredCapital = children.get("registeredCapital") != null ? "" + children.get("registeredCapital") : "";
					String province = children.get("province") != null ? "" + children.get("province") : "";
					String registeredAddressCN = children.get("registeredAddressCN") != null ? "" + children.get("registeredAddressCN") : "";
					String registeredAddressEN = children.get("registeredAddressEN") != null ? "" + children.get("registeredAddressEN") : "";
					String legalRepresentativeEN = children.get("legalRepresentativeEN") != null ? "" + children.get("legalRepresentativeEN") : "";
					String legalRepresentativeCN = children.get("legalRepresentativeCN") != null ? "" + children.get("legalRepresentativeCN") : "";

					String value = children.get("value") != null ? "" + children.get("value") : "";
					StringBuilder sector = new StringBuilder();
					Object sector1 = children.get("sector");
					if (sector1 != null) {
						List sectorList = (List) sector1;//数组
						if (sectorList != null && sectorList.size() > 0) {
							for (int i = 0; i < sectorList.size(); i++) {
								sector.append(sectorList.get(i));
								sector.append(",");
							}
						}
					}

					if (!StringUtils.isEmpty(sector)) {
						sector.deleteCharAt(sector.length() - 1);
						System.out.println("sector" + sector);
					}
					// StringBuilder shareholdersAndShare=new StringBuilder();
					StringBuilder shareholders = new StringBuilder();
					StringBuilder share = new StringBuilder();
					Object shareholdersAndShare = children.get("shareholdersAndShare");
					if (shareholdersAndShare != null) {
						List shareholdersAndShareList = (List) shareholdersAndShare;//数组
						if (shareholdersAndShareList != null && shareholdersAndShareList.size() > 0) {
							for (int i = 0; i < shareholdersAndShareList.size(); i++) {
								Map<String, Object> children1 = (Map<String, Object>) shareholdersAndShareList.get(i);
								String s1 = (String) children1.get("shareholdersCn");
								String s2 = (String) children1.get("shareholdersEn");
								String s3 = (String) children1.get("share");
								shareholders.append(s1).append("$|").append(s2).append("$|").append(s3).append("#");
								share.append(s3);
								share.append(",");
							}
						}
					}

					if (!StringUtils.isEmpty(shareholders)) {
						shareholders.deleteCharAt(shareholders.length() - 1);
						System.out.println("shareholders" + shareholders);
					}
					if (!StringUtils.isEmpty(share)) {
						share.deleteCharAt(share.length() - 1);
						System.out.println("share" + share);
					}

					BasicInformation basicInformation = new BasicInformation();
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
					Object relatedWorkingExperience = children.get("relatedWorkingExperience");
					if (relatedWorkingExperience != null) {
						int relatedWorkingExperienceInt = ((Long) relatedWorkingExperience).intValue();
						basicInformation.setRelatedWorkingExperience(relatedWorkingExperienceInt);
					}
					if (groupName != null) {
						basicInformation.setGroupNameCN((String) groupName.get("groupNameCN"));
						basicInformation.setGroupNameEN((String) groupName.get("groupNameEN"));
					}
					basicInformation.setId(IdUtils.simpleUUID());
					if (!StringUtils.isEmpty(gm)) {
						basicInformation.setGm(gm);
					}
					if (!StringUtils.isEmpty(registeredCapital)) {
						basicInformation.setRegisteredCapital(registeredCapital);
					}
					if (!StringUtils.isEmpty(registeredAddressCN)) {
						basicInformation.setRegisteredAddressCN(registeredAddressCN);
					}
					if (!StringUtils.isEmpty(registeredAddressEN)) {
						basicInformation.setRegisteredAddressEN(registeredAddressEN);
					}
					if (!StringUtils.isEmpty(legalRepresentativeCN)) {
						basicInformation.setLegalRepresentativeCN(legalRepresentativeCN);
						basicInformation.setOutLegalRepresentativeCN(legalRepresentativeCN);
					}
					if (!StringUtils.isEmpty(legalRepresentativeEN)) {
						basicInformation.setLegalRepresentativeEN(legalRepresentativeEN);
					}
					if (!StringUtils.isEmpty(dealerCodeFromManufacturer) && !( "[]".equals(dealerCodeFromManufacturer))) {
						basicInformation.setDealerCodeFromManufacturer(dealerCodeFromManufacturer);
					}
					if (!StringUtils.isEmpty(dealerCodeFromWFS)) {
						basicInformation.setDealerCodeFromWFS(dealerCodeFromWFS.replace(" ", ""));
					}
					if (!StringUtils.isEmpty(dealerNameCN)) {
						basicInformation.setDealerNameCN(dealerNameCN);
					}
					if (!StringUtils.isEmpty(dealerNameEN)) {
						basicInformation.setDealerNameEN(dealerNameEN);
					}
					if (!StringUtils.isEmpty(dealername)) {
						basicInformation.setDealername(dealername);
					}
					if (null != incorporationDate) {
						String format = simpleDateFormat.format(incorporationDate);
						basicInformation.setIncorporationDate(format);
					}
					if (null != joinSectorDealerNetworkDate) {
						String format = simpleDateFormat.format(joinSectorDealerNetworkDate);
						basicInformation.setJoinSectorDealerNetworkDate(format);
					}
					if (!StringUtils.isEmpty(paidupCapital)) {
						basicInformation.setPaidupCapital(paidupCapital);
					}
					if (!StringUtils.isEmpty(province)) {
						basicInformation.setProvince(province);
					}

					if (!StringUtils.isEmpty(sector)) {
						basicInformation.setSector(sector.toString());
					}
					if (!StringUtils.isEmpty(shareholders)) {
						basicInformation.setShareholders(shareholders.toString());
					}
					if (!StringUtils.isEmpty(share)) {
						basicInformation.setShare(share.toString());
					}
					if (!StringUtils.isEmpty(value)) {
						basicInformation.setDealerValue(value);
					}
					basicInformation.setInstanceId(instanceId);
					basicInformationMapper.insertBasicInformation(basicInformation);
				}

			}
		}
		data.put("groupName", groupName); // groupNameEN

		List<LimitCalculationForCommericalNeedVO> limitCalculationForCommericalNeedVOS = new ArrayList<>();

		List<LimitCalculationForCommericalNeeds> limitCalculationForCommericalNeedList = limitCalculationForCommericalNeedsMapper.selectLimitCalculationForCommericalNeedsByInstanceId(instanceId);
		HashMap<String, Object> objectObjectHashMap = new HashMap<>();

		List<String> dname = new ArrayList<String>();
		for (LimitCalculationForCommericalNeeds lcfcn : limitCalculationForCommericalNeedList) {
			String dealername = lcfcn.getDealername();
			dname.add(dealername);
		}
		List<String> listTemp = new ArrayList<String>();
		for (int i = 0; i < dname.size(); i++) {
			if (!listTemp.contains(dname.get(i))) {
				listTemp.add(dname.get(i));
			}
		}

		objectObjectHashMap.put("instanceId", instanceId);
		objectObjectHashMap.put("dealerName", listTemp);

		List<Map<String, Object>> needs = annualReviewyService.sectorinformationAndDealerinformation(dealerNames,instanceId);

		List<Map<String, Object>> maps = annualReviewyService.approvalLimitCalculationForCommericalNeeds(objectObjectHashMap);
		if (maps != null && maps.size() > 0) {
			data.put("limit_calculation_for_commerical_needs", maps);
		}
		/**
		 * WHOLESALE PERFORMANCE-RECENT 3 MONTHS OS获取刷新数据
		 *
		 */
		log.info("WHOLESALE PERFORMANCE-RECENT 3 MONTHS OS获取刷新数据");
		List<Map<String, Object>> wholesalePerformanceOSList = annualReviewyService.listWprvo(dealerNames);
		System.out.println(wholesalePerformanceOSList);
		if (wholesalePerformanceOSList != null && wholesalePerformanceOSList.size() > 0) {
			for (Map<String, Object> stringObjectMap1 : wholesalePerformanceOSList) {
				Object result = stringObjectMap1.get("row");
				List<String> titleObj = (List<String>) stringObjectMap1.get("title");
				String value = null;
				if (titleObj != null) {
					value = Joiner.on(",").join(titleObj);
				}
				if (result != null) {
					List<Map<String, Object>> wholesalePerformanceCurrentlyList = (List<Map<String, Object>>) result;
					if (wholesalePerformanceCurrentlyList != null && wholesalePerformanceCurrentlyList.size() > 0) {
						System.out.println("删除历史");
						WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentVO = new WholesalePerformanceRecentThreeMonths();
						wholesalePerformanceRecentVO.setInstanceId(instanceId);
						wholesalePerformanceRecentVO.setType("OS");
						List<WholesalePerformanceRecentThreeMonths> wholesalePerformanceRecentVOS = wholesalePerformanceRecentThreeMonthsMapper.selectWholesalePerformanceRecentThreeMonthsList(wholesalePerformanceRecentVO);
						if(wholesalePerformanceRecentVOS!= null && wholesalePerformanceRecentVOS.size()>0 ){
							for (WholesalePerformanceRecentThreeMonths performanceRecentVO : wholesalePerformanceRecentVOS) {
								wholesalePerformanceRecentThreeMonthsMapper.deleteWholesalePerformanceRecentThreeMonthsById(performanceRecentVO.getId());
							}
						}
						for (Map<String, Object> stringObjectMap : wholesalePerformanceCurrentlyList) {
							WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths = new WholesalePerformanceRecentThreeMonths();
							wholesalePerformanceRecentThreeMonths.setId(IdUtils.simpleUUID());
							wholesalePerformanceRecentThreeMonths.setDealerId(IdUtils.simpleUUID());
							wholesalePerformanceRecentThreeMonths.setInstanceId(instanceId);
							wholesalePerformanceRecentThreeMonths.setType("OS");
							if (!StringUtils.isEmpty(stringObjectMap.get(titleObj.get(2)).toString())) {
								wholesalePerformanceRecentThreeMonths.setLastLastYearMonth(new BigDecimal(stringObjectMap.get(titleObj.get(2)).toString()).toString());
							}
							if (!StringUtils.isEmpty(stringObjectMap.get(titleObj.get(3)).toString())) {
								wholesalePerformanceRecentThreeMonths.setLastYearMonth(new BigDecimal(stringObjectMap.get(titleObj.get(3)).toString()).toString());
							}
							if (!StringUtils.isEmpty(stringObjectMap.get(titleObj.get(4)).toString())) {
								wholesalePerformanceRecentThreeMonths.setCurrentYearMonth(new BigDecimal(stringObjectMap.get(titleObj.get(4)).toString()).toString());
							}
							wholesalePerformanceRecentThreeMonths.setDEALER(stringObjectMap.get("Dealer").toString());
							wholesalePerformanceRecentThreeMonths.setBRAND(stringObjectMap.get("Sector").toString());
							wholesalePerformanceRecentThreeMonths.setThead(value);
							wholesalePerformanceRecentThreeMonthsMapper.insertWholesalePerformanceRecentThreeMonths(wholesalePerformanceRecentThreeMonths);
						}
					}
				}
			}

			data.put("WholesalePerformanceOS", wholesalePerformanceOSList);
		}
		log.info("WHOLESALE PERFORMANCE-RECENT 3 MONTHS OS获取刷新数据结束");
		// List<Map<String, Object>> maps1 = annualReviewyService.listWprvoApproval(objectObjectHashMap);
		// if (maps1 != null && maps1.size() > 0) {
		// 	data.put("WholesalePerformanceOS", maps1);
		// }
		/**
		 *SALES PERFORMANCE AND TARGET (DATA FROM OEM)获取数据更新
		 * */
		log.info("SALES PERFORMANCE AND TARGET (DATA FROM OEM)获取数据更新");
		List<Map<String, Object>> spatList = annualReviewyService.listspat(dealerNames);
		List<Map<String, Object>> spatVos = (List<Map<String, Object>>) spatList.get(0).get("children");

		if (StringUtils.isNotEmpty(spatVos)) {
			for (Map<String, Object> target : spatVos) {
				List<String> resultList = new ArrayList<String>();
				Date DateTemp = null;
				String flagDate = "2024-02-01";
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					DateTemp = sdf.parse(flagDate);
				}catch (Exception e){

				}
				if(new Date().getTime()>DateTemp.getTime()){
					Calendar cal = Calendar.getInstance();
					//近三个月带“-”
					cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1); //要先+1,才能把本月的算进去
					for (int i = 0; i < 2; i++) {
						cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1); //逐次往前推1个月
						resultList.add(String.valueOf(cal.get(Calendar.YEAR))
						);
					}
					Collections.reverse(resultList);
				}else{
					resultList.add("2022");
					resultList.add("2023");
				}

				Object o = target.get("YR" + resultList.get(0));
				Object o1 = target.get("YTD" + resultList.get(1));
				target.put("YR2021",o);
				target.put("YTD2022",o1);
				target.put("instanceId",instanceId);
				target.put("YYYY","YR" + resultList.get(0)+","+"YTD" + resultList.get(1));
				refreshMapper.updatePerformanceAndTarget(target);
			}
		}
		List<Map<String, Object>> maps2 = annualReviewyService.listspatApproval(objectObjectHashMap);
		if (maps2 != null && maps2.size() > 0) {
			data.put("sales_performance_and_target", maps2);
		}
		log.info("SALES PERFORMANCE AND TARGET (DATA FROM OEM)获取数据更新结束");
		List<Map<String, Object>> maps3 = annualReviewyService.selectplmWhereDealerName(listTemp);
		if (maps3 != null && maps3.size() > 0) {
			data.put("stock", maps3);
		}
		List<String> dealerNamesss = creditConditionMapper.listByInstanceId1(instanceId);
		ArrayList<ThirdCreditInfoSimpleVO> thirdCreditInfoSimpleVOS1 = new ArrayList<>();
		if (dealerNamesss.size() > 0) {
			for (String dealerNamess : dealerNamesss) {
				List<CreditCondition> creditConditions = creditConditionMapper.listByInstanceId2(instanceId, dealerNamess);
				ThirdCreditInfoSimpleVO thirdCreditInfoSimpleVO = new ThirdCreditInfoSimpleVO();
				ArrayList<ThirdCreditInfoSimpleChild> childArrayList = new ArrayList<>();
				for (CreditCondition creditCondition : creditConditions) {
					ThirdCreditInfoSimpleChild thirdCreditInfoSimpleChild = new ThirdCreditInfoSimpleChild();
					thirdCreditInfoSimpleVO.setDealerId(creditCondition.getDealerid());
					thirdCreditInfoSimpleVO.setDealerName(creditCondition.getDealername());
					thirdCreditInfoSimpleChild.setDealerId(creditCondition.getDealerid());
					thirdCreditInfoSimpleChild.setDealerName(creditCondition.getDealername());
					thirdCreditInfoSimpleChild.setSector(creditCondition.getSector());
					thirdCreditInfoSimpleChild.setLimitType(creditCondition.getLimittype());
					thirdCreditInfoSimpleChild.setExpiryDate(creditCondition.getExpireddate());
					thirdCreditInfoSimpleChild.setBuyBack(creditCondition.getBuyback());
					thirdCreditInfoSimpleChild.setLoanTenor(creditCondition.getLoantenor());
					thirdCreditInfoSimpleChild.setReviewType(creditCondition.getReviewtype());
					childArrayList.add(thirdCreditInfoSimpleChild);
				}
				if(childArrayList!=null && childArrayList.size()>0 ){
					for (ThirdCreditInfoSimpleChild  chi : childArrayList) {
						chi.setSort(DayUtils.typeSort(chi.getLimitType()));
					}
					Collections.sort(childArrayList, new Comparator<ThirdCreditInfoSimpleChild>() {
						@Override
						public int compare(ThirdCreditInfoSimpleChild o1, ThirdCreditInfoSimpleChild o2) {
							return o1.getSort().compareTo(o2.getSort());
						}

						@Override
						public boolean equals(Object obj) {
							return false;
						}
					});
					Collections.sort(childArrayList, new Comparator<ThirdCreditInfoSimpleChild>() {
						@Override
						public int compare(ThirdCreditInfoSimpleChild o1, ThirdCreditInfoSimpleChild o2) {
							return o1.getSector().substring(0).compareTo(o2.getSector().substring(0));
						}

						@Override
						public boolean equals(Object obj) {
							return false;
						}
					});
				}
				thirdCreditInfoSimpleVO.setChild(childArrayList);
				thirdCreditInfoSimpleVOS1.add(thirdCreditInfoSimpleVO);
			}
/*            List<String> names = creditConditions.stream().map(CreditCondition::getDealername).distinct().collect(Collectors.toList());
            List<ThirdCreditInfoSimpleVO> thirdCreditInfoSimpleVOS = creditConditionService.thirdCreditResult(names);
            for (ThirdCreditInfoSimpleVO t : thirdCreditInfoSimpleVOS) {
                for (ThirdCreditInfoSimpleChild r : t.getChild()) {
                    for (CreditCondition c : creditConditions) {
                        if (r.getDealerName().equals(c.getDealername()) && r.getSector().equals(c.getSector()) && r.getLimitType().equals(c.getLimittype())) {
                            if (r.getExpiryDate() == null) {
                                r.setExpiryDate(c.getExpireddate());
                            }
                            r.setReviewType(c.getReviewtype());
                            r.setLoanTenor(c.getLoantenor());
                            r.setBuyBack(c.getBuyback());
                            r.setId(c.getId());
                        }
                    }
                    ;
                }
                ;
            }
            ;*/
			data.put("credit_condition", thirdCreditInfoSimpleVOS1);
		}

		List<WholesalePerformanceCurrently> wholesalePerformanceCurrentlys = new ArrayList<>();
		wholesalePerformanceCurrentlys = wholesalePerformanceCurrentlyMapper.selectWholesalePerformanceCurrentlyByInstanceId(instanceId);
		if (wholesalePerformanceCurrentlys != null && wholesalePerformanceCurrentlys.size() > 0) {
			for (WholesalePerformanceCurrently who : wholesalePerformanceCurrentlys) {
				who.setSort(DayUtils.typeSort(who.getLimitType()));
			}
			Collections.sort(wholesalePerformanceCurrentlys, new Comparator<WholesalePerformanceCurrently>() {
				@Override
				public int compare(WholesalePerformanceCurrently o1, WholesalePerformanceCurrently o2) {
					return o1.getSort().compareTo(o2.getSort());
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});
			Collections.sort(wholesalePerformanceCurrentlys, new Comparator<WholesalePerformanceCurrently>() {
				@Override
				public int compare(WholesalePerformanceCurrently o1, WholesalePerformanceCurrently o2) {
					return o1.getSector().substring(0).compareTo(o2.getSector().substring(0));
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});

			Collections.sort(wholesalePerformanceCurrentlys, new Comparator<WholesalePerformanceCurrently>() {
				@Override
				public int compare(WholesalePerformanceCurrently o1, WholesalePerformanceCurrently o2) {
					return o1.getDealername().compareTo(o2.getDealername());
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});
			data.put("WholesalePerformanceCurrent", wholesalePerformanceCurrentlys);
		}

// 		JSONObject jsonObject = new JSONObject();
// 		jsonObject.put("DealerCode", depositDecreaseDTO.getDealerCodeArr());
// 		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
// //        HttpPost httpPost = new HttpPost("http://10.226.186.86:8081/CreditLoanSuspenseAPIUAT7/CreditInfo");
// 		HttpPost httpPost = new HttpPost(wfsUrl + "CreditInfo");
// 		httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
// 		StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
// 		entity.setContentType("application/json;charset=UTF-8");
// 		entity.setContentEncoding("UTF-8");
// 		httpPost.setEntity(entity);
// 		httpPost.setHeader("Accept", "application/json, text/plain, */*");
// 		httpPost.setHeader("Accept-Encoding", "gzip, deflate");
// 		httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
// 		httpPost.setHeader("Connection", "keep-alive");
// 		httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
// 		Map<String, Object> objectList = new HashMap<>();
// 		try {
// 			HttpResponse response = httpClient.execute(httpPost);
// 			String responseStr = EntityUtils.toString(response.getEntity());
// 			log.info("WFS外部接口数据返回结果=[{}]", responseStr);
// 			objectList = JSONObject.parseObject(responseStr, Map.class);
// 			log.info("WFS外部接口数据返回JSON结果=[{}]", JSON.toJSONString(responseStr));
//
//
// 		} catch (IOException e) {
// 			e.printStackTrace();
// 		}
// 		return AjaxResult.success(objectList);



		List<OtherFinancingResourceVO> otherFinancingResourceVOS = new ArrayList<>();
		if(dealerNames !=null && dealerNames.size()>0 ){
			List<OtherFinancingResource> otherFinancingResourceList = otherFinancingResourceMapper.selectOtherFinancingResourceBydealername(dealerNames);
			if (otherFinancingResourceList != null && otherFinancingResourceList.size() > 0) {
				data.put("other_financing_resource", otherFinancingResourceList);
			}
		}

		List<TemporaryConditionFollowUp> temporaryConditionFollowUpList = temporaryConditionFollowUpMapper.selectTemporaryConditionFollowUpByDealername(dealerNames);
		if (temporaryConditionFollowUpList != null && temporaryConditionFollowUpList.size() > 0) {
			data.put("temporary_condition_follow_up", temporaryConditionFollowUpList);
		}

		/**
		 *
		 LOYALTY PERFORMANCE (FOR RECENT 4 QUARTERS)
		 * */
		log.info("LOYALTY PERFORMANCE (FOR RECENT 4 QUARTERS");

		List<LoyaltyPerformance4q> loyaltyPerformance4qUpList = loyaltyPerformance4qMapper.selectLoyaltyPerformance4qByInstanceId(instanceId);
		if (loyaltyPerformance4qUpList != null && loyaltyPerformance4qUpList.size() > 0) {
			for (LoyaltyPerformance4q loyaltyPerformance4q : loyaltyPerformance4qUpList) {
				loyaltyPerformance4qMapper.deleteLoyaltyPerformance4qById(loyaltyPerformance4q.getId());
			}
		}

		List<LoyaltyPerformance4q> loyaltyPerformance4qList = oracleService.getLoyaltyPerformance4Q(dealerList);
		if (StringUtils.isNotEmpty(loyaltyPerformance4qList)) {
			for (LoyaltyPerformance4q loyaltyPerformance4q : loyaltyPerformance4qList) {
				loyaltyPerformance4q.setId(IdUtils.simpleUUID());
				loyaltyPerformance4q.setInstanceId(instanceId);
				loyaltyPerformance4qMapper.insertLoyaltyPerformance4q(loyaltyPerformance4q);
			}
		}
		List<LoyaltyPerformance4q> loyaltyPerformance4qUpList1 = loyaltyPerformance4qMapper.selectLoyaltyPerformance4qByInstanceId(instanceId);
		if (loyaltyPerformance4qUpList1 != null && loyaltyPerformance4qUpList1.size() > 0) {
			data.put("loyalty_performance_4q", loyaltyPerformance4qUpList1);
		}
		log.info("LOYALTY PERFORMANCE (FOR RECENT 4 QUARTERS结束");
		/**
		 * LOYALTY PERFORMANCE (FOR RECENT 6 MONTHS)
		 * DO:刷新数据
		 */
		log.info("LOYALTY PERFORMANCE (FOR RECENT 6 MONTHS");
		LoyaltyPerformanceRecentDTO recentDTO = new LoyaltyPerformanceRecentDTO();
		recentDTO.setDealerName(refreshDTO.getDealerName());
		List<LoyaltyPerformance6month> loyaltyPerformance6monthList = depositDecreaseService.loyaltyPerformanceRecentMonths(recentDTO);
		if (StringUtils.isNotEmpty(loyaltyPerformance6monthList)) {
			for (LoyaltyPerformance6month recent6month : loyaltyPerformance6monthList) {
				refreshMapper.updateRecent6Month(recent6month);
			}
		}
		List<LoyaltyPerformance6month> loyaltyPerformance6monthUpList = loyaltyPerformance6monthMapper.selectLoyaltyPerformance6monthByInstanceId(instanceId);
		if (loyaltyPerformance6monthUpList != null && loyaltyPerformance6monthUpList.size() > 0) {
			data.put("loyalty_performance_6month", loyaltyPerformance6monthUpList);
		}
		log.info("LOYALTY PERFORMANCE (FOR RECENT 6 MONTHS结束");
		List<String> loyalty_performance_6month_hearders_result = new ArrayList<>();
		loyalty_performance_6month_hearders_result.add("Item");
		loyalty_performance_6month_hearders_result.add("Dealer");
		loyalty_performance_6month_hearders_result.add("Sector");
		List<String> loyalty_performance_6month_hearders = loyaltyPerformance6monthMapper.selectLoyaltyPerformance6monthHeardersByInstanceId(instanceId);
		if (loyalty_performance_6month_hearders != null && loyalty_performance_6month_hearders.size() > 0) {
			loyalty_performance_6month_hearders_result.addAll(loyalty_performance_6month_hearders);
			data.put("loyalty_performance_6month_hearders", loyalty_performance_6month_hearders_result);
		}

		/**
		 * 构造查询外部数据查询二维数组
		 */
		log.info("balance_information 数据更新");
		DepositDecreaseDTO depositDecreaseDTO = new DepositDecreaseDTO();
		List<String[]> dealerCodeArr = new ArrayList<>();
		for (int i = 0; i < refreshDTO.getDealerName().length; i++) {
			String dealerName = refreshDTO.getDealerName()[i];
			List<DealerInformation> dealerInfoList = dealerInformationMapper.selectDealerInformationByDealerName(dealerName);
			if (dealerInfoList.size() == 0) {
				continue;
			}
			String[] dealerCodeTemp = new String[dealerInfoList.size()];
			for (int j = 0; j < dealerInfoList.size(); j++) {
				dealerCodeTemp[j] = dealerInfoList.get(j).getDealerCode();
			}
			dealerCodeArr.add(dealerCodeTemp);
		}
		depositDecreaseDTO.setDealerCodeArr(dealerCodeArr);
		AjaxResult result = new AjaxResult();
		if (depositDecreaseDTO.getDealerCodeArr().size() > 0) {
			result = dealerInformationService.getSuspenseInfo(depositDecreaseDTO);
		}
		if (result != null) {
			Object objResult = result.get("data");
			if (objResult != null) {
				Map<String, Object> resultMap = (Map<String, Object>) objResult;
				List<DealerInformation> resultList = (List<DealerInformation>) resultMap.get("balanceInformation");
				for (int i = 0; i < resultList.size(); i++) {
					DealerInformation dealerInformation = resultList.get(i);
					balanceInformationMapper.updateBalanceInforMationByDealername(dealerInformation);
				}
			}
		}

		BalanceInformation balanceInformation = new BalanceInformation();
		balanceInformation.setInstanceId(instanceId);
		List<BalanceInformation> balanceInformationList = balanceInformationMapper.selectBalanceInformationList(balanceInformation);
		data.put("balance_information", balanceInformationList);
		log.info("balance_information 数据更新结束");
		List<ProposalByCommericalAndMarketingVO> proposalByCommericalAndMarketingVOS = new ArrayList<>();
		List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketingList = proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingByInstanceId(instanceId);
		if (proposalByCommericalAndMarketingList != null && proposalByCommericalAndMarketingList.size() > 0) {
			for (ProposalByCommericalAndMarketing pro : proposalByCommericalAndMarketingList) {
				pro.setSort(DayUtils.typeSort(pro.getLimitType()));
			}
			Collections.sort(proposalByCommericalAndMarketingList, new Comparator<ProposalByCommericalAndMarketing>() {
				@Override
				public int compare(ProposalByCommericalAndMarketing o1, ProposalByCommericalAndMarketing o2) {
					return o1.getSort().compareTo(o2.getSort());
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});
			Collections.sort(proposalByCommericalAndMarketingList, new Comparator<ProposalByCommericalAndMarketing>() {
				@Override
				public int compare(ProposalByCommericalAndMarketing o1, ProposalByCommericalAndMarketing o2) {
					return o1.getSector().substring(0).compareTo(o2.getSector().substring(0));
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});

			Collections.sort(proposalByCommericalAndMarketingList, new Comparator<ProposalByCommericalAndMarketing>() {
				@Override
				public int compare(ProposalByCommericalAndMarketing o1, ProposalByCommericalAndMarketing o2) {
					return o1.getDealername().compareTo(o2.getDealername());
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});
			data.put("proposal_by_commerical_and_marketing", proposalByCommericalAndMarketingList);
		}

		ActiveLimitAdjustmentProposal proposalParam = new ActiveLimitAdjustmentProposal();
		proposalParam.setInstanceId(instanceId);
		List<ActiveLimitAdjustmentProposal> proposalList = activeLimitAdjustmentProposalMapper.selectActiveLimitAdjustmentProposalList(proposalParam);
		if (proposalList != null && proposalList.size() > 0) {
			for (ActiveLimitAdjustmentProposal proposal : proposalList) {
				proposal.setSort(DayUtils.typeSort(proposal.getLimitType()));
			}
			Collections.sort(proposalList, new Comparator<ActiveLimitAdjustmentProposal>() {
				@Override
				public int compare(ActiveLimitAdjustmentProposal o1, ActiveLimitAdjustmentProposal o2) {
					return o1.getSort().compareTo(o2.getSort());
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});
			Collections.sort(proposalList, new Comparator<ActiveLimitAdjustmentProposal>() {
				@Override
				public int compare(ActiveLimitAdjustmentProposal o1, ActiveLimitAdjustmentProposal o2) {
					return o1.getSector().substring(0).compareTo(o2.getSector().substring(0));
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});

			Collections.sort(proposalList, new Comparator<ActiveLimitAdjustmentProposal>() {
				@Override
				public int compare(ActiveLimitAdjustmentProposal o1, ActiveLimitAdjustmentProposal o2) {
					return o1.getDealerName().compareTo(o2.getDealerName());
				}

				@Override
				public boolean equals(Object obj) {
					return false;
				}
			});
			data.put("active_limit_adjustment_proposal", proposalList);
		}

		List<Grade> gradeList = gradeService.selectGradeByInstanceId(instanceId);
		if (gradeList != null && gradeList.size() > 0) {
			data.put("grade", gradeList);
		}


		HBasicCorporateGuarantee hBasicCorporateGuarantee = new HBasicCorporateGuarantee();
		hBasicCorporateGuarantee.setInstanceid(instanceId);
		List<HBasicCorporateGuarantee> hBasicCorporateGuarantees = basicCorporateGuaranteeMapper.selectHBasicCorporateGuaranteeList(hBasicCorporateGuarantee);
		if (hBasicCorporateGuarantees != null && hBasicCorporateGuarantees.size() > 0) {
			data.put("GuaranteeInfo", hBasicCorporateGuarantees);
		}
		HBasicCorporate hBasicCorporate = new HBasicCorporate();
		hBasicCorporate.setInstanceid(instanceId);
		List<HBasicCorporate> hBasicCorporates = basicCorporateMapper.selectHBasicCorporateList(hBasicCorporate);
		if (hBasicCorporates != null && hBasicCorporates.size() > 0) {
			data.put("CorporateInfo", hBasicCorporates);
		}

		List<SecuritieVO> securitieVOS = new ArrayList<>();
		List<Securities> securitieList = securitiesMapper.selectSecuritiesByInstanceId(instanceId);
		/*LinkedHashSet<String> dealerNames = new LinkedHashSet<>();
		for (Securities securities : securitieList) {
			String dealername = securities.getDealername();
			dealerNames.add(dealername);
		}*/
		List<String> dealers = new ArrayList<>(dealerNames);
		for (String dealer : dealers) {
			SecuritieVO securitieVO = new SecuritieVO();
			securitieVO.setDealername(dealer);
			securitieVOS.add(securitieVO);
		}
		if (securitieList != null && securitieList.size() > 0) {
			for (int j = 0; j < securitieList.size(); j++) {
				Securities securities = securitieList.get(j);
				for (int i = 0; i < securitieVOS.size(); i++) {
					SecuritieVO securitieVO = securitieVOS.get(i);
					if (securitieVO.getDealername().equals(securities.getDealername())) {
						if (null == securitieVO.getChildren()) {
							securitieVO.setChildren(new ArrayList<Securities>());
						}
						securitieVO.getChildren().add(securities);
					}
				}
			}
		}
		data.put("securities", securitieVOS);
		List<TemporaryConditionFollowUp> temporaryConditionFollowUp = temporaryConditionFollowUpMapper.selectTemporaryConditionFollowUpByInstanceId(instanceId);
		if (null != temporaryConditionFollowUp) {
			map.put("temporaryConditionFollowUp", temporaryConditionFollowUp);
		}
		DealerNegativeInformationCheck dealerNegativeInformationCheck = dealerNegativeInformationCheckService.selectDealerNegativeInformationCheckById(instanceId);
		if (null != dealerNegativeInformationCheck) {
			map.put("dealerNegativeInformationCheck", dealerNegativeInformationCheck);
		}

//        /**
//         *STOCK AUDIT PERFORMANCE (FOR RECENT 6MONTHS)
//         * DO:刷新数据
//         */
		try {
			List<Map<String, Object>> maps4 = annualReviewyService.selectplmWhereDealerName(dealerList);
			map.put("sapForRecent", maps4);
		} catch (Exception e) {
			map.put("sapForRecent", new ArrayList<>());
			log.error(e.getMessage());
		}

		/**
		 *OTHER FINANCING RESOURCE
		 * DO:刷新数据
		 */
		OtherFinancingResourceShowVO vo = new OtherFinancingResourceShowVO();
		List<OtherFinancingResource> list = otherFinancingResourceService.selectOtherFinancingResourceList(dealerList);
		List<String> dealNames = new ArrayList<>();
		if (CollectionUtil.isNotEmpty(list)) {
			dealNames = list.stream().map(OtherFinancingResource::getDealername).distinct().collect(Collectors.toList());
		}
		vo.setTempList(list);
		vo.setDealNames(dealNames);
		for (OtherFinancingResource otherFinancingResource : list) {
			otherFinancingResource.setInstanceId(instanceId);
			otherFinancingResourceService.updateOtherFinancingResource(otherFinancingResource);
		}
		map.put("OtherFinancingResource", vo);
		/**
		 *PROPOSAL BY COMMERCIAL & MARKETING
		 * DO:刷新数据
		 */
		List<ThirdCreditInfoSimpleVO> thirdCreditInfoSimpleVOS = creditConditionService.thirdCreditResult(dealerList);
		for (ThirdCreditInfoSimpleVO thirdCreditInfoSimpleVO : thirdCreditInfoSimpleVOS) {
			List<ThirdCreditInfoSimpleChild> child = thirdCreditInfoSimpleVO.getChild();
			for (ThirdCreditInfoSimpleChild thirdCreditInfoSimpleChild : child) {
				CreditCondition creditCondition = new CreditCondition();
				BeanUtils.copyProperties(thirdCreditInfoSimpleChild, creditCondition);
				creditCondition.setDealername(thirdCreditInfoSimpleVO.getDealerName());
				creditCondition.setInstanceid(instanceId);
//                creditCondition.setDealerid(thirdCreditInfoSimpleVO.getDealerId());
				creditConditionService.updateCreditCondition(creditCondition);
			}

		}
		map.put("thirdCreditInfo", thirdCreditInfoSimpleVOS);

		Fileupload fileupload = new Fileupload();
		fileupload.setInstanceId(instanceId);
		fileupload.setType("application");
		List<Fileupload> application = fileuploadService.selectFileuploadList(fileupload);
		if (application != null) {
			map.put("application", application);
		}
		fileupload.setType("information");
		List<Fileupload> information = fileuploadService.selectFileuploadList(fileupload);
		if (information != null) {
			map.put("information", information);
		}
		fileupload.setType("financials");
		List<Fileupload> financials = fileuploadService.selectFileuploadList(fileupload);
		if (financials != null) {
			map.put("financials", financials);
		}
		fileupload.setType("guarantee");
		List<Fileupload> guarantee = fileuploadService.selectFileuploadList(fileupload);
		if (guarantee != null) {
			map.put("guarantee", guarantee);
		}
		fileupload.setType("corporate");
		List<Fileupload> corporate = fileuploadService.selectFileuploadList(fileupload);
		if (corporate != null) {
			map.put("corporate", corporate);
		}
		fileupload.setType("other");
		List<Fileupload> other = fileuploadService.selectFileuploadList(fileupload);
		if (other != null) {
			map.put("other", other);
		}
		fileupload.setType("financialAnalysis");
		List<Fileupload> financialAnalysis = fileuploadService.selectFileuploadList(fileupload);
		if (financialAnalysis != null) {
			map.put("financialAnalysis", financialAnalysis);
		}
		fileupload.setType("pboc");
		List<Fileupload> pboc = fileuploadService.selectFileuploadList(fileupload);
		if (pboc != null) {
			map.put("pboc", pboc);
		}
		fileupload.setType("lawsuit");
		List<Fileupload> lawsuit = fileuploadService.selectFileuploadList(fileupload);
		if (lawsuit != null) {
			map.put("lawsuit", lawsuit);
		}
		fileupload.setType("underwriterOther");
		List<Fileupload> underwriterOther = fileuploadService.selectFileuploadList(fileupload);
		if (underwriterOther != null) {
			map.put("underwriterOther", underwriterOther);
		}
		fileupload.setType("opDocument");
		List<Fileupload> opDocument = fileuploadService.selectFileuploadList(fileupload);
		if (opDocument != null) {
			map.put("opDocument", opDocument);
		}
		fileupload.setType("opDocumentFax");
		List<Fileupload> opDocumentFax = fileuploadService.selectFileuploadList(fileupload);
		if (opDocumentFax != null) {
			map.put("opDocumentFax", opDocumentFax);
		}

		if (null == annualReviewy.getLockname()) {
			String username = SecurityUtils.getLoginUser().getUsername();
			annualReviewyMapper.closeUpdateAnnualReviewyObj(instanceId, username);
		}
		data.put("children", childrenList);

		Notes notes = notesMapper.selectNotesByInstanceId(instanceId);
		if (notes != null) {
			data.put("basicNotes", notes.getBasicNotes());
			data.put("dealerNotes", notes.getDealerNotes());
			data.put("loyaltyNotes", notes.getLoyaltyNotes());
			data.put("auditNotes", notes.getAuditNotes());
			data.put("wholesaleNotes", notes.getWholesaleNotes());
			data.put("proposalNotes", notes.getProposalNotes());
			data.put("guaranteeNotes", notes.getGuaranteeNotes());
			data.put("amlNotes", notes.getAmlNotes());
		}
		map.put("data", data);
		return map;

	}
}
