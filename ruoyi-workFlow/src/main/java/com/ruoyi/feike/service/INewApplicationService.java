package com.ruoyi.feike.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.feike.domain.NewApplicationDTO;


/**
 * @author lss
 * @version 1.0
 * @description: 新经销商申请接口类
 * @date 2022/8/2 15:07
 */
public interface INewApplicationService {
    
    /**
    * @description: 新经销商申请审批流程接口
    * @param: newApplicationDTO
    * @return: AjaxResult
    * @author lss
    * @date: 2022/8/2 15:15
    */
    AjaxResult insertNewApplication(NewApplicationDTO newApplicationDTO);
}
