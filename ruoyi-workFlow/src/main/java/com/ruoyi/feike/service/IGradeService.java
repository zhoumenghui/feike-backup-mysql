package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.Grade;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-08-22
 */
public interface IGradeService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public Grade selectGradeById(String id);

    /**
     * 查询feike列表
     *
     * @param grade feike
     * @return feike集合
     */
    public List<Grade> selectGradeList(Grade grade);

    /**
     * 新增feike
     *
     * @param grade feike
     * @return 结果
     */
    public int insertGrade(Grade grade);

    /**
     * 修改feike
     *
     * @param grade feike
     * @return 结果
     */
    public int updateGrade(Grade grade);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteGradeByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteGradeById(String id);

    List<Grade> selectGradeByInstanceId(String instanceId);
}
