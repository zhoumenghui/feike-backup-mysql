package com.ruoyi.feike.service.impl;

import java.io.File;
import java.util.List;

import com.ruoyi.common.config.RuoYiConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.domain.Fileupload;
import com.ruoyi.feike.mapper.FileuploadMapper;
import com.ruoyi.feike.service.IFileuploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * feikeService业务层处理
 *
 * @author ruoyi
 * @date 2022-07-22
 */
@Service
@Slf4j
public class FileuploadServiceImpl implements IFileuploadService {

	@Autowired
	private FileuploadMapper fileuploadMapper;

	/**
	 * 查询feike
	 *
	 * @param id feikeID
	 * @return feike
	 */
	@Override
	public Fileupload selectFileuploadById(String id) {
		return fileuploadMapper.selectFileuploadById(id);
	}

	@Override
	public List<Fileupload> selectFileuploadByInstanceId(String instanceId) {
		return fileuploadMapper.selectFileuploadByInstanceId(instanceId);
	}

	@Override
	public List<Fileupload> selectFileuploadListByInstanceIdAndDm(String instanceId) {
		return fileuploadMapper.selectFileuploadListByInstanceIdAndDm(instanceId);
	}

	/**
	 * 查询feike列表
	 *
	 * @param fileupload feike
	 * @return feike
	 */
	@Override
	public List<Fileupload> selectFileuploadList(Fileupload fileupload) {
		return fileuploadMapper.selectFileuploadList(fileupload);
	}

	@Override
	public List<Fileupload> selectFileUploadByMAX(Fileupload fileupload) {
		return fileuploadMapper.selectFileUploadByMAX(fileupload);
	}

	/**
	 * 新增feike
	 *
	 * @param fileupload feike
	 * @return 结果
	 */
	@Override
	public int insertFileupload(Fileupload fileupload) {
		return fileuploadMapper.insertFileupload(fileupload);
	}

	/**
	 * 修改feike
	 *
	 * @param fileupload feike
	 * @return 结果
	 */
	@Override
	public int updateFileupload(Fileupload fileupload) {
		return fileuploadMapper.updateFileupload(fileupload);
	}

	/**
	 * 批量删除feike
	 *
	 * @param ids 需要删除的feikeID
	 * @return 结果
	 */
	@Override
	public int deleteFileuploadByIds(String[] ids) {
		String profile = RuoYiConfig.getProfile();
		for (int i = 0; i < ids.length; i++) {
			Fileupload fileupload = fileuploadMapper.selectFileuploadById(ids[i]);
			log.info("删除数据记录id:"+ids[i]);
			if (null != fileupload) {
				String fileName = fileupload.getFileName();
				fileName = fileName.replace("/profile", profile);
				File file = new File(fileName);
				if (file.exists()) {
					try {
						file.delete();
						log.info("删除服务器文件目录为:"+fileupload.getFileName());
					} catch (Exception e) {
						System.out.println("删除失败");
					}
				}
			}

		}
		return fileuploadMapper.deleteFileuploadByIds(ids);
	}

	/**
	 * 删除feike信息
	 *
	 * @param id feikeID
	 * @return 结果
	 */
	@Override
	public int deleteFileuploadById(String id) {
		return fileuploadMapper.deleteFileuploadById(id);
	}

	@Override
	public List<Fileupload> selectFileuploadListByInstanceId(String instanceId) {

		List<Fileupload> fileuploadList = fileuploadMapper.selectFileuploadListByInstanceId(instanceId);
		return fileuploadList;
	}

	@Override
	public int delFileUrl(String fileName) {
		String profile = RuoYiConfig.getProfile();
		fileName = fileName.replace("/profile", profile);
		log.info("文件删除，上传路径={}",fileName);
		File file = new File(fileName);
		if (file.exists()) {
			try {
				file.delete();
			} catch (Exception e) {
				log.error("文件删除失败，上传路径={}",profile);
			}
		}
		return 1;
	}
}
