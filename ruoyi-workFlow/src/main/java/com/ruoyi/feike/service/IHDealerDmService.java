package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.HDealerDm;

import java.util.List;

/**
 * 经销商与dm岗位用户映射Service接口
 *
 * @author ruoyi
 * @date 2022-12-23
 */
public interface IHDealerDmService
{
    /**
     * 查询经销商与dm岗位用户映射
     *
     * @param id 经销商与dm岗位用户映射ID
     * @return 经销商与dm岗位用户映射
     */
    public HDealerDm selectHDealerDmById(Long id);

    /**
     * 查询经销商与dm岗位用户映射列表
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 经销商与dm岗位用户映射集合
     */
    public List<HDealerDm> selectHDealerDmList(HDealerDm hDealerDm);

    /**
     * 新增经销商与dm岗位用户映射
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 结果
     */
    public int insertHDealerDm(HDealerDm hDealerDm);

    /**
     * 修改经销商与dm岗位用户映射
     *
     * @param hDealerDm 经销商与dm岗位用户映射
     * @return 结果
     */
    public int updateHDealerDm(HDealerDm hDealerDm);

    /**
     * 批量删除经销商与dm岗位用户映射
     *
     * @param ids 需要删除的经销商与dm岗位用户映射ID
     * @return 结果
     */
    public int deleteHDealerDmByIds(Long[] ids);

    /**
     * 删除经销商与dm岗位用户映射信息
     *
     * @param id 经销商与dm岗位用户映射ID
     * @return 结果
     */
    public int deleteHDealerDmById(Long id);
}
