package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.LoyaltyPerformance4q;

import java.util.List;
import java.util.Map;

public interface IOracleService {

	public List<LoyaltyPerformance4q> getLoyaltyPerformance4Q(List<String> dealerNames);

	List<LoyaltyPerformance4q> getLoyaltyPerformance6Month(List<String> dealerNames);
}
