package com.ruoyi.feike.service;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.BasicContract;
import com.ruoyi.feike.domain.ContractApproval;
import com.ruoyi.feike.domain.vo.VehicleContract;

/**
 * feikeService接口
 *
 * @author ruoyi
 * @date 2022-09-06
 */
public interface IContractApprovalService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public ContractApproval selectContractApprovalById(String id);

    /**
     * 查询feike列表
     *
     * @param contractApproval feike
     * @return feike集合
     */
    public List<ContractApproval> selectContractApprovalList(ContractApproval contractApproval);

    /**
     * 新增feike
     *
     * @param map feike
     * @return 结果
     */
    public int insertContractApproval(Map<String, Object> map);

    /**
     * 修改feike
     *
     * @param contractApproval feike
     * @return 结果
     */
    public int updateContractApproval(ContractApproval contractApproval);


    /**
     * 根据instanceId 修改feike
     *
     * @param contractApproval feike
     * @return 结果
     */
    public int updateContractApprovalByInstanceId(ContractApproval contractApproval);



    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteContractApprovalByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteContractApprovalById(String id);

    ContractApproval getInfoByInstanceId(String instanceId);

    List<BasicContract> generateContract(List<VehicleContract> vehicleContracts);

    int insertContractApprovalList(Map<String, Object> map);
}
