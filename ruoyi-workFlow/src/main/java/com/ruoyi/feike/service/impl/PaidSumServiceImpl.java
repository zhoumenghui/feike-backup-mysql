package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.PaidSumMapper;
import com.ruoyi.feike.domain.PaidSum;
import com.ruoyi.feike.service.IPaidSumService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class PaidSumServiceImpl implements IPaidSumService 
{
    @Autowired
    private PaidSumMapper paidSumMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public PaidSum selectPaidSumById(String id)
    {
        return paidSumMapper.selectPaidSumById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param paidSum feike
     * @return feike
     */
    @Override
    public List<PaidSum> selectPaidSumList(PaidSum paidSum)
    {
        return paidSumMapper.selectPaidSumList(paidSum);
    }

    /**
     * 新增feike
     * 
     * @param paidSum feike
     * @return 结果
     */
    @Override
    public int insertPaidSum(PaidSum paidSum)
    {
        return paidSumMapper.insertPaidSum(paidSum);
    }

    /**
     * 修改feike
     * 
     * @param paidSum feike
     * @return 结果
     */
    @Override
    public int updatePaidSum(PaidSum paidSum)
    {
        return paidSumMapper.updatePaidSum(paidSum);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deletePaidSumByIds(String[] ids)
    {
        return paidSumMapper.deletePaidSumByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deletePaidSumById(String id)
    {
        return paidSumMapper.deletePaidSumById(id);
    }
}
