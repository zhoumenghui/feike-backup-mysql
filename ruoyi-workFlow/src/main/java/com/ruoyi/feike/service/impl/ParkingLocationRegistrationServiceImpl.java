package com.ruoyi.feike.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.activiti.service.impl.ActTaskServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.InstanceIdUtil;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IAnnualReviewyService;
import com.ruoyi.feike.service.IFileuploadService;
import com.ruoyi.feike.service.IParkingLocationRegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * feikeService业务层处理
 *
 * @author ruoyi
 * @date 2022-08-02
 */
@Service
@Slf4j
public class ParkingLocationRegistrationServiceImpl implements IParkingLocationRegistrationService
{
    @Autowired
    private ParkingLocationRegistrationMapper parkingLocationRegistrationMapper;

    @Autowired
    private ParkingLocationMapper parkingLocationMapper;

    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;

    @Autowired
    private IFileuploadService fileuploadService;

    @Autowired
    private ProcessRuntime processRuntime;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private IAnnualReviewyService annualReviewyService;
    @Value("${wfs.url}")
    private String wfsUrl;

    @Autowired
    private InstanceIdUtil instanceIdUtil;

    @Autowired
    private NotesMapper notesMapper;

    @Autowired
    private ActTaskServiceImpl actTaskService;

    @Autowired
    private BasicInformationMapper basicInformationMapper;


    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public Map<String, Object> selectParkingLocationRegistrationById(String id)
    {
        Map<String, Object> map=new HashMap<>();
        List<ParkingLocationRegistration> list=new ArrayList();

        AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId1(id);
        List<ParkingLocationRegistration> parkingLocationRegistrationList = parkingLocationRegistrationMapper.selectParkingLocationRegistrationByInstancsId(annualReviewy.getInstanceId());
        System.out.println("parkingLocationRegistrationList=="+parkingLocationRegistrationList);
        if (parkingLocationRegistrationList!=null&&parkingLocationRegistrationList.size()>0){
            for (ParkingLocationRegistration parkingLocationRegistration : parkingLocationRegistrationList) {
                if(StringUtils.isEmpty(parkingLocationRegistration.getAddress())){
                    parkingLocationRegistration.setAddress("");
                }
                System.out.println("parkingLocationRegistration对象=="+parkingLocationRegistration);
                // String parkinglocation = parkingLocation.getProvince();
                String parkinglocation1 = parkingLocationRegistration.getAddress();
                if(!StringUtils.isEmpty(parkinglocation1)){
                    boolean contains = parkinglocation1.contains(",");
                    if (contains){
                        String[] split = parkinglocation1.split(",");
                        System.out.println("split=="+split.toString());
                        parkingLocationRegistration.setParkinglocations(split);
                    }
                }

                String vin = parkingLocationRegistration.getVin();
                if(null !=vin){
                    parkingLocationRegistration.setVin(vin.replaceAll("\"","").replaceAll("[\\[\\]{}]",""));
                    if(StringUtils.isNotEmpty(vin)){
                        parkingLocationRegistration.setVins(vin.replaceAll("\"","").replaceAll("[\\[\\]{}]","").split(","));
                    }
                }
                List<Fileupload> fileuploadList=fileuploadService.selectFileuploadByInstanceId(parkingLocationRegistration.getId());
                System.out.println("fileuploadList=="+fileuploadList);
                parkingLocationRegistration.setUploadFile(fileuploadList);
                String dealernamecn = parkingLocationRegistration.getDealerName();
                String groupName = parkingLocationRegistration.getGroupNameCn();

                map.put("groupName",groupName);
                map.put("dealerName",dealernamecn);
                list.add(parkingLocationRegistration);
            }
        }

        map.put("id",annualReviewy.getId());
        map.put("instanceId",annualReviewy.getInstanceId());
        map.put("data",list);
        System.out.println("返回的map数据=="+map);
        return map;
    }

    @Override
    public Map<String, Object> selectParkingLocationRegistrationByInstanceId(String instanceId) {
        Map<String, Object> map=new HashMap<>();
        List<ParkingLocationRegistration> list=new ArrayList();
        AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId(instanceId);
        List<ParkingLocationRegistration> parkingLocationRegistrationList = parkingLocationRegistrationMapper.selectParkingLocationRegistrationByInstancsId(annualReviewy.getInstanceId());
        System.out.println("parkingLocationRegistrationList=="+parkingLocationRegistrationList);
        if (parkingLocationRegistrationList!=null&&parkingLocationRegistrationList.size()>0){
            for (ParkingLocationRegistration parkingLocationRegistration : parkingLocationRegistrationList) {
                System.out.println("parkingLocationRegistration对象=="+parkingLocationRegistration);
                String parkinglocation1 = parkingLocationRegistration.getAddress();
                String[] split = parkinglocation1.split(",");
                System.out.println("split=="+split.toString());
                parkingLocationRegistration.setParkinglocations(split);
                String vin = parkingLocationRegistration.getVin();
                if(null != vin){
                    String[] vins = vin.split(",");
                    parkingLocationRegistration.setVins(vins);
                }else{
                    parkingLocationRegistration.setVins(null);
                }


                List<Fileupload> fileuploadList=fileuploadService.selectFileuploadByInstanceId(parkingLocationRegistration.getId());
                System.out.println("fileuploadList=="+fileuploadList);
                parkingLocationRegistration.setUploadFile(fileuploadList);
                String dealernamecn = parkingLocationRegistration.getDealerName();
                String groupName = parkingLocationRegistration.getGroupNameCn();

                map.put("groupName",groupName);
                map.put("dealerName",dealernamecn);
                list.add(parkingLocationRegistration);
            }
        }

        map.put("id",annualReviewy.getId());
        map.put("instanceId",annualReviewy.getInstanceId());
        map.put("data",list);
        System.out.println("返回的map数据=="+map);
        return map;

    }

    /**
     * 查询feike列表
     *
     * @param parkingLocationRegistration feike
     * @return feike
     */
    @Override
    public List<ParkingLocationRegistration> selectParkingLocationRegistrationList(ParkingLocationRegistration parkingLocationRegistration)
    {
        return parkingLocationRegistrationMapper.selectParkingLocationRegistrationList(parkingLocationRegistration);
    }

    /**
     * 新增feike
     *
     * @param parkingLocationRegistration feike
     * @return 结果
     */
    @Override
    public int insertParkingLocationRegistration(ParkingLocationRegistration parkingLocationRegistration)
    {
        System.out.println("parkingLocationRegistration新增对象： "+parkingLocationRegistration);
        //String instanceId = IdUtils.get16UUID();
        String currentVal = instanceIdUtil.getRedisInstanceId("PL");
        String instanceId = instanceIdUtil.getIstanceId("PL", currentVal);
        String id = instanceId;

        Notes notes = new Notes();
        notes.setComments(parkingLocationRegistration.getComments());
        notes.setId(IdUtils.simpleUUID());
        notes.setInstanceId(instanceId);
        notesMapper.insertNotes(notes);
        String title = "Parking Location Registration";
        Authentication.setAuthenticatedUserId(SecurityUtils.getLoginUser()
                                                           .getUser()
                                                           .getUserId()
                                                           .toString());
        ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder
                .start()
                .withProcessDefinitionKey("parkingLocationregistration")
                .withName(title)
                .withBusinessKey(instanceId)
                // .withVariable("deptLeader",join)
                .build());

        Task task = null;
        task = taskService.createTaskQuery()
                          .processInstanceId(processInstance.getId())
                          .singleResult();



        TaskQuery query = taskService.createTaskQuery().taskCandidateOrAssigned(SecurityUtils.getUsername()).active();
        List<Task> todoList = query.list();//获取申请人的待办任务列表
        for (Task tmp : todoList) {
            if(tmp.getProcessInstanceId().equals(processInstance.getId())){
                task = tmp;//获取当前流程实例，当前申请人的待办任务
                break;
            }
        }
        HashMap<String, Object> variables = new HashMap<String, Object>();
        // List<Map<String, Object>> actForm = (List<Map<String, Object>>) map.get("actForm");
        // for (Map<String, Object> form : actForm) {
        //     variables.put(String.valueOf(form.get("controlId")), form.get("controlValue"));
        // }
        variables.put("FormProperty_3dnunj2", 0);
        taskService.complete(task.getId(), variables);

        //发送邮件
        try {
            org.activiti.engine.runtime.ProcessInstance processInstanceMe = runtimeService.createProcessInstanceQuery()
                    .processInstanceId(task.getProcessInstanceId())
                    .singleResult();
            // 上一个节点的执行完，再次查询，获取下一个节点的任务信息
            org.activiti.engine.task.Task finalTask = taskService.createTaskQuery()
                    .processInstanceId(processInstanceMe.getProcessInstanceId())
                    .singleResult();
            ThreadUtil.execute(new Runnable() {

                @Override
                public void run() {
                    actTaskService.sendMail(finalTask, instanceId,null);
                }
            });
        } catch (Exception e) {
            log.error("邮件发送失败，{}",e.getMessage());
        }

        parkingLocationRegistration.setId(id);
        parkingLocationRegistration.setInstanceId(id);

        ParkingLocation parkingLocation=new ParkingLocation();


        AnnualReviewy annualReviewy=new AnnualReviewy();
        annualReviewy.setId(id);
        annualReviewy.setInstanceId(processInstance.getId());
        annualReviewy.setTitle(title);
        annualReviewy.setType(parkingLocationRegistration.getType());  // TODO 待确认，先暂时随便写一个
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        annualReviewy.setState("0");
        annualReviewy.setCreateName(SecurityUtils.getNickName());
        annualReviewy.setCreateBy(SecurityUtils.getUsername());
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        // annualReviewy.setUsername(); // TODO 先不填
        if(ObjectUtil.isNotEmpty(task)){
            annualReviewy.setStartNode(task.getName());
        }
        int i = annualReviewyMapper.insertAnnualReviewy(annualReviewy);
        annualReviewyService.saveDbLog("1","新增流程",null ,instanceId,null,annualReviewy,null);


        if (i==0){
            return 0;
        }else {
            int l = parkingLocationRegistrationMapper.insertParkingLocationRegistration(parkingLocationRegistration);
            annualReviewyService.saveDbLog("1","新增表单信息",null ,instanceId,null,parkingLocationRegistration,null);
            return l;

            // ParkingLocation parkingLocation=new ParkingLocation();
            // BeanUtils.copyProperties(parkingLocationRegistration,parkingLocation);
            // return parkingLocationMapper.insertParkingLocation(parkingLocation);
        }
    }

    /**
     * 修改feike
     *
     * @param parkingLocationRegistration feike
     * @return 结果
     */
    @Override
    public int updateParkingLocationRegistration(ParkingLocationRegistration parkingLocationRegistration)
    {
        System.out.println("parkingLocationRegistration修改对象： "+parkingLocationRegistration);
        return parkingLocationRegistrationMapper.updateParkingLocationRegistration(parkingLocationRegistration);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteParkingLocationRegistrationByIds(String[] ids)
    {
        System.out.println("parkingLocationRegistration删除对象： "+ids);
        List<AnnualReviewy> annualReviewyList = annualReviewyMapper.selectAnnualReviewyByIds(ids);
        int num=0;
        if (annualReviewyList != null && annualReviewyList.size() > 0) {
            for (AnnualReviewy annualReviewy : annualReviewyList) {
                int i = parkingLocationRegistrationMapper.deleteParkingLocationRegistrationById(annualReviewy.getId());
                if (i==0){
                    return 0;
                }else {
                    num=num+1;
                    return annualReviewyMapper.deleteAnnualReviewyById(annualReviewy.getId());
                }

            }
        }
       return num;
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteParkingLocationRegistrationById(String id)
    {
        return parkingLocationRegistrationMapper.deleteParkingLocationRegistrationById(id);
    }

    @Override
    public ParkingLocationRegistration selectParkingLocationRegistrationByid(String id) {
        return parkingLocationRegistrationMapper.selectParkingLocationRegistrationById(id);
    }

    @Override
    public List<AnnualReviewyHistoryVO> selectParkingLocationRegistrationListAll(AnnualReviewy annualReviewy, ParkingLocationRegistration parkingLocationRegistration) {
        return parkingLocationRegistrationMapper.selectParkingLocationRegistrationListBySelf(annualReviewy, parkingLocationRegistration, SecurityUtils.getLoginUser()
                                                                                                               .getUser()
                                                                                                               .getDeptId());
    }

    /**
     * 根据group和dealer获取详细信息
     */
    @Override
    public Map<String, Object> selectParkingLocationRegistration(Map<String, Object> map) {
        System.out.println("ParkingLocationRegistration查询： "+map);
        List<Map<String, Object>> dealerList = (List<Map<String, Object>>) map.get("dealerList");
        Map<String, Object> parkingLocationMap=new HashMap<>();
        List<ParkingLocation> list=new ArrayList<>();

        if (dealerList!=null&&dealerList.size()>0){
            for (Map<String, Object> dealerObj : dealerList) {
                // 获取groupId 和 dealerId信息
                String groupName = (String) dealerObj.get("groupName");
                String dealerName = (String) dealerObj.get("dealerName");
                // 根据groupId和dealerId获取详细信息
                ParkingLocation parkingLocation=parkingLocationRegistrationMapper.selectParkingLocationRegistrationByGroupNameAndDealerName(groupName,dealerName);
                list.add(parkingLocation);
            }
        }
        if (list!=null&&list.size()>0){
            parkingLocationMap.put("dealerList",list);
        }
        System.out.println("parkingLocationMap=="+parkingLocationMap);
        return parkingLocationMap;
    }

    @Override
    public int insertParkingLocationRegistrationMap(Map<String, Object> map) {
        System.out.println("=======1111===="+map);
        //String instanceId = IdUtils.get16UUID();
        String currentVal = instanceIdUtil.getRedisInstanceId("PL");
        String instanceId = instanceIdUtil.getIstanceId("PL", currentVal);

        Notes notes1 = new Notes();
        notes1.setComments((String)map.get("comments"));
        notes1.setId(IdUtils.simpleUUID());
        notes1.setInstanceId(instanceId);
        notesMapper.insertNotes(notes1);
        // String id = instanceId;
        // String id = (String) map.get("id");
        String title = "Parking Location Registration";

        Authentication.setAuthenticatedUserId(SecurityUtils.getLoginUser()
                                                           .getUser()
                                                           .getUserId()
                                                           .toString());
        Task task = null;

        if(map.get("caoGao") != null && map.get("caoGao").toString().equals("1")){

        }else {
        ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder
                .start()
                .withProcessDefinitionKey("parkingLocationregistration")
                .withName(title)
                .withBusinessKey(instanceId)
                // .withVariable("deptLeader",join)
                .build());

        task = taskService.createTaskQuery()
                          .processInstanceId(processInstance.getId())
                          .singleResult();



        TaskQuery query = taskService.createTaskQuery().taskCandidateOrAssigned(SecurityUtils.getUsername()).active();
        List<Task> todoList = query.list();//获取申请人的待办任务列表
        for (Task tmp : todoList) {
            if(tmp.getProcessInstanceId().equals(processInstance.getId())){
                task = tmp;//获取当前流程实例，当前申请人的待办任务
                break;
            }
        }
        HashMap<String, Object> variables = new HashMap<String, Object>();

        variables.put("FormProperty_3dnunj2", 0);
        taskService.complete(task.getId(), variables);
        }

        // 下面业务数据
        String groupName = map.get("groupName")!=null?(String) map.get("groupName"):"";
        String dealerName = map.get("dealerName")!=null?(String) map.get("dealerName"):"";
        BasicInformation basicInformationNew = new BasicInformation();
        basicInformationNew.setGroupNameEN(groupName);
        basicInformationNew.setInstanceId(instanceId);
        basicInformationNew.setId(IdUtils.simpleUUID());
        basicInformationNew.setDealerNameCN(dealerName);
        basicInformationMapper.insertBasicInformation(basicInformationNew);
        Object form = map.get("form");
        if (form!=null){
            List<Map<String,Object>> list = (List<Map<String, Object>>) form;
            if (list!=null&&list.size()>0){
                for (Map<String, Object> obj : list) {
                    ParkingLocationRegistration parkingLocationRegistration = new ParkingLocationRegistration();

                    parkingLocationRegistration.setInstanceId(instanceId);
                    if(!StringUtils.isEmpty(groupName)){
                        parkingLocationRegistration.setGroupNameCn(groupName);
                    }
                    if(!StringUtils.isEmpty(dealerName)){
                        parkingLocationRegistration.setDealerName(dealerName);
                    }
                    String address = (String) obj.get("address");//详细地址

                    String provinceCN = (String) obj.get("provinceCN"); //存中文地址
                    if (!StringUtils.isEmpty(provinceCN) && StringUtils.isNotEmpty(address)){
                        parkingLocationRegistration.setProvinceCN(provinceCN+"/"+address);
                    }
                    StringBuilder province = new StringBuilder();
                    Object province1 = obj.get("province");     // 存数组转的字符串  省市县
                    if(province1!=null && !StringUtils.isEmpty(String.valueOf(province1))){
                        List provinceList = (List) province1;// 前端传的是数组
                        if (provinceList!=null&&provinceList.size()>0){
                            String provinceName = (String) provinceList.get(0);
                            String city = (String) provinceList.get(1);
                            String county = (String) provinceList.get(2);
                            parkingLocationRegistration.setProvince(provinceName);   // 省
                            parkingLocationRegistration.setCity(city);       // 市
                            parkingLocationRegistration.setCounty(county);     // 县
                            for (int i = 0; i < provinceList.size(); i++) {
                                province.append(provinceList.get(i));
                                province.append(",");
                            }
                        }
                    }
                    if (!StringUtils.isEmpty(province)){
                        province.deleteCharAt(province.length()-1);
                        parkingLocationRegistration.setAddress(address);
                        System.out.println("province"+province);
                    }

                    Object type = obj.get("type");
                    if (type!=null){
                        parkingLocationRegistration.setType((String) type);
                    }

                    String startDate = (String) obj.get("startDate");
                    String endDate = (String) obj.get("endDate");
                    SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
                    Date startTime = new Date();
                    Date endTime = new Date();
                    try {
                        if (!StringUtils.isEmpty(startDate)){
                            startTime = format.parse(startDate);
                            parkingLocationRegistration.setStartDate(startTime);
                        }
                        if (!StringUtils.isEmpty(endDate)){
                            endTime = format.parse(endDate);
                            parkingLocationRegistration.setEndDate(endTime);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    StringBuilder vins = new StringBuilder();
                    List vinList = (List) obj.get("vin");//数组
                    if (vinList!=null&&vinList.size()>0){
                        for (int i = 0; i < vinList.size(); i++) {
                            vins.append(vinList.get(i));
                            vins.append(",");
                        }
                    }
                    if (!StringUtils.isEmpty(vins)){
                        vins.deleteCharAt(vins.length()-1);
                        parkingLocationRegistration.setVin(vins.toString());
                    }
                    Object distanceFrom4s = obj.get("distanceFrom4s");
                    if (distanceFrom4s!=null){
                        parkingLocationRegistration.setDistanceFrom4s((String) distanceFrom4s);
                    }
                    Object tel = obj.get("tel");
                    if (tel!=null){
                        parkingLocationRegistration.setTel((String) tel);
                    }
                    Object notes = obj.get("notes");
                    if (notes!=null){
                        parkingLocationRegistration.setNotes((String) notes);
                    }
                    parkingLocationRegistration.setId(IdUtils.simpleUUID());
                    parkingLocationRegistrationMapper.insertParkingLocationRegistration(parkingLocationRegistration);
                    annualReviewyService.saveDbLog("1","新增流程","新增Parking Location Registration表单信息",instanceId,null,parkingLocationRegistration,null);



                    // 保存上传文件到数据库中
                    Object uploadFile = obj.get("uploadFile");
                    if(uploadFile!=null){
                        List<Fileupload> fileuploads= (List<Fileupload>) uploadFile;
                        String s = JSON.toJSONString(fileuploads);
                        fileuploads = JSON.parseArray(s, Fileupload.class);
                        System.out.println("parkingLocationRegistration上传文件的内容： "+fileuploads);
                        if (fileuploads!=null&&fileuploads.size()>0){
                            for (Fileupload fileupload : fileuploads) {
                                fileupload.setId(IdUtils.simpleUUID());
                                fileupload.setInstanceId(parkingLocationRegistration.getId());
                                fileupload.setCreateBy(SecurityUtils.getLoginUser().getUser().getUserId().toString());
                                fileupload.setCreateName(SecurityUtils.getNickName());
                                fileupload.setCreateTime(new Date());
                                fileupload.setType("parkingLocationRegistration");
                                fileuploadService.insertFileupload(fileupload);
                                annualReviewyService.saveDbLog("1","新增流程","File Upload",instanceId,null,fileupload,null);

                            }
                        }
                    }

                }
            }

        }

        AnnualReviewy annualReviewy=new AnnualReviewy();
        annualReviewy.setId(IdUtils.simpleUUID());
        annualReviewy.setInstanceId(instanceId);
        annualReviewy.setTitle(title);
        annualReviewy.setType("parkingLocationRegistration");  // TODO 待确认，先暂时随便写一个
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        annualReviewy.setState("0");
        if (map.get("caoGao") != null && map.get("caoGao").toString().equals("1")) {//草稿行为时
            annualReviewy.setState("3");
        }
        annualReviewy.setCreateName(SecurityUtils.getNickName());
        annualReviewy.setCreateBy(SecurityUtils.getUsername());
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        // annualReviewy.setUsername(); // TODO 先不填
        if(ObjectUtil.isNotEmpty(task)){
            annualReviewy.setStartNode(task.getName());
        }
        int i = annualReviewyMapper.insertAnnualReviewy(annualReviewy);
        annualReviewyService.saveDbLog("1","新增流程",null ,instanceId,null,annualReviewy,null);


        if(map.get("caoGao") != null && map.get("caoGao").toString().equals("0")){
            //发送邮件
            try {
                org.activiti.engine.runtime.ProcessInstance processInstanceMe = runtimeService.createProcessInstanceQuery()
                        .processInstanceId(task.getProcessInstanceId())
                        .singleResult();
                // 上一个节点的执行完，再次查询，获取下一个节点的任务信息
                org.activiti.engine.task.Task task1 = taskService.createTaskQuery()
                        .processInstanceId(processInstanceMe.getProcessInstanceId())
                        .singleResult();

                ThreadUtil.execute(new Runnable() {

                    @Override
                    public void run() {
                        actTaskService.sendMail(task1, instanceId,null);
                    }
                });

            } catch (Exception e) {
                log.error("邮件发送失败，{}",e.getMessage());
            }
        }

        return i;
    }

    @Override
    public List<Map<String,Object>> vinlist(Map<String, Object> map){
        String urlStr=wfsUrl+"LoanInfo";   //第三方接口路径
        List dealerCodeList = new ArrayList();
        // String groupName = (String) map.get("groupName");
        String dealerNameList = (String) map.get("dealerName");
        // 根据集团名称和经销商名称获取dealercode对象   "DealerCode":[["L061701","M01002"],["L061702","M01003"]]
        List<Map<String,Object>> codeList =new ArrayList();
        if(dealerNameList!=null){

                List dealerInformationList = annualReviewyService.getInfoListByDealerName(dealerNameList);
                System.out.println("dealerInformationList=="+dealerInformationList);
                if (dealerInformationList!=null && dealerInformationList.size()>0){
                    for (int j = 0; j < dealerInformationList.size(); j++) {
                        Map<String,Object> dealerAndDealerCodeMap = new HashMap<>();
                        String dealerCodeStr = (String) dealerInformationList.get(j);
                        dealerAndDealerCodeMap.put(dealerCodeStr,dealerNameList);
                        codeList.add(dealerAndDealerCodeMap);
                    }
                }
                dealerCodeList.add(dealerInformationList);

        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("DealerCode",dealerCodeList);
        String response = HttpRequest.post(urlStr)
                .header("Content-Type", "application/json;charset=UTF-8")
                .body(jsonObject.toString())
                .execute()
                .body();
        cn.hutool.json.JSONObject entries = JSONUtil.parseObj(response, false);
        List<Map<String,Object>> data = new ArrayList<>();
        JSONArray arr = JSONArray.parseArray(JSON.toJSONString(entries.get("Data")));
        for (int i = 0; i < arr.size(); i++) {
            JSONArray arr1 = JSONArray.parseArray(JSON.toJSONString(arr.get(i)));
            for (int i1 = 0; i1 < arr1.size(); i1++) {
                Object o = arr1.get(i1);
                if(ObjectUtil.isNotEmpty(o)){
                    data.add((Map<String, Object>) o);
                }
            }
        }
        return data;
    }


}
