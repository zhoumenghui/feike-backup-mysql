package com.ruoyi.feike.service.impl;

import java.util.List;

import com.ruoyi.feike.domain.HLimitSetupResult;
import com.ruoyi.feike.mapper.HLimitSetupResultMapper;
import com.ruoyi.feike.service.IHLimitSetupResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
@Service
public class HLimitSetupResultServiceImpl implements IHLimitSetupResultService
{
    @Autowired
    private HLimitSetupResultMapper hLimitSetupResultMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public HLimitSetupResult selectHLimitSetupResultById(Long id)
    {
        return hLimitSetupResultMapper.selectHLimitSetupResultById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param hLimitSetupResult 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<HLimitSetupResult> selectHLimitSetupResultList(HLimitSetupResult hLimitSetupResult)
    {
        return hLimitSetupResultMapper.selectHLimitSetupResultList(hLimitSetupResult);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param hLimitSetupResult 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertHLimitSetupResult(HLimitSetupResult hLimitSetupResult)
    {
        return hLimitSetupResultMapper.insertHLimitSetupResult(hLimitSetupResult);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param hLimitSetupResult 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateHLimitSetupResult(HLimitSetupResult hLimitSetupResult)
    {
        return hLimitSetupResultMapper.updateHLimitSetupResult(hLimitSetupResult);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteHLimitSetupResultByIds(Long[] ids)
    {
        return hLimitSetupResultMapper.deleteHLimitSetupResultByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteHLimitSetupResultById(Long id)
    {
        return hLimitSetupResultMapper.deleteHLimitSetupResultById(id);
    }
}
