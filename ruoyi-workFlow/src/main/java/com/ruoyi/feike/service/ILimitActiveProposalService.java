package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.LimitActiveProposal;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface ILimitActiveProposalService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public LimitActiveProposal selectLimitActiveProposalById(String id);

    /**
     * 查询feike列表
     * 
     * @param limitActiveProposal feike
     * @return feike集合
     */
    public List<LimitActiveProposal> selectLimitActiveProposalList(LimitActiveProposal limitActiveProposal);

    public List<LimitActiveProposal> selectCurrentActiveLimitExpiryDate(LimitActiveProposal limitActiveProposal);


    /**
     * 新增feike
     * 
     * @param limitActiveProposal feike
     * @return 结果
     */
    public int insertLimitActiveProposal(LimitActiveProposal limitActiveProposal);

    /**
     * 修改feike
     * 
     * @param limitActiveProposal feike
     * @return 结果
     */
    public int updateLimitActiveProposal(LimitActiveProposal limitActiveProposal);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteLimitActiveProposalByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteLimitActiveProposalById(String id);
}
