package com.ruoyi.feike.service.impl;


import com.ruoyi.feike.domain.vo.ContractRecordTow;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IContractRecordTowSerivce;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ContractRecordTowSerivceImpl implements IContractRecordTowSerivce {


    @Autowired
    private ContractRecordTowMapper contractRecordTowMapper;


    @Override
    public List<ContractRecordTow> selectContractRecordTowList(ContractRecordTow contractRecordTow)
    {
        return contractRecordTowMapper.selectContractRecordTowList(contractRecordTow);
    }

    @Override
    public int insertContractRecordTow(ContractRecordTow contractRecordTow)
    {
        return contractRecordTowMapper.insertContractRecordTow(contractRecordTow);
    }




}
