package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.SalesPerformanceAndTargetMapper;
import com.ruoyi.feike.domain.SalesPerformanceAndTarget;
import com.ruoyi.feike.service.ISalesPerformanceAndTargetService;

/**
 * feikeService业务层处理
 *
 * @author zmh
 * @date 2022-07-11
 */
@Service
public class SalesPerformanceAndTargetServiceImpl implements ISalesPerformanceAndTargetService
{
    @Autowired
    private SalesPerformanceAndTargetMapper salesPerformanceAndTargetMapper;

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public SalesPerformanceAndTarget selectSalesPerformanceAndTargetById(String id)
    {
        return salesPerformanceAndTargetMapper.selectSalesPerformanceAndTargetById(id);
    }

    /**
     * 查询feike列表
     *
     * @param salesPerformanceAndTarget feike
     * @return feike
     */
    @Override
    public List<SalesPerformanceAndTarget> selectSalesPerformanceAndTargetList(SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        return salesPerformanceAndTargetMapper.selectSalesPerformanceAndTargetList(salesPerformanceAndTarget);
    }

    /**
     * 新增feike
     *
     * @param salesPerformanceAndTarget feike
     * @return 结果
     */
    @Override
    public int insertSalesPerformanceAndTarget(SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        return salesPerformanceAndTargetMapper.insertSalesPerformanceAndTarget(salesPerformanceAndTarget);
    }

    /**
     * 修改feike
     *
     * @param salesPerformanceAndTarget feike
     * @return 结果
     */
    @Override
    public int updateSalesPerformanceAndTarget(SalesPerformanceAndTarget salesPerformanceAndTarget)
    {
        return salesPerformanceAndTargetMapper.updateSalesPerformanceAndTarget(salesPerformanceAndTarget);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteSalesPerformanceAndTargetByIds(String[] ids)
    {
        return salesPerformanceAndTargetMapper.deleteSalesPerformanceAndTargetByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteSalesPerformanceAndTargetById(String id)
    {
        return salesPerformanceAndTargetMapper.deleteSalesPerformanceAndTargetById(id);
    }


    @Override
    public List<SalesPerformanceAndTarget> selectSalesPerformanceAndTargetByInstanceId(String instanceId) {
        return salesPerformanceAndTargetMapper.selectSalesPerformanceAndTargetByInstanceId(instanceId);
    }
}
