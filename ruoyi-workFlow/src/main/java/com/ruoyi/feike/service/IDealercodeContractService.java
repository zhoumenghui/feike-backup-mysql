package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.DealercodeContract;

/**
 * 生成的合同详细信息Service接口
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
public interface IDealercodeContractService 
{
    /**
     * 查询生成的合同详细信息
     * 
     * @param id 生成的合同详细信息ID
     * @return 生成的合同详细信息
     */
    public DealercodeContract selectDealercodeContractById(Long id);

    /**
     * 查询生成的合同详细信息列表
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 生成的合同详细信息集合
     */
    public List<DealercodeContract> selectDealercodeContractList(DealercodeContract dealercodeContract);

    /**
     * 新增生成的合同详细信息
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 结果
     */
    public int insertDealercodeContract(DealercodeContract dealercodeContract);

    /**
     * 修改生成的合同详细信息
     * 
     * @param dealercodeContract 生成的合同详细信息
     * @return 结果
     */
    public int updateDealercodeContract(DealercodeContract dealercodeContract);

    /**
     * 批量删除生成的合同详细信息
     * 
     * @param ids 需要删除的生成的合同详细信息ID
     * @return 结果
     */
    public int deleteDealercodeContractByIds(Long[] ids);

    /**
     * 删除生成的合同详细信息信息
     * 
     * @param id 生成的合同详细信息ID
     * @return 结果
     */
    public int deleteDealercodeContractById(Long id);
}
