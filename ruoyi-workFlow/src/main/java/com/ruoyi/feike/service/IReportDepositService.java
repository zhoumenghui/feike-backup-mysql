package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.ReportDeposit;

/**
 * feikeService接口
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
public interface IReportDepositService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ReportDeposit selectReportDepositById(String id);

    /**
     * 查询feike列表
     * 
     * @param reportDeposit feike
     * @return feike集合
     */
    public List<ReportDeposit> selectReportDepositList(ReportDeposit reportDeposit);

    /**
     * 新增feike
     * 
     * @param reportDeposit feike
     * @return 结果
     */
    public int insertReportDeposit(ReportDeposit reportDeposit);

    /**
     * 修改feike
     * 
     * @param reportDeposit feike
     * @return 结果
     */
    public int updateReportDeposit(ReportDeposit reportDeposit);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteReportDepositByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteReportDepositById(String id);
}
