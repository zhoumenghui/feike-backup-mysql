package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.WholesalePerformanceCurrentlyMapper;
import com.ruoyi.feike.domain.WholesalePerformanceCurrently;
import com.ruoyi.feike.service.IWholesalePerformanceCurrentlyService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class WholesalePerformanceCurrentlyServiceImpl implements IWholesalePerformanceCurrentlyService 
{
    @Autowired
    private WholesalePerformanceCurrentlyMapper wholesalePerformanceCurrentlyMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public WholesalePerformanceCurrently selectWholesalePerformanceCurrentlyById(String id)
    {
        return wholesalePerformanceCurrentlyMapper.selectWholesalePerformanceCurrentlyById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param wholesalePerformanceCurrently feike
     * @return feike
     */
    @Override
    public List<WholesalePerformanceCurrently> selectWholesalePerformanceCurrentlyList(WholesalePerformanceCurrently wholesalePerformanceCurrently)
    {
        return wholesalePerformanceCurrentlyMapper.selectWholesalePerformanceCurrentlyList(wholesalePerformanceCurrently);
    }

    /**
     * 新增feike
     * 
     * @param wholesalePerformanceCurrently feike
     * @return 结果
     */
    @Override
    public int insertWholesalePerformanceCurrently(WholesalePerformanceCurrently wholesalePerformanceCurrently)
    {
        return wholesalePerformanceCurrentlyMapper.insertWholesalePerformanceCurrently(wholesalePerformanceCurrently);
    }

    /**
     * 修改feike
     * 
     * @param wholesalePerformanceCurrently feike
     * @return 结果
     */
    @Override
    public int updateWholesalePerformanceCurrently(WholesalePerformanceCurrently wholesalePerformanceCurrently)
    {
        return wholesalePerformanceCurrentlyMapper.updateWholesalePerformanceCurrently(wholesalePerformanceCurrently);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteWholesalePerformanceCurrentlyByIds(String[] ids)
    {
        return wholesalePerformanceCurrentlyMapper.deleteWholesalePerformanceCurrentlyByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteWholesalePerformanceCurrentlyById(String id)
    {
        return wholesalePerformanceCurrentlyMapper.deleteWholesalePerformanceCurrentlyById(id);
    }
}
