package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.dto.WfsLimitSetUpdto;

import java.util.List;

public interface IWfsLimitSetUpService {

	int insertWfsLimitSetUp(WfsLimitSetUpdto wfsLimitSetUpdto);

	String updateWfsLimitSetUp(WfsLimitSetUpdto wfsLimitSetUpdto);

	WfsLimitSetUpdto selectWfsLimitSetUpdto(String instanceId);

}
