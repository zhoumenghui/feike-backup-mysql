package com.ruoyi.feike.service.impl;

import com.ruoyi.feike.domain.InsuranceDeclarationInfo;
import com.ruoyi.feike.mapper.InsuranceDeclarationInfoMapper;
import com.ruoyi.feike.service.InsuranceDeclarationInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class InsuranceDeclarationInfoServiceImpl implements InsuranceDeclarationInfoService
{
    @Autowired
    private InsuranceDeclarationInfoMapper insuranceDeclarationInfoMapper;


    @Override
    public List<InsuranceDeclarationInfo> selectInsuranceDeclarationInfoList(InsuranceDeclarationInfo insuranceDeclarationInfo) {
        return insuranceDeclarationInfoMapper.selectInsuranceDeclarationInfoList(insuranceDeclarationInfo);
    }

    @Override
    public List<InsuranceDeclarationInfo> selectDepositDeclarationInfoList(InsuranceDeclarationInfo insuranceDeclarationInfo) {
        return insuranceDeclarationInfoMapper.selectDepositDeclarationInfoList(insuranceDeclarationInfo);
    }

    @Override
    public int insertInsuranceDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo) {
        return insuranceDeclarationInfoMapper.insertInsuranceDeclarationInfo(insuranceDeclarationInfo);
    }

    @Override
    public int insertDepositDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo) {
        return insuranceDeclarationInfoMapper.insertDepositDeclarationInfo(insuranceDeclarationInfo);
    }

    @Override
    public int updateInsuranceDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo) {
        return insuranceDeclarationInfoMapper.updateInsuranceDeclarationInfo(insuranceDeclarationInfo);
    }

    @Override
    public List<InsuranceDeclarationInfo> getInsuranceDeclarationInfoByInstanceId(String instanceId) {
        return insuranceDeclarationInfoMapper.getInsuranceDeclarationInfoByInstanceId(instanceId);
    }

    @Override
    public InsuranceDeclarationInfo selectInsuranceDeclarationInfoById(String id) {
        return insuranceDeclarationInfoMapper.selectInsuranceDeclarationInfoById(id);
    }
}
