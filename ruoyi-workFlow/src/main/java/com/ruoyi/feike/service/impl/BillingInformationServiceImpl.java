package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.BillingInformationMapper;
import com.ruoyi.feike.domain.BillingInformation;
import com.ruoyi.feike.service.IBillingInformationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class BillingInformationServiceImpl implements IBillingInformationService 
{
    @Autowired
    private BillingInformationMapper billingInformationMapper;

    /**
     * 查询feike
     * 
     * @param instanceId feikeID
     * @return feike
     */
    @Override
    public BillingInformation selectBillingInformationById(String instanceId)
    {
        return billingInformationMapper.selectBillingInformationById(instanceId);
    }

    /**
     * 查询feike列表
     * 
     * @param billingInformation feike
     * @return feike
     */
    @Override
    public List<BillingInformation> selectBillingInformationList(BillingInformation billingInformation)
    {
        return billingInformationMapper.selectBillingInformationList(billingInformation);
    }

    /**
     * 新增feike
     * 
     * @param billingInformation feike
     * @return 结果
     */
    @Override
    public int insertBillingInformation(BillingInformation billingInformation)
    {
        return billingInformationMapper.insertBillingInformation(billingInformation);
    }

    /**
     * 修改feike
     * 
     * @param billingInformation feike
     * @return 结果
     */
    @Override
    public int updateBillingInformation(BillingInformation billingInformation)
    {
        return billingInformationMapper.updateBillingInformation(billingInformation);
    }

    /**
     * 批量删除feike
     * 
     * @param instanceIds 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteBillingInformationByIds(String[] instanceIds)
    {
        return billingInformationMapper.deleteBillingInformationByIds(instanceIds);
    }

    /**
     * 删除feike信息
     * 
     * @param instanceId feikeID
     * @return 结果
     */
    @Override
    public int deleteBillingInformationById(String instanceId)
    {
        return billingInformationMapper.deleteBillingInformationById(instanceId);
    }
}
