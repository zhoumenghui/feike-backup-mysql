package com.ruoyi.feike.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.ReportDepositMapper;
import com.ruoyi.feike.domain.ReportDeposit;
import com.ruoyi.feike.service.IReportDepositService;

/**
 * feikeService业务层处理
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
@Service
public class ReportDepositServiceImpl implements IReportDepositService 
{
    @Autowired
    private ReportDepositMapper reportDepositMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public ReportDeposit selectReportDepositById(String id)
    {
        return reportDepositMapper.selectReportDepositById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param reportDeposit feike
     * @return feike
     */
    @Override
    public List<ReportDeposit> selectReportDepositList(ReportDeposit reportDeposit)
    {
        return reportDepositMapper.selectReportDepositList(reportDeposit);
    }

    /**
     * 新增feike
     * 
     * @param reportDeposit feike
     * @return 结果
     */
    @Override
    public int insertReportDeposit(ReportDeposit reportDeposit)
    {
        reportDeposit.setCreateTime(DateUtils.getNowDate());
        return reportDepositMapper.insertReportDeposit(reportDeposit);
    }

    /**
     * 修改feike
     * 
     * @param reportDeposit feike
     * @return 结果
     */
    @Override
    public int updateReportDeposit(ReportDeposit reportDeposit)
    {
        reportDeposit.setUpdateTime(DateUtils.getNowDate());
        return reportDepositMapper.updateReportDeposit(reportDeposit);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteReportDepositByIds(String[] ids)
    {
        return reportDepositMapper.deleteReportDepositByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteReportDepositById(String id)
    {
        return reportDepositMapper.deleteReportDepositById(id);
    }
}
