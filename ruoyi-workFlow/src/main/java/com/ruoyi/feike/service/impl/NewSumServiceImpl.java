package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.NewSumMapper;
import com.ruoyi.feike.domain.NewSum;
import com.ruoyi.feike.service.INewSumService;

/**
 * feikeService业务层处理
 *
 * @author zmh
 * @date 2022-08-25
 */
@Service
public class NewSumServiceImpl implements INewSumService
{
    @Autowired
    private NewSumMapper newSumMapper;

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public NewSum selectNewSumById(String id)
    {
        return newSumMapper.selectNewSumById(id);
    }

    /**
     * 查询feike列表
     *
     * @param newSum feike
     * @return feike
     */
    @Override
    public List<NewSum> selectNewSumList(NewSum newSum)
    {
        return newSumMapper.selectNewSumList(newSum);
    }

    /**
     * 新增feike
     *
     * @param newSum feike
     * @return 结果
     */
    @Override
    public int insertNewSum(NewSum newSum)
    {
        return newSumMapper.insertNewSum(newSum);
    }

    /**
     * 修改feike
     *
     * @param newSum feike
     * @return 结果
     */
    @Override
    public int updateNewSum(NewSum newSum)
    {
        return newSumMapper.updateNewSum(newSum);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteNewSumByIds(String[] ids)
    {
        return newSumMapper.deleteNewSumByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteNewSumById(String id)
    {
        return newSumMapper.deleteNewSumById(id);
    }

}
