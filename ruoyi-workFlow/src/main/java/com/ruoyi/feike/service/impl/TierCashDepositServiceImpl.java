package com.ruoyi.feike.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.activiti.service.impl.ActTaskServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.domain.vo.ParkingLocationVO1;
import com.ruoyi.feike.mapper.*;
import com.ruoyi.feike.service.IAnnualReviewyService;
import com.ruoyi.feike.service.IFileuploadService;
import com.ruoyi.feike.service.ITierCashDepositService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * feikeService业务层处理
 *
 * @author ruoyi
 * @date 2022-08-02
 */
@Service
@Slf4j
public class TierCashDepositServiceImpl implements ITierCashDepositService {

    @Value("${wfs.url}")
    private String wfsUrl;
    @Autowired
    private TierCashDepositMapper tierCashDepositMapper;

    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;

    @Autowired
    private ParkingLocationRegistrationMapper parkingLocationRegistrationMapper;

    @Autowired
    private ParkingLocationMapper parkingLocationMapper;

    @Autowired
    private DealerInformationMapper dealerInformationMapper;

    @Autowired
    private PlmMapper plmMapper;

    @Autowired
    private ProcessRuntime processRuntime;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private IFileuploadService fileuploadService;

    @Autowired
    private ActTaskServiceImpl actTaskService;

    @Autowired
    private IAnnualReviewyService annualReviewyService;



    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public Map<String, Object> selectTierCashDepositById(String id) {
        Map<String, Object> map = new HashMap<>();
        AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId1(id);
        if (annualReviewy!=null){
            TierCashDeposit tierCashDeposit = tierCashDepositMapper.selectTierCashDepositById(annualReviewy.getInstanceId());

            String parkinglocation1 = tierCashDeposit.getParkinglocations();
            if (!StringUtils.isEmpty(parkinglocation1)){
                boolean contains = parkinglocation1.contains(",");
                if (contains){
                    String[] split = parkinglocation1.split(",");
                    tierCashDeposit.setParkinglocationList(split);
                }
            }

            List<Fileupload> fileList = fileuploadService.selectFileuploadByInstanceId(tierCashDeposit.getInstanceId());

            List<Fileupload> opFileList = new ArrayList<>();
            //2网op文件数据响应
            if(CollectionUtil.isNotEmpty(fileList)){
                for (int i = 0; i < fileList.size(); i++) {
                    if(fileList.get(i).getType().equals("opFileList")){
                        fileList.get(i).setName(fileList.get(i).getOriginalFileName());
                        opFileList.add(fileList.get(i));
                        fileList.remove(i);
                        i = i-1;
                    }
                }

               // List<Fileupload> opFileList1 = fileList.stream().filter(file-> file.getType().equals("opFileList")).collect(Collectors.toList());

            }

            tierCashDeposit.setDepositFileList(fileList);
            map.put("id", annualReviewy.getId());
            map.put("instanceId", annualReviewy.getInstanceId());
            map.put("type", annualReviewy.getType());
            map.put("title", annualReviewy.getTitle());
            map.put("state", annualReviewy.getState());
            map.put("create_name", annualReviewy.getCreateName());
            map.put("create_by", annualReviewy.getCreateBy());
            map.put("create_time", annualReviewy.getCreateTime());
            map.put("update_time", annualReviewy.getUpdateTime());
            map.put("opFileList", opFileList);
            map.put("data", tierCashDeposit);
        }

        return map;
    }

    @Override
    public Map<String, Object> selectTierCashDepositByInstanceId(String instanceId) {
        Map<String, Object> map = new HashMap<>();
        AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId(instanceId);
        if (annualReviewy!=null){
            TierCashDeposit tierCashDeposit = tierCashDepositMapper.selectTierCashDepositById(annualReviewy.getId());

            String parkinglocation1 = tierCashDeposit.getParkinglocations();
            if (!StringUtils.isEmpty(parkinglocation1)){
                boolean contains = parkinglocation1.contains(",");
                if (contains){
                    String[] split = parkinglocation1.split(",");
                    tierCashDeposit.setParkinglocationList(split);
                }
            }

            map.put("id", annualReviewy.getId());
            map.put("instanceId", annualReviewy.getInstanceId());
            map.put("type", annualReviewy.getType());
            map.put("title", annualReviewy.getTitle());
            map.put("state", annualReviewy.getState());
            map.put("create_name", annualReviewy.getCreateName());
            map.put("create_by", annualReviewy.getCreateBy());
            map.put("create_time", annualReviewy.getCreateTime());
            map.put("update_time", annualReviewy.getUpdateTime());
            map.put("data", tierCashDeposit);
        }

        return map;
    }

    /**
     * 查询feike列表
     *
     * @param tierCashDeposit feike
     * @return feike
     */
    @Override
    public List<TierCashDeposit> selectTierCashDepositList(TierCashDeposit tierCashDeposit) {
        return tierCashDepositMapper.selectTierCashDepositList(tierCashDeposit);
    }

    /**
     * 新增feike
     *
     * @param tierCashDeposit feike
     * @return 结果
     */
    @Override
    public int insertTierCashDeposit(TierCashDeposit tierCashDeposit ,String instanceId,String caoGao) {
        System.out.println("tierCashDeposit新增对象： " + tierCashDeposit);
        String id = instanceId;
        String title =  "2nd-Tier Cash Deposit";
        Authentication.setAuthenticatedUserId(SecurityUtils.getLoginUser()
                                                           .getUser()
                                                           .getUserId()
                                                           .toString());
        Task task = null;
        ProcessInstance processInstance = null;
        if(caoGao != null && caoGao.equals("1")){

        }else {
            processInstance = processRuntime.start(ProcessPayloadBuilder
                    .start()
                    .withProcessDefinitionKey("tierCashDeposit")
                    .withName(title)
                    .withBusinessKey(instanceId)
                    // .withVariable("deptLeader",join)
                    .build());

            task = taskService.createTaskQuery()
                    .processInstanceId(processInstance.getId())
                    .singleResult();


            TaskQuery query = taskService.createTaskQuery()
                    .taskCandidateOrAssigned(SecurityUtils.getUsername())
                    .active();
            List<Task> todoList = query.list();//获取申请人的待办任务列表
            for (Task tmp : todoList) {
                if (tmp.getProcessInstanceId()
                        .equals(processInstance.getId())) {
                    task = tmp;//获取当前流程实例，当前申请人的待办任务
                    break;
                }
            }

            HashMap<String, Object> variables = new HashMap<String, Object>();
            // List<Map<String, Object>> actForm = (List<Map<String, Object>>) map.get("actForm");
            // for (Map<String, Object> form : actForm) {
            //     variables.put(String.valueOf(form.get("controlId")), form.get("controlValue"));
            // }
            variables.put("FormProperty_27dui5t", 0);
            taskService.complete(task.getId(), variables);

        }




        // 对 ParkingLocation表 的 Dealer Code 做更新操作      根据经销商名称和位置做查询
        // ParkingLocation parkingLocation = parkingLocationMapper.selectParkingLocationByDealerAndParkingLocation(tierCashDeposit.getDealerNameCn(),tierCashDeposit.getParkingLocation());
        // if(parkingLocation!=null){
        //     parkingLocation.setDealercode(tierCashDeposit.getDealerCode());
        //     parkingLocationMapper.updateParkingLocation(parkingLocation);
        // }

        // 对 plm表 的 2nd-tier deposit receivable 做更新操作
        tierCashDeposit.setId(id);      // 这个只插入一条数据，id 和 instanceId 都设置一样，后面好查找
        tierCashDeposit.setInstanceId(id);
        tierCashDepositMapper.insertTierCashDeposit(tierCashDeposit);
        annualReviewyService.saveDbLog("1","新增流程","新增2网表单信息",instanceId,null,tierCashDeposit,null);



        AnnualReviewy annualReviewy = new AnnualReviewy();
        annualReviewy.setId(IdUtils.simpleUUID());
        annualReviewy.setInstanceId(instanceId);
        annualReviewy.setTitle(title);
        annualReviewy.setType("tierCashDeposit");  // TODO 待确认，先不填
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        annualReviewy.setState("0");
        if (caoGao != null && caoGao.equals("1")) {//草稿行为时
            annualReviewy.setState("3");
        }
        annualReviewy.setCreateName(SecurityUtils.getNickName());
        annualReviewy.setCreateBy(SecurityUtils.getUsername());
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        if(ObjectUtil.isNotEmpty(task)){
            annualReviewy.setStartNode(task.getName());
        }
        // annualReviewy.setUsername(); // TODO 先不填
        int i = annualReviewyMapper.insertAnnualReviewy(annualReviewy);
        annualReviewyService.saveDbLog("1","新增流程",null ,instanceId,null,annualReviewy,null);


        if(StringUtils.isNotEmpty(caoGao) && caoGao.equals("0")){
            //发送邮件
            try {
                org.activiti.engine.runtime.ProcessInstance processInstanceMe = runtimeService.createProcessInstanceQuery()
                        .processInstanceId(task.getProcessInstanceId())
                        .singleResult();
                // 上一个节点的执行完，再次查询，获取下一个节点的任务信息
                org.activiti.engine.task.Task task1 = taskService.createTaskQuery()
                        .processInstanceId(processInstanceMe.getProcessInstanceId())
                        .singleResult();

                ThreadUtil.execute(new Runnable() {

                    @Override
                    public void run() {
                        actTaskService.sendMail(task1, instanceId,null);
                    }
                });

            } catch (Exception e) {
                log.error("邮件发送失败，{}",e.getMessage());
            }
        }

        return i;
    }

    /**
     * 修改feike
     *
     * @param tierCashDeposit feike
     * @return 结果
     */
    @Override
    public int updateTierCashDeposit(TierCashDeposit tierCashDeposit,Map<String,Object> map) {
        System.out.println("tierCashDeposit修改对象： " + tierCashDeposit);

        String instanceId = map.get("instanceId").toString();
        AnnualReviewy annualReviewy = annualReviewyMapper.selectAnnualReviewyByInstanceId((String) map.get("instanceId"));

        Task task = null;
        if (map.get("caoGao") != null && map.get("caoGao").toString().equals("0")) { //草稿接口非保存，提交流程
            Authentication.setAuthenticatedUserId(SecurityUtils.getLoginUser()
                    .getUser()
                    .getUserId()
                    .toString());
            String title = "";
            if (StringUtils.isNotNull(map.get("name"))) {
                title = String.valueOf(map.get("name"));
            } else {
                title = "Annual Review";
            }
            //流程开始
            ProcessInstance processInstance = null;
            if (StringUtils.isNotNull(map.get("processDefinitionKey"))) {
                processInstance = processRuntime.start(ProcessPayloadBuilder
                        .start()
                        .withProcessDefinitionKey(map.get("processDefinitionKey")+"")
                        .withName(title)
                        .withBusinessKey(instanceId)
                        // .withVariable("deptLeader",join)
                        .build());
            } else {
                processInstance = processRuntime.start(ProcessPayloadBuilder
                        .start()
                        .withProcessDefinitionKey("annualVerification")
                        .withName(title)
                        .withBusinessKey(instanceId)
                        // .withVariable("deptLeader",join)
                        .build());
            }


            task = taskService.createTaskQuery()
                    .processInstanceId(processInstance.getId())
                    .singleResult();
            TaskQuery query = taskService.createTaskQuery().taskCandidateOrAssigned(SecurityUtils.getUsername()).active();
            List<Task> todoList = query.list();//获取申请人的待办任务列表
            for (Task tmp : todoList) {
                if (tmp.getProcessInstanceId().equals(processInstance.getId())) {
                    task = tmp;//获取当前流程实例，当前申请人的待办任务
                    break;
                }
            }
            HashMap<String, Object> variables = new HashMap<String, Object>();
            List<Map<String, Object>> actForm = (List<Map<String, Object>>) map.get("actForm");
            if(CollectionUtil.isNotEmpty(actForm)){
                for (Map<String, Object> form : actForm) {
                    variables.put(String.valueOf(form.get("controlId")), form.get("controlValue"));
                }
            }

            variables.put("toUnderwriter", 0);


            taskService.complete(task.getId(), variables);
            if(ObjectUtil.isNotEmpty(task)){
                annualReviewy.setStartNode(task.getName());
            }
        }

        if(null != map.get("instanceId")){
            //检查是否是从草稿处进行提交 是则 AnnualReviewy状态改回0
            if (map.get("caoGao") != null && map.get("caoGao").toString().equals("0")) {
                annualReviewy.setState("0");
            }
            String UpdateBy = annualReviewy.getUpdateBy();
            if(null != UpdateBy){
                annualReviewy.setUpdateBy(UpdateBy +"|"+ SecurityUtils.getUsername());
            }else{
                annualReviewy.setUpdateBy(SecurityUtils.getUsername());
            }
            annualReviewy.setLockname(null);
            annualReviewyMapper.updateAnnualReviewy(annualReviewy);

            if(map.get("caoGao") != null && map.get("caoGao").toString().equals("0")){
                //发送邮件
                try {
                    org.activiti.engine.runtime.ProcessInstance processInstanceMe = runtimeService.createProcessInstanceQuery()
                            .processInstanceId(task.getProcessInstanceId())
                            .singleResult();
                    // 上一个节点的执行完，再次查询，获取下一个节点的任务信息
                    org.activiti.engine.task.Task task1 = taskService.createTaskQuery()
                            .processInstanceId(processInstanceMe.getProcessInstanceId())
                            .singleResult();

                    ThreadUtil.execute(new Runnable() {

                        @Override
                        public void run() {
                            actTaskService.sendMail(task1, instanceId,null);
                        }
                    });

                } catch (Exception e) {
                    log.error("邮件发送失败，{}",e.getMessage());
                }
            }
        }
        TierCashDeposit tierCashDeposit1 = tierCashDepositMapper.selectTierCashDepositById(tierCashDeposit.getId());
        int i = tierCashDepositMapper.updateTierCashDeposit(tierCashDeposit);
        annualReviewyService.saveDbLog("3","修改表单信息",null,instanceId,tierCashDeposit1,tierCashDeposit,null);

        return i;
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteTierCashDepositByIds(String[] ids) {
        System.out.println("tierCashDeposit删除对象： " + ids);
        List<AnnualReviewy> annualReviewyList = annualReviewyMapper.selectAnnualReviewyByIds(ids);
        int num = 0;
        if (annualReviewyList != null && annualReviewyList.size() > 0) {
            for (AnnualReviewy annualReviewy : annualReviewyList) {
                int i = tierCashDepositMapper.deleteTierCashDepositById(annualReviewy.getId());
                if (i == 0) {
                    return 0;
                } else {
                    num = num + 1;
                    return annualReviewyMapper.deleteAnnualReviewyById(annualReviewy.getId());
                }

            }
        }
        return num;

    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteTierCashDepositById(String id) {
        return tierCashDepositMapper.deleteTierCashDepositById(id);
    }

    @Override
    public List<AnnualReviewyHistoryVO> selectTierCashDepositListAll(AnnualReviewy annualReviewy, TierCashDeposit tierCashDeposit) {
        return tierCashDepositMapper.selectTierCashDepositListBySelf(annualReviewy, tierCashDeposit, SecurityUtils.getLoginUser()
                                                                                                                  .getUser()
                                                                                                                  .getDeptId());
    }

    @Override
    public Map<String, Object> selectTierCashDepositBy(Map<String, Object> map) {
        System.out.println("tierCashDeposit==" + map);
        List<Map<String, Object>> dealerList = (List<Map<String, Object>>) map.get("dealerList");
        Map<String, Object> parkingLocationMap = new HashMap<>();
        List<ParkingLocation> parkingLocationList=null;
        if (dealerList != null && dealerList.size() > 0) {
            for (Map<String, Object> dealerObj : dealerList) {
                String dealerName = (String) dealerObj.get("dealerName");
                // 根据groupId和dealerId获取详细信息
                // TierCashDeposit tierCashDeposit=tierCashDepositMapper.selectTiierCashDepositByDealerName(dealerName);
               parkingLocationList = parkingLocationRegistrationMapper.selectParkingLocationRegistrationByDealerName(dealerName);

            }
        }
        System.out.println("parkingLocationMap==" + parkingLocationList);
        return parkingLocationMap;

    }

    @Override
    public List<TierCashDeposit> selectTierCashDepositByDralerName(String dealerName) {
        System.out.println("tierCashDeposit==" + dealerName);
        List<TierCashDeposit>  tierCashDepositList=null;
        if (!StringUtils.isEmpty(dealerName)){
            List<ParkingLocation> parkingLocationList= parkingLocationRegistrationMapper.selectParkingLocationRegistrationByDealerName(dealerName);

            if (parkingLocationList!=null&&parkingLocationList.size()>0){
                for (ParkingLocation parkingLocation : parkingLocationList) {
                    TierCashDeposit tierCashDeposit = new TierCashDeposit();
                    tierCashDeposit.setInstanceId(parkingLocation.getInstanceId());
                    tierCashDeposit.setDearler(parkingLocation.getDealernamecn());
                    tierCashDeposit.setDealerNameCn(parkingLocation.getDealernamecn());
                    tierCashDeposit.setStatus(parkingLocation.getStatus());
                    tierCashDeposit.setParkingLocation(parkingLocation.getParkinglocation());
                    // tierCashDeposit.setNotes(parkingLocation.getNotes());
                    tierCashDepositList.add(tierCashDeposit);
                }
            }
        }
        return tierCashDepositList;
    }

    @Override
    public ParkingLocationVO1 selectTierCashDepositLists(String dralerName) {
        ParkingLocationVO1 parkingLocationVO1 = new ParkingLocationVO1();
        List<ParkingLocation> parkingLocationList = parkingLocationMapper.getFlagByDealerName(dralerName);
        if(parkingLocationList!=null&&parkingLocationList.size()>0){
            for (ParkingLocation parkingLocation : parkingLocationList) {
                if(parkingLocation.getDealernamecn()!=null){
                    parkingLocationVO1.setDealernamecn(parkingLocation.getDealernamecn());
                }
            }
        }
        ParkingLocation parkingLocation1 = new ParkingLocation();
        parkingLocation1.setDealernamecn(dralerName);
        parkingLocation1.setType("二网");
        List<ParkingLocation> parkingLocations = parkingLocationMapper.selectParkingLocationList(parkingLocation1);
        ArrayList<Map<String,Object>> list = new ArrayList<>();
        for(ParkingLocation pk : parkingLocations){
            HashMap<String, Object> stringObjectHashMap = new HashMap<>();
            if(StringUtils.isNotEmpty(pk.getParkinglocation())){
                stringObjectHashMap.put("parkinglocation",pk.getParkinglocation());
                stringObjectHashMap.put("twondtierdeposit",pk.getTwondtierdeposit());
            }
            list.add(stringObjectHashMap);
        }
        parkingLocationVO1.setParkinglocations(list);
        List dealerCodeList=new ArrayList();
        List<DealerInformation> dealerInformationList = dealerInformationMapper.selectDealerInformationByDealerName(dralerName);
        System.out.println("dealerInformationList列表=="+dealerInformationList);
        if(dealerInformationList!=null&&dealerInformationList.size()>0){
            for (DealerInformation dealerInformation : dealerInformationList) {
                String dealerCode = dealerInformation.getDealerCode();
                parkingLocationVO1.setGroupName(dealerInformation.getGroupName());
                dealerCodeList.add(dealerCode);
            }
        }
        parkingLocationVO1.setDealercCodes(dealerCodeList);
        Map<String,Object> map = parkingLocationMapper.selectParkingLocationCountAndSumByType(dralerName,"二网",null);
        Map<String,Object> map2 = parkingLocationMapper.selectParkingLocationCountAndSumByType2(dralerName,"二网",null);
        System.out.println("map===="+map);
        Object registeredTwoTierNumber =  map.get("num");
        Object twoTierWithDesposit = map2.get("num");
        System.out.println("registeredTwoTierNumber=="+registeredTwoTierNumber);
        System.out.println("twoTierWithDesposit=="+twoTierWithDesposit);
        String s1="0";
        String s2="0";
        if(registeredTwoTierNumber!=null){
            s1 = String.valueOf(registeredTwoTierNumber);
        }
        if(twoTierWithDesposit!=null){
            s2 = String.valueOf(twoTierWithDesposit);
        }
        parkingLocationVO1.setRegisteredTwoTierNumber(new BigDecimal(s1));
        parkingLocationVO1.setTwoTierWithDesposit(new BigDecimal(s2));
        List<Plm> plmList = plmMapper.selectPlmListByDealerName(dralerName);
        if(null!=plmList && plmList.size()>0){
            Plm  plm =plmList.get(0);
            Long permitted2ndtierwithdesposit = plm.getPermitted2ndtierwithdesposit();
            Long permitted2ndtierwithoutdesposit = plm.getPermitted2ndtierwithoutdesposit();
            parkingLocationVO1.setPermitted2ndTierWithDesposit(permitted2ndtierwithdesposit);
            parkingLocationVO1.setPermitted2ndTierWithoutDesposit(permitted2ndtierwithoutdesposit);
            System.out.println("parkingLocation返回的数据=="+parkingLocationVO1);
        }


        return parkingLocationVO1;
    }
    @Override
    public int getTowndTierCashDeposit(String dealerCode){
        int  balance =0;
        ArrayList<List<String>> stringss = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();
        strings.add(dealerCode);
        stringss.add(strings);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("DealerCode", stringss);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost2 = new HttpPost(wfsUrl + "SuspenseInfo");
        httpPost2.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost2.setEntity(entity);
        httpPost2.setHeader("Accept", "application/json, text/plain, */*");
        httpPost2.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost2.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost2.setHeader("Connection", "keep-alive");
        httpPost2.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String, Object> objectList2 = new HashMap<>();
        try{
            HttpResponse response2 = httpClient.execute(httpPost2);
            String responseStr2 = EntityUtils.toString(response2.getEntity());
            log.info("WFS外部接口数据返回结果=[{}]", responseStr2);
            objectList2 = JSONObject.parseObject(responseStr2, Map.class);
            if (StringUtils.equals("200",objectList2.get("Code").toString())) {
                JSONArray data = JSONObject.parseArray(objectList2.get("Data").toString());
                JSONArray objects = JSONObject.parseArray(data.get(0).toString());
                Object suspenseInfo1 = JSONObject.parseObject(objects.get(0).toString()).get("SuspenseInfo");
                JSONObject jsonObject1 = JSONObject.parseObject(suspenseInfo1.toString());
                String tierCashDeposit = String.valueOf(jsonObject1.get("2ndTierCashDeposit"));
                log.info("接口返回值tierCashDeposit={}",tierCashDeposit);

                 balance = StringUtils.isBlank(tierCashDeposit)?0:Integer.valueOf(tierCashDeposit);
                return balance;
            }
        }catch (Exception e){
            log.error(e.toString());
        }

        return balance;

    }
}
