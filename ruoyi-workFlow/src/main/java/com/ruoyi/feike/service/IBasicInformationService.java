package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.BasicContract;
import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.BasicInformationDTO;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-07-05
 */
public interface IBasicInformationService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public BasicInformation selectBasicInformationById(String id);

    /**
     * 查询feike列表
     *
     * @param basicInformation feike
     * @return feike集合
     */
    public List<BasicInformation> selectBasicInformationList(BasicInformation basicInformation);

    /**
     * 新增feike
     *
     * @param basicInformation feike
     * @return 结果
     */
    public int insertBasicInformation(BasicInformation basicInformation);

    public int insertBasicInformationByDto(BasicInformationDTO basicInformation);
    /**
     * 修改feike
     *
     * @param basicInformation feike
     * @return 结果
     */
    public int updateBasicInformation(BasicInformation basicInformation);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteBasicInformationByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteBasicInformationById(String id);

    /**
     * 根据合同规则修改字段
     *
     * @param resultMap
     * @return 结果
     */
    public int updateBasicInformationRules(Map<String, Object> resultMap);

    public Map<String,Object> matchTheContract(Map<String, Object> resultMap);

    public List contracts(Map<String, Object> resultMap);

    public List contract(Map<String, Object> resultMap);

    public List<BasicContract> matchTheContractNew(Map<String, Object> resultMap);

    public int contractnew(Map<String, Object> resultMap);

}
