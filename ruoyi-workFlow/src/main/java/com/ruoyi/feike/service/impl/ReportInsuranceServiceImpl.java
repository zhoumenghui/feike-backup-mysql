package com.ruoyi.feike.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.feike.mapper.ReportInsuranceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.domain.ReportInsurance;
import com.ruoyi.feike.service.IReportInsuranceService;

/**
 * feikeService业务层处理
 * 
 * @author ruoyi
 * @date 2022-09-02
 */
@Service
public class ReportInsuranceServiceImpl implements IReportInsuranceService 
{
    @Autowired
    private ReportInsuranceMapper reportInsuranceMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public ReportInsurance selectReportInsuranceById(String id)
    {
        return reportInsuranceMapper.selectReportInsuranceById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param reportInsurance feike
     * @return feike
     */
    @Override
    public List<ReportInsurance> selectReportInsuranceList(ReportInsurance reportInsurance)
    {
        return reportInsuranceMapper.selectReportInsuranceList(reportInsurance);
    }

    /**
     * 新增feike
     * 
     * @param reportInsurance feike
     * @return 结果
     */
    @Override
    public int insertReportInsurance(ReportInsurance reportInsurance)
    {
        reportInsurance.setCreateTime(DateUtils.getNowDate());
        return reportInsuranceMapper.insertReportInsurance(reportInsurance);
    }

    /**
     * 修改feike
     * 
     * @param reportInsurance feike
     * @return 结果
     */
    @Override
    public int updateReportInsurance(ReportInsurance reportInsurance)
    {
        reportInsurance.setUpdateTime(DateUtils.getNowDate());
        return reportInsuranceMapper.updateReportInsurance(reportInsurance);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteReportInsuranceByIds(String[] ids)
    {
        return reportInsuranceMapper.deleteReportInsuranceByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteReportInsuranceById(String id)
    {
        return reportInsuranceMapper.deleteReportInsuranceById(id);
    }

    @Override
    public List<ReportInsurance> selectReportInsuranceExcelList(String type) {
        return reportInsuranceMapper.selectReportInsuranceExcelList(type);
    }
}
