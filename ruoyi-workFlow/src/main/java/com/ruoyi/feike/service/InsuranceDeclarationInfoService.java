package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.DepositCalculation;
import com.ruoyi.feike.domain.InsuranceDeclarationInfo;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface InsuranceDeclarationInfoService
{

    public List<InsuranceDeclarationInfo> selectInsuranceDeclarationInfoList(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public List<InsuranceDeclarationInfo> selectDepositDeclarationInfoList(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public int insertInsuranceDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public int insertDepositDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo);

    public int updateInsuranceDeclarationInfo(InsuranceDeclarationInfo insuranceDeclarationInfo);

    List<InsuranceDeclarationInfo> getInsuranceDeclarationInfoByInstanceId(String instanceId);

    public InsuranceDeclarationInfo selectInsuranceDeclarationInfoById(String id);

}
