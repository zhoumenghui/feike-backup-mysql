package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.SelfplanInformation;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-08-25
 */
public interface ISelfplanInformationService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public SelfplanInformation selectSelfplanInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param selfplanInformation feike
     * @return feike集合
     */
    public List<SelfplanInformation> selectSelfplanInformationList(SelfplanInformation selfplanInformation);

    /**
     * 新增feike
     * 
     * @param selfplanInformation feike
     * @return 结果
     */
    public int insertSelfplanInformation(SelfplanInformation selfplanInformation);

    /**
     * 修改feike
     * 
     * @param selfplanInformation feike
     * @return 结果
     */
    public int updateSelfplanInformation(SelfplanInformation selfplanInformation);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteSelfplanInformationByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteSelfplanInformationById(String id);
}
