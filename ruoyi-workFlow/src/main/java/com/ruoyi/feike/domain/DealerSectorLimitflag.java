package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * Flag对象 h_dealer_sector_limitflag
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public class DealerSectorLimitflag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealerid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    @ForUpdate(fieldName = "dealername")
    private String dealername;

    /** 品牌 */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "normalflag")
    private String normalflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "demolflag")
    private String demolflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "tempflag")
    private String tempflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "mrgflag")
    private String mrgflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "cbflag")
    private String cbflag;


    /** $column.columnComment */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "threepartyflag")
    private String threepartyflag;

    @ForUpdate(fieldName = "partsFlag")
    private String partsflag;


    private Integer  isTermination;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerid(String dealerid) 
    {
        this.dealerid = dealerid;
    }

    public String getDealerid() 
    {
        return dealerid;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setNormalflag(String normalflag) 
    {
        this.normalflag = normalflag;
    }

    public String getNormalflag() 
    {
        return normalflag;
    }
    public void setDemolflag(String demolflag) 
    {
        this.demolflag = demolflag;
    }

    public String getDemolflag() 
    {
        return demolflag;
    }
    public void setTempflag(String tempflag) 
    {
        this.tempflag = tempflag;
    }

    public String getTempflag() 
    {
        return tempflag;
    }
    public void setMrgflag(String mrgflag) 
    {
        this.mrgflag = mrgflag;
    }

    public String getMrgflag() 
    {
        return mrgflag;
    }
    public void setCbflag(String cbflag) 
    {
        this.cbflag = cbflag;
    }

    public String getCbflag() 
    {
        return cbflag;
    }
    public void setThreepartyflag(String threepartyflag) 
    {
        this.threepartyflag = threepartyflag;
    }

    public String getThreepartyflag() 
    {
        return threepartyflag;
    }

    public Integer getIsTermination() {
        return isTermination;
    }

    public void setIsTermination(Integer isTermination) {
        this.isTermination = isTermination;
    }

    public String getPartsFlag() {
        return partsflag;
    }

    public void setPartsFlag(String partsFlag) {
        this.partsflag = partsFlag;
    }

    @Override
    public String toString() {
        return "DealerSectorLimitflag{" +
                "id='" + id + '\'' +
                ", dealerid='" + dealerid + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", normalflag='" + normalflag + '\'' +
                ", demolflag='" + demolflag + '\'' +
                ", tempflag='" + tempflag + '\'' +
                ", mrgflag='" + mrgflag + '\'' +
                ", cbflag='" + cbflag + '\'' +
                ", threepartyflag='" + threepartyflag + '\'' +
                ", partsFlag='" + partsflag + '\'' +
                ", isTermination=" + isTermination +
                '}';
    }
}
