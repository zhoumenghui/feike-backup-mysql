package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * feike对象 fileupload
 *
 * @author ruoyi
 * @date 2022-07-22
 */
@Data
public class Fileupload extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    @Excel(name = "文件完整路径")
    @ForUpdate(fieldName = "fileName")
    private String fileName;

    @Excel(name = "浏览器可以访问的url路径")
    @ForUpdate(fieldName = "url")
    private String url;

    @Excel(name = "文件中文名加后缀")
    @ForUpdate(fieldName = "originalFileName")
    private String originalFileName;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "文件创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "fileCreateTime")
    private Date fileCreateTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "文件修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "fileEndTime")
    private Date fileEndTime;

    @Excel(name = "创建者名称")
    @ForUpdate(fieldName = "createName")
    private String createName;

    @Excel(name = "类型")
    @ForUpdate(fieldName = "type")
    private String type;

    @ForUpdate(fieldName = "name")
    private String name;

}
