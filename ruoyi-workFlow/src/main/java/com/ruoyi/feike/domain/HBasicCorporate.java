package com.ruoyi.feike.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 h_basic_corporate_guarantee
 *
 * @author ruoyi
 * @date 2023-04-21
 */
public class HBasicCorporate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    
    private String dealername;

    /** $column.columnComment */
    
    private String instanceid;

    /** $column.columnComment */
    
    private String province;

    /** $column.columnComment */
    
    private String city;

    /** $column.columnComment */
    
    private String peoplesum;

    /** $column.columnComment */
    
    private String downloanaccount;

    /** $column.columnComment */
    
    private String enterprisetype;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getInstanceid() {
        return instanceid;
    }

    public void setInstanceid(String instanceid) {
        this.instanceid = instanceid;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPeoplesum() {
        return peoplesum;
    }

    public void setPeoplesum(String peoplesum) {
        this.peoplesum = peoplesum;
    }

    public String getDownloanaccount() {
        return downloanaccount;
    }

    public void setDownloanaccount(String downloanaccount) {
        this.downloanaccount = downloanaccount;
    }

    public String getEnterprisetype() {
        return enterprisetype;
    }

    public void setEnterprisetype(String enterprisetype) {
        this.enterprisetype = enterprisetype;
    }
}