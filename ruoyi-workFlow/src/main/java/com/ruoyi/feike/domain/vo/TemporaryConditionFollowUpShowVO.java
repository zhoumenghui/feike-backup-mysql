package com.ruoyi.feike.domain.vo;

import com.ruoyi.feike.domain.TemporaryConditionFollowUp;
import lombok.Data;

import java.util.List;

@Data
public class TemporaryConditionFollowUpShowVO {

    private List<TemporaryConditionFollowUp> tempList;

    private List<String> dealNames;
}
