package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * feike对象 h_report_deposit
 *
 * @author ruoyi
 * @date 2022-09-06
 */
@Data
public class ReportDepositTow extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;

    @Excel(name = "InstanceId")
    private String instanceId;

    @Excel(name = "Dealer Name")
    private String dealerName;

    @Excel(name = "Sector")
    private String sector;

    @Excel(name = "Deposit Gap")
    private String depositGap;

    @Excel(name = "Bank Feedback No")
    private String bankFeedbackNo;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Deposit Received Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date depositReceivedDate;

    @Excel(name = "Received Amount")
    private String receivedAmount;

    @Excel(name = "Note")
    private String note;


}
