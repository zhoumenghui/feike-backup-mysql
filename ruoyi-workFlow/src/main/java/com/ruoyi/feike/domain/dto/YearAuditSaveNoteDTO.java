package com.ruoyi.feike.domain.dto;

import lombok.Data;

import java.io.Serializable;

//年审核保存DTO,又称草稿行为，用于年审时保存部分信息
@Data
public class YearAuditSaveNoteDTO implements Serializable {

    private String instanceId;

    private String basicNote;

    private String dealerNotes;

    private String loyaltyNotes;

    private String auditNotes;

    private String wholesaleNotes;

    private String proposalNotes;




}
