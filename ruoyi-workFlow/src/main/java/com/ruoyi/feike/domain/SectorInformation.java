package com.ruoyi.feike.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_sector_information
 *
 * @author ruoyi
 * @date 2022-08-17
 */
@Data
public class SectorInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 制造厂商 */
    @Excel(name = "制造厂商")
    private String sector;

    /** 型号 */
    @Excel(name = "型号")
    private String model;

    /** Avg loan amount */
    @Excel(name = "Avg loan amount")
    private Long avgLoanAmt;

    /** Actual Avg DSO */
    @Excel(name = "Actual Avg DSO")
    private Long actualAvgDso;

    private String estimatedAnnualWholesaleUnits;

    private String creditLimitForNewVehicle;

}
