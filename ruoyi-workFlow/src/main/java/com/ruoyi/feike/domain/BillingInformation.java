package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_billing_information
 * 
 * @author zmh
 * @date 2022-08-25
 */
public class BillingInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** 公司名称 */
    @Excel(name = "公司名称")
    @ForUpdate(fieldName = "companyName")
    private String companyName;

    /** 纳税人识别号 */
    @Excel(name = "纳税人识别号")
    @ForUpdate(fieldName = "registrationNumber")
    private String registrationNumber;

    /** 地址 */
    @Excel(name = "地址")
    @ForUpdate(fieldName = "address")
    private String address;

    /** 电话 */
    @Excel(name = "电话")
    @ForUpdate(fieldName = "telephone")
    private String telephone;

    /** 开户行 */
    @Excel(name = "开户行")
    @ForUpdate(fieldName = "openingBank")
    private String openingBank;

    /** 银行账号 */
    @Excel(name = "银行账号")
    @ForUpdate(fieldName = "bankAccount")
    private String bankAccount;

    /** 增值税一般纳税人 */
    @Excel(name = "增值税一般纳税人")
    @ForUpdate(fieldName = "isGeneral")
    private String isGeneral;

    /** 收件人 */
    @Excel(name = "收件人")
    @ForUpdate(fieldName = "receiver")
    private String receiver;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ForUpdate(fieldName = "receiverTelephone")
    private String receiverTelephone;

    /** 公司名称 */
    @Excel(name = "公司名称")
    @ForUpdate(fieldName = "receiverCompanyName")
    private String receiverCompanyName;

    /** 公司地址 */
    @Excel(name = "公司地址")
    @ForUpdate(fieldName = "receiverCompanyAddress")
    private String receiverCompanyAddress;

    /** 经销商接收电子保单邮箱地址 */
    @Excel(name = "经销商接收电子保单邮箱地址")
    @ForUpdate(fieldName = "emailAddress")
    private String emailAddress;

    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setCompanyName(String companyName) 
    {
        this.companyName = companyName;
    }

    public String getCompanyName() 
    {
        return companyName;
    }
    public void setRegistrationNumber(String registrationNumber) 
    {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() 
    {
        return registrationNumber;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public String getTelephone() 
    {
        return telephone;
    }
    public void setOpeningBank(String openingBank) 
    {
        this.openingBank = openingBank;
    }

    public String getOpeningBank() 
    {
        return openingBank;
    }
    public void setBankAccount(String bankAccount) 
    {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() 
    {
        return bankAccount;
    }


    public void setReceiver(String receiver)
    {
        this.receiver = receiver;
    }

    public String getReceiver() 
    {
        return receiver;
    }
    public void setReceiverTelephone(String receiverTelephone) 
    {
        this.receiverTelephone = receiverTelephone;
    }

    public String getReceiverTelephone() 
    {
        return receiverTelephone;
    }
    public void setReceiverCompanyName(String receiverCompanyName) 
    {
        this.receiverCompanyName = receiverCompanyName;
    }

    public String getReceiverCompanyName() 
    {
        return receiverCompanyName;
    }
    public void setReceiverCompanyAddress(String receiverCompanyAddress) 
    {
        this.receiverCompanyAddress = receiverCompanyAddress;
    }

    public String getReceiverCompanyAddress() 
    {
        return receiverCompanyAddress;
    }
    public void setEmailAddress(String emailAddress) 
    {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() 
    {
        return emailAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsGeneral() {
        return isGeneral;
    }

    public void setIsGeneral(String isGeneral) {
        this.isGeneral = isGeneral;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("instanceId", getInstanceId())
            .append("companyName", getCompanyName())
            .append("registrationNumber", getRegistrationNumber())
            .append("address", getAddress())
            .append("telephone", getTelephone())
            .append("openingBank", getOpeningBank())
            .append("bankAccount", getBankAccount())

            .append("receiver", getReceiver())
            .append("receiverTelephone", getReceiverTelephone())
            .append("receiverCompanyName", getReceiverCompanyName())
            .append("receiverCompanyAddress", getReceiverCompanyAddress())
            .append("emailAddress", getEmailAddress())
            .toString();
    }
}
