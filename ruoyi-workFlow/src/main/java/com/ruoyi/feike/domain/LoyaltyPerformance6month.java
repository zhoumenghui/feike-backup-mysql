package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_loyalty_performance6month
 * 
 * @author zmh
 * @date 2022-08-24
 */
public class LoyaltyPerformance6month extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;


    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "item")
    private String item;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "value")
    private String value;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "month")
    private String month;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setItem(String item) 
    {
        this.item = item;
    }

    public String getItem() 
    {
        return item;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setValue(String value) 
    {
        this.value = value;
    }

    public String getValue() 
    {
        return value;
    }
    public void setMonth(String month) 
    {
        this.month = month;
    }

    public String getMonth() 
    {
        return month;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerName", getDealerName())
            .append("item", getItem())
            .append("sector", getSector())
            .append("value", getValue())
            .append("month", getMonth())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
