package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * feike对象 h_new_sum_keep
 *
 * @author zmh
 * @date 2022-08-31
 */
@Data
public class NewSumKeepFour extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    private String id;

    @Excel(name = "Dealer Name")
    private String dealerName;

    @Excel(name = "Sector")
    private String sector;

    @Excel(name = "Effective Date")
    private String effectiveDate;


    @Excel(name = "Expiry Date")
    private String expiryDate;

    private String instanceId;

    @Excel(name = "Coverage")
    private Long sum;

    @Excel(name = "Rate")
    private String planRate;

    private String oldRate;

    private String oldPlanRate;

    private String premium;

    private String planMethod;

    private String premiumReceivedDate;

    private Integer inTransition;

    private String note;

    @Excel(name = "Temp Limit")
    private String tempLimit;

}
