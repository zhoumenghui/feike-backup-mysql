package com.ruoyi.feike.domain.dto;

import com.ruoyi.feike.domain.LimitActiveProposal;
import com.ruoyi.feike.domain.NewSumKeep;
import lombok.Data;

import java.util.List;

@Data
public class NewSumDTO {
	private List<LimitActiveProposal> limitActiveProposals;
	private List<NewSumKeep> insuranceCalculations;
	private String planMethod ;
}
