package com.ruoyi.feike.domain.dto;

import com.ruoyi.feike.domain.*;
import lombok.*;

import java.util.List;
import java.util.Map;

/**
 * @author zmh
 * @version 1.0
 * @description: WfsLimitSetUp流程表单DTO
 * @date 2022/8/26 23:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class WfsLimitSetUpdto {

    public Object getDepositCalculationList;
    private String instanceId;
	private String name;

	private String caoGao;
	private String processDefinitionKey;
	private String comments;
	private String conditionNotes;

	private String  groupName;
	private String  dealerName;
	private String  sectorName;
	private String  groupNameCn;
	private Integer  depositFlag;


	private List<Map<String,Object>> actForm;

	private List<BasicInformationDTO2> basicInformationList;
	private List<TemporaryConditionFollowUp> temporaryConditionFollowUpList;
	private List<LimitActiveProposal> limitActiveProposalList;
	private List<DepositCalculation> depositCalculationList;
	private List<InsuranceCalculation> insuranceCalculationList;
	private List<InsuranceAddress> insuranceAddressList;
	private List<SelfplanInformation> selfplanInformationList;
	private List<BillingInformation> billingInformationList;
	private List<NewSum> newSumList;
	private List<PaidSum> paidSumList;
	private AnnualReviewy annualReviewy;
	private List<Fileupload> application;
	private List<Fileupload> information;
	private List<Fileupload> financials;
	private List<Fileupload> guarantee;
	private List<Fileupload> corporate;
	private List<Fileupload> other;
	private List<Fileupload> opDocument;
	private List<InsuranceDeclarationInfo> insuranceDeclarationInfo;
	private List<InsuranceDeclarationInfo> depositDeclarationInfo;
	private Integer isAdopt;
	private List<HLimitSetupResult> limitSetupResult ;
	private List<NewSumKeep> insuranceHistory;
	private Integer isSamePerson;

}
