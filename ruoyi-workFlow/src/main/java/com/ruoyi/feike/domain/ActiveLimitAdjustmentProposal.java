package com.ruoyi.feike.domain;

import java.math.BigDecimal;

import com.ruoyi.common.annotation.ForUpdate;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_active_limit_adjustment_proposal
 *
 * @author zmh
 * @date 2022-07-05
 */
@Data
public class ActiveLimitAdjustmentProposal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "currentPermanentLimit")
    private Long currentPermanentLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "activeLimit")
    private Long activeLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "currentCashDeposit")
    private String currentCashDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "activeOS")
    private BigDecimal activeOS;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposedActiveLimit")
    private String proposedActiveLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "activeLimitChange")
    private String activeLimitChange;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "amountOfMargin")
    private Long amountOfMargin;

    @ForUpdate(fieldName = "limitType")
    private String limitType;

    @ForUpdate(fieldName = "creditLimit")
    private Long creditLimit;

    @ForUpdate(fieldName = "deposit")
    private String deposit;

    @ForUpdate(fieldName = "currentDeposit")
    private String currentDeposit;

    private String dealerCode;
    private  Integer sort;

}
