package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ForUpdate;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 合同归档表对象 h_dealercode_contract_filing
 *
 * @author ruoyi
 * @date 2022-08-23
 */
@Data
public class DealercodeContractFiling extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;


    @Excel(name = "Sector")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /**合同签回日期*/
    @Excel(name = "Signing Date", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "effectiveDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date effectiveDate;

    /** 经销商中文 */
    @Excel(name = "Dealer Name")
    @ForUpdate(fieldName = "dealerNameCN")
    private String dealerNameCN;

    /** 额度金额 */
    @Excel(name = "Facility Amount")
    @ForUpdate(fieldName = "facilityAmount")
    private String facilityAmount;

    /** 合同号 */
    @ForUpdate(fieldName = "contract")
    private String contract;

    @Excel(name = "Contract Type")
    private String contractType;






    /** 授信类型 */
    @ForUpdate(fieldName = "limitType")
    private String limitType;

    /** 合同地址 */
    @ForUpdate(fieldName = "contractLocation")
    private String contractLocation;

    /** 旧合同地址 */
    @ForUpdate(fieldName = "contractLocationHistory")
    private String contractLocationHistory;

    /** 合同名称 */
    @ForUpdate(fieldName = "contractName")
    private String contractName;

    /** 归档合同名称 */
    @ForUpdate(fieldName = "contractNameHistory")
    private String contractNameHistory;

    /** fax/original */
    @ForUpdate(fieldName = "fax")
    private String fax;

    /** 业务场景 */
    @ForUpdate(fieldName = "priority")
    private String priority;

    /** $column.columnComment */
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;


    /** 子流程ID */
    @ForUpdate(fieldName = "subProcessId")
    private String subProcessId;


    /**到期日*/
    @ForUpdate(fieldName = "expiredDate")
    private String expiredDate;
    /**保证金比例*/
    @ForUpdate(fieldName = "deposit")
    private String deposit;
    /**RC批复日期*/
    @ForUpdate(fieldName = "rcIssueDate")
    private Date rcIssueDate;


    @ForUpdate(fieldName = "status")
    private String status;

    private String note;
    @Excel(name = "Contract No")
    private String contractno;

    private String flag;

    private Integer sort;

    private String opFlag;

    private String shareholdersPath;

    private String start;

    private String end;

    private String partsAmount;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date filingDate;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contract", getContract())
            .append("dealerNameCN", getDealerNameCN())
            .append("sector", getSector())
            .append("limitType", getLimitType())
            .append("contractLocation", getContractLocation())
            .append("contractLocationHistory", getContractLocationHistory())
            .append("contractName", getContractName())
            .append("contractNameHistory", getContractLocationHistory())
            .append("fax", getFax())
            .append("priority", getPriority())
            .append("instanceId", getInstanceId())
            .append("subProcessId", getSubProcessId())
            .append("facilityAmount", getFacilityAmount())
            .append("expiredDate", getExpiredDate())
            .append("deposit", getDeposit())
            .append("rcIssueDate", getRcIssueDate())
            .append("effectiveDate", getEffectiveDate())
            .toString();
    }
}
