package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;


public class InsuranceDeclarationInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    private String id;

    private String instanceId;
    @ForUpdate(fieldName = "bankFeedbackNo")
    private String bankFeedbackNo;
    @ForUpdate(fieldName = "premiumReceivedDate")
    private Date premiumReceivedDate;
    @ForUpdate(fieldName = "receivedAmount")
    private String receivedAmount;
    @ForUpdate(fieldName = "note")
    private String note;


    private Integer flag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getBankFeedbackNo() {
        return bankFeedbackNo;
    }

    public void setBankFeedbackNo(String bankFeedbackNo) {
        this.bankFeedbackNo = bankFeedbackNo;
    }

    public Date getPremiumReceivedDate() {
        return premiumReceivedDate;
    }

    public void setPremiumReceivedDate(Date premiumReceivedDate) {
        this.premiumReceivedDate = premiumReceivedDate;
    }

    public String getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(String receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "InsuranceDeclarationInfo{" +
                "id='" + id + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", bankFeedbackNo='" + bankFeedbackNo + '\'' +
                ", premiumReceivedDate='" + premiumReceivedDate + '\'' +
                ", receivedAmount='" + receivedAmount + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
