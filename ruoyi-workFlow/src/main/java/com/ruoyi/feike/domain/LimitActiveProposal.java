package com.ruoyi.feike.domain;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ForUpdate;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_limit_active_proposal
 *
 * @author zmh
 * @date 2022-08-25
 */
@Data
public class LimitActiveProposal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "limitType")
    private String limitType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "os")
    private String os;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "CurrentCreditLimit")
    private Long CurrentCreditLimit;

    private Long CurrentCreditLimit2;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposalLimit")
    private Long proposalLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "approvedCashDeposit")
    private String approvedCashDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposalCashDeposit")
    private String proposalCashDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "CurrentActiveLimit")
    private String CurrentActiveLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposalActiveLimit")
    private String proposalActiveLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "limitCoveredDeposit")
    private String limitCoveredDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposedStart")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date proposedStart;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposedEnd")
    private Date proposedEnd;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date proposedExpiredDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "contractFaciltyAmount")
    private String contractFaciltyAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "contractExpireDate")
    private Date contractExpireDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "ContractFacilityAmount")
    private String ContractFacilityAmount;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "ContractExpiredDate")
    private Date ContractExpiredDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;

    private int isRefundOrder;

    @ForUpdate(fieldName = "contractDepositRatio")
    private String contractDepositRatio;

    @ForUpdate(fieldName = "contractHyperlink")
    private String contractHyperlink;

    private String contractStatus;

    private String dealerStatus;

    private Integer sort;

    @ForUpdate(fieldName = "flag")
    private Integer flag;

    private String CurrentActiveLimitExpiryDate;

    @ForUpdate(fieldName = "isReplace")
    private Integer isReplace;



}
