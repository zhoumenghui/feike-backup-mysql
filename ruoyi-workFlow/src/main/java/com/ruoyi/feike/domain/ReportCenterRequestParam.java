package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class ReportCenterRequestParam extends BaseEntity {


    private String dealerCode;
    private String dealerName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String requestDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String processingDate;
    private String requestType;
    private String status;



}
