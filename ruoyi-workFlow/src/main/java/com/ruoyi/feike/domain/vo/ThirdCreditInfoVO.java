package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


/**
 * Copyright 2022 json.cn
 */
import java.util.List;

@Data
public class ThirdCreditInfoVO implements Serializable {

    private int Code;
    private String Message;
    private DataSummary DataSummary;
    private List<List<Data>> Data;

    @lombok.Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DataSummary implements Serializable{

        private int DataCount;
        private int CreditInfoCount;
        private int CreditDetCount;
        private long CreditLimitSum;
        private int ActiveLimitSum;
        private double ActiveOSSum;
    }


    @lombok.Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data implements Serializable{

        private String DealerCode;
        private List<CreditInfo> CreditInfo;
    }


    @lombok.Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CreditInfo implements Serializable{

        private String Sector;
        private List<CrixpGrade> CrixpGrade;
        private List<CreditDet> CreditDet;
    }

    @lombok.Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CreditDet implements Serializable{

        private String LimitType;
        private long CreditLimit;
        private String ExpiryDate;
        private String SecurityRatio;
        private int ActiveLimit;
        private int ActiveOS;
    }

    @lombok.Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CrixpGrade implements Serializable{

        private int Year;
        private String Grade;
    }
}





/*
@Data
public class ThirdCreditInfoVO implements Serializable {

    private Integer Code;

    private String Message;

    private DataSummary dataSummary;

    private List<dealerCreditList> Data;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DataSummary implements Serializable{
        private Integer DataCount;
        private Integer CreditInfoCount;
        private Integer CreditDetCount;
        private BigDecimal CreditLimitSum;
        private BigDecimal ActiveLimitSum;
        private BigDecimal ActiveOSSum;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class dealerCreditList implements Serializable{
        private List<dealerCredit> credits;

    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class dealerCredit implements Serializable{
        private String DealerCode;

        private List<creditInfoList> CreditInfo;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class creditInfoList implements Serializable{
        private String Sector;
        private List<CrixpGradeList> CrixpGrade;
        private List<CreditDetList> CreditDet;
    }
    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CrixpGradeList implements Serializable{
        private Integer Year;
        private String Grade;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CreditDetList implements Serializable{
        private String LimitType;
        private Long CreditLimit;
        private String ExpiryDate;
        private String SecurityRatio;
        private BigDecimal ActiveLimit;
        private BigDecimal ActiveOS;
    }

}
*/
