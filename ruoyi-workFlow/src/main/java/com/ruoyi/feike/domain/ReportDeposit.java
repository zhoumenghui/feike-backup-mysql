package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_report_deposit
 *
 * @author ruoyi
 * @date 2022-09-06
 */
public class ReportDeposit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 流程编号 */
    @Excel(name = "流程编号")
    private String instanceId;

    /** Dealer Code */
    @Excel(name = "Dealer Code")
    private String dealerCode;

    /** Dealer Name */
    @Excel(name = "Dealer Name")
    private String dealerName;

    /** Request Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Request Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date requestDate;

    /** Processing Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Processing Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date processingDate;

    /** Request Type */
    @Excel(name = "Request Type")
    private String requestType;

    /** Deposit Type */
    @Excel(name = "Deposit Type")
    private String depositType;

    /** Request Amount */
    @Excel(name = "Request Amount")
    private String requestAmount;

    /** Transfer to Dealer Code */


    /** Old Balance Amount */
    @Excel(name = "Old Balance Amount")
    private String oldBalanceAmount;

    /** New Balance Amount */
    @Excel(name = "New Balance Amount")
    private String newBalanceAmount;

    @Excel(name = "Transfer to Dealer Code")
    private String transferToDealerCode;

    /** Transfer to Dealer Name */
    @Excel(name = "Transfer to Dealer Name")
    private String transferToDealerName;

    /** GroupNameCN */

    private String groupNameCn;

    /** GroupNameEN */

    private String groupNameEn;


    @Excel(name = "Deposit Type")
    private String toDepositType;
    @Excel(name = "Old Balance Amount")
    private String toOldBalanceAmount;

    @Excel(name = "New Balance Amount")
    private String toNewBalanceAmount;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }
    public void setDealerCode(String dealerCode)
    {
        this.dealerCode = dealerCode;
    }

    public String getDealerCode()
    {
        return dealerCode;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setRequestDate(Date requestDate)
    {
        this.requestDate = requestDate;
    }

    public Date getRequestDate()
    {
        return requestDate;
    }
    public void setProcessingDate(Date processingDate)
    {
        this.processingDate = processingDate;
    }

    public Date getProcessingDate()
    {
        return processingDate;
    }
    public void setRequestType(String requestType)
    {
        this.requestType = requestType;
    }

    public String getRequestType()
    {
        return requestType;
    }
    public void setDepositType(String depositType)
    {
        this.depositType = depositType;
    }

    public String getDepositType()
    {
        return depositType;
    }
    public void setRequestAmount(String requestAmount)
    {
        this.requestAmount = requestAmount;
    }

    public String getRequestAmount()
    {
        return requestAmount;
    }
    public void setTransferToDealerCode(String transferToDealerCode)
    {
        this.transferToDealerCode = transferToDealerCode;
    }

    public String getTransferToDealerCode()
    {
        return transferToDealerCode;
    }
    public void setTransferToDealerName(String transferToDealerName)
    {
        this.transferToDealerName = transferToDealerName;
    }

    public String getTransferToDealerName()
    {
        return transferToDealerName;
    }
    public void setOldBalanceAmount(String oldBalanceAmount)
    {
        this.oldBalanceAmount = oldBalanceAmount;
    }

    public String getOldBalanceAmount()
    {
        return oldBalanceAmount;
    }
    public void setNewBalanceAmount(String newBalanceAmount)
    {
        this.newBalanceAmount = newBalanceAmount;
    }

    public String getNewBalanceAmount()
    {
        return newBalanceAmount;
    }
    public void setGroupNameCn(String groupNameCn)
    {
        this.groupNameCn = groupNameCn;
    }

    public String getGroupNameCn()
    {
        return groupNameCn;
    }
    public void setGroupNameEn(String groupNameEn)
    {
        this.groupNameEn = groupNameEn;
    }

    public String getGroupNameEn()
    {
        return groupNameEn;
    }

    public String getToDepositType() {
        return toDepositType;
    }

    public void setToDepositType(String toDepositType) {
        this.toDepositType = toDepositType;
    }

    public String getToOldBalanceAmount() {
        return toOldBalanceAmount;
    }

    public void setToOldBalanceAmount(String toOldBalanceAmount) {
        this.toOldBalanceAmount = toOldBalanceAmount;
    }

    public String getToNewBalanceAmount() {
        return toNewBalanceAmount;
    }

    public void setToNewBalanceAmount(String toNewBalanceAmount) {
        this.toNewBalanceAmount = toNewBalanceAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("instanceId", getInstanceId())
            .append("dealerCode", getDealerCode())
            .append("dealerName", getDealerName())
            .append("requestDate", getRequestDate())
            .append("processingDate", getProcessingDate())
            .append("requestType", getRequestType())
            .append("depositType", getDepositType())
            .append("requestAmount", getRequestAmount())
            .append("transferToDealerCode", getTransferToDealerCode())
            .append("transferToDealerName", getTransferToDealerName())
            .append("oldBalanceAmount", getOldBalanceAmount())
            .append("newBalanceAmount", getNewBalanceAmount())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("groupNameCn", getGroupNameCn())
            .append("groupNameEn", getGroupNameEn())
            .toString();
    }
}
