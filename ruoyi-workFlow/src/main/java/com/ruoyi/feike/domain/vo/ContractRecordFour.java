package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 生成的合同详细信息对象 h_dealercode_contract
 *
 * @author ruoyi
 * @date 2022-08-10
 */
@Data
public class ContractRecordFour extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    private String instanceid;


    @Excel(name = "Sector")
    private String sector;

    @Excel(name = "Dealer Code")
    private String dealerCode;

    @Excel(name = "LimitType")
    private String limitType;

    @Excel(name = "Dealer Name")
    private String dealerName;

    @Excel(name = "Contract No")
    private String contractNo;

    @Excel(name = "Facility Amount")
    private String facilityAmount;

    @Excel(name = "Effective Date", width = 30, dateFormat = "yyyy/MM/dd")
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date effectiveDate;

    @Excel(name = "Expiry Date")
    private String expiryDate;

    @Excel(name = "Temp Limit Amount")
    private String tempLimitAmount;

    @Excel(name = "Temp Limit Effective Date", width = 30, dateFormat = "yyyy/MM/dd")
    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date tempLimitEffectiveDate;

    @Excel(name = "Temp Limit Expiry Date")
    private String tempLimitExpiryDate;

    @Excel(name = "IF New Contract")
    private String ifNewContract;

    @Excel(name = "IF Guarantee Change")
    private String ifGuaranteeChange;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date limitStartDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date limitEndDate;


}
