package com.ruoyi.feike.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Auther: 兵兵有你
 * @Data: 2022/08/23/10:16
 * @Description:
 */
@Data
public class WaitForSaveNoteDTO implements Serializable {

    private String id;
    private String auditNotes;
    private String basicNotes;
    private String loyaltyNotes;
    private String proposalNotes;
    private String wholesaleNotes;
    private String dealerNotes;
    private String instanceId;
}
