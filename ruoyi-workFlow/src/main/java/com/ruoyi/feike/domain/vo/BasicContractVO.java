package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
public class BasicContractVO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractname;

    /** 合同地址 */
    @Excel(name = "合同地址")
    private String contractlocation;

    /** 优先级 */
    @Excel(name = "优先级")
    private String priority;

    /** 主机厂 */
    @Excel(name = "主机厂")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    private String limittype;

    private String contractlocations;

}
