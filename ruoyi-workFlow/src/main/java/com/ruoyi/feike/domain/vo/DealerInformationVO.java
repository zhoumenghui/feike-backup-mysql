package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class DealerInformationVO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String groupName;
    private String groupNameEN;
    private String groupNameCN;

    private String dealerNameEN;
    private String dealerNameCN;
    private String dealerValue;

    private List sector;
    private List dealerCodeFromWFS;
    private List dealerCodeFromManufacturer;

    private String registeredAddressCN;
    private String registeredAddressEN;
    private String corporateGuarantorCn;
    private String corporateGuarantorEn;
    private String individualGuarantorCn;
    private String individualGuarantorEn;
    private String shareholderStructure;
    private String newdealername;
    private String newdealernameEn;
    private String newgroupnameCn;
    private String newgroupnameEn;
    private List newsector;
    private List newdealerCodeFromWFS;

    private String legalRepresentativeCN;
    private String legalRepresentativeEN;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date incorporationDate;
    private String registeredCapitalCn;
    private String registeredCapitalEn;
    private String registeredUrbanCn;
    private String registeredUrbanEn;
    private String uniformCreditCode;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date joinSectorDealerNetworkDate;
    private String paidUpCapital;

    private String gm;
    private Long relatedWorkingExperience;
    private List<Map<String,Object>> shareholdersAndShare;
    private List shareholdersCn;
    private List shareholdersEn;
    private List share;

    private String instanceId;
    private Integer priority;
    private String priorityId;

    private Long serialNumber;
    private String formerDealerName;
    private String dmName;


    private String province;

    private String provinceEN;

    private String city;

    private String peopleSum;

    private String enterpriseType;

    private String downLoanAccount;

    private List<Map<String,Object>> corporateGuarantorJson;

    private List<Map<String,Object>> cityJson;
}
