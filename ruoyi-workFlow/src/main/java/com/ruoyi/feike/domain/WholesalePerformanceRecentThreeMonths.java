package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * feike对象 h_wholesale_performance_recent_three_months
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class WholesalePerformanceRecentThreeMonths extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealername")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "creditType")
    private String creditType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "month")
    private Date month;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "os")
    private Long os;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dso")
    private Long dso;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "aging")
    private Long aging;
    @ForUpdate(fieldName = "limitType")
    private String limitType;
    @ForUpdate(fieldName = "type")
    private String type;
    @ForUpdate(fieldName = "BRAND")
    private String BRAND;
    @ForUpdate(fieldName = "DEALER")
    private String DEALER;
    @ForUpdate(fieldName = "lastLastYearMonth")
    private String lastLastYearMonth;
    @ForUpdate(fieldName = "lastYearMonth")
    private String lastYearMonth;
    @ForUpdate(fieldName = "currentYearMonth")
    private String currentYearMonth;
    @ForUpdate(fieldName = "thead")
    private String thead;

    public String getThead() {
        return thead;
    }

    public void setThead(String thead) {
        this.thead = thead;
    }

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setCreditType(String creditType) 
    {
        this.creditType = creditType;
    }

    public String getCreditType() 
    {
        return creditType;
    }
    public void setMonth(Date month) 
    {
        this.month = month;
    }

    public Date getMonth() 
    {
        return month;
    }
    public void setOs(Long os) 
    {
        this.os = os;
    }

    public Long getOs() 
    {
        return os;
    }
    public void setDso(Long dso) 
    {
        this.dso = dso;
    }

    public Long getDso() 
    {
        return dso;
    }
    public void setAging(Long aging) 
    {
        this.aging = aging;
    }

    public Long getAging() 
    {
        return aging;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    public String getDEALER() {
        return DEALER;
    }

    public void setDEALER(String DEALER) {
        this.DEALER = DEALER;
    }

    public String getLastLastYearMonth() {
        return lastLastYearMonth;
    }

    public void setLastLastYearMonth(String lastLastYearMonth) {
        this.lastLastYearMonth = lastLastYearMonth;
    }

    public String getLastYearMonth() {
        return lastYearMonth;
    }

    public void setLastYearMonth(String lastYearMonth) {
        this.lastYearMonth = lastYearMonth;
    }

    public String getCurrentYearMonth() {
        return currentYearMonth;
    }

    public void setCurrentYearMonth(String currentYearMonth) {
        this.currentYearMonth = currentYearMonth;
    }

    @Override
    public String toString() {
        return "WholesalePerformanceRecentThreeMonths{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", creditType='" + creditType + '\'' +
                ", month=" + month +
                ", os=" + os +
                ", dso=" + dso +
                ", aging=" + aging +
                ", limitType='" + limitType + '\'' +
                ", type='" + type + '\'' +
                ", BRAND='" + BRAND + '\'' +
                ", DEALER='" + DEALER + '\'' +
                ", lastLastYearMonth='" + lastLastYearMonth + '\'' +
                ", lastYearMonth='" + lastYearMonth + '\'' +
                ", currentYearMonth='" + currentYearMonth + '\'' +
                ", thead='" + thead + '\'' +
                ", instanceId='" + instanceId + '\'' +
                '}';
    }
}
