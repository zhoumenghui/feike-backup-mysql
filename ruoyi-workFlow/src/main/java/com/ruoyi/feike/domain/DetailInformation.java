package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * feike对象 h_detail_information
 * 
 * @author zmh
 * @date 2022-07-06
 */
public class DetailInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String month;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String loyaltyGrade;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal wsPen;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal rtPen;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer contract;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal interestRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String auditperformance;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String plmClassification;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal gacFcaRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal maseratiRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal alfaRomeoRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal mclarenRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal navecoRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal chryslerRate;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setMonth(String month)
    {
        this.month = month;
    }

    public String getMonth() 
    {
        return month;
    }
    public void setLoyaltyGrade(String loyaltyGrade) 
    {
        this.loyaltyGrade = loyaltyGrade;
    }

    public String getLoyaltyGrade() 
    {
        return loyaltyGrade;
    }
    public void setWsPen(BigDecimal wsPen) 
    {
        this.wsPen = wsPen;
    }

    public BigDecimal getWsPen() 
    {
        return wsPen;
    }
    public void setRtPen(BigDecimal rtPen) 
    {
        this.rtPen = rtPen;
    }

    public BigDecimal getRtPen() 
    {
        return rtPen;
    }
    public void setContract(Integer contract) 
    {
        this.contract = contract;
    }

    public Integer getContract() 
    {
        return contract;
    }
    public void setInterestRate(BigDecimal interestRate) 
    {
        this.interestRate = interestRate;
    }

    public BigDecimal getInterestRate() 
    {
        return interestRate;
    }
    public void setAuditperformance(String auditperformance) 
    {
        this.auditperformance = auditperformance;
    }

    public String getAuditperformance() 
    {
        return auditperformance;
    }
    public void setPlmClassification(String plmClassification) 
    {
        this.plmClassification = plmClassification;
    }

    public String getPlmClassification() 
    {
        return plmClassification;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setGacFcaRate(BigDecimal gacFcaRate) 
    {
        this.gacFcaRate = gacFcaRate;
    }

    public BigDecimal getGacFcaRate() 
    {
        return gacFcaRate;
    }
    public void setMaseratiRate(BigDecimal maseratiRate) 
    {
        this.maseratiRate = maseratiRate;
    }

    public BigDecimal getMaseratiRate() 
    {
        return maseratiRate;
    }
    public void setAlfaRomeoRate(BigDecimal alfaRomeoRate) 
    {
        this.alfaRomeoRate = alfaRomeoRate;
    }

    public BigDecimal getAlfaRomeoRate() 
    {
        return alfaRomeoRate;
    }
    public void setMclarenRate(BigDecimal mclarenRate) 
    {
        this.mclarenRate = mclarenRate;
    }

    public BigDecimal getMclarenRate() 
    {
        return mclarenRate;
    }
    public void setNavecoRate(BigDecimal navecoRate) 
    {
        this.navecoRate = navecoRate;
    }

    public BigDecimal getNavecoRate() 
    {
        return navecoRate;
    }
    public void setChryslerRate(BigDecimal chryslerRate) 
    {
        this.chryslerRate = chryslerRate;
    }

    public BigDecimal getChryslerRate() 
    {
        return chryslerRate;
    }

    @Override
    public String toString() {
        return "DetailInformation{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", month='" + month + '\'' +
                ", loyaltyGrade='" + loyaltyGrade + '\'' +
                ", wsPen=" + wsPen +
                ", rtPen=" + rtPen +
                ", contract=" + contract +
                ", interestRate=" + interestRate +
                ", auditperformance='" + auditperformance + '\'' +
                ", plmClassification='" + plmClassification + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", gacFcaRate=" + gacFcaRate +
                ", maseratiRate=" + maseratiRate +
                ", alfaRomeoRate=" + alfaRomeoRate +
                ", mclarenRate=" + mclarenRate +
                ", navecoRate=" + navecoRate +
                ", chryslerRate=" + chryslerRate +
                '}';
    }
}
