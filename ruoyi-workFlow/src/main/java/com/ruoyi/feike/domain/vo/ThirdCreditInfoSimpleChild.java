package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * Copyright 2022 json.cn
 */

@Data
public class ThirdCreditInfoSimpleChild implements Serializable {

        private String id;

        private String Sector;
        private String LimitType;
        private long CreditLimit;
        @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
        private Date ExpiryDate;
        private String SecurityRatio;
        private int ActiveLimit;
        private int ActiveOS;

        private String reviewType;

        private String loanTenor;

        private String  buyBack;

        private String instanceId;

        private String dealerId;

        private String dealerName;

        private Integer sort;

}




