package com.ruoyi.feike.domain;

import java.util.Date;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 h_new_sum_keep_modify
 *
 * @author ruoyi
 * @date 2023-01-11
 */
@Data
public class HNewSumKeepModify extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date effectivedate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date neweffectivedate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date expirydate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date newexpirydate;

    /** 保额 */
    @Excel(name = "保额")
    private Long sum;

    /** 新保额 */
    @Excel(name = "新保额")
    private Long newsum;

    /** 利率 */
    @Excel(name = "利率")
    private String planrate;

    /** 新利率 */
    @Excel(name = "新利率")
    private String newplanrate;

    /** 保费 */
    @Excel(name = "保费")
    private String premium;

    private String newPremium;

    /** 购买方式 */
    @Excel(name = "购买方式")
    private String planmethod;

    /** $column.columnComment */
    @Excel(name = "购买方式")
    private String dealercode;

    private String updateBy;

    private Date updateTime;

    private String instanceId;

    /** 原数据ID */
    @Excel(name = "原数据ID")
    private String oldid;

    private String note;

    private String newNote;

}
