package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;


@Data
public class WFSLimitSetup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String instanceId;

    private String dealerName;

    private String sector;

    private String req;

    private String resp;

}
