package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 生成的合同详细信息对象 h_dealercode_contract
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
@Data
public class ContractRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "instanceid" )
    private String instanceid;

    /** $column.columnComment */

    @Excel(name = "issueDate", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ForUpdate(fieldName = "issuedate")
    private Date issuedate;

    /** $column.columnComment */
    @Excel(name = "effectivedate", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "effectivedate")
    private Date effectivedate;

    @Excel(name = "status")
    @ForUpdate(fieldName = "status")
    private String status;

    /** $column.columnComment */
    @Excel(name = "oemDealerCode" )
    private String oemdealercode;

    /** $column.columnComment */
    @Excel(name = "dealerCode" )
    private String dealercode;

    /** $column.columnComment */
    @Excel(name = "dealerName")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "facilityamount")
    private String facilityamount;

    @Excel(name = "deposit")
    private String deposit;

    @Excel(name = "partsAmount")
    private String partsAmount;

    @Excel(name = "partsDepositRatio")
    private String partsDepositRatio;

    @Excel(name = "limitType")
    private String limitType;

    /** $column.columnComment */
    @Excel(name = "contractType")
    private String contracttype;

    /** $column.columnComment */
    @Excel(name = "contractNo")
    private String contractno;

    /** $column.columnComment */
    @Excel(name = "expiryDate", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expirydate;

    /** $column.columnComment */
    @Excel(name = "personalGuarantee")
    private String personalguarantee;

    /** $column.columnComment */
    @Excel(name = "companyGuarantee")
    private String companyguarantee;

    @Excel(name = "groupName")
    private String groupName;

    /** $column.columnComment */
    @Excel(name = "scenario")
    private String scenario;


    /** $column.columnComment */
    @Excel(name = "Fax/Original")
    @ForUpdate(fieldName = "original")
    private String original;

    @ForUpdate(fieldName = "fileUrl")
    private String fileUrl;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date effStartDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date effEndDate;

    private String note;


    private String opFlag;


}
