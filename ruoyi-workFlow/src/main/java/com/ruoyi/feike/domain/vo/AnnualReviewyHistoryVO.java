package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class AnnualReviewyHistoryVO extends BaseEntity {

	/** 主键ID */
	private String id;

	/** 请假类型 */
	@Excel(name = "请假类型")
	private String type;

	/** 标题 */
	@Excel(name = "标题")
	private String title;

	/** 原因 */
	@Excel(name = "原因")
	private String reason;

	/** 开始时间 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
	private Date leaveStartTime;

	/** 结束时间 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
	private Date leaveEndTime;

	/** 附件 */
	@Excel(name = "附件")
	private String attachmentLink;

	/** 流程实例ID */
	//@Excel(name = "流程实例ID")
	private String instanceId;

	private String taskName;

	/** 状态 */
	@Excel(name = "状态")
	private String state;

	/** 创建者名称 */
	@Excel(name = "创建者名称")
	private String createName;

	/** 用户ID */
	@Excel(name = "用户账号")
	private String username;

	private String dealerName;

	private String groupName;
	private String node;

	private String sector;

	private String uw;

	private String exeId;

	private String taskId;

}
