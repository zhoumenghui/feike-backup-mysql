package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报表h_report-insurance-3对象 h_report-insurance-3
 * 
 * @author ruoyi
 * @date 2022-12-27
 */
public class ReportInsuranceThree extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 流程编码 */
    @Excel(name = "流程编码")
    private String instanceId;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealerName;

    /** 品牌 */
    @Excel(name = "品牌")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String premium;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String bankFeedbackNo;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "品牌", width = 30, dateFormat = "yyyy-MM-dd")
    private Date premiumReceivedDate;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String receivedAmount;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String noteOne;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String total;

    private String opFlag;

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setPremium(String premium) 
    {
        this.premium = premium;
    }

    public String getPremium() 
    {
        return premium;
    }
    public void setBankFeedbackNo(String bankFeedbackNo) 
    {
        this.bankFeedbackNo = bankFeedbackNo;
    }

    public String getBankFeedbackNo() 
    {
        return bankFeedbackNo;
    }
    public void setPremiumReceivedDate(Date premiumReceivedDate) 
    {
        this.premiumReceivedDate = premiumReceivedDate;
    }

    public Date getPremiumReceivedDate() 
    {
        return premiumReceivedDate;
    }
    public void setReceivedAmount(String receivedAmount) 
    {
        this.receivedAmount = receivedAmount;
    }

    public String getReceivedAmount() 
    {
        return receivedAmount;
    }
    public void setNoteOne(String noteOne) 
    {
        this.noteOne = noteOne;
    }

    public String getNoteOne() 
    {
        return noteOne;
    }
    public void setTotal(String total) 
    {
        this.total = total;
    }

    public String getTotal() 
    {
        return total;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("instanceId", getInstanceId())
            .append("dealerName", getDealerName())
            .append("sector", getSector())
            .append("premium", getPremium())
            .append("bankFeedbackNo", getBankFeedbackNo())
            .append("premiumReceivedDate", getPremiumReceivedDate())
            .append("receivedAmount", getReceivedAmount())
            .append("noteOne", getNoteOne())
            .append("total", getTotal())
            .toString();
    }
}
