package com.ruoyi.feike.domain;


import lombok.Data;

@Data
public class WholesalePerformanceRecentMonthsOsDTO {
    private String[] months;
    private String[] dealerName;
    private String monthsStr;
}
