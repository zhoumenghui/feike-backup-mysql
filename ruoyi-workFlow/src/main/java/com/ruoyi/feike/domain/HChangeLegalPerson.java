package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

public class HChangeLegalPerson extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    private String dealerName;

    private String candidateNo;

    private Integer isSuccess;

    private String legalRepresentativeCN;

    public String getLegalRepresentativeCN() {
        return legalRepresentativeCN;
    }

    public void setLegalRepresentativeCN(String legalRepresentativeCN) {
        this.legalRepresentativeCN = legalRepresentativeCN;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getCandidateNo() {
        return candidateNo;
    }

    public void setCandidateNo(String candidateNo) {
        this.candidateNo = candidateNo;
    }

    public Integer getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Integer isSuccess) {
        this.isSuccess = isSuccess;
    }

    @Override
    public String toString() {
        return "HChangeLegalPerson{" +
                "id=" + id +
                ", dealerName='" + dealerName + '\'' +
                ", candidateNo='" + candidateNo + '\'' +
                ", isSuccess=" + isSuccess +
                '}';
    }
}