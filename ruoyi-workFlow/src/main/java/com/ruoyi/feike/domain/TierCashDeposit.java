package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * feike对象 h_tier_cash_deposit
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public class TierCashDeposit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dearler;

    /** 经销商中文名称 */
    @Excel(name = "经销商中文名称")
    @ForUpdate(fieldName = "dealerNameCn")
    private String dealerNameCn;

    /** 集团中文名称 */
    @Excel(name = "集团中文名称")
    @ForUpdate(fieldName = "groupNameCn")
    private String groupNameCn;

    /** 经销商编码 */
    @Excel(name = "经销商编码")
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;
    // private String dealerCodes;

    /** 状态 */
    @Excel(name = "状态")
    @ForUpdate(fieldName = "status")
    private String status;
    @ForUpdate(fieldName = "permitted2ndTierWithoutDesposit")
    private Long permitted2ndTierWithoutDesposit;
    @ForUpdate(fieldName = "permitted2ndTierWithDesposit")
    private Long permitted2ndTierWithDesposit;
    @ForUpdate(fieldName = "registeredTwoTierNumber")
    private BigDecimal registeredTwoTierNumber;
    @ForUpdate(fieldName = "twoTierWithDesposit")
    private BigDecimal twoTierWithDesposit;
    @ForUpdate(fieldName = "towndTierCashDeposit")
    private BigDecimal towndTierCashDeposit;
    @ForUpdate(fieldName = "twondTierDeposit")
    private BigDecimal twondTierDeposit;
    @ForUpdate(fieldName = "parkingLocation")
    /** 停车位置 */
    @Excel(name = "停车位置")
    private String parkingLocation;
    @ForUpdate(fieldName = "parkinglocations")
    /*  新加的字段  */
    private String parkinglocations;
    private String[] parkinglocationList;
    @ForUpdate(fieldName = "units")
    /** 单位 */
    @Excel(name = "单位")
    private String units;
    @ForUpdate(fieldName = "tierDepositReceivable")
    /** 应收账款 */
    @Excel(name = "应收账款")
    private Long tierDepositReceivable;
    @ForUpdate(fieldName = "tierDepositGap")
    /** 保证金差额 */
    @Excel(name = "保证金差额")
    private Long tierDepositGap;
    @ForUpdate(fieldName = "refundType")
    /** 退款类型 */
    @Excel(name = "退款类型")
    private String refundType;
    @ForUpdate(fieldName = "reason")
    @Excel(name = "原因")
    private String reason;
    @ForUpdate(fieldName = "dailyBankFeedback")
    @Excel(name = "每日银行反馈")
    private String dailyBankFeedback;
    @ForUpdate(fieldName = "notes")
    @Excel(name = "备注信息")
    private String notes;

    /** 上传资料 */
    @Excel(name = "上传资料")
    @ForUpdate(fieldName = "uploadFile")
    private String uploadFile;
    /** 实例id */
    @Excel(name = "实例id")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;
    @ForUpdate(fieldName = "fileName")
    private String fileName;
    @ForUpdate(fieldName = "fileUrl")
    private String fileUrl;
    private List<Fileupload> depositFileList;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDearler(String dearler) 
    {
        this.dearler = dearler;
    }

    public String getDearler() 
    {
        return dearler;
    }
    public void setDealerNameCn(String dealerNameCn) 
    {
        this.dealerNameCn = dealerNameCn;
    }

    public String getDealerNameCn() 
    {
        return dealerNameCn;
    }
    public void setGroupNameCn(String groupNameCn) 
    {
        this.groupNameCn = groupNameCn;
    }

    public String getGroupNameCn() 
    {
        return groupNameCn;
    }
    public void setDealerCode(String dealerCode) 
    {
        this.dealerCode = dealerCode;
    }

    public String getDealerCode() 
    {
        return dealerCode;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setParkingLocation(String parkingLocation) 
    {
        this.parkingLocation = parkingLocation;
    }

    public String getParkingLocation() 
    {
        return parkingLocation;
    }
    public void setUnits(String units) 
    {
        this.units = units;
    }

    public String getUnits() 
    {
        return units;
    }
    public void setTierDepositReceivable(Long tierDepositReceivable) 
    {
        this.tierDepositReceivable = tierDepositReceivable;
    }

    public Long getTierDepositReceivable() 
    {
        return tierDepositReceivable;
    }
    public void setTierDepositGap(Long tierDepositGap) 
    {
        this.tierDepositGap = tierDepositGap;
    }

    public Long getTierDepositGap() 
    {
        return tierDepositGap;
    }
    public void setRefundType(String refundType) 
    {
        this.refundType = refundType;
    }

    public String getRefundType() 
    {
        return refundType;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setDailyBankFeedback(String dailyBankFeedback) 
    {
        this.dailyBankFeedback = dailyBankFeedback;
    }

    public String getDailyBankFeedback() 
    {
        return dailyBankFeedback;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setUploadFile(String uploadFile) 
    {
        this.uploadFile = uploadFile;
    }

    public String getUploadFile() 
    {
        return uploadFile;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public List<Fileupload> getDepositFileList() {
        return depositFileList;
    }

    public void setDepositFileList(List<Fileupload> depositFileList) {
        this.depositFileList = depositFileList;
    }

    public Long getPermitted2ndTierWithoutDesposit() {
        return permitted2ndTierWithoutDesposit;
    }

    public void setPermitted2ndTierWithoutDesposit(Long permitted2ndTierWithoutDesposit) {
        this.permitted2ndTierWithoutDesposit = permitted2ndTierWithoutDesposit;
    }

    public Long getPermitted2ndTierWithDesposit() {
        return permitted2ndTierWithDesposit;
    }

    public void setPermitted2ndTierWithDesposit(Long permitted2ndTierWithDesposit) {
        this.permitted2ndTierWithDesposit = permitted2ndTierWithDesposit;
    }

    public BigDecimal getRegisteredTwoTierNumber() {
        return registeredTwoTierNumber;
    }

    public void setRegisteredTwoTierNumber(BigDecimal registeredTwoTierNumber) {
        this.registeredTwoTierNumber = registeredTwoTierNumber;
    }

    public BigDecimal getTwoTierWithDesposit() {
        return twoTierWithDesposit;
    }

    public void setTwoTierWithDesposit(BigDecimal twoTierWithDesposit) {
        this.twoTierWithDesposit = twoTierWithDesposit;
    }

    public String getParkinglocations() {
        return parkinglocations;
    }

    public void setParkinglocations(String parkinglocations) {
        this.parkinglocations = parkinglocations;
    }

    public String[] getParkinglocationList() {
        return parkinglocationList;
    }

    public void setParkinglocationList(String[] parkinglocationList) {
        this.parkinglocationList = parkinglocationList;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public BigDecimal getTowndTierCashDeposit() {
        return towndTierCashDeposit;
    }

    public void setTowndTierCashDeposit(BigDecimal towndTierCashDeposit) {
        this.towndTierCashDeposit = towndTierCashDeposit;
    }

    public BigDecimal getTwondTierDeposit() {
        return twondTierDeposit;
    }

    public void setTwondTierDeposit(BigDecimal twondTierDeposit) {
        this.twondTierDeposit = twondTierDeposit;
    }

    @Override
    public String toString() {
        return "TierCashDeposit{" +
                "id='" + id + '\'' +
                ", dearler='" + dearler + '\'' +
                ", dealerNameCn='" + dealerNameCn + '\'' +
                ", groupNameCn='" + groupNameCn + '\'' +
                ", dealerCode='" + dealerCode + '\'' +
                ", status='" + status + '\'' +
                ", permitted2ndTierWithoutDesposit=" + permitted2ndTierWithoutDesposit +
                ", permitted2ndTierWithDesposit=" + permitted2ndTierWithDesposit +
                ", registeredTwoTierNumber=" + registeredTwoTierNumber +
                ", twoTierWithDesposit=" + twoTierWithDesposit +
                ", towndTierCashDeposit=" + towndTierCashDeposit +
                ", twondTierDeposit=" + twondTierDeposit +
                ", parkingLocation='" + parkingLocation + '\'' +
                ", parkinglocations='" + parkinglocations + '\'' +
                ", parkinglocationList=" + Arrays.toString(parkinglocationList) +
                ", units='" + units + '\'' +
                ", tierDepositReceivable=" + tierDepositReceivable +
                ", tierDepositGap=" + tierDepositGap +
                ", refundType='" + refundType + '\'' +
                ", reason='" + reason + '\'' +
                ", dailyBankFeedback='" + dailyBankFeedback + '\'' +
                ", notes='" + notes + '\'' +
                ", uploadFile='" + uploadFile + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileUrl='" + fileUrl + '\'' +
                ", depositFileList=" + depositFileList +
                '}';
    }
}
