package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * feike对象 h_wholesale_performance_currently
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class WholesalePerformanceCurrently extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealername")
    private String dealername;
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "creditType")
    private String creditType;

    @ForUpdate(fieldName = "limitType")
    private String limitType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "creditLimit")
    private Long creditLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "activatedLimit")
    private Long activatedLimit;
    @ForUpdate(fieldName = "activeLimit")
    private String activeLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "expiredDate")
    private Date expiredDate;
    private String expiryDate;

    private String expiredDates;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "os")
    private Long os;

    @ForUpdate(fieldName = "activeOS")
    private String activeOS;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;
    @ForUpdate(fieldName = "securityRatio")
    private String securityRatio;

    private  Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setCreditType(String creditType) 
    {
        this.creditType = creditType;
    }

    public String getCreditType() 
    {
        return creditType;
    }
    public void setCreditLimit(Long creditLimit) 
    {
        this.creditLimit = creditLimit;
    }

    public Long getCreditLimit() 
    {
        return creditLimit;
    }
    public void setActivatedLimit(Long activatedLimit) 
    {
        this.activatedLimit = activatedLimit;
    }

    public Long getActivatedLimit() 
    {
        return activatedLimit;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getExpiredDates() {
        return expiredDates;
    }

    public void setExpiredDates(String expiredDates) {
        this.expiredDates = expiredDates;
    }

    public void setOs(Long os)
    {
        this.os = os;
    }

    public Long getOs() 
    {
        return os;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getActiveOS() {
        return activeOS;
    }

    public void setActiveOS(String activeOS) {
        this.activeOS = activeOS;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getActiveLimit() {
        return activeLimit;
    }

    public void setActiveLimit(String activeLimit) {
        this.activeLimit = activeLimit;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getSecurityRatio() {
        return securityRatio;
    }

    public void setSecurityRatio(String securityRatio) {
        this.securityRatio = securityRatio;
    }

    @Override
    public String toString() {
        return "WholesalePerformanceCurrently{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", dealerCode='" + dealerCode + '\'' +
                ", sector='" + sector + '\'' +
                ", creditType='" + creditType + '\'' +
                ", limitType='" + limitType + '\'' +
                ", creditLimit=" + creditLimit +
                ", activatedLimit=" + activatedLimit +
                ", activeLimit='" + activeLimit + '\'' +
                ", expiredDate=" + expiredDate +
                ", expiryDate='" + expiryDate + '\'' +
                ", expiredDates='" + expiredDates + '\'' +
                ", os=" + os +
                ", activeOS='" + activeOS + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", securityRatio='" + securityRatio + '\'' +
                '}';
    }
}
