package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_insurance_calculation
 * 
 * @author zmh
 * @date 2022-08-25
 */
public class InsuranceCalculation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** 保险方案 */
    @Excel(name = "保险方案")
    @ForUpdate(fieldName = "planRate")
    private String planRate;

    /** 购买保险方案 */
    @Excel(name = "购买保险方案")
    @ForUpdate(fieldName = "planMethod")
    private String planMethod;

    /** 购买保险总计 */
    @Excel(name = "购买保险总计")
    @ForUpdate(fieldName = "planTotal")
    private String planTotal;

    /** $column.columnComment */
    @Excel(name = "购买保险总计")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setPlanRate(String planRate) 
    {
        this.planRate = planRate;
    }

    public String getPlanRate() 
    {
        return planRate;
    }
    public void setPlanMethod(String planMethod) 
    {
        this.planMethod = planMethod;
    }

    public String getPlanMethod() 
    {
        return planMethod;
    }
    public void setPlanTotal(String planTotal) 
    {
        this.planTotal = planTotal;
    }

    public String getPlanTotal() 
    {
        return planTotal;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("dealerName", getDealerName())
            .append("planRate", getPlanRate())
            .append("planMethod", getPlanMethod())
            .append("planTotal", getPlanTotal())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
