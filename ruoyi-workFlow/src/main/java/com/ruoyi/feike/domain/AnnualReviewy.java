package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * feike对象 annual_reviewy
 *
 * @author ruoyi
 * @date 2022-07-07
 */
@Data
public class AnnualReviewy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 请假类型 */
    @ForUpdate(fieldName = "type")
    @Excel(name = "请假类型")
    private String type;

    /** 标题 */
    @ForUpdate(fieldName = "title")
    @Excel(name = "标题")
    private String title;

    /** 原因 */
    @Excel(name = "原因")
    private String reason;

    /** 开始时间 */

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leaveStartTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leaveEndTime;

    /** 附件 */
    @ForUpdate(fieldName = "type")
    @Excel(name = "附件")
    private String attachmentLink;

    /** 流程实例ID */
    @ForUpdate(fieldName = "instanceId")
    //@Excel(name = "流程实例ID")
    private String instanceId;
    private String taskName;

    /** 状态 */
    @Excel(name = "状态")
    @ForUpdate(fieldName = "state")
    private String state;

    /** 创建者名称 */
    @ForUpdate(fieldName = "createName")
    @Excel(name = "创建者名称")
    private String createName;

    /** 用户ID */
    @Excel(name = "用户账号")
    private String lockname;

    /** 起始节点*/
    @ForUpdate(fieldName = "startNode")
    private String startNode;

    @ForUpdate(fieldName = "GAP")
    private Long GAP;

    @ForUpdate(fieldName = "TOTAL")
    private Long TOTAL;


    private String isFlag;

    private String isHis;

    private String uwFlag;

    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    @ForUpdate(fieldName = "sector")
    private String sector;

    @ForUpdate(fieldName = "limitType")
    private String limitType;

    @ForUpdate(fieldName = "approvalName")
    private String approvalName;

    @ForUpdate(fieldName = "uwApprovalName")
    private String uwApprovalName;

    //排序字段名

    private String orderByColumn;

    //升序or降序
    private String order;

    private String uw;

    private Date completedDate;

    private Date startDate;

    private Date endDate;
}
