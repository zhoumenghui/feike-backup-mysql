package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * count对象 h_dealercode_count
 * 
 * @author ybw
 * @date 2022-08-15
 */
public class DealercodeCount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealercode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long count;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String temp;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contractname;

    public String getContractname() {
        return contractname;
    }

    public void setContractname(String contractname) {
        this.contractname = contractname;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDealercode(String dealercode) 
    {
        this.dealercode = dealercode;
    }

    public String getDealercode() 
    {
        return dealercode;
    }
    public void setCount(Long count) 
    {
        this.count = count;
    }

    public Long getCount() 
    {
        return count;
    }
    public void setTemp(String temp) 
    {
        this.temp = temp;
    }

    public String getTemp() 
    {
        return temp;
    }
    public void setInstanceid(String instanceid) 
    {
        this.instanceid = instanceid;
    }

    public String getInstanceid() 
    {
        return instanceid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealercode", getDealercode())
            .append("count", getCount())
            .append("temp", getTemp())
            .append("instanceid", getInstanceid())
            .append("contractname", getContractname())
            .toString();
    }
}
