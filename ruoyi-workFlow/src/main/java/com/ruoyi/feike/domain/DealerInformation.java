package com.ruoyi.feike.domain;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ForUpdate;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_dealer_information
 *
 * @author zmh
 * @date 2022-08-05
 */
@Data
public class DealerInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;


    private String dealerId;


    private String groupId;

    @Excel(name = "主机厂")
    @ForUpdate(fieldName = "make")
    private String make;

    @Excel(name = "集团英文")
    @ForUpdate(fieldName = "groupName")
    private String groupName;

    @Excel(name = "集团中文")
    @ForUpdate(fieldName = "groupNameCN")
    private String groupNameCN;

    @Excel(name = "经销商名称")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /**  注册日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "incorporationDate")
    private Date incorporationDate;

    /** 厂商授权日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "joinSectorDealerNetworkDate")
    private Date joinSectorDealerNetworkDate;

    /** 经销商英文名 */
    @Excel(name = "经销商英文名")
    @ForUpdate(fieldName = "enName")
    private String enName;

    /** 经销商编码 */
    @Excel(name = "经销商编码")
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;

    /** 经销商编码2 */
    @ForUpdate(fieldName = "dealerCodeFromManufacturer")
    private String dealerCodeFromManufacturer;

    /** 注册资本中文 */
    @ForUpdate(fieldName = "registeredCapitalCn")
    private String registeredCapitalCn;

    /** 注册资本英文 */
    @ForUpdate(fieldName = "registeredCapitalEn")
    private String registeredCapitalEn;

    /** 注册城市中文 */
    @ForUpdate(fieldName = "registeredUrbanCn")
    private String registeredUrbanCn;

    /** 注册城市英文 */
    @ForUpdate(fieldName = "registeredUrbanEn")
    private String registeredUrbanEn;

    /** 注册地址中文 */
    @ForUpdate(fieldName = "registeredAddressCN")
    private String registeredAddressCN;

    /** 注册地址英文 */
    @ForUpdate(fieldName = "registeredAddressEN")
    private String registeredAddressEN;

    /** 法人中文 */


    /** 法人英文 */
    @ForUpdate(fieldName = "legalRepresentativeEN")
    private String legalRepresentativeEN;

    /** 统一信用代码 */
    @Excel(name = "统一信用代码")
    @ForUpdate(fieldName = "uniformCreditCode")
    private String uniformCreditCode;

    /** 实缴资本 */
    @Excel(name = "实缴资本")
    @ForUpdate(fieldName = "paidUpCapital")
    private String paidUpCapital;

    /** 主机厂 */

    @Excel(name = "法人中文")
    @ForUpdate(fieldName = "legalRepresentativeCN")
    private String legalRepresentativeCN;

    /** 总经理 */
    @Excel(name = "总经理")
    @ForUpdate(fieldName = "gm")
    private String gm;

    /** 个人担保英文 */
    @ForUpdate(fieldName = "individualGuarantorEn")
    private String individualGuarantorEn;

    /** 个人担保中文 */
    @Excel(name = "个人担保中文")
    @ForUpdate(fieldName = "individualGuarantorCn")
    private String individualGuarantorCn;

    /** 机构担保英文 */
    @ForUpdate(fieldName = "corporateGuarantorEn")
    private String corporateGuarantorEn;

    /** 机构担保中文 */
    @Excel(name = "担保公司")
    @ForUpdate(fieldName = "corporateGuarantorCn")
    private String corporateGuarantorCn;


    /** 工作年限 */
    @ForUpdate(fieldName = "relatedWorkingExperience")
    private Long relatedWorkingExperience;

    /** 股东and股份占比 */
    @Excel(name = "股东and股份占比")
    @ForUpdate(fieldName = "shareholderStructure")
    private String shareholderStructure;


    private String shareholdersCn;


    private String shareholdersEn;


    private Double share;

    private String instanceId;

    private Integer priority;

    private String priorityId;

    private Long serialNumber;

    private String formerDealerName;

    private double depositCashAmount;

    private double currentSuspenseAmount;

    private double os;


    private String interest;

    private String mortgageAmount;

    private String writeOff;

    private String netBalance;
    @Excel(name = "经销商省份")
    private String province;

    private String provinceEN;
    @Excel(name = "经销商城市")
    private String city;
    @Excel(name = "经销商人数")
    private String peopleSum;
    @Excel(name = "企业类型")
    private String enterpriseType;
    @Excel(name = "首贷户")
    private String downLoanAccount;
    @Excel(name = "经销商担保信息")
    private String corporateGuarantorJson;
}
