package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_grade
 *
 * @author zmh
 * @date 2022-08-22
 */
public class Grade extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** 经销商名称 */
    @Excel(name = "经销商code")
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;

    /** 年份 */
    @Excel(name = "年份")
    @ForUpdate(fieldName = "year")
    private String year;

    /** 等级 */
    @Excel(name = "等级")
    @ForUpdate(fieldName = "grade")
    private String grade;

    /** 流程id */
    @Excel(name = "流程id")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setYear(String year)
    {
        this.year = year;
    }

    public String getYear()
    {
        return year;
    }
    public void setGrade(String grade)
    {
        this.grade = grade;
    }

    public String getGrade()
    {
        return grade;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerName", getDealerName())
            .append("year", getYear())
            .append("grade", getGrade())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
