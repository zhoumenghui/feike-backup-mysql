package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * feike对象 h_parking_location
 *
 * @author zmh
 * @date 2022-07-22
 */
@Data
public class ParkingLocationVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealernamecnid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealernamecn;

    /** 经销商英文 */
    @Excel(name = "经销商英文")
    private String dealernameen;

    /** 经销商集团中文 */
    @Excel(name = "经销商集团中文")
    private String groupnamecn;

    /** 经销商集团英文 */
    @Excel(name = "经销商集团英文")
    private String groupnameen;

    /** 经销商品牌代码 */
    @Excel(name = "经销商品牌代码")
    private String dealercode;

    /** 网点地址 */
    @Excel(name = "网点地址")
    private String parkinglocation;

    /** 网点性质 */
    @Excel(name = "网点性质")
    private String type;

    /** 起始日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date effectivedate;

    /** 到期日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date duedate;

    /** 车架号 */
    @Excel(name = "车架号")
    private String vin;

    /** 与本部距离 */
    @Excel(name = "与本部距离")
    private String distancefrom4s;

    /** 联系人电话 */
    @Excel(name = "联系人电话")
    private String tel;

    /** $column.columnComment */
//    @Excel(name = "联系人电话")
    private String instanceId;

    /** 当前网点状态 */
    @Excel(name = "当前网点状态",readConverterExp = "0=启用,1=禁用,2=失效")
    private String status;

    /** 已支付二网保证金 */
    @Excel(name = "已支付二网保证金")
    private BigDecimal twondtierdeposit;


    private Long permitted2ndtierwithdesposit;

}
