package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_balance_information
 *
 * @author zmh
 * @date 2022-07-05
 */
public class BalanceInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "depositCashAmount")
    private String depositCashAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "currentSuspenseAmount")
    private String currentSuspenseAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "os")
    private String os;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "unPaidInterestAndFees")
    private String unPaidInterestAndFees;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "writeOff")
    private String writeOff;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "unmortgageamount")
    private Long unmortgageamount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "netBalance")
    private String netBalance;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    @ForUpdate(fieldName = "mortgageAmount")
    private String mortgageAmount;
    @ForUpdate(fieldName = "interest")
    private String interest;

    public String getMortgageAmount() {
        return mortgageAmount;
    }

    public void setMortgageAmount(String mortgageAmount) {
        this.mortgageAmount = mortgageAmount;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId()
    {
        return dealerId;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setDepositCashAmount(String depositCashAmount)
    {
        this.depositCashAmount = depositCashAmount;
    }

    public String getDepositCashAmount()
    {
        return depositCashAmount;
    }
    public void setCurrentSuspenseAmount(String currentSuspenseAmount)
    {
        this.currentSuspenseAmount = currentSuspenseAmount;
    }

    public String getCurrentSuspenseAmount()
    {
        return currentSuspenseAmount;
    }
    public void setOs(String os)
    {
        this.os = os;
    }

    public String getOs()
    {
        return os;
    }
    public void setUnPaidInterestAndFees(String unPaidInterestAndFees)
    {
        this.unPaidInterestAndFees = unPaidInterestAndFees;
    }

    public String getUnPaidInterestAndFees()
    {
        return unPaidInterestAndFees;
    }
    public void setWriteOff(String writeOff)
    {
        this.writeOff = writeOff;
    }

    public String getWriteOff()
    {
        return writeOff;
    }
    public void setUnmortgageamount(Long unmortgageamount)
    {
        this.unmortgageamount = unmortgageamount;
    }

    public Long getUnmortgageamount()
    {
        return unmortgageamount;
    }
    public void setNetBalance(String netBalance)
    {
        this.netBalance = netBalance;
    }

    public String getNetBalance()
    {
        return netBalance;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("dealerName", getDealerName())
            .append("depositCashAmount", getDepositCashAmount())
            .append("currentSuspenseAmount", getCurrentSuspenseAmount())
            .append("os", getOs())
            .append("unPaidInterestAndFees", getUnPaidInterestAndFees())
            .append("writeOff", getWriteOff())
            .append("unmortgageamount", getUnmortgageamount())
            .append("netBalance", getNetBalance())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
