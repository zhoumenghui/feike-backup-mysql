package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_limit_calculation_for_commerical_needs
 * 
 * @author zmh
 * @date 2022-07-11
 */
public class LimitCalculationForCommericalNeedVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String dealername;
    private String sector;
    private String model;
    private Long aip;
    private String atd;
    private Long estimatedannualwholesaleunits;

    private Long creditlimitfornewvehicle;
    private String instanceId;
    private String index;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }

    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setModel(String model) 
    {
        this.model = model;
    }

    public String getModel() 
    {
        return model;
    }
    public void setAip(Long aip) 
    {
        this.aip = aip;
    }

    public Long getAip() 
    {
        return aip;
    }
    public void setAtd(String atd) 
    {
        this.atd = atd;
    }

    public String getAtd() 
    {
        return atd;
    }
    public void setEstimatedannualwholesaleunits(Long estimatedannualwholesaleunits) 
    {
        this.estimatedannualwholesaleunits = estimatedannualwholesaleunits;
    }

    public Long getEstimatedannualwholesaleunits() 
    {
        return estimatedannualwholesaleunits;
    }
    public void setCreditlimitfornewvehicle(Long creditlimitfornewvehicle) 
    {
        this.creditlimitfornewvehicle = creditlimitfornewvehicle;
    }

    public Long getCreditlimitfornewvehicle() 
    {
        return creditlimitfornewvehicle;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "LimitCalculationForCommericalNeedVO{" +
                "id='" + id + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", model='" + model + '\'' +
                ", aip=" + aip +
                ", atd='" + atd + '\'' +
                ", estimatedannualwholesaleunits=" + estimatedannualwholesaleunits +
                ", creditlimitfornewvehicle=" + creditlimitfornewvehicle +
                ", instanceId='" + instanceId + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
}
