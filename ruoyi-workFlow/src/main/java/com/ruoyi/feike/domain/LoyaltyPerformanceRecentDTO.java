package com.ruoyi.feike.domain;


import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.text.SimpleDateFormat;
import java.util.*;

@Data
public class LoyaltyPerformanceRecentDTO {
    @NotEmpty(message = "经销商名称不能为空!")
    private String[] dealerName;

    private List<String> lastYearTimeStr;
    private List<String> currentYearTimeStr;

    private List<String> lastYearTimeText;
    private List<String> currentYearTimeText;

    private String currentYear;
    private String lastYear;

    private String pivotStr;

    private String fieldStr;


    public void transformRecentQuarters() {
        currentYear = DateUtils.getCurrentYear();
        lastYear = DateUtils.getLastYear();
        lastYearTimeStr = new ArrayList<>();
        currentYearTimeStr = new ArrayList<>();
        lastYearTimeText = new ArrayList<>();
        currentYearTimeText = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        Date now = calendar.getTime();
        for (int i = 0; i < 4; i++) {
            int year = getYear(now);
            int q = (getMM(now) + 2) / 3;
            if (year >= currentYear) {
                currentYearTimeStr.add("Q" + q);
                currentYearTimeText.add(year + "Q" + q);
            } else {
                lastYearTimeStr.add("Q" + q);
                lastYearTimeText.add(year + "Q" + q);
            }
            now = monthAddNum(now, -3);
        }
        Collections.reverse(currentYearTimeText);
        Collections.reverse(lastYearTimeText);
        List pivotList = new ArrayList();
        pivotList.addAll(lastYearTimeStr);
        pivotList.addAll(currentYearTimeStr);
        pivotStr = StringUtils.join(pivotList, "','");
        pivotStr = "'" + pivotStr + "'";
    }

    public void transformRecentMonths() {
        currentYear = DateUtils.getCurrentYear();
        lastYear = DateUtils.getLastYear();
        lastYearTimeStr = new ArrayList<>();
        currentYearTimeStr = new ArrayList<>();
        lastYearTimeText = new ArrayList<>();
        currentYearTimeText = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        Date now = calendar.getTime();
        now = monthAddNum(now, -1);
        for (int i = 0; i < 6; i++) {
            int year = getYear(now);
            int m = getMM(now);
            if (year >= currentYear) {
                currentYearTimeStr.add(getmonthEnglish(m));
                currentYearTimeText.add(year + "" + sdf.format(now));
            } else {
                lastYearTimeStr.add(getmonthEnglish(m));
                lastYearTimeText.add(year + "" + sdf.format(now));
            }
            now = monthAddNum(now, -1);
        }
        List pivotList = new ArrayList();
        Collections.reverse(currentYearTimeText);
        Collections.reverse(lastYearTimeText);
        pivotList.addAll(lastYearTimeText);
        pivotList.addAll(currentYearTimeText);
        pivotStr = StringUtils.join(pivotList, "','");
        pivotStr = "'" + pivotStr + "'";
        fieldStr="";
        for (int i = 0; i < pivotList.size(); i++) {
            fieldStr+= "DECODE(\""+pivotList.get(i)+"\","+null+",0,"+"\""+pivotList.get(i)+"\") AS \""+pivotList.get(i)+"\",";
        }
        fieldStr=fieldStr.substring(0,fieldStr.length()-1);
    }

    private String getmonthEnglish(int m) {
        switch (m) {
            case 1:
                return "Jan-Feb";
            case 2:
                return "Feb-Mar";
            case 3:
                return "Mar-Apr";
            case 4:
                return "Apr-May";
            case 5:
                return "May-Jun";
            case 6:
//                return "Jun-Jul";
                return "Jan-Feb";
            case 7:
                return "Jul-Aug";
            case 8:
                return "Aug-Sept";
            case 9:
                return "Sept-Oct";
            case 10:
                return "Oct-Nov";
            case 11:
                return "Nov-Dec";
            case 12:
                return "Dec-Jan";
        }
        return "";
    }



    /**
     * 获取日期的年
     *
     * @param date 日期
     * @return 年
     */
    public int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获取日期的月份
     *
     * @param date 日期
     * @return 月份
     */
    public static int getMM(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }


    /**
     * @param time 时间
     * @param num  加的数，-num就是减去
     * @return 减去相应的数量的月份的日期
     */
    public Date monthAddNum(Date time, Integer num) {
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date date = format.parse(time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.add(Calendar.MONTH, num);
        Date newTime = calendar.getTime();
        return newTime;
    }


}
