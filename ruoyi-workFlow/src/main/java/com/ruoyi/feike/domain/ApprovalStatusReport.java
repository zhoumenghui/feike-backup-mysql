package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * DM提交流程报表对象 h_approval_status_report
 * 
 * @author feike
 * @date 2023-01-18
 */
public class ApprovalStatusReport extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 流程编码 */
    @Excel(name = "Processing Coding")
    private String instanceId;

    @Excel(name = "Dealer Code")
    private String dealerCode;


    @Excel(name = "Sector")
    private String sector;


    /** 经销商名称英文 */
    @Excel(name = "Dealer Name（EN）")
    private String dealerNameEn;

    /** 经销商名称中文 */
    @Excel(name = "Dealer Name（CN）")
    private String dealerNameCn;

    /** 流程名称 */
    @Excel(name = "Process Name")
    private String requirement;

    /** 后续UW节点页面增加此信息 */
    @Excel(name = "UW")
    private String responsibleUw;

    /** 状态 */
    @Excel(name = "Situation")
    private String situation;

    /** 当前节点 */
    @Excel(name = "Task Node")
    private String status;

    /** saleshead批过的日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "Initiation Date", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;

    /** 到UW节点时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "UW Date", width = 30, dateFormat = "yyyy-MM-dd" )
    private Date uwDate;


    /** normal 额度到期日 */

    @Excel(name = "Expiry Date")
    private String expiryDate;

    /** CRO审批时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Completed Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completedDate;

    @Excel(name = "Delegation")
    private String delegation;

    /** Original Limit */
    @Excel(name = "Original Limit")
    private String originalLimit;

    /** Updated Limit */
    @Excel(name = "Updated Limit")
    private String updatedLimit;

    /** Original Security Ratio */
    @Excel(name = "Original Security Ratio")
    private String originalSecurityRatio;

    /** Updated Security Ratio */
    @Excel(name = "Updated Security Ratio")
    private String updatedSecurityRatio;

    /** Group */
    @Excel(name = "Group")
    private String group;

    @Excel(name = "Province")
    private String province;

    @Excel(name = "City")
    private String city;

    @Excel(name = "People Sum")
    private String peopleSum;

    @Excel(name = "Down Loan Account")
    private String downLoanAccount;

    @Excel(name = "Enterprise Type")
    private String enterpriseType;

    @Excel(name = "Guarantor Industry")
    private String guarantorIndustry;

    @Excel(name = "Guarantor Region Code")
    private String guarantorRegionCode;

    @Excel(name = "Guarantor Scale")
    private String guarantorScale;

    private Date startDate;

    private Date endDate;

    private Integer flag;

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPeopleSum() {
        return peopleSum;
    }

    public void setPeopleSum(String peopleSum) {
        this.peopleSum = peopleSum;
    }

    public String getDownLoanAccount() {
        return downLoanAccount;
    }

    public void setDownLoanAccount(String downLoanAccount) {
        this.downLoanAccount = downLoanAccount;
    }

    public String getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(String enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    public String getGuarantorIndustry() {
        return guarantorIndustry;
    }

    public void setGuarantorIndustry(String guarantorIndustry) {
        this.guarantorIndustry = guarantorIndustry;
    }

    public String getGuarantorRegionCode() {
        return guarantorRegionCode;
    }

    public void setGuarantorRegionCode(String guarantorRegionCode) {
        this.guarantorRegionCode = guarantorRegionCode;
    }

    public String getGuarantorScale() {
        return guarantorScale;
    }

    public void setGuarantorScale(String guarantorScale) {
        this.guarantorScale = guarantorScale;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setDealerNameEn(String dealerNameEn) 
    {
        this.dealerNameEn = dealerNameEn;
    }

    public String getDealerNameEn() 
    {
        return dealerNameEn;
    }
    public void setDealerNameCn(String dealerNameCn) 
    {
        this.dealerNameCn = dealerNameCn;
    }

    public String getDealerNameCn() 
    {
        return dealerNameCn;
    }
    public void setRequirement(String requirement) 
    {
        this.requirement = requirement;
    }

    public String getRequirement() 
    {
        return requirement;
    }
    public void setResponsibleUw(String responsibleUw) 
    {
        this.responsibleUw = responsibleUw;
    }

    public String getResponsibleUw() 
    {
        return responsibleUw;
    }
    public void setCreatedDate(Date createdDate) 
    {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() 
    {
        return createdDate;
    }
    public void setUwDate(Date uwDate) 
    {
        this.uwDate = uwDate;
    }

    public Date getUwDate() 
    {
        return uwDate;
    }
    public void setSituation(String situation) 
    {
        this.situation = situation;
    }

    public String getSituation() 
    {
        return situation;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setCompletedDate(Date completedDate)
    {
        this.completedDate = completedDate;
    }

    public Date getCompletedDate() 
    {
        return completedDate;
    }
    public void setOriginalLimit(String originalLimit) 
    {
        this.originalLimit = originalLimit;
    }

    public String getOriginalLimit() 
    {
        return originalLimit;
    }
    public void setUpdatedLimit(String updatedLimit) 
    {
        this.updatedLimit = updatedLimit;
    }

    public String getUpdatedLimit() 
    {
        return updatedLimit;
    }
    public void setOriginalSecurityRatio(String originalSecurityRatio) 
    {
        this.originalSecurityRatio = originalSecurityRatio;
    }

    public String getOriginalSecurityRatio() 
    {
        return originalSecurityRatio;
    }
    public void setUpdatedSecurityRatio(String updatedSecurityRatio) 
    {
        this.updatedSecurityRatio = updatedSecurityRatio;
    }

    public String getUpdatedSecurityRatio() 
    {
        return updatedSecurityRatio;
    }
    public void setGroup(String group) 
    {
        this.group = group;
    }

    public String getGroup() 
    {
        return group;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

    @Override
    public String toString() {
        return "ApprovalStatusReport{" +
                "id=" + id +
                ", instanceId='" + instanceId + '\'' +
                ", sector='" + sector + '\'' +
                ", dealerCode='" + dealerCode + '\'' +
                ", dealerNameEn='" + dealerNameEn + '\'' +
                ", dealerNameCn='" + dealerNameCn + '\'' +
                ", requirement='" + requirement + '\'' +
                ", responsibleUw='" + responsibleUw + '\'' +
                ", createdDate=" + createdDate +
                ", uwDate=" + uwDate +
                ", situation='" + situation + '\'' +
                ", status='" + status + '\'' +
                ", expiryDate=" + expiryDate +
                ", completedDate=" + completedDate +
                ", originalLimit='" + originalLimit + '\'' +
                ", updatedLimit='" + updatedLimit + '\'' +
                ", originalSecurityRatio='" + originalSecurityRatio + '\'' +
                ", updatedSecurityRatio='" + updatedSecurityRatio + '\'' +
                ", group='" + group + '\'' +
                '}';
    }
}
