package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * feike对象 h_other_financing_resource
 *
 * @author zmh
 * @date 2022-07-11
 */
@ApiModel(value = "机构对象")
@Data
public class OtherFinancingResource extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 经销商ID */
    @ApiModelProperty( value = "经销商ID",required = true)
    @Excel(name = "经销商ID", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** 列表索引id */
    @ApiModelProperty("列表索引id")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String index;

    /** 经销商中文名 */
    @ApiModelProperty("经销商中文名")
    @Excel(name = "经销商中文名", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealername")
    private String dealername;

    /** 机构名称 */
    @ApiModelProperty("机构名称")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "institutionName")
    private String institutionName;

    /** 贷款类型 */
    @ApiModelProperty("贷款类型")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "loanType")
    private String loanType;

    /** 授信额度 */
    @ApiModelProperty("授信额度")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "approvedLimit")
    private String approvedLimit;

    /** 贷款余额 */
    @ApiModelProperty("贷款余额")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "os")
    private String os;

    /** 保证金比例 */
    @ApiModelProperty("保证金比例")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "security")
    private String security;

    /** 利率 */
    @ApiModelProperty("利率")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "interestRate")
    private String interestRate;

    /** 到期日 */
    @ApiModelProperty( "到期日")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @ForUpdate(fieldName = "dueDate")
    private Date dueDate;

    /** 用途 */
    @ApiModelProperty("用途")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "purpose")
    private String purpose;

    /** 流程ID */
    @ApiModelProperty("流程ID")
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** 是否删除*/
    private Integer isDelete;

}
