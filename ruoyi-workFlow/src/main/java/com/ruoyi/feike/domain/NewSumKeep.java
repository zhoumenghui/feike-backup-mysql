package com.ruoyi.feike.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * feike对象 h_new_sum_keep
 *
 * @author zmh
 * @date 2022-08-31
 */
@Data
public class NewSumKeep extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "effectiveDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date effectiveDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "expiryDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expiryDate;

    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** 保额 */
    @Excel(name = "保额")
    @ForUpdate(fieldName = "sum")
    private Long sum;

    /** 利率 */
    @Excel(name = "利率")
    @ForUpdate(fieldName = "planRate")
    private String planRate;

    @ForUpdate(fieldName = "oldRate")
    private String oldRate;

    @ForUpdate(fieldName = "oldPlanRate")
    private String oldPlanRate;

    /** 保费 */
    @Excel(name = "保费")
    @ForUpdate(fieldName = "premium")
    private String premium;

    /** 购买方式 */
    @Excel(name = "购买方式")
    @ForUpdate(fieldName = "planMethod")
    private String planMethod;

    @ForUpdate(fieldName = "premiumReceivedDate")
    private String premiumReceivedDate;

    @ForUpdate(fieldName = "inTransition")
    private Integer inTransition;

    private String type;

    private String note;

    private String tempLimit;

    private String insuranceNo;
}
