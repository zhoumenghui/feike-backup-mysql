package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

@Data
public class ApprovalStatusVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private String instanceId;
    private String sector;
    private String dealerName;
    private String dealerNameEn;
    private String requirement;
    private String responsibleUw;
    private Date createdDate;
    private Date uwDate;
    private String situation;
    private String Status;
    private Date expiryDate;
    private Date completedDate;
    private String originalLimit;
    private String updatedLimit;
    private String originalSecurityRatio;
    private String updatedSecurityRatio;
    private String Group;

}
