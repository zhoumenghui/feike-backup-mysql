package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * feike对象 h_approved_limit_and_condition
 *
 * @author ybw
 * @date 2022-07-12
 */
public class ApprovedLimitAndConditionVO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 经销商ID
     */
    private String dealerid;

    /**
     * 经销商名称
     */
    private String dealername;

    /**
     * 品牌
     */
    private String sector;

    /**
     * 授信类型
     */
    private Long limittype;

    /**
     * 当前额度
     */
    private Long currentlimit;

    /**
     * 当前保证金比例
     */
    private BigDecimal currentsecurityratio;

    /**
     * 申请额度
     */
    private Long aprovedlimit;

    /**
     * 批复保证金比例
     */
    private BigDecimal approvedsecurityratio;

    private String dealer;
    private String month;
    private String loyaltyGrade;
    private Long Priority;

    public Long getPriority() {
        return Priority;
    }

    public void setPriority(Long priority) {
        Priority = priority;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealerid() {
        return dealerid;
    }

    public void setDealerid(String dealerid) {
        this.dealerid = dealerid;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public Long getLimittype() {
        return limittype;
    }

    public void setLimittype(Long limittype) {
        this.limittype = limittype;
    }

    public Long getCurrentlimit() {
        return currentlimit;
    }

    public void setCurrentlimit(Long currentlimit) {
        this.currentlimit = currentlimit;
    }

    public BigDecimal getCurrentsecurityratio() {
        return currentsecurityratio;
    }

    public void setCurrentsecurityratio(BigDecimal currentsecurityratio) {
        this.currentsecurityratio = currentsecurityratio;
    }

    public Long getAprovedlimit() {
        return aprovedlimit;
    }

    public void setAprovedlimit(Long aprovedlimit) {
        this.aprovedlimit = aprovedlimit;
    }

    public BigDecimal getApprovedsecurityratio() {
        return approvedsecurityratio;
    }

    public void setApprovedsecurityratio(BigDecimal approvedsecurityratio) {
        this.approvedsecurityratio = approvedsecurityratio;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getLoyaltyGrade() {
        return loyaltyGrade;
    }

    public void setLoyaltyGrade(String loyaltyGrade) {
        this.loyaltyGrade = loyaltyGrade;
    }

    @Override
    public String toString() {
        return "ApprovedLimitAndConditionVO{" +
                "id='" + id + '\'' +
                ", dealerid='" + dealerid + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", limittype=" + limittype +
                ", currentlimit=" + currentlimit +
                ", currentsecurityratio=" + currentsecurityratio +
                ", aprovedlimit=" + aprovedlimit +
                ", approvedsecurityratio=" + approvedsecurityratio +
                ", dealer='" + dealer + '\'' +
                ", month='" + month + '\'' +
                ", loyaltyGrade='" + loyaltyGrade + '\'' +
                ", Priority='" + Priority + '\'' +
                '}';
    }
}
