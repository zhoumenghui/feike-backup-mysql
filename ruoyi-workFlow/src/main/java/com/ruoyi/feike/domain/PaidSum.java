package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_paid_sum
 *
 * @author zmh
 * @date 2022-08-25
 */
public class PaidSum extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "effectiveDate")
    private Date effectiveDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "expiryDate")
    private Date expiryDate;

    /** 保额 */
    @Excel(name = "保额")
    @ForUpdate(fieldName = "sum")
    private String sum;

    /** 费率 */
    @Excel(name = "费率")
    @ForUpdate(fieldName = "rate")
    private String rate;

    /** 应补缴保费 */
    @Excel(name = "应补缴保费")
    @ForUpdate(fieldName = "paidsum")
    private String paidsum;

    /** $column.columnComment */
    @Excel(name = "应补缴保费")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId()
    {
        return dealerId;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public void setExpiryDate(Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public Date getExpiryDate()
    {
        return expiryDate;
    }
    public void setSum(String sum)
    {
        this.sum = sum;
    }

    public String getSum()
    {
        return sum;
    }
    public void setRate(String rate)
    {
        this.rate = rate;
    }

    public String getRate()
    {
        return rate;
    }
    public void setPaidsum(String paidsum)
    {
        this.paidsum = paidsum;
    }

    public String getPaidsum()
    {
        return paidsum;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("dealerName", getDealerName())
            .append("sector", getSector())
            .append("effectiveDate", getEffectiveDate())
            .append("expiryDate", getExpiryDate())
            .append("sum", getSum())
            .append("rate", getRate())
            .append("paidsum", getPaidsum())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
