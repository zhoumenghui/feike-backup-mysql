package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 h_expiry_date_his
 *
 * @author ruoyi
 * @date 2023-02-07
 */
public class HExpiryDateHis extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String limittype;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date currentactivelimitexpirydate;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDealername(String dealername)
    {
        this.dealername = dealername;
    }

    public String getDealername()
    {
        return dealername;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setLimittype(String limittype)
    {
        this.limittype = limittype;
    }

    public String getLimittype()
    {
        return limittype;
    }
    public void setCurrentactivelimitexpirydate(Date currentactivelimitexpirydate)
    {
        this.currentactivelimitexpirydate = currentactivelimitexpirydate;
    }

    public Date getCurrentactivelimitexpirydate()
    {
        return currentactivelimitexpirydate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("dealername", getDealername())
                .append("sector", getSector())
                .append("limittype", getLimittype())
                .append("currentactivelimitexpirydate", getCurrentactivelimitexpirydate())
                .toString();
    }
}