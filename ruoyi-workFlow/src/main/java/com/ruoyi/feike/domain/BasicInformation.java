package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * feike对象 h_basic_information
 *
 * @author zmh
 * @date 2022-07-05
 */
public class BasicInformation extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;
    @ForUpdate(fieldName = "groupNameEN")
    private String groupNameEN;
    @ForUpdate(fieldName = "groupNameCN")
    private String groupNameCN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerNameEN")
    private String dealerNameEN;
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "    private String dealerNameCN;\n")
    private String dealerNameCN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "sector")
    private String sector;
    private String dealerValue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerCodeFromWFS")
    private String dealerCodeFromWFS;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerCodeFromManufacturer")
    private String dealerCodeFromManufacturer;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String province;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    // private Date incorporationDate;
    @ForUpdate(fieldName = "incorporationDate")
    private String incorporationDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    // private Date joinSectorDealerNetworkDate;
    @ForUpdate(fieldName = "joinSectorDealerNetworkDate")
    private String joinSectorDealerNetworkDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "registeredCapital")
    private String registeredCapital;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "paidupCapital")
    private String paidupCapital;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String shareholders;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    // private BigDecimal share;
    private String share;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "gm")
    private String gm;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "relatedWorkingExperience")
    private Integer relatedWorkingExperience;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** 优先级New application(4) &gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)； */
    @Excel(name = "优先级New application(4) &gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)；")
    @ForUpdate(fieldName = "priority")
    private Long priority;

    /** 触发新增修改后存主键ID */
    @Excel(name = "触发新增修改后存主键ID")
    @ForUpdate(fieldName = "priorityId")
    private String priorityId;
    @Excel(name = "法人中文")
    @ForUpdate(fieldName = "legalRepresentativeCN")
    private String legalRepresentativeCN;
    @Excel(name = "法人英文")
    @ForUpdate(fieldName = "legalRepresentativeEN")
    private String legalRepresentativeEN;
    @Excel(name = "注册地址中文")
    @ForUpdate(fieldName = "registeredAddressCN")
    private String registeredAddressCN;
    @Excel(name = "注册地址英文")
    @ForUpdate(fieldName = "registeredAddressEN")
    private String registeredAddressEN;

    @ForUpdate(fieldName = "outLegalRepresentativeCN")
    private String outLegalRepresentativeCN;

    @ForUpdate(fieldName = "newLegalRepresentativeCN")
    private String newLegalRepresentativeCN;

    public String getOutLegalRepresentativeCN() {
        return outLegalRepresentativeCN;
    }

    public void setOutLegalRepresentativeCN(String outLegalRepresentativeCN) {
        this.outLegalRepresentativeCN = outLegalRepresentativeCN;
    }

    public String getNewLegalRepresentativeCN() {
        return newLegalRepresentativeCN;
    }

    public void setNewLegalRepresentativeCN(String newLegalRepresentativeCN) {
        this.newLegalRepresentativeCN = newLegalRepresentativeCN;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerNameEN(String dealerNameEN)
    {
        this.dealerNameEN = dealerNameEN;
    }

    public String getDealerNameEN()
    {
        return dealerNameEN;
    }
    public void setDealerNameCN(String dealerNameCN)
    {
        this.dealerNameCN = dealerNameCN;
    }

    public String getDealerNameCN()
    {
        return dealerNameCN;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setDealerCodeFromWFS(String dealerCodeFromWFS)
    {
        this.dealerCodeFromWFS = dealerCodeFromWFS;
    }

    public String getDealerCodeFromWFS()
    {
        return dealerCodeFromWFS;
    }
    public void setDealerCodeFromManufacturer(String dealerCodeFromManufacturer)
    {
        this.dealerCodeFromManufacturer = dealerCodeFromManufacturer;
    }

    public String getDealerCodeFromManufacturer()
    {
        return dealerCodeFromManufacturer;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }

    public String getIncorporationDate() {
        return incorporationDate;
    }

    public void setIncorporationDate(String incorporationDate) {
        this.incorporationDate = incorporationDate;
    }

    public String getJoinSectorDealerNetworkDate() {
        return joinSectorDealerNetworkDate;
    }

    public void setJoinSectorDealerNetworkDate(String joinSectorDealerNetworkDate) {
        this.joinSectorDealerNetworkDate = joinSectorDealerNetworkDate;
    }



    public String getPaidupCapital() {
        return paidupCapital;
    }

    public void setPaidupCapital(String paidupCapital) {
        this.paidupCapital = paidupCapital;
    }

    public void setShareholders(String shareholders)
    {
        this.shareholders = shareholders;
    }

    public String getShareholders()
    {
        return shareholders;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public void setGm(String gm)
    {
        this.gm = gm;
    }

    public String getGm()
    {
        return gm;
    }
    public void setRelatedWorkingExperience(Integer relatedWorkingExperience)
    {
        this.relatedWorkingExperience = relatedWorkingExperience;
    }

    public Integer getRelatedWorkingExperience()
    {
        return relatedWorkingExperience;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    public String getGroupNameEN() {
        return groupNameEN;
    }

    public void setGroupNameEN(String groupNameEN) {
        this.groupNameEN = groupNameEN;
    }

    public String getGroupNameCN() {
        return groupNameCN;
    }

    public void setGroupNameCN(String groupNameCN) {
        this.groupNameCN = groupNameCN;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getDealerValue() {
        return dealerValue;
    }

    public void setDealerValue(String dealerValue) {
        this.dealerValue = dealerValue;
    }

    public String getLegalRepresentativeCN() {
        return legalRepresentativeCN;
    }

    public void setLegalRepresentativeCN(String legalRepresentativeCN) {
        this.legalRepresentativeCN = legalRepresentativeCN;
    }

    public String getLegalRepresentativeEN() {
        return legalRepresentativeEN;
    }

    public void setLegalRepresentativeEN(String legalRepresentativeEN) {
        this.legalRepresentativeEN = legalRepresentativeEN;
    }

    public String getRegisteredAddressCN() {
        return registeredAddressCN;
    }

    public void setRegisteredAddressCN(String registeredAddressCN) {
        this.registeredAddressCN = registeredAddressCN;
    }

    public String getRegisteredAddressEN() {
        return registeredAddressEN;
    }

    public void setRegisteredAddressEN(String registeredAddressEN) {
        this.registeredAddressEN = registeredAddressEN;
    }

    public String getRegisteredCapital() {
        return registeredCapital;
    }

    public void setRegisteredCapital(String registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    @Override
    public String toString() {
        return "BasicInformation{" +
                "id='" + id + '\'' +
                ", groupNameEN='" + groupNameEN + '\'' +
                ", groupNameCN='" + groupNameCN + '\'' +
                ", dealerNameEN='" + dealerNameEN + '\'' +
                ", dealername='" + dealername + '\'' +
                ", dealerNameCN='" + dealerNameCN + '\'' +
                ", sector='" + sector + '\'' +
                ", dealerCodeFromWFS='" + dealerCodeFromWFS + '\'' +
                ", dealerCodeFromManufacturer='" + dealerCodeFromManufacturer + '\'' +
                ", province='" + province + '\'' +
                ", incorporationDate=" + incorporationDate +
                ", joinSectorDealerNetworkDate=" + joinSectorDealerNetworkDate +
                ", registeredCapital=" + registeredCapital +
                ", paidupCapital=" + paidupCapital +
                ", shareholders='" + shareholders + '\'' +
                ", share=" + share +
                ", gm='" + gm + '\'' +
                ", relatedWorkingExperience=" + relatedWorkingExperience +
                ", instanceId='" + instanceId + '\'' +
                ", priority=" + priority +
                ", priorityId='" + priorityId + '\'' +
                '}';
    }
}
