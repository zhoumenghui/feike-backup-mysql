package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 经销商与dm岗位用户映射对象 h_dealer_dm
 *
 * @author ruoyi
 * @date 2022-12-23
 */
public class HDealerDm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** 品牌 */
    private String sector;

    /** 省份 */
    private String province;

    /** 城市 */
    private String city;

    /** dm名称 */
    @Excel(name = "dm名称")
    @ForUpdate(fieldName = "dmName")
    private String dmName;


    @Excel(name = "uw名称")
    @ForUpdate(fieldName = "uwName")
    private String uwName;


    /** 是否删除 0 未删除 1已删除 */
    private Integer isDelete;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCity()
    {
        return city;
    }
    public void setDmName(String dmName)
    {
        this.dmName = dmName;
    }

    public String getDmName()
    {
        return dmName;
    }
    public void setIsDelete(Integer isDelete)
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete()
    {
        return isDelete;
    }

    public String getUwName() {
        return uwName;
    }

    public void setUwName(String uwName) {
        this.uwName = uwName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerName", getDealerName())
            .append("sector", getSector())
            .append("province", getProvince())
            .append("city", getCity())
            .append("dmName", getDmName())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
