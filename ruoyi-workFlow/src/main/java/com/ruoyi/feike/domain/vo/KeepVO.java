package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

public class KeepVO extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String dayTime;
	private Long outSum;
	private Long newSum;
	private Long limitAmt;
	private Double currentDay;

	public String getDayTime() {
		return dayTime;
	}

	public void setDayTime(String dayTime) {
		this.dayTime = dayTime;
	}

	public Long getOutSum() {
		return outSum;
	}

	public void setOutSum(Long outSum) {
		this.outSum = outSum;
	}

	public Long getNewSum() {
		return newSum;
	}

	public void setNewSum(Long newSum) {
		this.newSum = newSum;
	}

	public Long getLimitAmt() {
		return limitAmt;
	}

	public void setLimitAmt(Long limitAmt) {
		this.limitAmt = limitAmt;
	}

	public Double getCurrentDay() {
		return currentDay;
	}

	public void setCurrentDay(Double currentDay) {
		this.currentDay = currentDay;
	}
}
