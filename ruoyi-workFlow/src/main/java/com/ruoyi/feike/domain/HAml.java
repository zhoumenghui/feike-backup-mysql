package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


@Data
public class HAml extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Excel(name = "DishonestyName")
    private String name;

    private Long id;

    private String dealerName;

    private String instanceId;

    private String position;

    private int isBlackList;

    private int isRelatedList;

    private String note;

    private int sort;


}
