package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class SalesPerformanceAndTargetOrcaleVO extends BaseEntity {
	private String item;
	private String dealerName;
	private String brand;
	private String year;
	private String sumsell;

	private String timehead;
	private String timeend;

	private String time;

	private Integer id;


}
