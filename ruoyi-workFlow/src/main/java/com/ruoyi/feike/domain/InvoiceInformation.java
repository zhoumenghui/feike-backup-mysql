package com.ruoyi.feike.domain;

import lombok.Data;

@Data
public class InvoiceInformation {

    private String instanceId;

    private String receiverCompanyName;

    private String registrationNumber;

    private String receiverCompanyAddress;

    private String telephone;

    private String openingBank;

    private String bankAccount;

    private String isGeneral;
}
