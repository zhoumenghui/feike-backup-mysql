package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class WholesalePerformanceRecentVO extends BaseEntity {
	private String business;
	private String dealerName;
	private String sector;
	private String nianyue;
	private String time;
	private String amount;

}
