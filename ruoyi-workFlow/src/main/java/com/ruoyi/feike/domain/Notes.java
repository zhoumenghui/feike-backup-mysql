package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_notes
 *
 * @author lsn
 * @date 2022-07-06
 */
@Data
public class Notes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "basicNotes")
    private String basicNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "dealerNotes")
    private String dealerNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "loyaltyNotes")
    private String loyaltyNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "auditNotes")
    private String auditNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "wholesaleNotes")
    private String wholesaleNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "proposalNotes")
    private String proposalNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "creditComments")
    private String creditComments;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "guaranteeNotes")
    private String guaranteeNotes;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "comments")
    private String comments;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "conditionNotes")
    private String conditionNotes;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "amlNotes")
    private String amlNotes;

}
