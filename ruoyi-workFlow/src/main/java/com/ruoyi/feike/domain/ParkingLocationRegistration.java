package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * feike对象 h_parking_location_registration
 *
 * @author ruoyi
 * @date 2022-08-02
 */
@Data
public class ParkingLocationRegistration extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 集团英文名称 */
    @Excel(name = "集团英文名称")
    @ForUpdate(fieldName = "groupNameEn")
    private String groupNameEn;

    /** 集团中文名称 */
    @Excel(name = "集团中文名称")
    @ForUpdate(fieldName = "groupNameCn")
    private String groupNameCn;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** 经销商集团名称 */
    @Excel(name = "经销商集团名称")
    @ForUpdate(fieldName = "groupAme")
    private String groupAme;

    /** 经销商品牌代码 */
    @Excel(name = "经销商品牌代码")
    @ForUpdate(fieldName = "dealercode")
    private String dealercode;

    private List dealercCodes;

    /** 省 */
    @Excel(name = "省")
    @ForUpdate(fieldName = "province")
    private String province;
    @ForUpdate(fieldName = "provinceCN")
    private String provinceCN;

    /** 市 */
    @Excel(name = "市")
    @ForUpdate(fieldName = "city")
    private String city;

    /** 县 */
    @Excel(name = "县")
    @ForUpdate(fieldName = "county")
    private String county;


    /** 详细地址 */
    @Excel(name = "详细地址")
    @ForUpdate(fieldName = "address")
    private String address;

    private String[] parkinglocations;


    /** 停车地点模式：4S，仓库，二网，同集团，车展; */
    @Excel(name = "停车地点模式：4S，仓库，二网，同集团，车展;")
    @ForUpdate(fieldName = "type")
    private String type;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "startDate")
    private Date startDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "endDate")
    private Date endDate;

    /** 车架号 */
    @Excel(name = "车架号")
    @ForUpdate(fieldName = "vin")
    private String vin;
    private String[] vins;

    /** 与4S店距离：100Km； */
    @Excel(name = "与4S店距离：100Km；")
    @ForUpdate(fieldName = "distanceFrom4s")
    private String distanceFrom4s;

    /** 电话号码 */
    @Excel(name = "电话号码")
    @ForUpdate(fieldName = "tel")
    private String tel;

    /** 注释 */
    @Excel(name = "注释")
    @ForUpdate(fieldName = "notes")
    private String notes;

    /** 已支付二网保证金 */
    @Excel(name = "已支付二网保证金")
    @ForUpdate(fieldName = "twondtierdeposit")
    private BigDecimal twondtierdeposit;

    /** 上传资料 */
    @Excel(name = "上传资料")
    private List<Fileupload> uploadFile;

    /** 实例id */
    @Excel(name = "实例id")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    @ForUpdate(fieldName = "comments")
    private String comments;

    private List<Fileupload> parkingLocationRegistrationFileList;

}
