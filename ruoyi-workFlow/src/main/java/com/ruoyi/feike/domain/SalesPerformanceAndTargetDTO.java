package com.ruoyi.feike.domain;


import lombok.Data;

@Data
public class SalesPerformanceAndTargetDTO  {
   private String currentYear;
    private String lastYear;
    private String[] dealerName;
}
