package com.ruoyi.feike.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同信息对象 h_basic_contract
 *
 * @author ybw
 * @date 2022-07-19
 */
@Data
public class BasicContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractname;

    /** 合同地址 */
    @Excel(name = "合同地址")
    private String contractlocation;

    /** 优先级 */
    @Excel(name = "优先级")
    private String priority;

    /** 主机厂 */
    @Excel(name = "主机厂")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    private String limittype;

    /** 合同号 */
    private String contractNumber;

    private Integer sort;

}
