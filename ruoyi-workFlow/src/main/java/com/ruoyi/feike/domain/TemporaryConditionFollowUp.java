package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.ForUpdate;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * feike对象 h_temporary_condition_follow_up
 * 
 * @author zmh
 * @date 2022-07-12
 */
public class TemporaryConditionFollowUp extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 经销商ID */

    private String dealerId;

    /** 经销商中文名 */
    @Excel(name = "Dealer")
    @ForUpdate(fieldName = "dealername")
    private String dealername;

    /** 列表索引id */
    private String index;

    /** $创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @Excel(name = "Condition Date", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "conditionDate")
    private Date conditionDate;

    /** 条件 */
    @Excel(name = "Condition")
    @ForUpdate(fieldName = "condition")
    private String condition;

    /** 截止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @Excel(name = "Due Date", width = 30, dateFormat = "yyyy-MM-dd")
    @ForUpdate(fieldName = "dueDate")
    private Date dueDate;

    /** 角色 GM/VGM/CFO/CRO */
    @Excel(name = "Role")
    @ForUpdate(fieldName = "role")
    private String role;

    /** 备注 */
    @Excel(name = "Comments")
    @ForUpdate(fieldName = "comments")
    private String comments;

    /** 状态 */
    @Excel(name = "Current Status")
    @ForUpdate(fieldName = "currentStatus")
    private String currentStatus;

    /** 是否己存在 1存在 0不存在 */

    private Integer isExist;

    /** 流程ID */

    private String instanceId;

    private String dealernameEn;
    @Excel(name = "DM")
    private String DM;
    @Excel(name = "UW")
    private String UW;

    public String getDM() {
        return DM;
    }

    public void setDM(String DM) {
        this.DM = DM;
    }

    public String getUW() {
        return UW;
    }

    public void setUW(String UW) {
        this.UW = UW;
    }

    public String getDealernameEn() {
        return dealernameEn;
    }

    public void setDealernameEn(String dealernameEn) {
        this.dealernameEn = dealernameEn;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setConditionDate(Date conditionDate)
    {
        this.conditionDate = conditionDate;
    }

    public Date getConditionDate() 
    {
        return conditionDate;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void setDueDate(Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public Date getDueDate() 
    {
        return dueDate;
    }
    public void setRole(String role) 
    {
        this.role = role;
    }

    public String getRole() 
    {
        return role;
    }
    public void setComments(String comments) 
    {
        this.comments = comments;
    }

    public String getComments() 
    {
        return comments;
    }
    public void setCurrentStatus(String currentStatus) 
    {
        this.currentStatus = currentStatus;
    }

    public String getCurrentStatus() 
    {
        return currentStatus;
    }
    public void setIsExist(Integer isExist) 
    {
        this.isExist = isExist;
    }

    public Integer getIsExist() 
    {
        return isExist;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return "TemporaryConditionFollowUp{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", index='" + index + '\'' +
                ", conditionDate=" + conditionDate +
                ", condition='" + condition + '\'' +
                ", dueDate=" + dueDate +
                ", role='" + role + '\'' +
                ", comments='" + comments + '\'' +
                ", currentStatus='" + currentStatus + '\'' +
                ", isExist=" + isExist +
                ", instanceId='" + instanceId + '\'' +
                '}';
    }
}
