package com.ruoyi.feike.domain;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 h_basic_corporate_guarantee
 *
 * @author ruoyi
 * @date 2023-04-21
 */
public class HBasicCorporateGuarantee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    
    private String dealername;

    /** $column.columnComment */
    
    private String instanceid;

    /** $column.columnComment */
    
    private String guarantorindustry;

    /** $column.columnComment */
    
    private String guarantorregioncode;

    /** $column.columnComment */
    private String guarantorscale;

    private String proposalNameCN;

    public String getProposalNameCN() {
        return proposalNameCN;
    }

    public void setProposalNameCN(String proposalNameCN) {
        this.proposalNameCN = proposalNameCN;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getInstanceid() {
        return instanceid;
    }

    public void setInstanceid(String instanceid) {
        this.instanceid = instanceid;
    }

    public String getGuarantorindustry() {
        return guarantorindustry;
    }

    public void setGuarantorindustry(String guarantorindustry) {
        this.guarantorindustry = guarantorindustry;
    }

    public String getGuarantorregioncode() {
        return guarantorregioncode;
    }

    public void setGuarantorregioncode(String guarantorregioncode) {
        this.guarantorregioncode = guarantorregioncode;
    }

    public String getGuarantorscale() {
        return guarantorscale;
    }

    public void setGuarantorscale(String guarantorscale) {
        this.guarantorscale = guarantorscale;
    }
}