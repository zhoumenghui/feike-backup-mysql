package com.ruoyi.feike.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.ruoyi.common.annotation.ForUpdate;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_proposal_by_commerical_and_marketing
 *
 * @author zmh
 * @date 2022-07-18
 */
@Data
public class ProposalByCommericalAndMarketing extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealerId;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    @ForUpdate(fieldName = "dealername")
    private String dealername;

    /** 品牌 */
    @Excel(name = "品牌")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    @ForUpdate(fieldName = "limitType")
    private String limitType;

    /** $column.columnComment */
    @Excel(name = "")
    @ForUpdate(fieldName = "activeLimit")
    private String activeLimit;

    /** $column.columnComment */
    @Excel(name = "")
    @ForUpdate(fieldName = "os")
    private Long os;

    /** 当前额度 */
    @Excel(name = "当前额度")
    @ForUpdate(fieldName = "approvedLimit")
    private Long approvedLimit;

    /** 当前保证金比例 */
    @Excel(name = "当前保证金比例")
    @ForUpdate(fieldName = "approvedCashDeposit")
    private String approvedCashDeposit;

    /** 申请额度 */
    @Excel(name = "申请额度")
    @ForUpdate(fieldName = "proposalLimit")
    private Long proposalLimit;

    /** $column.columnComment */
    @Excel(name = "")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    /** 批复保证金比例 */
    @Excel(name = "批复保证金比例")
    @ForUpdate(fieldName = "proposalCashDeposit")
    private String proposalCashDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String index;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "thisYear")
    private String thisYear;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    @ForUpdate(fieldName = "lastYear")
    private String lastYear;
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;

    private Integer isNew;

    @ForUpdate(fieldName = "ExpiryDate")
    private Date ExpiryDate;

    private Date ExpiryDateNew;

    private  Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Date getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        ExpiryDate = expiryDate;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public String getActiveLimit() {
        return activeLimit;
    }

    public void setActiveLimit(String activeLimit) {
        this.activeLimit = activeLimit;
    }

    public Long getOs() {
        return os;
    }

    public void setOs(Long os) {
        this.os = os;
    }

    public Long getApprovedLimit() {
        return approvedLimit;
    }

    public void setApprovedLimit(Long approvedLimit) {
        this.approvedLimit = approvedLimit;
    }

    public String getApprovedCashDeposit() {
        return approvedCashDeposit;
    }

    public void setApprovedCashDeposit(String approvedCashDeposit) {
        this.approvedCashDeposit = approvedCashDeposit;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getProposalCashDeposit() {
        return proposalCashDeposit;
    }

    public void setProposalCashDeposit(String proposalCashDeposit) {
        this.proposalCashDeposit = proposalCashDeposit;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getThisYear() {
        return thisYear;
    }

    public void setThisYear(String thisYear) {
        this.thisYear = thisYear;
    }

    public String getLastYear() {
        return lastYear;
    }

    public void setLastYear(String lastYear) {
        this.lastYear = lastYear;
    }

    public Date getExpiryDateNew() {
        return ExpiryDateNew;
    }

    public void setExpiryDateNew(Date expiryDateNew) {
        ExpiryDateNew = expiryDateNew;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("dealerId", getDealerId())
                .append("dealername", getDealername())
                .append("sector", getSector())
                .append("limitType", getLimitType())
                .append("os", getOs())
                .append("approvedLimit", getApprovedLimit())
                .append("proposalLimit", getProposalLimit())
                .append("approvedCashDeposit", getApprovedCashDeposit())
                .append("instanceId", getInstanceId())
                .append("proposalCashDeposit", getProposalCashDeposit())
                .append("index", getIndex())
                .append("activeLimit", getActiveLimit())
                .append("thisYear", getThisYear())
                .append("lastYear", getLastYear())
                .append("dealerCode", getDealerCode())
                .toString();
    }
}
