package com.ruoyi.feike.domain.dto;

import lombok.Data;

@Data
public class DeclaredInformationDTO {

    private String instanceId;

    private String dealerName;

    private String sector;

    private String effectiveDate;

    private String expiryDate;

    private String coverage;

    private String rate;

    private String premium;

    private String premiumReceivedDate;
}
