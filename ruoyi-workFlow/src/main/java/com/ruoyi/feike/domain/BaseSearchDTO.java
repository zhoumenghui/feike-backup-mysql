package com.ruoyi.feike.domain;


import lombok.Data;

@Data
public class BaseSearchDTO {
    private  String groupName;
    private String[] dealerName;
}
