package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_dealer_group
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class DealerGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupNameEN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupNameCN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGroupNameEN(String groupNameEN) 
    {
        this.groupNameEN = groupNameEN;
    }

    public String getGroupNameEN() 
    {
        return groupNameEN;
    }
    public void setGroupNameCN(String groupNameCN) 
    {
        this.groupNameCN = groupNameCN;
    }

    public String getGroupNameCN() 
    {
        return groupNameCN;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("groupNameEN", getGroupNameEN())
            .append("groupNameCN", getGroupNameCN())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
