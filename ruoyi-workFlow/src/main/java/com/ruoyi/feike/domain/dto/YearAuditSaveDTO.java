package com.ruoyi.feike.domain.dto;

import com.ruoyi.feike.domain.*;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

//年审核保存DTO,又称草稿行为，用于年审时保存部分信息
@Data
public class YearAuditSaveDTO extends YearAuditSaveNoteDTO {

    private List<SalesPerformanceAndTarget> salesPerformanceAndTargets;

    private List<LimitCalculationForCommericalNeeds> limitCalculationForCommericalNeeds;

    private List<OtherFinancingResource> otherFinancingResources;

    private List<TemporaryConditionFollowUp> temporaryConditionFollowUps;

    private List<ProposalByCommericalAndMarketing> proposalByCommericalAndMarketings;

    private DealerNegativeInformationCheck dealerNegativeInformationCheck;

    private List<Fileupload> fileuploads;





}
