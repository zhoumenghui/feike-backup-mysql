package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * Copyright 2022 json.cn
 */

@Data
public class ThirdCreditInfoSimpleVO implements Serializable {

    private String dealerId;

    private String dealerName;

    private List<ThirdCreditInfoSimpleChild> child;

}




