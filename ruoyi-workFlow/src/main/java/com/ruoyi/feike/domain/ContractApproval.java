package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_contract_approval
 * 
 * @author ruoyi
 * @date 2022-09-06
 */
public class ContractApproval extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** Dealer Code */
    @Excel(name = "Dealer Code")
    @ForUpdate(fieldName = "dealerCode")
    private String dealerCode;

    /** Dealer Name */
    @Excel(name = "Dealer Name")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** Sector */
    @Excel(name = "Sector")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** File Url */
    @Excel(name = "File Url")
    @ForUpdate(fieldName = "fileUrl")
    private String fileUrl;

    /** InstanceId */
    @Excel(name = "InstanceId")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;
    @ForUpdate(fieldName = "groupName")
    private String groupName;
    @ForUpdate(fieldName = "fileName")
    private String fileName;
    @ForUpdate(fieldName = "contractNumber")
    private String contractNumber;
    @ForUpdate(fieldName = "pdffileUrl")
    private String pdffileUrl;
    @ForUpdate(fieldName = "pdfFileName")
    private String pdfFileName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerCode(String dealerCode) 
    {
        this.dealerCode = dealerCode;
    }

    public String getDealerCode() 
    {
        return dealerCode;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setFileUrl(String fileUrl) 
    {
        this.fileUrl = fileUrl;
    }

    public String getFileUrl() 
    {
        return fileUrl;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getPdffileUrl() {
        return pdffileUrl;
    }

    public void setPdffileUrl(String pdffileUrl) {
        this.pdffileUrl = pdffileUrl;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerCode", getDealerCode())
            .append("dealerName", getDealerName())
            .append("sector", getSector())
            .append("fileUrl", getFileUrl())
            .append("instanceId", getInstanceId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
