package com.ruoyi.feike.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @author lss
 * @version 1.0
 * @description: TODO
 * @date 2022/8/28 16:50
 */
@Data
public class RefreshDTO {
    @NotBlank(message = "流程ID不能为空!")
    private String instanceId;
    @NotEmpty(message = "经销商名称不能为空!")
    private String[] dealerName;
}
