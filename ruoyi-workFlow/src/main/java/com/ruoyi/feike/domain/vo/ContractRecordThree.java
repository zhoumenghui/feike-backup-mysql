package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 生成的合同详细信息对象 h_dealercode_contract
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
@Data
public class ContractRecordThree extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    private String instanceid;

    @Excel(name = "Sector")
    private String sector;

    @Excel(name = "Dealer Name")
    private String dealername;

    @Excel(name = "Facility Amount")
    private String facilityamount;

    @Excel(name = "Contract Type")
    private String contracttype;

    @Excel(name = "Contract No")
    private String contractno;

    @Excel(name = "Effective Date", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date effectivedate;

    @Excel(name = "Contract Scenario")
    private String contractScenario;

    @Excel(name = "Note")
    private String note;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;


    private String status;



}
