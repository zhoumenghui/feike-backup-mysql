package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_new_sum
 *
 * @author zmh
 * @date 2022-08-25
 */
public class NewSum extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** 经销商 */
    @Excel(name = "经销商")
    @ForUpdate(fieldName = "dealerName")
    private String dealerName;

    /** 生产商 */
    @Excel(name = "生产商")
    @ForUpdate(fieldName = "sector")
    private String sector;

    /** 保额 */
    @Excel(name = "保额")
    @ForUpdate(fieldName = "sum")
    private String sum;

    /** 最小值项:日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "startDate")
    @Excel(name = "最小值项:日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 最大值项:日期2 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ForUpdate(fieldName = "endDate")
    @Excel(name = "最大值项:日期2", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 求和项:保费/天 */
    @Excel(name = "求和项:保费/天")
    @ForUpdate(fieldName = "sumDay")
    private String sumDay;

    /** $column.columnComment */
    @Excel(name = "求和项:保费/天")
    @ForUpdate(fieldName = "instanceId")
    private String instanceId;

    private String dayAmt;

    private String tempLimit;

    private String insuranceNo;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId()
    {
        return dealerId;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setSum(String sum)
    {
        this.sum = sum;
    }

    public String getSum()
    {
        return sum;
    }
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getStartDate()
    {
        return startDate;
    }
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }
    public void setSumDay(String sumDay)
    {
        this.sumDay = sumDay;
    }

    public String getSumDay()
    {
        return sumDay;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    public String getDayAmt() {
        return dayAmt;
    }

    public void setDayAmt(String dayAmt) {
        this.dayAmt = dayAmt;
    }

    public String getTempLimit() {
        return tempLimit;
    }

    public void setTempLimit(String tempLimit) {
        this.tempLimit = tempLimit;
    }

    public String getInsuranceNo() {
        return insuranceNo;
    }

    public void setInsuranceNo(String insuranceNo) {
        this.insuranceNo = insuranceNo;
    }

    @Override
    public String toString() {
        return "NewSum{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealerName='" + dealerName + '\'' +
                ", sector='" + sector + '\'' +
                ", sum='" + sum + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", sumDay='" + sumDay + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", dayAmt='" + dayAmt + '\'' +
                ", tempLimit='" + tempLimit + '\'' +
                ", insuranceNo='" + insuranceNo + '\'' +
                '}';
    }
}
