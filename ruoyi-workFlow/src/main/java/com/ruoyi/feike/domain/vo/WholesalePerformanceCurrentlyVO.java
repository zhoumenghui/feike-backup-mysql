package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * @author 神
 * @date 2022年08月12日 17:12
 */
public class WholesalePerformanceCurrentlyVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private String expiryDate;
    private String activeLimit;
    private String activeOS;
    private String dealername;
    private String dealerCode;
    private String creditLimit;
    private String securityRatio;
    private String sector;
    private String limitType;

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getActiveLimit() {
        return activeLimit;
    }

    public void setActiveLimit(String activeLimit) {
        this.activeLimit = activeLimit;
    }

    public String getActiveOS() {
        return activeOS;
    }

    public void setActiveOS(String activeOS) {
        this.activeOS = activeOS;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getSecurityRatio() {
        return securityRatio;
    }

    public void setSecurityRatio(String securityRatio) {
        this.securityRatio = securityRatio;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    @Override
    public String toString() {
        return "WholesalePerformanceCurrentlyVO{" +
                "expiryDate='" + expiryDate + '\'' +
                ", activeLimit='" + activeLimit + '\'' +
                ", activeOS='" + activeOS + '\'' +
                ", dealername='" + dealername + '\'' +
                ", dealerCode='" + dealerCode + '\'' +
                ", creditLimit='" + creditLimit + '\'' +
                ", securityRatio='" + securityRatio + '\'' +
                ", sector='" + sector + '\'' +
                ", limitType='" + limitType + '\'' +
                '}';
    }
}
