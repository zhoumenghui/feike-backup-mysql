package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.ForUpdate;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 h_limit_setup_result
 * 
 * @author ruoyi
 * @date 2023-03-12
 */
public class HLimitSetupResult extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    private Long id;

    @ForUpdate(fieldName = "dealername")
    private String dealername;

    @ForUpdate(fieldName = "sector")
    private String sector;

    @ForUpdate(fieldName = "limittype")
    private String limittype;

    @ForUpdate(fieldName = "currentactivelimit")
    private String currentactivelimit;

    @ForUpdate(fieldName = "currentactivelimitexpirydate")
    private Date currentactivelimitexpirydate;

    @ForUpdate(fieldName = "instanceid")
    private String instanceid;

    private String os;

    private Integer sort;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setLimittype(String limittype) 
    {
        this.limittype = limittype;
    }

    public String getLimittype() 
    {
        return limittype;
    }
    public void setCurrentactivelimit(String currentactivelimit) 
    {
        this.currentactivelimit = currentactivelimit;
    }

    public String getCurrentactivelimit() 
    {
        return currentactivelimit;
    }
    public void setCurrentactivelimitexpirydate(Date currentactivelimitexpirydate) 
    {
        this.currentactivelimitexpirydate = currentactivelimitexpirydate;
    }

    public Date getCurrentactivelimitexpirydate() 
    {
        return currentactivelimitexpirydate;
    }
    public void setInstanceid(String instanceid) 
    {
        this.instanceid = instanceid;
    }

    public String getInstanceid() 
    {
        return instanceid;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealername", getDealername())
            .append("sector", getSector())
            .append("limittype", getLimittype())
            .append("currentactivelimit", getCurrentactivelimit())
            .append("currentactivelimitexpirydate", getCurrentactivelimitexpirydate())
            .append("instanceid", getInstanceid())
            .toString();
    }
}
