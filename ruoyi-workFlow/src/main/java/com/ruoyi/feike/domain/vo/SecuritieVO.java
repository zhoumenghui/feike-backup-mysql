package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.feike.domain.Securities;

import java.util.List;

/**
 * feike对象 h_securities
 *
 * @author zmh
 * @date 2022-07-11
 */
public class SecuritieVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String dealername;
    private List<Securities> children;
    private String index;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public List<Securities> getChildren() {
        return children;
    }

    public void setChildren(List<Securities> children) {
        this.children = children;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
