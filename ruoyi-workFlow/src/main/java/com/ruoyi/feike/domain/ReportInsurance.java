package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_report_insurance
 *
 * @author ruoyi
 * @date 2022-09-02
 */
public class ReportInsurance extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** Dealer Name */
    private String instanceId;

    /** Dealer Name */
    @Excel(name = "Dealer Name")
    private String dealerName;

    /** Sector */
    @Excel(name = "Sector")
    private String sector;

    /** Effective Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Effective Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date effectiveDate;

    /** Expory Date */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "Expiry Date", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expiryDate;

    /** 保额 */
    @Excel(name = "Coverage")
    private String coverage;

    @Excel(name = "Rate")
    private String rate;

    @Excel(name = "Premium")
    private String premium;

    @Excel(name = "Temp Limit")
    private String tempLimit;
    @Excel(name = "Note")
    private String note;


    private String type;

    private String insuranceNo;


    public String getTempLimit() {
        return tempLimit;
    }

    public void setTempLimit(String tempLimit) {
        this.tempLimit = tempLimit;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setEffectiveDate(Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    public Date getEffectiveDate()
    {
        return effectiveDate;
    }
    public void setExpiryDate(Date expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public Date getExpiryDate()
    {
        return expiryDate;
    }
    public void setCoverage(String coverage)
    {
        this.coverage = coverage;
    }

    public String getCoverage()
    {
        return coverage;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getInsuranceNo() {
        return insuranceNo;
    }

    public void setInsuranceNo(String insuranceNo) {
        this.insuranceNo = insuranceNo;
    }

    @Override
    public String toString() {
        return "ReportInsurance{" +
                "id=" + id +
                ", instanceId='" + instanceId + '\'' +
                ", dealerName='" + dealerName + '\'' +
                ", sector='" + sector + '\'' +
                ", effectiveDate=" + effectiveDate +
                ", expiryDate=" + expiryDate +
                ", coverage='" + coverage + '\'' +
                ", rate='" + rate + '\'' +
                ", premium='" + premium + '\'' +
                ", tempLimit='" + tempLimit + '\'' +
                ", note='" + note + '\'' +
                ", type='" + type + '\'' +
                ", insuranceNo='" + insuranceNo + '\'' +
                '}';
    }
}
