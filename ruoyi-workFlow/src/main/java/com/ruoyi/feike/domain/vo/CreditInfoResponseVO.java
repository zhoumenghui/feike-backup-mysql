package com.ruoyi.feike.domain.vo;

import lombok.Data;

@Data
public class CreditInfoResponseVO {

    private Integer Code;

    private Object Data;

    private Object DataSummary;

    private String Message;
}
