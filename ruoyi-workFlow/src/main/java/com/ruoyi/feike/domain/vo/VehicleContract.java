package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class VehicleContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /**
     * 颜色
     */
    private String color;
    //数量
    private String quantity;
    //车架号
    private String vin;
    //金额
    private String amount;
    //法人
    private String legalPerson;

    private String sector;

    private String dealerName;
}
