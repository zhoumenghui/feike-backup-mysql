package com.ruoyi.feike.domain.vo;

import com.ruoyi.feike.domain.OtherFinancingResource;
import com.ruoyi.feike.domain.TemporaryConditionFollowUp;
import lombok.Data;

import java.util.List;

@Data
public class OtherFinancingResourceShowVO {

    private List<OtherFinancingResource> tempList;

    private List<String> dealNames;
}
