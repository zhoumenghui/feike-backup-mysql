package com.ruoyi.feike.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
public class HTempContract extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    private String dealerName;

    /**
     * $column.columnComment
     */
    private String sector;

    /**
     * $column.columnComment
     */
    private String limitType;

    /**
     * $column.columnComment
     */
    private Date issueDate;

    /**
     * $column.columnComment
     */
    private Date effectivedate;

    /**
     * $column.columnComment
     */
    private String dealerCode;

    /**
     * $column.columnComment
     */
    private String facilityamount;

    /**
     * $column.columnComment
     */
    private String contractType;

    /**
     * $column.columnComment
     */
    private String contractNo;

    /**
     * $column.columnComment
     */
    private Date expiryDate;

    /**
     * $column.columnComment
     */
    private String fileUrl;

    private String newFileUrl;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(Date effectivedate) {
        this.effectivedate = effectivedate;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getFacilityamount() {
        return facilityamount;
    }

    public void setFacilityamount(String facilityamount) {
        this.facilityamount = facilityamount;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getNewFileUrl() {
        return newFileUrl;
    }

    public void setNewFileUrl(String newFileUrl) {
        this.newFileUrl = newFileUrl;
    }
}
