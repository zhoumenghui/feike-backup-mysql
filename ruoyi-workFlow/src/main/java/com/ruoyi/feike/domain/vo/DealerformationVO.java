package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * feike对象 h_basic_information
 *
 * @author zmh
 * @date 2022-07-05
 */
@Data
public class DealerformationVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;
    private String groupName;
    private String groupNameEN;
    private String groupNameCN;

    private String dealerNameEN;
    private String dealerNameCN;
    private String dealerValue;

    private List sector;
    private List dealerCodeFromWFS;
    private List dealerCodeFromManufacturer;
    private List<Map<String,Object>> shareholdersAndShare;


    private String registeredAddressCN;
    private String registeredAddressEN;
    private String corporateGuarantor;
    private String corporateGuarantorEn;
    private String individualGuarantor;
    private String individualGuarantorEn;
    private String shareholderStructure;
    private String newdealername;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date incorporationDate;
    private String registeredCapitalCn;
    private String registeredCapital;
    private String registeredUrbanCn;
    private String registeredUrbanEn;
    private String uniformCreditCode;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date joinSectorDealerNetworkDate;
    private String paidupCapital;

    private String legalRepresentativeCN;
    private String legalRepresentativeEN;

    private String gm;
    private Long relatedWorkingExperience;
    private List shareholders;
    private List share;

    private String priorityId;
    private String province;
    private String instanceId;
    private Long priority;

    private Long serialNumber;

}
