package com.ruoyi.feike.domain.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 神
 * @date 2022年07月11日 16:29
 */
@Data
public class OtherFinancingResourceVO {

    private String id;
    private String index;
    private String dealername;
    private String institutionName;
    private String loanType;
    private String approvedLimit;
    private String os;
    private String security;
    private String interestRate;
    private Date dueDate;
    private String purpose;

}
