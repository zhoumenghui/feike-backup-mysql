package com.ruoyi.feike.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_approved_limit_and_condition
 * 
 * @author ybw
 * @date 2022-07-27
 */
public class ApprovedLimitAndCondition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealerid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealername;

    /** 品牌 */
    @Excel(name = "品牌")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    private String limittype;

    /** $column.columnComment */
    @Excel(name = "")
    private BigDecimal activelimit;

    /** $column.columnComment */
    @Excel(name = "")
    private BigDecimal os;

    /** 当前额度 */
    @Excel(name = "当前额度")
    private Long approvedlimit;

    /** 当前保证金比例 */
    @Excel(name = "当前保证金比例")
    private Double approvedcashdeposit;

    /** 申请额度 */
    @Excel(name = "申请额度")
    private Long proposallimit;

    /** $column.columnComment */
    @Excel(name = "")
    private String instanceid;

    /** 批复保证金比例 */
    @Excel(name = "批复保证金比例")
    private Double proposalcashdeposit;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerid(String dealerid) 
    {
        this.dealerid = dealerid;
    }

    public String getDealerid() 
    {
        return dealerid;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setLimittype(String limittype) 
    {
        this.limittype = limittype;
    }

    public String getLimittype() 
    {
        return limittype;
    }
    public void setActivelimit(BigDecimal activelimit) 
    {
        this.activelimit = activelimit;
    }

    public BigDecimal getActivelimit() 
    {
        return activelimit;
    }
    public void setOs(BigDecimal os) 
    {
        this.os = os;
    }

    public BigDecimal getOs() 
    {
        return os;
    }
    public void setApprovedlimit(Long approvedlimit) 
    {
        this.approvedlimit = approvedlimit;
    }

    public Long getApprovedlimit() 
    {
        return approvedlimit;
    }
    public void setApprovedcashdeposit(Double approvedcashdeposit)
    {
        this.approvedcashdeposit = approvedcashdeposit;
    }

    public Double getApprovedcashdeposit()
    {
        return approvedcashdeposit;
    }
    public void setProposallimit(Long proposallimit) 
    {
        this.proposallimit = proposallimit;
    }

    public Long getProposallimit() 
    {
        return proposallimit;
    }
    public void setInstanceid(String instanceid) 
    {
        this.instanceid = instanceid;
    }

    public String getInstanceid() 
    {
        return instanceid;
    }
    public void setProposalcashdeposit(Double proposalcashdeposit)
    {
        this.proposalcashdeposit = proposalcashdeposit;
    }

    public Double getProposalcashdeposit()
    {
        return proposalcashdeposit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerid", getDealerid())
            .append("dealername", getDealername())
            .append("sector", getSector())
            .append("limittype", getLimittype())
            .append("activelimit", getActivelimit())
            .append("os", getOs())
            .append("approvedlimit", getApprovedlimit())
            .append("approvedcashdeposit", getApprovedcashdeposit())
            .append("proposallimit", getProposallimit())
            .append("instanceid", getInstanceid())
            .append("proposalcashdeposit", getProposalcashdeposit())
            .toString();
    }
}
