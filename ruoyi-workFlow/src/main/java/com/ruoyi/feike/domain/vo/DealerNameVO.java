package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * feike对象 h_securities
 *
 * @author zmh
 * @date 2022-07-11
 */
public class DealerNameVO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String GM;
    private String dealerCodeFromManufacturer;
    private String dealerCodeFromWFS;
    private String dealerNameCN;
    private String dealerNameEN;
    private String dealername;
    private String incorporationDate;
    private String joinSectorDealerNetworkDate;
    private String paidupCapital;
    private String province;
    private String relatedWorkingExperience;
    private String value;
    private String sector;
    private List<Map<String,Object>> shareholdersAndShare;
    private String groupName;
    private String basicNotes;
    private String loyaltyNotes;
    private String auditNotes;
    private String wholesaleNotes;
    private String proposalNotes;
    private SalesPerformanceAndTargetVO[] sales_performance_and_target;
    private LimitCalculationForCommericalNeedVO[] limit_calculation_for_commerical_needs;
    private List<Map<String,Object>> actForm;

    public String getGM() {
        return GM;
    }

    public void setGM(String GM) {
        this.GM = GM;
    }

    public String getDealerCodeFromManufacturer() {
        return dealerCodeFromManufacturer;
    }

    public void setDealerCodeFromManufacturer(String dealerCodeFromManufacturer) {
        this.dealerCodeFromManufacturer = dealerCodeFromManufacturer;
    }

    public String getDealerCodeFromWFS() {
        return dealerCodeFromWFS;
    }

    public void setDealerCodeFromWFS(String dealerCodeFromWFS) {
        this.dealerCodeFromWFS = dealerCodeFromWFS;
    }

    public String getDealerNameCN() {
        return dealerNameCN;
    }

    public void setDealerNameCN(String dealerNameCN) {
        this.dealerNameCN = dealerNameCN;
    }

    public String getDealerNameEN() {
        return dealerNameEN;
    }

    public void setDealerNameEN(String dealerNameEN) {
        this.dealerNameEN = dealerNameEN;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getIncorporationDate() {
        return incorporationDate;
    }

    public void setIncorporationDate(String incorporationDate) {
        this.incorporationDate = incorporationDate;
    }

    public String getJoinSectorDealerNetworkDate() {
        return joinSectorDealerNetworkDate;
    }

    public void setJoinSectorDealerNetworkDate(String joinSectorDealerNetworkDate) {
        this.joinSectorDealerNetworkDate = joinSectorDealerNetworkDate;
    }

    public String getPaidupCapital() {
        return paidupCapital;
    }

    public void setPaidupCapital(String paidupCapital) {
        this.paidupCapital = paidupCapital;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRelatedWorkingExperience() {
        return relatedWorkingExperience;
    }

    public void setRelatedWorkingExperience(String relatedWorkingExperience) {
        this.relatedWorkingExperience = relatedWorkingExperience;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public List getShareholdersAndShare() {
        return shareholdersAndShare;
    }

    public void setShareholdersAndShare(List shareholdersAndShare) {
        this.shareholdersAndShare = shareholdersAndShare;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getBasicNotes() {
        return basicNotes;
    }

    public void setBasicNotes(String basicNotes) {
        this.basicNotes = basicNotes;
    }

    public String getLoyaltyNotes() {
        return loyaltyNotes;
    }

    public void setLoyaltyNotes(String loyaltyNotes) {
        this.loyaltyNotes = loyaltyNotes;
    }

    public String getAuditNotes() {
        return auditNotes;
    }

    public void setAuditNotes(String auditNotes) {
        this.auditNotes = auditNotes;
    }

    public String getWholesaleNotes() {
        return wholesaleNotes;
    }

    public void setWholesaleNotes(String wholesaleNotes) {
        this.wholesaleNotes = wholesaleNotes;
    }

    public String getProposalNotes() {
        return proposalNotes;
    }

    public void setProposalNotes(String proposalNotes) {
        this.proposalNotes = proposalNotes;
    }

    public SalesPerformanceAndTargetVO[] getSales_performance_and_target() {
        return sales_performance_and_target;
    }

    public void setSales_performance_and_target(SalesPerformanceAndTargetVO[] sales_performance_and_target) {
        this.sales_performance_and_target = sales_performance_and_target;
    }

    public LimitCalculationForCommericalNeedVO[] getLimit_calculation_for_commerical_needs() {
        return limit_calculation_for_commerical_needs;
    }

    public void setLimit_calculation_for_commerical_needs(LimitCalculationForCommericalNeedVO[] limit_calculation_for_commerical_needs) {
        this.limit_calculation_for_commerical_needs = limit_calculation_for_commerical_needs;
    }

    public List getActForm() {
        return actForm;
    }

    public void setActForm(List actForm) {
        this.actForm = actForm;
    }

    @Override
    public String toString() {
        return "DealerNameVO{" +
                "GM='" + GM + '\'' +
                ", dealerCodeFromManufacturer='" + dealerCodeFromManufacturer + '\'' +
                ", dealerCodeFromWFS='" + dealerCodeFromWFS + '\'' +
                ", dealerNameCN='" + dealerNameCN + '\'' +
                ", dealerNameEN='" + dealerNameEN + '\'' +
                ", dealername='" + dealername + '\'' +
                ", incorporationDate='" + incorporationDate + '\'' +
                ", joinSectorDealerNetworkDate='" + joinSectorDealerNetworkDate + '\'' +
                ", paidupCapital='" + paidupCapital + '\'' +
                ", province='" + province + '\'' +
                ", relatedWorkingExperience='" + relatedWorkingExperience + '\'' +
                ", value='" + value + '\'' +
                ", sector='" + sector + '\'' +
                ", shareholdersAndShare=" + shareholdersAndShare +
                ", groupName='" + groupName + '\'' +
                ", basicNotes='" + basicNotes + '\'' +
                ", loyaltyNotes='" + loyaltyNotes + '\'' +
                ", auditNotes='" + auditNotes + '\'' +
                ", wholesaleNotes='" + wholesaleNotes + '\'' +
                ", proposalNotes='" + proposalNotes + '\'' +
                ", sales_performance_and_target=" + Arrays.toString(sales_performance_and_target) +
                ", limit_calculation_for_commerical_needs=" + Arrays.toString(limit_calculation_for_commerical_needs) +
                ", actForm=" + actForm +
                '}';
    }
}
