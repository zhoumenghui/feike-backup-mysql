package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

public class SysDbLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    private Long id;

    private String operName;

    private String account;

    private String logType;

    private String operIp;

    private String operType;

    private String operMethod;

    private String instanceid;

    private String title;

    private String beforeModifi;

    private String afterModifi;

    private String columnModifi;

    private String status;

    private String screenName;

    private Date operTime;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public String getOperIp() {
        return operIp;
    }

    public void setOperIp(String operIp) {
        this.operIp = operIp;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public String getOperMethod() {
        return operMethod;
    }

    public void setOperMethod(String operMethod) {
        this.operMethod = operMethod;
    }

    public String getInstanceid() {
        return instanceid;
    }

    public void setInstanceid(String instanceid) {
        this.instanceid = instanceid;
    }

    public String getBeforeModifi() {
        return beforeModifi;
    }

    public void setBeforeModifi(String beforeModifi) {
        this.beforeModifi = beforeModifi;
    }

    public String getAfterModifi() {
        return afterModifi;
    }

    public void setAfterModifi(String afterModifi) {
        this.afterModifi = afterModifi;
    }

    public String getColumnModifi() {
        return columnModifi;
    }

    public void setColumnModifi(String columnModifi) {
        this.columnModifi = columnModifi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    @Override
    public String toString() {
        return "SysDbOperLog{" +
                "id=" + id +
                ", operName='" + operName + '\'' +
                ", operIp='" + operIp + '\'' +
                ", operType='" + operType + '\'' +
                ", operMethod='" + operMethod + '\'' +
                ", instanceid='" + instanceid + '\'' +
                ", title='" + title + '\'' +
                ", beforeModifi='" + beforeModifi + '\'' +
                ", afterModifi='" + afterModifi + '\'' +
                ", columnModifi='" + columnModifi + '\'' +
                ", status='" + status + '\'' +
                ", screenName='" + screenName + '\'' +
                ", operTime=" + operTime +
                '}';
    }
}