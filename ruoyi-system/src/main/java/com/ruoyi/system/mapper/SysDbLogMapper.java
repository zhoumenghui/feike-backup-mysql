package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.SysDbLog;

public interface SysDbLogMapper
{
    public int insertSysDbLog(SysDbLog sysDbOperLog);

}
