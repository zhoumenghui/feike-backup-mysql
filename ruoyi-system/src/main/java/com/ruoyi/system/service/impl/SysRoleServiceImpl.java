package com.ruoyi.system.service.impl;

import java.util.*;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.system.domain.SysDbLog;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.SysRoleDept;
import com.ruoyi.system.domain.SysRoleMenu;
import com.ruoyi.system.service.ISysRoleService;

/**
 * 角色 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysRoleServiceImpl implements ISysRoleService
{
    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private SysRoleDeptMapper roleDeptMapper;

    @Autowired
    private SysDbLogMapper sysDbOperLogMapper;

    @Autowired
    private ISysMenuService menuService;

    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysRole> selectRoleList(SysRole role)
    {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId)
    {
        List<SysRole> perms = roleMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms)
        {
            if (StringUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    @Override
    public List<SysRole> selectRoleAll()
    {
        return SpringUtils.getAopProxy(this).selectRoleList(new SysRole());
    }

    /**
     * 根据用户ID获取角色选择框列表
     * 
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    @Override
    public List<Integer> selectRoleListByUserId(Long userId)
    {
        return roleMapper.selectRoleListByUserId(userId);
    }

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    public SysRole selectRoleById(Long roleId)
    {
        return roleMapper.selectRoleById(roleId);
    }

    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(SysRole role)
    {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = roleMapper.checkRoleNameUnique(role.getRoleName());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(SysRole role)
    {
        Long roleId = StringUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = roleMapper.checkRoleKeyUnique(role.getRoleKey());
        if (StringUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
    @Override
    public void checkRoleAllowed(SysRole role)
    {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin())
        {
            throw new CustomException("不允许操作超级管理员角色");
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int countUserRoleByRoleId(Long roleId)
    {
        return userRoleMapper.countUserRoleByRoleId(roleId);
    }

    /**
     * 新增保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertRole(SysRole role)
    {
        // 新增角色信息
        roleMapper.insertRole(role);
        this.saveDbLog("3","新增角色信息","新增角色信息",null,null,role,"角色管理/新增");

        return insertRoleMenu(role);
    }

    /**
     * 修改保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateRole(SysRole role)
    {
        // 修改角色信息
        SysRole sysRole = roleMapper.selectRoleById(role.getRoleId());
        roleMapper.updateRole(role);
        this.saveDbLog("3","修改角色信息","修改角色",null,sysRole,role,"角色管理/修改");

        // 删除角色与菜单关联
        List<String> strings = menuService.selectMenuListByRoleId2(role.getRoleId());
        roleMenuMapper.deleteRoleMenuByRoleId(role.getRoleId());
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("角色:"+sysRole.getRoleName() +" 移除菜单权限:");
        for (String string : strings) {
            stringBuffer.append(string).append(",");
        }
        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        this.saveDbLog("2","修改角色菜单权限","修改角色",stringBuffer.toString(),strings,null,"角色管理/修改");

        return insertRoleMenu(role);
    }

    /**
     * 修改角色状态
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public int updateRoleStatus(SysRole role)
    {
        return roleMapper.updateRole(role);
    }

    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int authDataScope(SysRole role)
    {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }

    /**
     * 新增角色菜单信息
     * 
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRole role)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        ArrayList<String> strings = new ArrayList<>();
        Arrays.sort(role.getMenuIds());
        for (Long menuId : role.getMenuIds())
        {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
            SysMenu sysMenu = sysMenuMapper.selectMenuById(menuId);
            strings.add(sysMenu.getMenuName());
        }
        if (list.size() > 0)
        {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("角色:"+role.getRoleName() +" 新增菜单权限:");
            for (String string : strings) {
                stringBuffer.append(string).append(",");
            }
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            rows = roleMenuMapper.batchRoleMenu(list);
            this.saveDbLog("1","修改角色菜单权限","修改角色",stringBuffer.toString() ,null,strings,"角色管理/修改");

        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(SysRole role)
    {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (Long deptId : role.getDeptIds())
        {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0)
        {
            rows = roleDeptMapper.batchRoleDept(list);
        }
        return rows;
    }

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int deleteRoleById(Long roleId)
    {
        return roleMapper.deleteRoleById(roleId);
    }

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    @Override
    public int deleteRoleByIds(Long[] roleIds)
    {
        for (Long roleId : roleIds)
        {
            checkRoleAllowed(new SysRole(roleId));
            SysRole role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                throw new CustomException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
            SysRole sysRole = roleMapper.selectRoleById(roleId);
            this.saveDbLog("2","删除角色","删除角色",null,sysRole,null,"角色管理/删除");

        }
        return roleMapper.deleteRoleByIds(roleIds);
    }

    @Override
    @Async
    public <T> String  saveDbLog(String operType, String operMethod,String title , String instanceId, T oldBean ,T newBean,String screenName) {
        try {
            SysDbLog sysDbOperLog = new SysDbLog();
            String ip = IpUtils.getIpAddr(ServletUtils.getRequest());
            sysDbOperLog.setOperIp(ip);
            sysDbOperLog.setOperName(SecurityUtils.getNickName());
            sysDbOperLog.setAccount(SecurityUtils.getUsername());
            sysDbOperLog.setLogType("账户权限日志");
            sysDbOperLog.setScreenName(screenName);
            sysDbOperLog.setOperMethod(operMethod);
            sysDbOperLog.setOperTime(new Date());
            sysDbOperLog.setTitle(title);
            if(operType.equals("1")){
                sysDbOperLog.setOperType("新增");
                String newB = JSON.toJSONString(newBean);
                sysDbOperLog.setAfterModifi(newB);
                sysDbOperLog.setColumnModifi(instanceId);
            }else if (operType.equals("2")){
                sysDbOperLog.setOperType("删除");
                String oldB = JSON.toJSONString(oldBean);
                sysDbOperLog.setBeforeModifi(oldB);
                sysDbOperLog.setColumnModifi(instanceId);
            }else if (operType.equals("3")){
                sysDbOperLog.setOperType("修改");
                String oldB = JSON.toJSONString(oldBean);
                sysDbOperLog.setBeforeModifi(oldB);
                String newB = JSON.toJSONString(newBean);
                sysDbOperLog.setAfterModifi(newB);
                String changedFields = com.ruoyi.common.utils.bean.BeanUtils.getChangedFields(oldBean, newBean);
                if(!StringUtils.isEmpty(changedFields)){
                    sysDbOperLog.setColumnModifi(changedFields);
                }
            }
            sysDbOperLogMapper.insertSysDbLog(sysDbOperLog);
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }
}
